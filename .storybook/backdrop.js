/**
 * @Author: Ben Briggs <benbriggs>
 * @Date:   2017-12-08T16:00:07+00:00
 * @Email:  ben.briggs@thedistance.co.uk
 * @Last modified by:   benbriggs
 * @Last modified time: 2017-12-08T16:01:37+00:00
 * @Copyright: The Distance
 */
 
import styles from "@sambego/storybook-styles";

export default styles({
  background: '#E9F0F5',
  padding: '50px',
  minHeight: '100%',
});
