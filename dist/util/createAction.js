"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

/**
 * @Author: Ben Briggs <benbriggs>
 * @Date:   2017-10-02T10:05:47+01:00
 * @Email:  ben.briggs@thedistance.co.uk
 * @Last modified by:   benbriggs
 * @Last modified time: 2018-01-02T13:59:41+00:00
 * @Copyright: The Distance
 */

var createAction = function createAction(type) {
  return function (result) {
    return _extends({
      type: type
    }, result);
  };
};

exports.default = createAction;