'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.createError = exports.createEnd = exports.createStart = undefined;

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

var _reactReduxSpinner = require('react-redux-spinner');

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; } /**
                                                                                                                                                                                                                   * @Author: Ben Briggs <benbriggs>
                                                                                                                                                                                                                   * @Date:   2017-09-27T11:10:32+01:00
                                                                                                                                                                                                                   * @Email:  ben.briggs@thedistance.co.uk
                                                                                                                                                                                                                   * @Last modified by:   benbriggs
                                                                                                                                                                                                                   * @Last modified time: 2018-01-11T09:21:25+00:00
                                                                                                                                                                                                                   * @Copyright: The Distance
                                                                                                                                                                                                                   */

var createStart = exports.createStart = function createStart(_ref) {
  var type = _ref.type;
  return function () {
    return _defineProperty({
      type: type
    }, _reactReduxSpinner.pendingTask, _reactReduxSpinner.begin);
  };
};

var createEnd = exports.createEnd = function createEnd(_ref3) {
  var type = _ref3.type;
  return function (result) {
    return _extends({
      type: type
    }, result, _defineProperty({}, _reactReduxSpinner.pendingTask, _reactReduxSpinner.end));
  };
};

var createError = exports.createError = function createError(_ref4) {
  var type = _ref4.type;
  return function (error) {
    return _extends({
      type: type
    }, error, _defineProperty({}, _reactReduxSpinner.pendingTask, _reactReduxSpinner.end));
  };
};