"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
/**
 * @Author: Ben Briggs <benbriggs>
 * @Date:   2017-12-08T17:21:27+00:00
 * @Email:  ben.briggs@thedistance.co.uk
 * @Last modified by:   benbriggs
 * @Last modified time: 2017-12-08T17:21:44+00:00
 * @Copyright: The Distance
 */

var selectParse = function selectParse(state) {
  return state.Parse;
};

exports.default = selectParse;