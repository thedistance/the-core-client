'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _rubix = require('@sketchpixy/rubix');

var _Panel = require('../Panel');

var _Panel2 = _interopRequireDefault(_Panel);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var Welcome = function Welcome() {
  return _react2.default.createElement(
    _Panel2.default,
    { title: 'Welcome!' },
    _react2.default.createElement(
      _rubix.Grid,
      null,
      _react2.default.createElement(
        _rubix.Row,
        null,
        _react2.default.createElement(
          _rubix.Col,
          { xs: 12 },
          _react2.default.createElement(
            'p',
            null,
            'Welcome to The Core!'
          )
        )
      )
    )
  );
}; /**
    * @Author: Ben Briggs <benbriggs>
    * @Date:   2017-12-19T17:06:29+00:00
    * @Email:  ben.briggs@thedistance.co.uk
    * @Last modified by:   benbriggs
    * @Last modified time: 2018-01-22T10:55:35+00:00
    * @Copyright: The Distance
    */

exports.default = Welcome;