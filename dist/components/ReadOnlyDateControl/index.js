'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _propTypes = require('prop-types');

var _propTypes2 = _interopRequireDefault(_propTypes);

var _rubix = require('@sketchpixy/rubix');

var _GenericControl = require('../GenericControl');

var _GenericControl2 = _interopRequireDefault(_GenericControl);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/*
 * Created Date: Wed, 18th Apr 2018, 13:17:33 pm
 * Author: Harry Crank
 * Email: harry.crank@thedistance.co.uk
 * Copyright (c) 2018 The Distance
 */

var ReadOnlyDateControl = function ReadOnlyDateControl(props) {
  return _react2.default.createElement(
    _GenericControl2.default,
    props,
    _react2.default.createElement(
      _rubix.FormControl.Static,
      null,
      props.value ? props.value.toLocaleString('en-GB', {
        weekday: 'long',
        year: 'numeric',
        month: 'long',
        day: 'numeric',
        hour: 'numeric',
        minute: 'numeric',
        second: 'numeric',
        timeZone: 'UTC'
      }) : 'N/A'
    )
  );
};

ReadOnlyDateControl.propTypes = {
  value: _propTypes2.default.instanceOf(Date)
};

exports.default = ReadOnlyDateControl;