'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _keys = require('ramda/src/keys');

var _keys2 = _interopRequireDefault(_keys);

var _path = require('ramda/src/path');

var _path2 = _interopRequireDefault(_path);

var _invoker = require('ramda/src/invoker');

var _invoker2 = _interopRequireDefault(_invoker);

var _compose = require('ramda/src/compose');

var _compose2 = _interopRequireDefault(_compose);

var _prop = require('ramda/src/prop');

var _prop2 = _interopRequireDefault(_prop);

var _flip = require('ramda/src/flip');

var _flip2 = _interopRequireDefault(_flip);

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _propTypes = require('prop-types');

var _propTypes2 = _interopRequireDefault(_propTypes);

var _rubix = require('@sketchpixy/rubix');

var _Entity = require('../Entity');

var _Entity2 = _interopRequireDefault(_Entity);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/**
 * @Author: benbriggs
 * @Date:   2018-04-20T11:06:19+01:00
 * @Email:  ben.briggs@thedistance.co.uk
 * @Last modified by:   benbriggs
 * @Last modified time: 2018-04-25T13:49:10+01:00
 * @Copyright: The Distance
 */

var propOf = (0, _flip2.default)(_prop2.default);
var parseNumber = (0, _flip2.default)(parseInt);
var parseDecimal = parseNumber(10);

var platforms = {
  1: 'iOS',
  2: 'Android'
};

var Platform = (0, _compose2.default)(propOf(platforms), parseDecimal, (0, _prop2.default)('value'));

var ReleaseDate = (0, _compose2.default)((0, _invoker2.default)(2, 'toLocaleString')('en-GB', {
  weekday: 'long',
  year: 'numeric',
  month: 'long',
  day: 'numeric',
  hour: 'numeric',
  minute: 'numeric',
  second: 'numeric',
  timeZone: 'UTC'
}), (0, _prop2.default)('value'));

var ReadFile = function ReadFile(props) {
  return _react2.default.createElement(
    _rubix.FormGroup,
    null,
    _react2.default.createElement(
      _rubix.ControlLabel,
      null,
      props.label
    ),
    _react2.default.createElement(
      _rubix.FormControl.Static,
      null,
      _react2.default.createElement(
        'a',
        { href: (0, _path2.default)(['value', '_url'], props) },
        (0, _path2.default)(['value', '_name'], props)
      )
    )
  );
};

ReadFile.propTypes = {
  label: _propTypes2.default.string.isRequired
};

var OtaRelease = (0, _Entity2.default)({
  name: 'OtaRelease',
  inflections: {
    singular: 'OTA Release',
    plural: 'OTA Releases'
  },
  readOnlyEditView: true,
  schema: {
    version: {
      type: 'string',
      showInIndex: true
    },
    releaseNotes: {
      type: 'textarea'
    },
    platform: {
      type: 'enumNumeric',
      choices: (0, _keys2.default)(platforms).map(function (key) {
        return {
          value: key,
          label: platforms[key]
        };
      }),
      IndexComponent: Platform,
      showInIndex: true,
      defaultTo: 1
    },
    release: {
      type: 'file',
      UpdateComponent: ReadFile
    },
    createdAt: {
      label: 'Release Date',
      type: 'staticDate',
      showInIndex: true,
      IndexComponent: ReleaseDate
    }
  },
  onLoadCollection: function onLoadCollection(query) {
    return query.descending('createdAt');
  }
});

exports.default = OtaRelease;