'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _StaticTextControl = require('../StaticTextControl');

var _StaticTextControl2 = _interopRequireDefault(_StaticTextControl);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/*
 * Created Date: Wed, 18th Apr 2018, 12:47:11 pm
 * Author: Harry Crank
 * Email: harry.crank@thedistance.co.uk
 * Copyright (c) 2018 The Distance
 */

var ReadOnlyNumberControl = function ReadOnlyNumberControl(rest) {
  return _react2.default.createElement(_StaticTextControl2.default, rest);
};

exports.default = ReadOnlyNumberControl;