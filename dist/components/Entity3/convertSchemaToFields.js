'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _inflected = require('inflected');

var _fieldToComponent = require('../Entity/fieldToComponent');

var _fieldToComponent2 = _interopRequireDefault(_fieldToComponent);

var _fieldToReadOnlyComponent = require('../Entity/fieldToReadOnlyComponent');

var _fieldToReadOnlyComponent2 = _interopRequireDefault(_fieldToReadOnlyComponent);

var _parseObjectAccessor = require('./parseObjectAccessor');

var _parseObjectAccessor2 = _interopRequireDefault(_parseObjectAccessor);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/**
 * @Author: benbriggs
 * @Date:   2018-05-22T15:09:32+01:00
 * @Email:  ben.briggs@thedistance.co.uk
 * @Last modified by:   benbriggs
 * @Last modified time: 2018-05-22T16:45:12+01:00
 * @Copyright: The Distance
 */

var convertSchemaToFields = function convertSchemaToFields(schema, view) {
  return Object.keys(schema).map(function (key) {
    return {
      accessor: (0, _parseObjectAccessor2.default)(schema[key].editAccessor || schema[key].accessor, key),
      EditComponent: (0, _fieldToComponent2.default)(schema[key], view),
      ReadComponent: (0, _fieldToReadOnlyComponent2.default)(schema[key], view),
      key: key,
      label: schema[key].label || (0, _inflected.titleize)(key),
      validator: schema[key].validator,
      helpText: schema[key].helpText
    };
  });
};

exports.default = convertSchemaToFields;