"use strict";

/**
 * @Author: benbriggs
 * @Date:   2018-05-22T16:44:07+01:00
 * @Email:  ben.briggs@thedistance.co.uk
 * @Last modified by:   benbriggs
 * @Last modified time: 2018-05-22T16:44:48+01:00
 * @Copyright: The Distance
 */

var parseObjectAccessor = function parseObjectAccessor(accessor, key) {
  return function (data) {
    var object = data.object;

    if (accessor) {
      return accessor(object);
    }
    return object.get(key);
  };
};

module.exports = parseObjectAccessor;