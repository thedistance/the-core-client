'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _mergeDeepRight = require('ramda/src/mergeDeepRight');

var _mergeDeepRight2 = _interopRequireDefault(_mergeDeepRight);

var _reactRedux = require('react-redux');

var _mapConnectors = require('../../Entity2/views/mapConnectors');

var _list = require('../views/list');

var _list2 = _interopRequireDefault(_list);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/**
 * @Author: benbriggs
 * @Date:   2018-05-22T15:22:32+01:00
 * @Email:  ben.briggs@thedistance.co.uk
 * @Last modified by:   benbriggs
 * @Last modified time: 2018-09-03T12:59:52+01:00
 * @Copyright: The Distance
 */

var listView = function listView() {
  var View = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : _list2.default;
  return function (_ref) {
    var actions = _ref.actions,
        listConnector = _ref.listConnector,
        loadCollection = _ref.loadCollection,
        unloadCollection = _ref.unloadCollection,
        selectPermissions = _ref.selectPermissions,
        selectCollection = _ref.selectCollection,
        selectPages = _ref.selectPages,
        selectErrorMessage = _ref.selectErrorMessage,
        slug = _ref.slug,
        singular = _ref.singular,
        plural = _ref.plural,
        collectionIndex = _ref.collectionIndex;

    var mapStateToProps = function mapStateToProps(state) {
      return (0, _mergeDeepRight2.default)({
        slug: slug,
        singular: singular,
        plural: plural,
        columns: collectionIndex,
        permissions: selectPermissions(state),
        data: selectCollection(state) || [],
        pages: selectPages(state),
        alert: selectErrorMessage(state)
      }, (0, _mapConnectors.mapState)(listConnector)(state));
    };

    var mapDispatchToProps = function mapDispatchToProps(dispatch, ownProps) {
      return (0, _mergeDeepRight2.default)({
        actions: {
          load: function load(tableState) {
            return dispatch(loadCollection(tableState));
          },
          unload: function unload() {
            return dispatch(unloadCollection());
          }
        }
      }, (0, _mapConnectors.mapDispatch)(listConnector)(actions)(dispatch, ownProps));
    };

    return (0, _reactRedux.connect)(mapStateToProps, mapDispatchToProps)(View);
  };
};

exports.default = listView;