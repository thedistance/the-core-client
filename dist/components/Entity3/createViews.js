'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; /**
                                                                                                                                                                                                                                                                   * @Author: benbriggs
                                                                                                                                                                                                                                                                   * @Date:   2018-05-22T15:15:01+01:00
                                                                                                                                                                                                                                                                   * @Email:  ben.briggs@thedistance.co.uk
                                                                                                                                                                                                                                                                   * @Last modified by:   benbriggs
                                                                                                                                                                                                                                                                   * @Last modified time: 2018-09-10T10:48:56+01:00
                                                                                                                                                                                                                                                                   * @Copyright: The Distance
                                                                                                                                                                                                                                                                   */

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _reactRouter = require('react-router');

var _list = require('./containers/list');

var _list2 = _interopRequireDefault(_list);

var _create = require('./containers/create');

var _create2 = _interopRequireDefault(_create);

var _detail = require('./containers/detail');

var _detail2 = _interopRequireDefault(_detail);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function createViews(entity, _ref) {
  var createConnector = _ref.createConnector,
      editConnector = _ref.editConnector,
      listConnector = _ref.listConnector,
      createView = _ref.createView,
      editView = _ref.editView,
      listView = _ref.listView,
      extraActions = _ref.extraActions,
      extraCreateActions = _ref.extraCreateActions,
      extraEditActions = _ref.extraEditActions,
      warnOnDelete = _ref.warnOnDelete;
  var camelised = entity.camelised,
      collectionIndex = entity.collectionIndex,
      plural = entity.plural,
      createSchemaFields = entity.createSchemaFields,
      editSchemaFields = entity.editSchemaFields,
      singular = entity.singular,
      slug = entity.slug,
      _entity$selectors = entity.selectors,
      selectCurrent = _entity$selectors.selectCurrent,
      selectErrorMessage = _entity$selectors.selectErrorMessage,
      selectPermissions = _entity$selectors.selectPermissions,
      selectPages = _entity$selectors.selectPages,
      selectCollection = _entity$selectors.selectCollection,
      selectSaved = _entity$selectors.selectSaved,
      actions = entity.actions,
      _entity$actions = entity.actions,
      create = _entity$actions.create,
      destroy = _entity$actions.destroy,
      loadCollection = _entity$actions.loadCollection,
      loadCurrent = _entity$actions.loadCurrent,
      save = _entity$actions.save,
      revert = _entity$actions.revert,
      unloadCollection = _entity$actions.unloadCollection,
      unloadCurrent = _entity$actions.unloadCurrent,
      update = _entity$actions.update;


  var routeId = camelised + 'Id';

  var exposedCreateView = (0, _create2.default)(createView)({
    actions: actions,
    createConnector: createConnector,
    create: create,
    save: save,
    unloadCurrent: unloadCurrent,
    update: update,
    plural: plural,
    revert: revert,
    extraActions: extraActions,
    extraCreateActions: extraCreateActions,
    selectCurrent: selectCurrent,
    selectErrorMessage: selectErrorMessage,
    selectSaved: selectSaved,
    schemaFields: createSchemaFields,
    slug: slug,
    singular: singular
  });

  var exposedEditView = (0, _detail2.default)(editView)({
    actions: actions,
    editConnector: editConnector,
    save: save,
    destroy: destroy,
    loadCurrent: loadCurrent,
    unloadCurrent: unloadCurrent,
    update: update,
    plural: plural,
    revert: revert,
    extraActions: extraActions,
    extraEditActions: extraEditActions,
    routeId: routeId,
    selectCurrent: selectCurrent,
    selectErrorMessage: selectErrorMessage,
    selectSaved: selectSaved,
    schemaFields: editSchemaFields,
    slug: slug,
    singular: singular,
    warnOnDelete: warnOnDelete
  });

  var exposedListView = (0, _list2.default)(listView)({
    actions: actions,
    listConnector: listConnector,
    loadCollection: loadCollection,
    unloadCollection: unloadCollection,
    selectPermissions: selectPermissions,
    selectPages: selectPages,
    selectCollection: selectCollection,
    selectErrorMessage: selectErrorMessage,
    singular: singular,
    slug: slug,
    plural: plural,
    collectionIndex: collectionIndex
  });

  return _extends({}, entity, {
    routeId: routeId,
    createView: exposedCreateView,
    editView: exposedEditView,
    listView: exposedListView,
    routes: [_react2.default.createElement(_reactRouter.Route, {
      path: '/' + slug,
      component: exposedListView,
      key: camelised + '__list'
    }), _react2.default.createElement(_reactRouter.Route, {
      path: '/' + slug + '/new',
      component: exposedCreateView,
      key: camelised + '__create'
    }), _react2.default.createElement(_reactRouter.Route, {
      path: '/' + slug + '/:' + routeId,
      component: exposedEditView,
      key: camelised + '__edit'
    })]
  });
}

exports.default = createViews;