'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _parse = require('parse');

var _parse2 = _interopRequireDefault(_parse);

var _redirect = require('../../Entity/redirect');

var _redirect2 = _interopRequireDefault(_redirect);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/**
 * @Author: benbriggs
 * @Date:   2018-04-30T11:46:26+01:00
 * @Email:  ben.briggs@thedistance.co.uk
 * @Last modified by:   benbriggs
 * @Last modified time: 2018-05-23T08:50:35+01:00
 * @Copyright: The Distance
 */

var save = function save(_ref) {
  var selectCurrent = _ref.selectCurrent,
      startedAction = _ref.startedAction,
      completedAction = _ref.completedAction,
      failedAction = _ref.failedAction;
  return function (redirectOptions) {
    return function (dispatch, getState) {
      var result = selectCurrent(getState());
      dispatch(startedAction({ result: result }));

      return result.object.save().then(function () {
        return dispatch(completedAction({ result: result }));
      }).then(function () {
        return redirectOptions && (0, _redirect2.default)(redirectOptions);
      }).catch(function (error) {
        if (error instanceof _parse2.default.Error) {
          return dispatch(failedAction({ error: error }));
        }
        // Handle large file error
        if (error.status === 413) {
          return dispatch(failedAction({
            error: {
              code: error.status,
              message: 'The file you tried to upload is too large.'
            }
          }));
        }
        // Other http error
        return dispatch(failedAction({
          error: { code: error.status, message: error.statusText }
        }));
      });
    };
  };
};

exports.default = save;