'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _parse = require('parse');

var _parse2 = _interopRequireDefault(_parse);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var loadCurrent = function loadCurrent(_ref) {
  var findOne = _ref.findOne,
      startedSyncAction = _ref.startedSyncAction,
      startedAction = _ref.startedAction,
      completedAction = _ref.completedAction,
      selectCurrent = _ref.selectCurrent;
  return function () {
    var _ref2 = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {},
        id = _ref2.id;

    return function (dispatch, getState) {
      var current = selectCurrent(getState());

      if (current) {
        return Promise.resolve(dispatch(startedSyncAction({
          result: current
        })));
      }

      if (!id) {
        return;
      }

      dispatch(startedAction());

      return _parse2.default.Cloud.run(findOne, { id: id }).then(function (result) {
        dispatch(completedAction({ result: result }));
        return result;
      });
    };
  };
}; /**
    * @Author: benbriggs
    * @Date:   2018-05-22T15:53:29+01:00
    * @Email:  ben.briggs@thedistance.co.uk
    * @Last modified by:   benbriggs
    * @Last modified time: 2018-05-22T15:54:31+01:00
    * @Copyright: The Distance
    */

exports.default = loadCurrent;