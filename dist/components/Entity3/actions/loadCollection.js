'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _parse = require('parse');

var _parse2 = _interopRequireDefault(_parse);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var loadCollection = function loadCollection(_ref) {
  var findAll = _ref.findAll,
      startedAction = _ref.startedAction,
      completedAction = _ref.completedAction,
      failedAction = _ref.failedAction;
  return function () {
    var _ref2 = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {},
        _ref2$pageSize = _ref2.pageSize,
        pageSize = _ref2$pageSize === undefined ? 20 : _ref2$pageSize,
        _ref2$page = _ref2.page,
        page = _ref2$page === undefined ? 0 : _ref2$page,
        sorted = _ref2.sorted,
        filtered = _ref2.filtered;

    return function (dispatch) {
      dispatch(startedAction());

      return _parse2.default.Cloud.run(findAll, { pageSize: pageSize, page: page, sorted: sorted, filtered: filtered }).then(function (result) {
        return dispatch(completedAction(result));
      }).catch(function (error) {
        return dispatch(failedAction({ error: error }));
      });
    };
  };
}; /**
    * @Author: benbriggs
    * @Date:   2018-05-22T15:45:35+01:00
    * @Email:  ben.briggs@thedistance.co.uk
    * @Last modified by:   benbriggs
    * @Last modified time: 2018-05-24T12:05:03+01:00
    * @Copyright: The Distance
    */

exports.default = loadCollection;