'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _path = require('ramda/src/path');

var _path2 = _interopRequireDefault(_path);

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _propTypes = require('prop-types');

var _propTypes2 = _interopRequireDefault(_propTypes);

var _rubix = require('@sketchpixy/rubix');

var _Alert = require('../../Alert');

var _Alert2 = _interopRequireDefault(_Alert);

var _Panel = require('../../Panel');

var _Panel2 = _interopRequireDefault(_Panel);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; } /**
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                * @Author: benbriggs
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                * @Date:   2018-05-22T14:30:19+01:00
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                * @Email:  ben.briggs@thedistance.co.uk
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                * @Last modified by:   benbriggs
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                * @Last modified time: 2018-09-10T10:53:58+01:00
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                * @Copyright: The Distance
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                */

var EditActions = function EditActions(_ref) {
  var actions = _ref.actions;
  return actions.delete ? _react2.default.createElement(
    _rubix.Button,
    { bsStyle: 'danger', onClick: actions.delete },
    'Delete'
  ) : _react2.default.createElement(
    _rubix.Button,
    { onClick: actions.cancel },
    'Cancel'
  );
};

EditActions.propTypes = {
  actions: _propTypes2.default.objectOf(_propTypes2.default.func).isRequired
};

var EditView = function (_React$Component) {
  _inherits(EditView, _React$Component);

  function EditView(props) {
    _classCallCheck(this, EditView);

    var _this = _possibleConstructorReturn(this, (EditView.__proto__ || Object.getPrototypeOf(EditView)).call(this, props));

    _this.state = {
      saved: true,
      showModal: false
    };

    _this.closeModal = _this.closeModal.bind(_this);
    _this.openModal = _this.openModal.bind(_this);
    _this.deleteFromModal = _this.deleteFromModal.bind(_this);
    return _this;
  }

  _createClass(EditView, [{
    key: 'onChange',
    value: function onChange(key, value) {
      this.setState({ saved: false });
      this.props.actions.update({ key: key, value: value });
    }
  }, {
    key: 'componentWillMount',
    value: function componentWillMount() {
      this.props.actions.load();
    }
  }, {
    key: 'componentWillUnmount',
    value: function componentWillUnmount() {
      if (!this.state.saved) {
        this.props.actions.revert();
      }
      this.props.actions.unload();
    }
  }, {
    key: 'closeModal',
    value: function closeModal() {
      this.setState({ showModal: false });
    }
  }, {
    key: 'openModal',
    value: function openModal() {
      this.setState({ showModal: true });
    }
  }, {
    key: 'deleteFromModal',
    value: function deleteFromModal() {
      this.props.actions.delete();
      this.closeModal();
    }
  }, {
    key: 'save',
    value: function save() {
      this.setState({ saved: true });
      this.props.actions.save();
    }
  }, {
    key: 'render',
    value: function render() {
      var _this2 = this;

      var _props = this.props,
          actions = _props.actions,
          alert = _props.alert,
          model = _props.model,
          fields = _props.fields,
          saved = _props.saved,
          singular = _props.singular,
          warnOnDelete = _props.warnOnDelete,
          hideDelete = _props.hideDelete;

      var savedOverride = (0, _path2.default)(['location', 'state', 'savedOverride'], this.props);
      if (!model) {
        return null;
      }
      var object = model.object;

      return _react2.default.createElement(
        'div',
        null,
        warnOnDelete ? _react2.default.createElement(
          _rubix.Modal,
          { show: this.state.showModal, onHide: this.closeModal },
          _react2.default.createElement(
            _rubix.Modal.Body,
            null,
            _react2.default.createElement(
              'p',
              null,
              'Are you sure that you want to delete this record? This cannot be undone.'
            )
          ),
          _react2.default.createElement(
            _rubix.Modal.Footer,
            null,
            _react2.default.createElement(
              _rubix.ButtonToolbar,
              null,
              _react2.default.createElement(
                _rubix.Button,
                { onClick: this.closeModal },
                'Cancel'
              ),
              _react2.default.createElement(
                _rubix.Button,
                { bsStyle: 'danger', onClick: this.deleteFromModal },
                'Delete'
              )
            )
          )
        ) : null,
        alert ? _react2.default.createElement(
          _Alert2.default,
          { danger: true },
          alert
        ) : null,
        saved || savedOverride ? _react2.default.createElement(
          _Alert2.default,
          { success: true },
          'Saved successfully.'
        ) : null,
        _react2.default.createElement(
          _Panel2.default,
          {
            title: object.isNew() ? 'New ' + singular : singular + ' Details'
          },
          _react2.default.createElement(
            _rubix.Grid,
            null,
            _react2.default.createElement(
              _rubix.Row,
              null,
              _react2.default.createElement(
                _rubix.Col,
                { xs: 12 },
                _react2.default.createElement(
                  _rubix.Form,
                  { autoComplete: 'off' },
                  fields.map(function (_ref2, index) {
                    var label = _ref2.label,
                        accessor = _ref2.accessor,
                        key = _ref2.key,
                        validator = _ref2.validator,
                        helpText = _ref2.helpText,
                        EditComponent = _ref2.EditComponent,
                        ReadComponent = _ref2.ReadComponent;

                    var Component = (0, _path2.default)(['permissions', 'update'], model) ? EditComponent : ReadComponent;

                    return _react2.default.createElement(Component, {
                      actions: actions,
                      key: index,
                      label: label,
                      value: accessor(model),
                      validator: validator,
                      helpText: helpText,
                      onChange: _this2.onChange.bind(_this2, key)
                    });
                  }),
                  _react2.default.createElement(
                    _rubix.Grid,
                    { style: { marginBottom: '1em' } },
                    _react2.default.createElement(
                      _rubix.Row,
                      null,
                      _react2.default.createElement(
                        _rubix.Col,
                        { xs: 6 },
                        _react2.default.createElement(
                          _rubix.ButtonToolbar,
                          null,
                          _react2.default.createElement(
                            _rubix.Button,
                            { onClick: actions.cancel },
                            'Cancel'
                          ),
                          (0, _path2.default)(['permissions', 'delete'], model) && !hideDelete && _react2.default.createElement(
                            _rubix.Button,
                            {
                              bsStyle: 'danger',
                              onClick: warnOnDelete ? this.openModal : actions.delete
                            },
                            'Delete'
                          )
                        )
                      ),
                      (0, _path2.default)(['permissions', 'update'], model) && _react2.default.createElement(
                        _rubix.Col,
                        { xs: 6, style: { textAlign: 'right' } },
                        _react2.default.createElement(
                          _rubix.Button,
                          {
                            bsStyle: 'primary',
                            onClick: this.save.bind(this)
                          },
                          'Save'
                        )
                      )
                    )
                  )
                )
              )
            )
          )
        )
      );
    }
  }]);

  return EditView;
}(_react2.default.Component);

EditView.propTypes = {
  actions: _propTypes2.default.objectOf(_propTypes2.default.func).isRequired,
  alert: _propTypes2.default.string,
  fields: _propTypes2.default.array.isRequired,
  model: _propTypes2.default.object,
  saved: _propTypes2.default.bool,
  singular: _propTypes2.default.string.isRequired,
  warnOnDelete: _propTypes2.default.bool
};

exports.default = EditView;