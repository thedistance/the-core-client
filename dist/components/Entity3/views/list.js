'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _concat = require('ramda/src/concat');

var _concat2 = _interopRequireDefault(_concat);

var _path = require('ramda/src/path');

var _path2 = _interopRequireDefault(_path);

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _propTypes = require('prop-types');

var _propTypes2 = _interopRequireDefault(_propTypes);

var _reactTable = require('react-table');

var _reactTable2 = _interopRequireDefault(_reactTable);

var _rubix = require('@sketchpixy/rubix');

var _getOrDotPath = require('the-core-utils/src/getOrDotPath');

var _getOrDotPath2 = _interopRequireDefault(_getOrDotPath);

var _Alert = require('../../Alert');

var _Alert2 = _interopRequireDefault(_Alert);

var _Panel = require('../../Panel');

var _Panel2 = _interopRequireDefault(_Panel);

var _LinkButton = require('../../LinkButton');

var _LinkButton2 = _interopRequireDefault(_LinkButton);

require('react-table/react-table.css');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; } /**
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                * @Author: benbriggs
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                * @Date:   2018-05-21T12:22:37+01:00
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                * @Email:  ben.briggs@thedistance.co.uk
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                * @Last modified by:   benbriggs
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                * @Last modified time: 2018-09-10T10:50:56+01:00
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                * @Copyright: The Distance
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                */

var ListView = function (_React$Component) {
  _inherits(ListView, _React$Component);

  function ListView(props) {
    _classCallCheck(this, ListView);

    var _this = _possibleConstructorReturn(this, (ListView.__proto__ || Object.getPrototypeOf(ListView)).call(this, props));

    _this.state = {
      loading: true
    };
    _this.fetchData = _this.fetchData.bind(_this);
    return _this;
  }

  _createClass(ListView, [{
    key: 'componentWillUnmount',
    value: function componentWillUnmount() {
      this.props.actions.unload();
    }
  }, {
    key: 'fetchData',
    value: function fetchData(tableState) {
      var _this2 = this;

      this.setState({ loading: true });

      this.props.actions.load(tableState).then(function () {
        _this2.setState({
          loading: false
        });
      });
    }
  }, {
    key: 'render',
    value: function render() {
      var _this3 = this;

      var loading = this.state.loading;
      var _props = this.props,
          alert = _props.alert,
          data = _props.data,
          pages = _props.pages,
          columns = _props.columns,
          singular = _props.singular,
          plural = _props.plural,
          route = _props.route,
          getTheadFilterThProps = _props.getTheadFilterThProps;

      var create = (0, _path2.default)(['permissions', 'create'], this.props);
      var createPath = this.props.createPath ? this.props.createPath(route) : route.path + '/new';
      return _react2.default.createElement(
        'div',
        null,
        alert && _react2.default.createElement(
          _Alert2.default,
          { danger: true },
          alert
        ),
        _react2.default.createElement(
          _Panel2.default,
          {
            title: plural,
            buttonTo: create ? createPath : null,
            buttonTitle: create ? 'New ' + singular : null
          },
          _react2.default.createElement(
            _rubix.Grid,
            null,
            _react2.default.createElement(
              _rubix.Row,
              null,
              _react2.default.createElement(
                _rubix.Col,
                { xs: 12 },
                _react2.default.createElement(_reactTable2.default, {
                  data: data,
                  pages: pages,
                  loading: loading,
                  getTheadFilterThProps: getTheadFilterThProps,
                  onFetchData: this.fetchData,
                  columns: (0, _concat2.default)(columns, [{
                    accessor: (0, _getOrDotPath2.default)(['object', 'id']),
                    id: 'id',
                    Cell: function Cell(props) {
                      var editPath = _this3.props.editPath ? _this3.props.editPath(route, props) : route.path + '/' + props.value;

                      return _react2.default.createElement(
                        _LinkButton2.default,
                        { pathname: editPath, outlined: true },
                        (0, _path2.default)(['original', 'permissions', 'update'], props) ? 'Edit' : 'View'
                      );
                    },
                    filterable: false,
                    sortable: false
                  }]),
                  style: { marginTop: '10px', marginBottom: '40px' },
                  filterable: true,
                  manual: true
                })
              )
            )
          )
        )
      );
    }
  }]);

  return ListView;
}(_react2.default.Component);

ListView.propTypes = {
  actions: _propTypes2.default.objectOf(_propTypes2.default.func).isRequired,
  alert: _propTypes2.default.string,
  columns: _propTypes2.default.array.isRequired,
  createPath: _propTypes2.default.func,
  editPath: _propTypes2.default.func,
  data: _propTypes2.default.array,
  pages: _propTypes2.default.number,
  route: _propTypes2.default.any,
  saved: _propTypes2.default.bool,
  singular: _propTypes2.default.string.isRequired,
  plural: _propTypes2.default.string.isRequired,
  getTheadFilterThProps: _propTypes2.default.func
};

exports.default = ListView;