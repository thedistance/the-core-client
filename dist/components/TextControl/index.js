'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _propTypes = require('prop-types');

var _propTypes2 = _interopRequireDefault(_propTypes);

var _rubix = require('@sketchpixy/rubix');

var _GenericControl = require('../GenericControl');

var _GenericControl2 = _interopRequireDefault(_GenericControl);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/**
 * @Author: Ben Briggs <benbriggs>
 * @Date:   2017-12-15T12:05:26+00:00
 * @Email:  ben.briggs@thedistance.co.uk
 * @Last modified by:   benbriggs
 * @Last modified time: 2017-12-19T09:40:31+00:00
 * @Copyright: The Distance
 */

var TextControl = function TextControl(props) {
  return _react2.default.createElement(
    _GenericControl2.default,
    props,
    _react2.default.createElement(_rubix.FormControl, { type: 'text' })
  );
};

TextControl.propTypes = {
  value: _propTypes2.default.string
};

exports.default = TextControl;