"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
/**
 * @Author: benbriggs
 * @Date:   2018-05-08T15:48:03+01:00
 * @Email:  ben.briggs@thedistance.co.uk
 * @Last modified by:   benbriggs
 * @Last modified time: 2018-05-08T15:48:26+01:00
 * @Copyright: The Distance
 */

// A function that returns a placeholder function no matter
// how many times it is called.
var fnPlaceholder = function fnPlaceholder() {
  return fnPlaceholder;
};

exports.default = fnPlaceholder;