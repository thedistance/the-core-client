"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
/**
 * @Author: benbriggs
 * @Date:   2018-04-30T11:34:52+01:00
 * @Email:  ben.briggs@thedistance.co.uk
 * @Last modified by:   benbriggs
 * @Last modified time: 2018-04-30T12:13:09+01:00
 * @Copyright: The Distance
 */

var revert = function revert(_ref) {
  var selectCurrent = _ref.selectCurrent,
      action = _ref.action;
  return function () {
    return function (dispatch, getState) {
      var result = selectCurrent(getState());
      if (!result) {
        return;
      }
      result.revert();
      return dispatch(action({ result: result }));
    };
  };
};

exports.default = revert;