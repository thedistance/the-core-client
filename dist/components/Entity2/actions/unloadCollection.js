"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
/**
 * @Author: benbriggs
 * @Date:   2018-04-30T11:48:30+01:00
 * @Email:  ben.briggs@thedistance.co.uk
 * @Last modified by:   benbriggs
 * @Last modified time: 2018-04-30T12:13:24+01:00
 * @Copyright: The Distance
 */

var unloadCollection = function unloadCollection(_ref) {
  var selectCollection = _ref.selectCollection,
      action = _ref.action;
  return function () {
    return function (dispatch, getState) {
      return dispatch(action({ result: selectCollection(getState()) }));
    };
  };
};

exports.default = unloadCollection;