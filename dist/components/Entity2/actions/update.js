"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
/**
 * @Author: benbriggs
 * @Date:   2018-04-30T11:50:47+01:00
 * @Email:  ben.briggs@thedistance.co.uk
 * @Last modified by:   benbriggs
 * @Last modified time: 2018-04-30T12:13:43+01:00
 * @Copyright: The Distance
 */

var update = function update(_ref) {
  var selectCurrent = _ref.selectCurrent,
      action = _ref.action;
  return function (_ref2) {
    var key = _ref2.key,
        value = _ref2.value;
    return function (dispatch, getState) {
      var result = selectCurrent(getState());
      result.set(key, value);
      return dispatch(action({ result: result }));
    };
  };
};

exports.default = update;