"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
/**
 * @Author: benbriggs
 * @Date:   2018-04-30T11:49:58+01:00
 * @Email:  ben.briggs@thedistance.co.uk
 * @Last modified by:   benbriggs
 * @Last modified time: 2018-04-30T12:13:34+01:00
 * @Copyright: The Distance
 */

var unloadCurrent = function unloadCurrent(_ref) {
  var selectCurrent = _ref.selectCurrent,
      action = _ref.action;
  return function () {
    return function (dispatch, getState) {
      return dispatch(action({ result: selectCurrent(getState()) }));
    };
  };
};

exports.default = unloadCurrent;