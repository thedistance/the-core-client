'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _identity = require('ramda/src/identity');

var _identity2 = _interopRequireDefault(_identity);

var _parse = require('parse');

var _parse2 = _interopRequireDefault(_parse);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var loadCurrent = function loadCurrent(_ref) {
  var className = _ref.className,
      _ref$queryTransform = _ref.queryTransform,
      queryTransform = _ref$queryTransform === undefined ? _identity2.default : _ref$queryTransform,
      startedSyncAction = _ref.startedSyncAction,
      startedAction = _ref.startedAction,
      completedAction = _ref.completedAction,
      selectCurrent = _ref.selectCurrent;
  return function () {
    var _ref2 = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {},
        id = _ref2.id;

    return function (dispatch, getState) {
      var current = selectCurrent(getState());

      if (current) {
        return Promise.resolve(dispatch(startedSyncAction({
          result: current
        })));
      }

      if (!id) {
        return;
      }

      dispatch(startedAction());

      return queryTransform(new _parse2.default.Query(className)).get(id).then(function (result) {
        dispatch(completedAction({ result: result }));
        return result;
      });
    };
  };
}; /**
    * @Author: benbriggs
    * @Date:   2018-04-30T11:26:59+01:00
    * @Email:  ben.briggs@thedistance.co.uk
    * @Last modified by:   benbriggs
    * @Last modified time: 2018-04-30T12:19:50+01:00
    * @Copyright: The Distance
    */

exports.default = loadCurrent;