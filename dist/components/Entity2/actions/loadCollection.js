'use strict';

Object.defineProperty(exports, '__esModule', {
  value: true,
});

var _identity = require('ramda/src/identity');

var _identity2 = _interopRequireDefault(_identity);

var _slicedToArray = (function() {
  function sliceIterator(arr, i) {
    var _arr = [];
    var _n = true;
    var _d = false;
    var _e = undefined;
    try {
      for (
        var _i = arr[Symbol.iterator](), _s;
        !(_n = (_s = _i.next()).done);
        _n = true
      ) {
        _arr.push(_s.value);
        if (i && _arr.length === i) break;
      }
    } catch (err) {
      _d = true;
      _e = err;
    } finally {
      try {
        if (!_n && _i['return']) _i['return']();
      } finally {
        if (_d) throw _e;
      }
    }
    return _arr;
  }
  return function(arr, i) {
    if (Array.isArray(arr)) {
      return arr;
    } else if (Symbol.iterator in Object(arr)) {
      return sliceIterator(arr, i);
    } else {
      throw new TypeError(
        'Invalid attempt to destructure non-iterable instance'
      );
    }
  };
})(); /**
 * @Author: benbriggs
 * @Date:   2018-04-30T11:30:54+01:00
 * @Email:  ben.briggs@thedistance.co.uk
 * @Last modified by:   benbriggs
 * @Last modified time: 2018-04-30T12:19:16+01:00
 * @Copyright: The Distance
 */

var _parse = require('parse');

var _parse2 = _interopRequireDefault(_parse);

var _escapeStringRegexp = require('escape-string-regexp');

var _escapeStringRegexp2 = _interopRequireDefault(_escapeStringRegexp);

function _interopRequireDefault(obj) {
  return obj && obj.__esModule ? obj : { default: obj };
}

var loadCollection = function loadCollection(_ref) {
  var className = _ref.className,
    _ref$queryTransform = _ref.queryTransform,
    queryTransform =
      _ref$queryTransform === undefined
        ? _identity2.default
        : _ref$queryTransform,
    startedAction = _ref.startedAction,
    completedAction = _ref.completedAction;
  return function() {
    var _ref2 =
        arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {},
      _ref2$pageSize = _ref2.pageSize,
      pageSize = _ref2$pageSize === undefined ? 20 : _ref2$pageSize,
      _ref2$page = _ref2.page,
      page = _ref2$page === undefined ? 0 : _ref2$page,
      sorted = _ref2.sorted,
      filtered = _ref2.filtered;

    return function(dispatch) {
      dispatch(startedAction());

      var data = queryTransform(new _parse2.default.Query(className))
        .limit(pageSize)
        .skip(pageSize * page);

      if (filtered && filtered.length) {
        filtered.forEach(function(_ref3) {
          var id = _ref3.id,
            value = _ref3.value;
          return data.matches(
            id,
            new RegExp((0, _escapeStringRegexp2.default)(value), 'i')
          );
        });
      }

      if (sorted && sorted.length) {
        sorted.forEach(function(_ref4) {
          var id = _ref4.id,
            desc = _ref4.desc;
          return desc ? data.descending(id) : data.ascending(id);
        });
      }

      return Promise.all([data.count(), data.find()]).then(function(_ref5) {
        var _ref6 = _slicedToArray(_ref5, 2),
          count = _ref6[0],
          result = _ref6[1];

        return dispatch(
          completedAction({
            result: result,
            pages: Math.ceil(count / pageSize),
          })
        );
      });
    };
  };
};

exports.default = loadCollection;
