'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _merge = require('ramda/src/merge');

var _merge2 = _interopRequireDefault(_merge);

var _parse = require('parse');

var _parse2 = _interopRequireDefault(_parse);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var create = function create(_ref) {
  var action = _ref.action,
      className = _ref.className,
      _ref$defaultValues = _ref.defaultValues,
      defaultValues = _ref$defaultValues === undefined ? {} : _ref$defaultValues;
  return function () {
    var params = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {};
    return function (dispatch) {
      var Instance = _parse2.default.Object.extend(className);
      return dispatch(action({
        result: new Instance((0, _merge2.default)(defaultValues, params))
      }));
    };
  };
}; /**
    * @Author: benbriggs
    * @Date:   2018-04-30T10:57:31+01:00
    * @Email:  ben.briggs@thedistance.co.uk
    * @Last modified by:   benbriggs
    * @Last modified time: 2018-04-30T11:16:42+01:00
    * @Copyright: The Distance
    */

exports.default = create;