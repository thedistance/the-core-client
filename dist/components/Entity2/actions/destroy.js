'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _always = require('ramda/src/always');

var _always2 = _interopRequireDefault(_always);

var _redirect = require('../../Entity/redirect');

var _redirect2 = _interopRequireDefault(_redirect);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/**
 * @Author: benbriggs
 * @Date:   2018-04-30T11:21:46+01:00
 * @Email:  ben.briggs@thedistance.co.uk
 * @Last modified by:   benbriggs
 * @Last modified time: 2018-04-30T12:20:21+01:00
 * @Copyright: The Distance
 */

var parseDestroy = function parseDestroy(object) {
  return new Promise(function (resolve, reject) {
    object.destroy({
      success: function success(obj) {
        return resolve(obj);
      },
      error: function error(err) {
        return reject(err);
      }
    });
  });
};

var destroy = function destroy(_ref) {
  var startedAction = _ref.startedAction,
      completedAction = _ref.completedAction,
      failedAction = _ref.failedAction,
      selectCurrent = _ref.selectCurrent;
  return function (redirectOptions) {
    return function (dispatch, getState) {
      var result = selectCurrent(getState());
      dispatch(startedAction({ result: result }));

      return parseDestroy(result).then(function () {
        return dispatch(completedAction({ result: result }));
      }).then((0, _always2.default)(redirectOptions)).then(_redirect2.default).catch(function (error) {
        return dispatch(failedAction({ error: error }));
      });
    };
  };
};

exports.default = destroy;