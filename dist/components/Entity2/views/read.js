'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _mergeDeepRight = require('ramda/src/mergeDeepRight');

var _mergeDeepRight2 = _interopRequireDefault(_mergeDeepRight);

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; /**
                                                                                                                                                                                                                                                                   * @Author: benbriggs
                                                                                                                                                                                                                                                                   * @Date:   2018-05-01T12:22:49+01:00
                                                                                                                                                                                                                                                                   * @Email:  ben.briggs@thedistance.co.uk
                                                                                                                                                                                                                                                                   * @Last modified by:   benbriggs
                                                                                                                                                                                                                                                                   * @Last modified time: 2018-05-08T15:50:36+01:00
                                                                                                                                                                                                                                                                   * @Copyright: The Distance
                                                                                                                                                                                                                                                                   */

var _reactRedux = require('react-redux');

var _readOnlyView = require('./../../Entity/readOnlyView');

var _readOnlyView2 = _interopRequireDefault(_readOnlyView);

var _mapConnectors = require('./mapConnectors');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var readView = function readView() {
  var View = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : _readOnlyView2.default;
  return function (_ref) {
    var actions = _ref.actions,
        editConnector = _ref.editConnector,
        loadCurrent = _ref.loadCurrent,
        unloadCurrent = _ref.unloadCurrent,
        routeId = _ref.routeId,
        _revert = _ref.revert,
        selectCurrent = _ref.selectCurrent,
        schemaFields = _ref.schemaFields,
        singular = _ref.singular,
        _ref$goBack = _ref.goBack,
        goBack = _ref$goBack === undefined ? 1 : _ref$goBack,
        extraActions = _ref.extraActions,
        extraEditActions = _ref.extraEditActions;

    var mapStateToProps = function mapStateToProps(state) {
      return (0, _mergeDeepRight2.default)({
        model: selectCurrent(state),
        fields: schemaFields,
        title: 'View ' + singular
      }, (0, _mapConnectors.mapState)(editConnector)(state));
    };

    var mapDispatchToProps = function mapDispatchToProps(dispatch, ownProps) {
      return (0, _mergeDeepRight2.default)({
        actions: _extends({
          load: function load() {
            return dispatch(loadCurrent({ id: ownProps.routeParams[routeId] }));
          },
          cancel: function cancel() {
            var routeParts = ownProps.location.pathname.split('/').slice(0, goBack * -1);
            ownProps.router.push(routeParts.join('/'));
          },
          unload: function unload() {
            return dispatch(unloadCurrent());
          },
          revert: function revert() {
            return dispatch(_revert());
          }
        }, extraActions(dispatch, ownProps), extraEditActions(dispatch, ownProps))
      }, (0, _mapConnectors.mapDispatch)(editConnector)(actions)(dispatch, ownProps));
    };

    return (0, _reactRedux.connect)(mapStateToProps, mapDispatchToProps)(View);
  };
};

exports.default = readView;