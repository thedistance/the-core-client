'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _mergeDeepRight = require('ramda/src/mergeDeepRight');

var _mergeDeepRight2 = _interopRequireDefault(_mergeDeepRight);

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; /**
                                                                                                                                                                                                                                                                   * @Author: benbriggs
                                                                                                                                                                                                                                                                   * @Date:   2018-05-01T11:40:16+01:00
                                                                                                                                                                                                                                                                   * @Email:  ben.briggs@thedistance.co.uk
                                                                                                                                                                                                                                                                   * @Last modified by:   benbriggs
                                                                                                                                                                                                                                                                   * @Last modified time: 2018-05-08T15:50:47+01:00
                                                                                                                                                                                                                                                                   * @Copyright: The Distance
                                                                                                                                                                                                                                                                   */

var _reactRedux = require('react-redux');

var _editView = require('./../../Entity/editView');

var _editView2 = _interopRequireDefault(_editView);

var _mapConnectors = require('./mapConnectors');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var editView = function editView() {
  var View = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : _editView2.default;
  return function (_ref) {
    var actions = _ref.actions,
        editConnector = _ref.editConnector,
        _save = _ref.save,
        destroy = _ref.destroy,
        loadCurrent = _ref.loadCurrent,
        unloadCurrent = _ref.unloadCurrent,
        _update = _ref.update,
        routeId = _ref.routeId,
        _revert = _ref.revert,
        selectCurrent = _ref.selectCurrent,
        selectErrorMessage = _ref.selectErrorMessage,
        schemaFields = _ref.schemaFields,
        singular = _ref.singular,
        _ref$goBack = _ref.goBack,
        goBack = _ref$goBack === undefined ? 1 : _ref$goBack,
        extraActions = _ref.extraActions,
        extraEditActions = _ref.extraEditActions;

    var mapStateToProps = function mapStateToProps(state) {
      return (0, _mergeDeepRight2.default)({
        alert: selectErrorMessage(state),
        model: selectCurrent(state),
        fields: schemaFields,
        title: 'Edit ' + singular
      }, (0, _mapConnectors.mapState)(editConnector)(state));
    };

    var mapDispatchToProps = function mapDispatchToProps(dispatch, ownProps) {
      return (0, _mergeDeepRight2.default)({
        actions: _extends({
          load: function load() {
            return dispatch(loadCurrent({ id: ownProps.routeParams[routeId] }));
          },
          save: function save() {
            return dispatch(_save({
              pathname: ownProps.location.pathname,
              router: ownProps.router,
              goBack: goBack
            }));
          },
          delete: function _delete() {
            return dispatch(destroy({
              pathname: ownProps.location.pathname,
              router: ownProps.router,
              goBack: goBack
            }));
          },
          unload: function unload() {
            return dispatch(unloadCurrent());
          },
          update: function update(options) {
            return dispatch(_update(options));
          },
          revert: function revert() {
            return dispatch(_revert());
          }
        }, extraActions(dispatch, ownProps), extraEditActions(dispatch, ownProps))
      }, (0, _mapConnectors.mapDispatch)(editConnector)(actions)(dispatch, ownProps));
    };

    return (0, _reactRedux.connect)(mapStateToProps, mapDispatchToProps)(View);
  };
};

exports.default = editView;