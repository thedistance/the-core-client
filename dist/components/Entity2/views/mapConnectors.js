'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.mapState = exports.mapDispatch = undefined;

var _prop = require('ramda/src/prop');

var _prop2 = _interopRequireDefault(_prop);

var _defaultTo = require('ramda/src/defaultTo');

var _defaultTo2 = _interopRequireDefault(_defaultTo);

var _compose = require('ramda/src/compose');

var _compose2 = _interopRequireDefault(_compose);

var _placeholder = require('./../placeholder');

var _placeholder2 = _interopRequireDefault(_placeholder);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/**
 * @Author: benbriggs
 * @Date:   2018-05-08T15:12:56+01:00
 * @Email:  ben.briggs@thedistance.co.uk
 * @Last modified by:   benbriggs
 * @Last modified time: 2018-05-08T15:49:08+01:00
 * @Copyright: The Distance
 */

var mapDispatch = exports.mapDispatch = (0, _compose2.default)((0, _defaultTo2.default)(_placeholder2.default), (0, _prop2.default)('mapDispatchToProps'));

var mapState = exports.mapState = (0, _compose2.default)((0, _defaultTo2.default)(_placeholder2.default), (0, _prop2.default)('mapStateToProps'));