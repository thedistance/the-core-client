'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _mergeDeepRight = require('ramda/src/mergeDeepRight');

var _mergeDeepRight2 = _interopRequireDefault(_mergeDeepRight);

var _reactRedux = require('react-redux');

var _listView = require('./../../Entity/listView');

var _listView2 = _interopRequireDefault(_listView);

var _mapConnectors = require('./mapConnectors');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/**
 * @Author: benbriggs
 * @Date:   2018-05-01T11:30:51+01:00
 * @Email:  ben.briggs@thedistance.co.uk
 * @Last modified by:   benbriggs
 * @Last modified time: 2018-05-08T15:50:42+01:00
 * @Copyright: The Distance
 */

var listView = function listView() {
  var View = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : _listView2.default;
  return function (_ref) {
    var actions = _ref.actions,
        listConnector = _ref.listConnector,
        loadCollection = _ref.loadCollection,
        unloadCollection = _ref.unloadCollection,
        selectCollection = _ref.selectCollection,
        selectPages = _ref.selectPages,
        singular = _ref.singular,
        plural = _ref.plural,
        collectionIndex = _ref.collectionIndex,
        canCreateNew = _ref.canCreateNew,
        readOnly = _ref.readOnly,
        getTheadFilterThProps = _ref.getTheadFilterThProps;

    var mapStateToProps = function mapStateToProps(state) {
      return (0, _mergeDeepRight2.default)({
        readOnly: readOnly,
        singular: singular,
        plural: plural,
        canCreateNew: canCreateNew,
        columns: collectionIndex,
        data: selectCollection(state) || [],
        pages: selectPages(state),
        getTheadFilterThProps: getTheadFilterThProps
      }, (0, _mapConnectors.mapState)(listConnector)(state));
    };

    var mapDispatchToProps = function mapDispatchToProps(dispatch, ownProps) {
      return (0, _mergeDeepRight2.default)({
        actions: {
          load: function load(tableState) {
            return dispatch(loadCollection(tableState));
          },
          unload: function unload() {
            return dispatch(unloadCollection());
          }
        }
      }, (0, _mapConnectors.mapDispatch)(listConnector)(actions)(dispatch, ownProps));
    };

    return (0, _reactRedux.connect)(mapStateToProps, mapDispatchToProps)(View);
  };
};

exports.default = listView;