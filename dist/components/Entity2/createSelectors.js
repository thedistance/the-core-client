'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.defaultSelectors = undefined;

var _concat = require('ramda/src/concat');

var _concat2 = _interopRequireDefault(_concat);

var _path = require('ramda/src/path');

var _path2 = _interopRequireDefault(_path);

var _compose = require('ramda/src/compose');

var _compose2 = _interopRequireDefault(_compose);

var _map = require('ramda/src/map');

var _map2 = _interopRequireDefault(_map);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var defaultSelectors = exports.defaultSelectors = {
  selectCollection: ['collection'],
  selectCurrent: ['current'],
  selectError: ['error'],
  selectErrorMessage: ['error', 'message'],
  selectPages: ['pages']
}; /**
    * @Author: benbriggs
    * @Date:   2018-04-30T10:14:13+01:00
    * @Email:  ben.briggs@thedistance.co.uk
    * @Last modified by:   benbriggs
    * @Last modified time: 2018-05-14T13:29:47+01:00
    * @Copyright: The Distance
    */

var createSelectors = function createSelectors(rootPath) {
  var selectors = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : defaultSelectors;
  return (0, _map2.default)((0, _compose2.default)(_path2.default, (0, _concat2.default)(rootPath)), selectors);
};

exports.default = createSelectors;