'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _mergeDeepLeft = require('ramda/src/mergeDeepLeft');

var _mergeDeepLeft2 = _interopRequireDefault(_mergeDeepLeft);

var _merge = require('ramda/src/merge');

var _merge2 = _interopRequireDefault(_merge);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

var hasError = function hasError(state, _ref) {
  var error = _ref.error;
  return (0, _merge2.default)(state, { error: error });
}; /**
    * @Author: benbriggs
    * @Date:   2018-04-30T14:03:23+01:00
    * @Email:  ben.briggs@thedistance.co.uk
    * @Last modified by:   benbriggs
    * @Last modified time: 2018-05-01T09:50:10+01:00
    * @Copyright: The Distance
    */

var nullCurrent = (0, _mergeDeepLeft2.default)({ current: null, error: null });
var updateCurrent = function updateCurrent(state, _ref2) {
  var result = _ref2.result;
  return (0, _merge2.default)(state, { current: result });
};

var createReducerHandlers = function createReducerHandlers(_ref3) {
  var _ref5;

  var deletedAction = _ref3.deletedAction,
      deleteFailedAction = _ref3.deleteFailedAction,
      loadedCurrentAction = _ref3.loadedCurrentAction,
      loadedCollectionAction = _ref3.loadedCollectionAction,
      newAction = _ref3.newAction,
      revertAction = _ref3.revertAction,
      saveFailedAction = _ref3.saveFailedAction,
      unloadCollectionAction = _ref3.unloadCollectionAction,
      unloadCurrentAction = _ref3.unloadCurrentAction,
      updateAction = _ref3.updateAction;
  return _ref5 = {}, _defineProperty(_ref5, deletedAction, nullCurrent), _defineProperty(_ref5, deleteFailedAction, hasError), _defineProperty(_ref5, loadedCurrentAction, updateCurrent), _defineProperty(_ref5, loadedCollectionAction, function (state, _ref4) {
    var result = _ref4.result,
        pages = _ref4.pages;
    return (0, _merge2.default)(state, { collection: result, pages: pages });
  }), _defineProperty(_ref5, newAction, updateCurrent), _defineProperty(_ref5, revertAction, nullCurrent), _defineProperty(_ref5, saveFailedAction, hasError), _defineProperty(_ref5, unloadCollectionAction, (0, _mergeDeepLeft2.default)({ collection: null, pages: null })), _defineProperty(_ref5, unloadCurrentAction, nullCurrent), _defineProperty(_ref5, updateAction, updateCurrent), _ref5;
};

exports.default = createReducerHandlers;