'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _last = require('ramda/src/last');

var _last2 = _interopRequireDefault(_last);

var _prop = require('ramda/src/prop');

var _prop2 = _interopRequireDefault(_prop);

var _head = require('ramda/src/head');

var _head2 = _interopRequireDefault(_head);

var _objOf = require('ramda/src/objOf');

var _objOf2 = _interopRequireDefault(_objOf);

var _map = require('ramda/src/map');

var _map2 = _interopRequireDefault(_map);

var _juxt = require('ramda/src/juxt');

var _juxt2 = _interopRequireDefault(_juxt);

var _mergeAll = require('ramda/src/mergeAll');

var _mergeAll2 = _interopRequireDefault(_mergeAll);

var _compose = require('ramda/src/compose');

var _compose2 = _interopRequireDefault(_compose);

var _asyncActions = require('./../../util/asyncActions');

var asyncActions = _interopRequireWildcard(_asyncActions);

var _createAction = require('./../../util/createAction');

var _createAction2 = _interopRequireDefault(_createAction);

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj.default = obj; return newObj; } }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var createActionCreators = function createActionCreators(_ref) {
  var startActions = _ref.startActions,
      endActions = _ref.endActions,
      syncActions = _ref.syncActions;
  return (0, _compose2.default)(_mergeAll2.default, (0, _juxt2.default)([(0, _compose2.default)((0, _map2.default)((0, _compose2.default)(asyncActions.createStart, (0, _objOf2.default)('type'))), _head2.default), (0, _compose2.default)((0, _map2.default)((0, _compose2.default)(asyncActions.createEnd, (0, _objOf2.default)('type'))), (0, _prop2.default)(1)), (0, _compose2.default)((0, _map2.default)(_createAction2.default), _last2.default)]))([startActions, endActions, syncActions]);
}; /**
    * @Author: benbriggs
    * @Date:   2018-04-30T13:14:55+01:00
    * @Email:  ben.briggs@thedistance.co.uk
    * @Last modified by:   benbriggs
    * @Last modified time: 2018-05-01T15:06:10+01:00
    * @Copyright: The Distance
    */

exports.default = createActionCreators;