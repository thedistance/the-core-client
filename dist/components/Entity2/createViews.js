'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; /**
                                                                                                                                                                                                                                                                   * @Author: benbriggs
                                                                                                                                                                                                                                                                   * @Date:   2018-05-01T11:24:48+01:00
                                                                                                                                                                                                                                                                   * @Email:  ben.briggs@thedistance.co.uk
                                                                                                                                                                                                                                                                   * @Last modified by:   benbriggs
                                                                                                                                                                                                                                                                   * @Last modified time: 2018-09-03T08:22:13+01:00
                                                                                                                                                                                                                                                                   * @Copyright: The Distance
                                                                                                                                                                                                                                                                   */

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _reactRouter = require('react-router');

var _list = require('./views/list');

var _list2 = _interopRequireDefault(_list);

var _create = require('./views/create');

var _create2 = _interopRequireDefault(_create);

var _edit = require('./views/edit');

var _edit2 = _interopRequireDefault(_edit);

var _read = require('./views/read');

var _read2 = _interopRequireDefault(_read);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function createViews(entity, _ref) {
  var _ref$canCreateNew = _ref.canCreateNew,
      canCreateNew = _ref$canCreateNew === undefined ? true : _ref$canCreateNew,
      createConnector = _ref.createConnector,
      editConnector = _ref.editConnector,
      listConnector = _ref.listConnector,
      createView = _ref.createView,
      editView = _ref.editView,
      listView = _ref.listView,
      extraActions = _ref.extraActions,
      extraCreateActions = _ref.extraCreateActions,
      extraEditActions = _ref.extraEditActions;
  var readOnly = entity.readOnly,
      camelised = entity.camelised,
      collectionIndex = entity.collectionIndex,
      plural = entity.plural,
      createSchemaFields = entity.createSchemaFields,
      editSchemaFields = entity.editSchemaFields,
      singular = entity.singular,
      slug = entity.slug,
      _entity$selectors = entity.selectors,
      selectCollection = _entity$selectors.selectCollection,
      selectCurrent = _entity$selectors.selectCurrent,
      selectErrorMessage = _entity$selectors.selectErrorMessage,
      selectPages = _entity$selectors.selectPages,
      actions = entity.actions,
      _entity$actions = entity.actions,
      create = _entity$actions.create,
      destroy = _entity$actions.destroy,
      loadCollection = _entity$actions.loadCollection,
      loadCurrent = _entity$actions.loadCurrent,
      save = _entity$actions.save,
      revert = _entity$actions.revert,
      unloadCollection = _entity$actions.unloadCollection,
      unloadCurrent = _entity$actions.unloadCurrent,
      update = _entity$actions.update;


  var routeId = camelised + 'Id';

  var exposedCreateView = (0, _create2.default)(createView)({
    actions: actions,
    createConnector: createConnector,
    create: create,
    save: save,
    unloadCurrent: unloadCurrent,
    update: update,
    revert: revert,
    extraActions: extraActions,
    extraCreateActions: extraCreateActions,
    selectCurrent: selectCurrent,
    selectErrorMessage: selectErrorMessage,
    schemaFields: createSchemaFields,
    singular: singular
  });

  var exposedEditView = readOnly ? (0, _read2.default)(editView)({
    actions: actions,
    editConnector: editConnector,
    loadCurrent: loadCurrent,
    unloadCurrent: unloadCurrent,
    revert: revert,
    extraActions: extraActions,
    extraEditActions: extraEditActions,
    routeId: routeId,
    selectCurrent: selectCurrent,
    selectErrorMessage: selectErrorMessage,
    schemaFields: editSchemaFields,
    singular: singular
  }) : (0, _edit2.default)(editView)({
    actions: actions,
    editConnector: editConnector,
    save: save,
    destroy: destroy,
    loadCurrent: loadCurrent,
    unloadCurrent: unloadCurrent,
    update: update,
    revert: revert,
    extraActions: extraActions,
    extraEditActions: extraEditActions,
    routeId: routeId,
    selectCurrent: selectCurrent,
    selectErrorMessage: selectErrorMessage,
    schemaFields: editSchemaFields,
    singular: singular
  });

  var exposedListView = (0, _list2.default)(listView)({
    actions: actions,
    listConnector: listConnector,
    loadCollection: loadCollection,
    unloadCollection: unloadCollection,
    selectCollection: selectCollection,
    selectPages: selectPages,
    singular: singular,
    plural: plural,
    collectionIndex: collectionIndex,
    canCreateNew: canCreateNew,
    readOnly: readOnly
  });

  return _extends({}, entity, {
    routeId: routeId,
    createView: exposedCreateView,
    editView: exposedEditView,
    listView: exposedListView,
    routes: [_react2.default.createElement(_reactRouter.Route, {
      path: '/' + slug,
      component: exposedListView,
      key: camelised + '__list'
    }), _react2.default.createElement(_reactRouter.Route, {
      path: '/' + slug + '/new',
      component: exposedCreateView,
      key: camelised + '__create'
    }), _react2.default.createElement(_reactRouter.Route, {
      path: '/' + slug + '/:' + routeId,
      component: exposedEditView,
      key: camelised + '__edit'
    })]
  });
}

exports.default = createViews;