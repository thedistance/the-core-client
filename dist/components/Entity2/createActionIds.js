'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _prop = require('ramda/src/prop');

var _prop2 = _interopRequireDefault(_prop);

var _objOf = require('ramda/src/objOf');

var _objOf2 = _interopRequireDefault(_objOf);

var _juxt = require('ramda/src/juxt');

var _juxt2 = _interopRequireDefault(_juxt);

var _mergeAll = require('ramda/src/mergeAll');

var _mergeAll2 = _interopRequireDefault(_mergeAll);

var _map = require('ramda/src/map');

var _map2 = _interopRequireDefault(_map);

var _compose = require('ramda/src/compose');

var _compose2 = _interopRequireDefault(_compose);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var createActionIds = function createActionIds(_ref) {
  var className = _ref.className,
      startActions = _ref.startActions,
      endActions = _ref.endActions,
      syncActions = _ref.syncActions;
  return (0, _compose2.default)((0, _map2.default)((0, _map2.default)(function (action) {
    return className + ':' + action;
  })), _mergeAll2.default, (0, _juxt2.default)([(0, _compose2.default)((0, _objOf2.default)('startActions'), (0, _prop2.default)(0)), (0, _compose2.default)((0, _objOf2.default)('endActions'), (0, _prop2.default)(1)), (0, _compose2.default)((0, _objOf2.default)('syncActions'), (0, _prop2.default)(2))]))([startActions, endActions, syncActions]);
}; /**
    * @Author: benbriggs
    * @Date:   2018-05-01T14:58:19+01:00
    * @Email:  ben.briggs@thedistance.co.uk
    * @Last modified by:   benbriggs
    * @Last modified time: 2018-05-01T15:17:35+01:00
    * @Copyright: The Distance
    */

exports.default = createActionIds;