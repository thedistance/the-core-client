'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _merge = require('ramda/src/merge');

var _merge2 = _interopRequireDefault(_merge);

var _evolve = require('ramda/src/evolve');

var _evolve2 = _interopRequireDefault(_evolve);

var _identity = require('ramda/src/identity');

var _identity2 = _interopRequireDefault(_identity);

var _always = require('ramda/src/always');

var _always2 = _interopRequireDefault(_always);

var _contains = require('ramda/src/contains');

var _contains2 = _interopRequireDefault(_contains);

var _ifElse = require('ramda/src/ifElse');

var _ifElse2 = _interopRequireDefault(_ifElse);

var _mapObjIndexed = require('ramda/src/mapObjIndexed');

var _mapObjIndexed2 = _interopRequireDefault(_mapObjIndexed);

var _values = require('ramda/src/values');

var _values2 = _interopRequireDefault(_values);

var _mergeAll = require('ramda/src/mergeAll');

var _mergeAll2 = _interopRequireDefault(_mergeAll);

var _compose = require('ramda/src/compose');

var _compose2 = _interopRequireDefault(_compose);

var _mergeDeepRight = require('ramda/src/mergeDeepRight');

var _mergeDeepRight2 = _interopRequireDefault(_mergeDeepRight);

var _call = require('ramda/src/call');

var _call2 = _interopRequireDefault(_call);

var _binary = require('ramda/src/binary');

var _binary2 = _interopRequireDefault(_binary);

var _flip = require('ramda/src/flip');

var _flip2 = _interopRequireDefault(_flip);

var _createReducer = require('./../../util/createReducer');

var _createReducer2 = _interopRequireDefault(_createReducer);

var _inflections = require('./../Entity/inflections');

var _inflections2 = _interopRequireDefault(_inflections);

var _createObjectFromSchemaDefaults = require('./../Entity/createObjectFromSchemaDefaults');

var _createObjectFromSchemaDefaults2 = _interopRequireDefault(_createObjectFromSchemaDefaults);

var _convertSchemaToFields = require('./../Entity/convertSchemaToFields');

var _convertSchemaToFields2 = _interopRequireDefault(_convertSchemaToFields);

var _convertSchemaToIndex = require('./../Entity/convertSchemaToIndex');

var _convertSchemaToIndex2 = _interopRequireDefault(_convertSchemaToIndex);

var _convertSchemaToReadOnlyFields = require('./../Entity/convertSchemaToReadOnlyFields');

var _convertSchemaToReadOnlyFields2 = _interopRequireDefault(_convertSchemaToReadOnlyFields);

var _filterSchemaByPropExists = require('./../Entity/filterSchemaByPropExists');

var _filterSchemaByPropExists2 = _interopRequireDefault(_filterSchemaByPropExists);

var _filterSchemaByPropNotExists = require('./../Entity/filterSchemaByPropNotExists');

var _filterSchemaByPropNotExists2 = _interopRequireDefault(_filterSchemaByPropNotExists);

var _create = require('./actions/create');

var _create2 = _interopRequireDefault(_create);

var _destroy = require('./actions/destroy');

var _destroy2 = _interopRequireDefault(_destroy);

var _loadCollection = require('./actions/loadCollection');

var _loadCollection2 = _interopRequireDefault(_loadCollection);

var _loadCurrent = require('./actions/loadCurrent');

var _loadCurrent2 = _interopRequireDefault(_loadCurrent);

var _revert = require('./actions/revert');

var _revert2 = _interopRequireDefault(_revert);

var _save = require('./actions/save');

var _save2 = _interopRequireDefault(_save);

var _unloadCollection = require('./actions/unloadCollection');

var _unloadCollection2 = _interopRequireDefault(_unloadCollection);

var _unloadCurrent = require('./actions/unloadCurrent');

var _unloadCurrent2 = _interopRequireDefault(_unloadCurrent);

var _update = require('./actions/update');

var _update2 = _interopRequireDefault(_update);

var _createActionCreators = require('./createActionCreators');

var _createActionCreators2 = _interopRequireDefault(_createActionCreators);

var _createActionIds = require('./createActionIds');

var _createActionIds2 = _interopRequireDefault(_createActionIds);

var _createReducerHandlers = require('./createReducerHandlers');

var _createReducerHandlers2 = _interopRequireDefault(_createReducerHandlers);

var _createSelectors = require('./createSelectors');

var _createSelectors2 = _interopRequireDefault(_createSelectors);

var _createViews = require('./createViews');

var _createViews2 = _interopRequireDefault(_createViews);

var _placeholder = require('./placeholder');

var _placeholder2 = _interopRequireDefault(_placeholder);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/**
 * @Author: benbriggs
 * @Date:   2018-04-30T08:32:16+01:00
 * @Email:  ben.briggs@thedistance.co.uk
 * @Last modified by:   benbriggs
 * @Last modified time: 2018-05-14T13:34:26+01:00
 * @Copyright: The Distance
 */

var callWith = (0, _flip2.default)((0, _binary2.default)(_call2.default));

var _startActions = {
  deletingAction: 'deleting',
  loadingCollectionAction: 'loading:collection',
  loadingCurrentAction: 'loading:current',
  savingAction: 'saving'
};

var _endActions = {
  deletedAction: 'deleted',
  deleteFailedAction: 'delete:failed',
  loadedCollectionAction: 'loaded:collection',
  loadedCurrentAction: 'loaded:current',
  savedAction: 'saved',
  saveFailedAction: 'save:failed'
};

var _syncActions = {
  loadedCurrentSyncAction: 'loaded:current',
  newAction: 'new',
  revertAction: 'revert',
  unloadCollectionAction: 'unload:collection',
  unloadCurrentAction: 'unload:current',
  updateAction: 'update'
};

var createEntity = function createEntity(_ref) {
  var _ref$actions = _ref.actions,
      actions = _ref$actions === undefined ? {} : _ref$actions,
      _ref$actionCreators = _ref.actionCreators,
      actionCreators = _ref$actionCreators === undefined ? {} : _ref$actionCreators,
      name = _ref.name,
      _ref$className = _ref.className,
      className = _ref$className === undefined ? name : _ref$className,
      _ref$inflections = _ref.inflections,
      inflections = _ref$inflections === undefined ? {} : _ref$inflections,
      readOnly = _ref.readOnly,
      _ref$reducerHandlers = _ref.reducerHandlers,
      reducerHandlers = _ref$reducerHandlers === undefined ? {} : _ref$reducerHandlers,
      _ref$reducerState = _ref.reducerState,
      reducerState = _ref$reducerState === undefined ? {
    collection: null,
    current: null,
    pages: null,
    error: null
  } : _ref$reducerState,
      _ref$schema = _ref.schema,
      schema = _ref$schema === undefined ? {} : _ref$schema,
      selectors = _ref.selectors,
      onLoadCollection = _ref.onLoadCollection,
      onLoadCurrent = _ref.onLoadCurrent,
      _ref$canCreateNew = _ref.canCreateNew,
      canCreateNew = _ref$canCreateNew === undefined ? true : _ref$canCreateNew,
      _ref$createConnector = _ref.createConnector,
      createConnector = _ref$createConnector === undefined ? _placeholder2.default : _ref$createConnector,
      _ref$editConnector = _ref.editConnector,
      editConnector = _ref$editConnector === undefined ? _placeholder2.default : _ref$editConnector,
      _ref$listConnector = _ref.listConnector,
      listConnector = _ref$listConnector === undefined ? _placeholder2.default : _ref$listConnector,
      createView = _ref.createView,
      editView = _ref.editView,
      listView = _ref.listView,
      _ref$extraActions = _ref.extraActions,
      extraActions = _ref$extraActions === undefined ? _placeholder2.default : _ref$extraActions,
      _ref$extraCreateActio = _ref.extraCreateActions,
      extraCreateActions = _ref$extraCreateActio === undefined ? _placeholder2.default : _ref$extraCreateActio,
      _ref$extraEditActions = _ref.extraEditActions,
      extraEditActions = _ref$extraEditActions === undefined ? _placeholder2.default : _ref$extraEditActions;

  // 1. Inflections
  var _createInflections = (0, _inflections2.default)(name, inflections),
      camelised = _createInflections.camelised,
      singular = _createInflections.singular,
      plural = _createInflections.plural,
      slug = _createInflections.slug;

  // 2. Schema


  var filteredSchema = (0, _filterSchemaByPropNotExists2.default)('hideInEdit', schema);
  var createSchemaFields = (0, _convertSchemaToFields2.default)(filteredSchema, 'create');
  var editSchemaFields = readOnly ? (0, _convertSchemaToReadOnlyFields2.default)(filteredSchema, 'edit') : (0, _convertSchemaToFields2.default)(filteredSchema, 'edit');

  var collectionIndex = (0, _convertSchemaToIndex2.default)((0, _filterSchemaByPropExists2.default)('showInIndex', schema));

  // 3. Selectors
  var mergedSelectors = (0, _createSelectors2.default)([camelised], selectors);
  var selectCollection = mergedSelectors.selectCollection,
      selectCurrent = mergedSelectors.selectCurrent;

  // 4. Actions

  var namespacedActionIds = (0, _createActionIds2.default)((0, _mergeDeepRight2.default)({
    className: className,
    startActions: _startActions,
    endActions: _endActions,
    syncActions: _syncActions
  }, actionCreators));

  var actionIds = (0, _compose2.default)(_mergeAll2.default, _values2.default)(namespacedActionIds);

  var mergedActionCreators = (0, _createActionCreators2.default)(namespacedActionIds);

  var mergedActions = (0, _compose2.default)((0, _mapObjIndexed2.default)(function (value, key) {
    return (0, _ifElse2.default)((0, _contains2.default)(key), (0, _always2.default)((0, _identity2.default)(value)), (0, _always2.default)(callWith({
      actionCreators: mergedActionCreators,
      selectors: mergedSelectors
    }, value)))(['create', 'destroy', 'loadCollection', 'loadCurrent', 'revert', 'save', 'unloadCollection', 'unloadCurrent', 'update']);
  }), (0, _evolve2.default)({
    create: callWith({
      action: mergedActionCreators.newAction,
      className: className,
      defaultValues: (0, _createObjectFromSchemaDefaults2.default)(schema)
    }),
    destroy: callWith({
      startedAction: mergedActionCreators.deletingAction,
      completedAction: mergedActionCreators.deletedAction,
      failedAction: mergedActionCreators.deleteFailedAction,
      selectCurrent: selectCurrent
    }),
    loadCollection: callWith({
      className: className,
      queryTransform: onLoadCollection,
      startedAction: mergedActionCreators.loadingCollectionAction,
      completedAction: mergedActionCreators.loadedCollectionAction
    }),
    loadCurrent: callWith({
      className: className,
      queryTransform: onLoadCurrent,
      startedAction: mergedActionCreators.loadingCurrentAction,
      startedSyncAction: mergedActionCreators.loadedCurrentSyncAction,
      completedAction: mergedActionCreators.loadedCurrentAction,
      selectCurrent: selectCurrent
    }),
    revert: callWith({
      action: mergedActionCreators.revertAction,
      selectCurrent: selectCurrent
    }),
    save: callWith({
      selectCurrent: selectCurrent,
      startedAction: mergedActionCreators.savingAction,
      completedAction: mergedActionCreators.savedAction,
      failedAction: mergedActionCreators.saveFailedAction
    }),
    unloadCollection: callWith({
      selectCollection: selectCollection,
      action: mergedActionCreators.unloadCollectionAction
    }),
    unloadCurrent: callWith({
      selectCurrent: selectCurrent,
      action: mergedActionCreators.unloadCurrentAction
    }),
    update: callWith({
      selectCurrent: selectCurrent,
      action: mergedActionCreators.updateAction
    })
  }), (0, _merge2.default)({
    create: _create2.default,
    destroy: _destroy2.default,
    loadCollection: _loadCollection2.default,
    loadCurrent: _loadCurrent2.default,
    revert: _revert2.default,
    save: _save2.default,
    unloadCollection: _unloadCollection2.default,
    unloadCurrent: _unloadCurrent2.default,
    update: _update2.default
  }))(actions);

  // 5. Reducer
  var reducer = (0, _createReducer2.default)(reducerState, (0, _merge2.default)((0, _createReducerHandlers2.default)(actionIds), reducerHandlers));

  // 6. Views

  return (0, _createViews2.default)({
    // Actions
    actions: mergedActions,
    actionCreators: mergedActionCreators,
    actionIds: actionIds,
    // Inflections
    camelised: camelised,
    plural: plural,
    singular: singular,
    slug: slug,
    // Read only?
    readOnly: readOnly,
    // Reducer
    reducer: reducer,
    // Schema
    collectionIndex: collectionIndex,
    createSchemaFields: createSchemaFields,
    editSchemaFields: editSchemaFields,
    // Selectors
    selectors: mergedSelectors
  }, {
    canCreateNew: canCreateNew,
    createConnector: createConnector,
    editConnector: editConnector,
    listConnector: listConnector,
    createView: createView,
    editView: editView,
    listView: listView,
    extraActions: callWith(mergedActions, extraActions),
    extraCreateActions: callWith(mergedActions, extraCreateActions),
    extraEditActions: callWith(mergedActions, extraEditActions)
  });
};

exports.default = createEntity;