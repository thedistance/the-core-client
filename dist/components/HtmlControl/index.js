'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _propTypes = require('prop-types');

var _propTypes2 = _interopRequireDefault(_propTypes);

var _rubix = require('@sketchpixy/rubix');

var _reactDraftWysiwyg = require('react-draft-wysiwyg');

require('react-draft-wysiwyg/dist/react-draft-wysiwyg.css');

var _draftJs = require('draft-js');

var _draftjsToHtml = require('draftjs-to-html');

var _draftjsToHtml2 = _interopRequireDefault(_draftjsToHtml);

var _htmlToDraftjs = require('html-to-draftjs');

var _htmlToDraftjs2 = _interopRequireDefault(_htmlToDraftjs);

var _FormGroup = require('../FormGroup');

var _FormGroup2 = _interopRequireDefault(_FormGroup);

var _GenericControl2 = require('../GenericControl');

var _GenericControl3 = _interopRequireDefault(_GenericControl2);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; } /**
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                * @Author: Ben Briggs <benbriggs>
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                * @Date:   2017-11-27T16:18:54+00:00
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                * @Email:  ben.briggs@thedistance.co.uk
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                * @Last modified by:   benbriggs
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                * @Last modified time: 2018-01-11T09:53:22+00:00
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                * @Copyright: The Distance
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                */

var HtmlControl = function (_GenericControl) {
  _inherits(HtmlControl, _GenericControl);

  function HtmlControl(props) {
    _classCallCheck(this, HtmlControl);

    var _this = _possibleConstructorReturn(this, (HtmlControl.__proto__ || Object.getPrototypeOf(HtmlControl)).call(this, props));

    var _htmlToDraft = (0, _htmlToDraftjs2.default)(props.value),
        contentBlocks = _htmlToDraft.contentBlocks,
        entityMap = _htmlToDraft.entityMap;

    var editor = _draftJs.EditorState.createWithContent(_draftJs.ContentState.createFromBlockArray(contentBlocks, entityMap));

    _this.state.editor = editor;
    return _this;
  }

  _createClass(HtmlControl, [{
    key: 'handleChange',
    value: function handleChange(editorState) {
      var value = (0, _draftjsToHtml2.default)((0, _draftJs.convertToRaw)(editorState.getCurrentContent()));
      var valid = this.props.validator ? this.props.validator(value) : null;

      this.setState({
        editor: editorState,
        value: value,
        valid: valid
      });

      this.props.onChange(value, valid);
    }
  }, {
    key: 'render',
    value: function render() {
      return _react2.default.createElement(
        _FormGroup2.default,
        {
          validationState: this.validationState(),
          required: this.props.required
        },
        _react2.default.createElement(
          _rubix.ControlLabel,
          null,
          this.props.label
        ),
        _react2.default.createElement(_reactDraftWysiwyg.Editor, {
          editorState: this.state.editor,
          onEditorStateChange: this.handleChange,
          toolbar: {
            options: ['inline', 'blockType', 'list', 'textAlign', 'colorPicker', 'link', 'emoji', 'image', 'remove', 'history']
          },
          editorClassName: 'draftJs-wrapper',
          toolbarClassName: 'draftJs-wrapper'
        })
      );
    }
  }]);

  return HtmlControl;
}(_GenericControl3.default);

HtmlControl.propTypes = {
  value: _propTypes2.default.string,
  onChange: _propTypes2.default.func.isRequired
};

HtmlControl.defaultProps = {
  value: ''
};

exports.default = HtmlControl;