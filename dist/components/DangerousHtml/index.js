'use strict';

Object.defineProperty(exports, '__esModule', {
  value: true,
});

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _propTypes = require('prop-types');

var _propTypes2 = _interopRequireDefault(_propTypes);

var _rubix = require('@sketchpixy/rubix');

var _GenericControl = require('../GenericControl');

var _GenericControl2 = _interopRequireDefault(_GenericControl);

function _interopRequireDefault(obj) {
  return obj && obj.__esModule ? obj : { default: obj };
}

/**
 * @Author: benbriggs
 * @Date:   2018-09-25T13:09:53+01:00
 * @Email:  ben.briggs@thedistance.co.uk
 * @Last modified by:   benbriggs
 * @Last modified time: 2018-09-25T13:14:01+01:00
 * @Copyright: The Distance
 */

var DangerousHtmlControl = function DangerousHtmlControl(props) {
  return _react2.default.createElement(
    _GenericControl2.default,
    props,
    _react2.default.createElement(
      _rubix.FormControl.Static,
      null,
      _react2.default.createElement('div', {
        dangerouslySetInnerHTML: { __html: props.value },
      })
    )
  );
};

DangerousHtmlControl.propTypes = {
  value: _propTypes2.default.string,
};

exports.default = DangerousHtmlControl;
