'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _reactTimeago = require('react-timeago');

var _reactTimeago2 = _interopRequireDefault(_reactTimeago);

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _propTypes = require('prop-types');

var _propTypes2 = _interopRequireDefault(_propTypes);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var Timeago = function Timeago(_ref) {
  var value = _ref.value;
  return _react2.default.createElement(_reactTimeago2.default, { date: value });
}; /**
    * @Author: Ben Briggs <benbriggs>
    * @Date:   2018-01-11T14:54:37+00:00
    * @Email:  ben.briggs@thedistance.co.uk
    * @Last modified by:   benbriggs
    * @Last modified time: 2018-02-06T12:22:59+00:00
    * @Copyright: The Distance
    */

Timeago.propTypes = {
  value: _propTypes2.default.instanceOf(Date)
};

exports.default = Timeago;