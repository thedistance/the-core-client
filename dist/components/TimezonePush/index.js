'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.mapDispatchToProps = exports.mapStateToProps = exports.TimezonePush = undefined;

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _reactRedux = require('react-redux');

var _rubix = require('@sketchpixy/rubix');

var _propTypes = require('prop-types');

var _propTypes2 = _interopRequireDefault(_propTypes);

var _Alert = require('../Alert');

var _Alert2 = _interopRequireDefault(_Alert);

var _Panel = require('../Panel');

var _Panel2 = _interopRequireDefault(_Panel);

var _TextControl = require('../TextControl');

var _TextControl2 = _interopRequireDefault(_TextControl);

var _TimeControl = require('../TimeControl');

var _TimeControl2 = _interopRequireDefault(_TimeControl);

var _actions = require('./actions');

var actions = _interopRequireWildcard(_actions);

var _reducer = require('./reducer');

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj.default = obj; return newObj; } }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; } /**
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                * @Author: Ben Briggs <benbriggs>
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                * @Date:   2018-01-09T15:52:40+00:00
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                * @Email:  ben.briggs@thedistance.co.uk
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                * @Last modified by:   benbriggs
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                * @Last modified time: 2018-01-11T12:02:42+00:00
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                * @Copyright: The Distance
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                */

var TimezonePush = exports.TimezonePush = function (_React$Component) {
  _inherits(TimezonePush, _React$Component);

  function TimezonePush(props) {
    _classCallCheck(this, TimezonePush);

    var _this = _possibleConstructorReturn(this, (TimezonePush.__proto__ || Object.getPrototypeOf(TimezonePush)).call(this, props));

    _this.state = {
      from: '',
      until: '',
      message: ''
    };
    return _this;
  }

  _createClass(TimezonePush, [{
    key: 'handleFrom',
    value: function handleFrom(from) {
      this.setState({ from: from });
    }
  }, {
    key: 'handleUntil',
    value: function handleUntil(until) {
      this.setState({ until: until });
    }
  }, {
    key: 'handleMessage',
    value: function handleMessage(message) {
      this.setState({ message: message });
    }
  }, {
    key: 'renderMessage',
    value: function renderMessage() {
      var _props = this.props,
          error = _props.error,
          sent = _props.sent;

      if (error) {
        return _react2.default.createElement(
          _Alert2.default,
          { danger: true },
          error.message
        );
      }
      if (sent) {
        return _react2.default.createElement(
          _Alert2.default,
          { success: true },
          'Your message has been successfully scheduled!'
        );
      }
      return null;
    }
  }, {
    key: 'sendMessage',
    value: function sendMessage() {
      this.props.actions.send(this.state);
      this.setState({ message: '', from: '', until: '' });
    }
  }, {
    key: 'render',
    value: function render() {
      return _react2.default.createElement(
        'div',
        null,
        this.renderMessage(),
        _react2.default.createElement(
          _Panel2.default,
          { title: 'Send a Push Message' },
          _react2.default.createElement(
            _rubix.Form,
            { autoComplete: false, onSubmit: function onSubmit(e) {
                return e.preventDefault();
              } },
            _react2.default.createElement(
              _rubix.Grid,
              null,
              _react2.default.createElement(
                _rubix.Row,
                null,
                _react2.default.createElement(
                  _rubix.Col,
                  { xs: 12 },
                  _react2.default.createElement(_TextControl2.default, {
                    label: 'Message',
                    onChange: this.handleMessage.bind(this),
                    value: this.state.message
                  })
                )
              ),
              _react2.default.createElement(
                _rubix.Row,
                null,
                _react2.default.createElement(
                  _rubix.Col,
                  { sm: 5 },
                  _react2.default.createElement(_TimeControl2.default, {
                    label: 'From',
                    value: this.state.from,
                    onChange: this.handleFrom.bind(this)
                  }),
                  _react2.default.createElement(_rubix.FormControlFeedback, null)
                ),
                _react2.default.createElement(
                  _rubix.Col,
                  { sm: 5, smOffset: 1 },
                  _react2.default.createElement(_TimeControl2.default, {
                    label: 'Until',
                    value: this.state.until,
                    onChange: this.handleUntil.bind(this)
                  }),
                  _react2.default.createElement(_rubix.FormControlFeedback, null)
                )
              ),
              _react2.default.createElement(
                _rubix.Row,
                null,
                _react2.default.createElement(
                  _rubix.Col,
                  { xs: 12 },
                  this.state.from && this.state.until ? _react2.default.createElement(
                    'p',
                    null,
                    'If it is not ',
                    this.state.from,
                    ' in their local time yet, the message will be sent at ',
                    this.state.from,
                    '. If it is past ',
                    this.state.from,
                    ' but before ',
                    this.state.until,
                    ' in their local time, it will be sent straight away. Otherwise, we will wait until tomorrow and send at',
                    ' ',
                    this.state.from,
                    '.'
                  ) : _react2.default.createElement(
                    'p',
                    null,
                    'The message will be sent to each user at a different time, depending on their local time.'
                  )
                )
              ),
              _react2.default.createElement(
                _rubix.Row,
                null,
                _react2.default.createElement(
                  _rubix.Col,
                  { xs: 12 },
                  _react2.default.createElement(
                    _rubix.ButtonToolbar,
                    { style: { marginBottom: '1em' } },
                    _react2.default.createElement(
                      _rubix.Button,
                      { onClick: this.sendMessage.bind(this) },
                      'Send'
                    )
                  )
                )
              )
            )
          )
        )
      );
    }
  }]);

  return TimezonePush;
}(_react2.default.Component);

TimezonePush.propTypes = {
  actions: _propTypes2.default.objectOf(_propTypes2.default.func).isRequired,
  error: _propTypes2.default.any,
  sent: _propTypes2.default.any
};

var mapStateToProps = exports.mapStateToProps = function mapStateToProps(state) {
  return {
    error: (0, _reducer.selectError)(state),
    sent: (0, _reducer.selectSent)(state)
  };
};

var mapDispatchToProps = exports.mapDispatchToProps = function mapDispatchToProps(dispatch) {
  return {
    actions: {
      send: function send(params) {
        return dispatch(actions.sendPushMessage(params));
      }
    }
  };
};

exports.default = (0, _reactRedux.connect)(mapStateToProps, mapDispatchToProps)(TimezonePush);