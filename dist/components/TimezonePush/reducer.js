'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.selectPages = exports.selectCollection = exports.selectError = exports.selectSent = undefined;

var _createReducer;

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; /**
                                                                                                                                                                                                                                                                   * @Author: Ben Briggs <benbriggs>
                                                                                                                                                                                                                                                                   * @Date:   2018-01-10T13:17:57+00:00
                                                                                                                                                                                                                                                                   * @Email:  ben.briggs@thedistance.co.uk
                                                                                                                                                                                                                                                                   * @Last modified by:   benbriggs
                                                                                                                                                                                                                                                                   * @Last modified time: 2018-01-11T10:00:17+00:00
                                                                                                                                                                                                                                                                   * @Copyright: The Distance
                                                                                                                                                                                                                                                                   */

var _createReducer2 = require('../../util/createReducer');

var _createReducer3 = _interopRequireDefault(_createReducer2);

var _actions = require('./actions');

var actions = _interopRequireWildcard(_actions);

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj.default = obj; return newObj; } }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

exports.default = (0, _createReducer3.default)({ collection: [], sent: null, error: null, pages: null }, (_createReducer = {}, _defineProperty(_createReducer, actions.loadedPushMessage, function (state, _ref) {
  var data = _ref.data,
      pages = _ref.pages;
  return _extends({}, state, {
    collection: data,
    pages: pages
  });
}), _defineProperty(_createReducer, actions.sentPushMessage, function (state) {
  return _extends({}, state, {
    sent: true,
    error: null
  });
}), _defineProperty(_createReducer, actions.sendPushMessageFailed, function (state, _ref2) {
  var error = _ref2.error;
  return _extends({}, state, {
    sent: false,
    error: error
  });
}), _defineProperty(_createReducer, actions.unloadAll, function () {
  return {
    collection: [],
    sent: null,
    error: null,
    pages: null
  };
}), _createReducer));
var selectSent = exports.selectSent = function selectSent(state) {
  return state.pushMessage.sent;
};
var selectError = exports.selectError = function selectError(state) {
  return state.pushMessage.error;
};
var selectCollection = exports.selectCollection = function selectCollection(state) {
  return state.pushMessage.collection;
};
var selectPages = exports.selectPages = function selectPages(state) {
  return state.pushMessage.pages;
};