'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _identity = require('ramda/src/identity');

var _identity2 = _interopRequireDefault(_identity);

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _propTypes = require('prop-types');

var _propTypes2 = _interopRequireDefault(_propTypes);

var _reactSelect = require('react-select');

var _rubix = require('@sketchpixy/rubix');

var _FormGroup = require('../FormGroup');

var _FormGroup2 = _interopRequireDefault(_FormGroup);

var _GenericControl2 = require('../GenericControl');

var _GenericControl3 = _interopRequireDefault(_GenericControl2);

require('react-select/dist/react-select.css');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; } /**
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                * @Author: Ben Briggs <benbriggs>
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                * @Date:   2018-01-29T15:50:50+00:00
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                * @Email:  ben.briggs@thedistance.co.uk
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                * @Last modified by:   benbriggs
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                * @Last modified time: 2018-02-12T15:35:06+00:00
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                * @Copyright: The Distance
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                */

var PointerControl = function (_GenericControl) {
  _inherits(PointerControl, _GenericControl);

  function PointerControl(props) {
    _classCallCheck(this, PointerControl);

    var _this = _possibleConstructorReturn(this, (PointerControl.__proto__ || Object.getPrototypeOf(PointerControl)).call(this, props));

    _this.state = {
      displayValue: props.value,
      value: props.value,
      valid: null
    };
    return _this;
  }

  _createClass(PointerControl, [{
    key: 'handleChange',
    value: function handleChange(opts) {
      var _props = this.props,
          ParseObject = _props.ParseObject,
          validator = _props.validator;

      var valid = validator ? validator(opts === null ? opts : opts.value) : null;
      var value = opts === null ? opts : ParseObject.createWithoutData(opts.value).toPointer();

      this.setState({
        displayValue: opts,
        value: value,
        valid: valid
      });

      this.props.onChange(value, valid);
    }
  }, {
    key: 'render',
    value: function render() {
      var displayValue = this.state.displayValue;
      return _react2.default.createElement(
        'div',
        null,
        _react2.default.createElement(
          _FormGroup2.default,
          {
            validationState: this.validationState(),
            required: this.props.required
          },
          _react2.default.createElement(
            _rubix.ControlLabel,
            null,
            this.props.label
          ),
          _react2.default.createElement(_reactSelect.Async, {
            value: displayValue,
            loadOptions: this.props.loadOptions,
            options: this.props.options,
            onChange: this.handleChange,
            filterOptions: _identity2.default
          })
        )
      );
    }
  }]);

  return PointerControl;
}(_GenericControl3.default);

PointerControl.propTypes = {
  ParseObject: _propTypes2.default.func.isRequired,
  loadOptions: _propTypes2.default.func.isRequired
};

exports.default = PointerControl;