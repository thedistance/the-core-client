'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _propTypes = require('prop-types');

var _propTypes2 = _interopRequireDefault(_propTypes);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/**
 * @Author: Ben Briggs <benbriggs>
 * @Date:   2018-02-23T12:00:18+00:00
 * @Email:  ben.briggs@thedistance.co.uk
 * @Last modified by:   benbriggs
 * @Last modified time: 2018-02-23T12:01:07+00:00
 * @Copyright: The Distance
 */

var YesNo = function YesNo(_ref) {
  var value = _ref.value;
  return _react2.default.createElement(
    'span',
    null,
    value ? 'Yes' : 'No'
  );
};

YesNo.propTypes = {
  value: _propTypes2.default.bool.isRequired
};

exports.default = YesNo;