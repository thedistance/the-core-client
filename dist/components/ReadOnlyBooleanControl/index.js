'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _propTypes = require('prop-types');

var _propTypes2 = _interopRequireDefault(_propTypes);

var _rubix = require('@sketchpixy/rubix');

var _GenericControl = require('../GenericControl');

var _GenericControl2 = _interopRequireDefault(_GenericControl);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _objectWithoutProperties(obj, keys) { var target = {}; for (var i in obj) { if (keys.indexOf(i) >= 0) continue; if (!Object.prototype.hasOwnProperty.call(obj, i)) continue; target[i] = obj[i]; } return target; } /*
                                                                                                                                                                                                                              * Created Date: Mon, 16th Apr 2018, 12:19:48 pm
                                                                                                                                                                                                                              * Author: Harry Crank
                                                                                                                                                                                                                              * Email: harry.crank@thedistance.co.uk
                                                                                                                                                                                                                              * Copyright (c) 2018 The Distance
                                                                                                                                                                                                                              */

var ReadOnlyBooleanControl = function ReadOnlyBooleanControl(_ref) {
  var choices = _ref.choices,
      props = _objectWithoutProperties(_ref, ['choices']);

  return _react2.default.createElement(
    _GenericControl2.default,
    props,
    _react2.default.createElement(
      _rubix.FormControl.Static,
      null,
      choices[Number(!props.value)]
    )
  );
};

ReadOnlyBooleanControl.propTypes = {
  value: _propTypes2.default.bool,
  choices: _propTypes2.default.any
};

exports.default = ReadOnlyBooleanControl;