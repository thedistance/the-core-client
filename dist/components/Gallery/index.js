'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _propTypes = require('prop-types');

var _propTypes2 = _interopRequireDefault(_propTypes);

var _rubix = require('@sketchpixy/rubix');

var _reactImageGallery = require('react-image-gallery');

var _reactImageGallery2 = _interopRequireDefault(_reactImageGallery);

var _Panel = require('../Panel');

var _Panel2 = _interopRequireDefault(_Panel);

require('react-image-gallery/styles/css/image-gallery.css');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; } /**
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                * @Author: Ben Briggs <benbriggs>
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                * @Date:   2018-01-04T13:22:29+00:00
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                * @Email:  ben.briggs@thedistance.co.uk
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                * @Last modified by:   benbriggs
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                * @Last modified time: 2018-01-11T09:49:42+00:00
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                * @Copyright: The Distance
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                */

var Gallery = function (_React$Component) {
  _inherits(Gallery, _React$Component);

  function Gallery() {
    _classCallCheck(this, Gallery);

    return _possibleConstructorReturn(this, (Gallery.__proto__ || Object.getPrototypeOf(Gallery)).apply(this, arguments));
  }

  _createClass(Gallery, [{
    key: 'handleDelete',
    value: function handleDelete() {
      var index = this._gallery.getCurrentIndex();
      this.props.onDelete(index);
      this._gallery.slideToIndex(index === 0 ? 0 : index - 1);
    }
  }, {
    key: 'render',
    value: function render() {
      var _this2 = this;

      var _props = this.props,
          images = _props.images,
          onDelete = _props.onDelete,
          title = _props.title;

      var renderCustomControls = onDelete ? function () {
        return _react2.default.createElement('button', {
          type: 'button',
          className: 'icon-outlined-delete image-gallery-icon',
          onClick: _this2.handleDelete.bind(_this2)
        });
      } : null;
      return _react2.default.createElement(
        _Panel2.default,
        { title: title },
        images ? _react2.default.createElement(_reactImageGallery2.default, {
          ref: function ref(i) {
            _this2._gallery = i;
          },
          items: images,
          renderCustomControls: renderCustomControls
        }) : _react2.default.createElement(
          _rubix.Grid,
          null,
          _react2.default.createElement(
            _rubix.Row,
            null,
            _react2.default.createElement(
              _rubix.Col,
              { xs: 12 },
              _react2.default.createElement(
                'p',
                null,
                'No images to display.'
              )
            )
          )
        )
      );
    }
  }]);

  return Gallery;
}(_react2.default.Component);

Gallery.propTypes = {
  title: _propTypes2.default.string.isRequired,
  onDelete: _propTypes2.default.func,
  images: _propTypes2.default.array
};

Gallery.defaultProps = {
  title: 'Images'
};

exports.default = Gallery;