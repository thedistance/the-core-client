'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _propTypes = require('prop-types');

var _propTypes2 = _interopRequireDefault(_propTypes);

var _rubix = require('@sketchpixy/rubix');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var WrappedFormGroup = function WrappedFormGroup(_ref) {
  var children = _ref.children,
      required = _ref.required,
      validationState = _ref.validationState;
  return _react2.default.createElement(
    _rubix.FormGroup,
    {
      validationState: validationState,
      bsClass: required && 'form-group required'
    },
    children
  );
}; /**
    * @Author: Ben Briggs <benbriggs>
    * @Date:   2017-12-19T12:37:51+00:00
    * @Email:  ben.briggs@thedistance.co.uk
    * @Last modified by:   benbriggs
    * @Last modified time: 2017-12-19T12:40:34+00:00
    * @Copyright: The Distance
    */

WrappedFormGroup.propTypes = {
  children: _propTypes2.default.any,
  required: _propTypes2.default.bool,
  validationState: _propTypes2.default.any
};

exports.default = WrappedFormGroup;