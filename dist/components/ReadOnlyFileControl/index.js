'use strict';

Object.defineProperty(exports, '__esModule', {
  value: true,
});

var _createClass = (function() {
  function defineProperties(target, props) {
    for (var i = 0; i < props.length; i++) {
      var descriptor = props[i];
      descriptor.enumerable = descriptor.enumerable || false;
      descriptor.configurable = true;
      if ('value' in descriptor) descriptor.writable = true;
      Object.defineProperty(target, descriptor.key, descriptor);
    }
  }
  return function(Constructor, protoProps, staticProps) {
    if (protoProps) defineProperties(Constructor.prototype, protoProps);
    if (staticProps) defineProperties(Constructor, staticProps);
    return Constructor;
  };
})();

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _rubix = require('@sketchpixy/rubix');

var _FormGroup = require('../FormGroup');

var _FormGroup2 = _interopRequireDefault(_FormGroup);

var _GenericControl2 = require('../GenericControl');

var _GenericControl3 = _interopRequireDefault(_GenericControl2);

function _interopRequireDefault(obj) {
  return obj && obj.__esModule ? obj : { default: obj };
}

function _classCallCheck(instance, Constructor) {
  if (!(instance instanceof Constructor)) {
    throw new TypeError('Cannot call a class as a function');
  }
}

function _possibleConstructorReturn(self, call) {
  if (!self) {
    throw new ReferenceError(
      "this hasn't been initialised - super() hasn't been called"
    );
  }
  return call && (typeof call === 'object' || typeof call === 'function')
    ? call
    : self;
}

function _inherits(subClass, superClass) {
  if (typeof superClass !== 'function' && superClass !== null) {
    throw new TypeError(
      'Super expression must either be null or a function, not ' +
        typeof superClass
    );
  }
  subClass.prototype = Object.create(superClass && superClass.prototype, {
    constructor: {
      value: subClass,
      enumerable: false,
      writable: true,
      configurable: true,
    },
  });
  if (superClass)
    Object.setPrototypeOf
      ? Object.setPrototypeOf(subClass, superClass)
      : (subClass.__proto__ = superClass);
} /**
 * @Author: benbriggs
 * @Date:   2018-08-06T16:35:40+01:00
 * @Email:  ben.briggs@thedistance.co.uk
 * @Last modified by:   benbriggs
 * @Last modified time: 2018-08-06T16:39:54+01:00
 * @Copyright: The Distance
 */

var divStyle = {
  marginBottom: '10px',
};

var ReadOnlyFileControl = (function(_GenericControl) {
  _inherits(ReadOnlyFileControl, _GenericControl);

  function ReadOnlyFileControl() {
    _classCallCheck(this, ReadOnlyFileControl);

    return _possibleConstructorReturn(
      this,
      (
        ReadOnlyFileControl.__proto__ ||
        Object.getPrototypeOf(ReadOnlyFileControl)
      ).apply(this, arguments)
    );
  }

  _createClass(ReadOnlyFileControl, [
    {
      key: 'render',
      value: function render() {
        return _react2.default.createElement(
          'div',
          { style: divStyle },
          _react2.default.createElement(
            _FormGroup2.default,
            {
              validationState: this.validationState(),
              required: this.props.required,
            },
            _react2.default.createElement(
              _rubix.ControlLabel,
              null,
              this.props.label
            )
          ),
          this.state.value &&
            _react2.default.createElement(
              'a',
              { href: this.state.value.url(), target: '_blank' },
              _react2.default.createElement(
                _rubix.Button,
                { bsStyle: 'info', outlined: true },
                'Open ',
                this.state.value.name()
              )
            )
        );
      },
    },
  ]);

  return ReadOnlyFileControl;
})(_GenericControl3.default);

exports.default = ReadOnlyFileControl;
