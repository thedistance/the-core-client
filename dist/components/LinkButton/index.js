'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _propTypes = require('prop-types');

var _propTypes2 = _interopRequireDefault(_propTypes);

var _rubix = require('@sketchpixy/rubix');

var _reactRouterBootstrap = require('react-router-bootstrap');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _objectWithoutProperties(obj, keys) { var target = {}; for (var i in obj) { if (keys.indexOf(i) >= 0) continue; if (!Object.prototype.hasOwnProperty.call(obj, i)) continue; target[i] = obj[i]; } return target; } /**
                                                                                                                                                                                                                              * @Author: Ben Briggs <benbriggs>
                                                                                                                                                                                                                              * @Date:   2017-09-27T15:29:10+01:00
                                                                                                                                                                                                                              * @Email:  ben.briggs@thedistance.co.uk
                                                                                                                                                                                                                              * @Last modified by:   benbriggs
                                                                                                                                                                                                                              * @Last modified time: 2018-01-11T09:58:16+00:00
                                                                                                                                                                                                                              * @Copyright: The Distance
                                                                                                                                                                                                                              */

var LinkButton = function LinkButton(_ref) {
  var pathname = _ref.pathname,
      children = _ref.children,
      rest = _objectWithoutProperties(_ref, ['pathname', 'children']);

  return _react2.default.createElement(
    _reactRouterBootstrap.LinkContainer,
    { to: { pathname: pathname } },
    _react2.default.createElement(
      _rubix.Button,
      rest,
      children
    )
  );
};

LinkButton.propTypes = {
  pathname: _propTypes2.default.string,
  children: _propTypes2.default.any
};

exports.default = LinkButton;