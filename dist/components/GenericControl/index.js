'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _propTypes = require('prop-types');

var _propTypes2 = _interopRequireDefault(_propTypes);

var _rubix = require('@sketchpixy/rubix');

var _FormGroup = require('../FormGroup');

var _FormGroup2 = _interopRequireDefault(_FormGroup);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _objectWithoutProperties(obj, keys) { var target = {}; for (var i in obj) { if (keys.indexOf(i) >= 0) continue; if (!Object.prototype.hasOwnProperty.call(obj, i)) continue; target[i] = obj[i]; } return target; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; } /**
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                * @Author: Ben Briggs <benbriggs>
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                * @Date:   2017-12-15T14:09:40+00:00
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                * @Email:  ben.briggs@thedistance.co.uk
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                * @Last modified by:   benbriggs
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                * @Last modified time: 2018-04-20T12:21:53+01:00
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                * @Copyright: The Distance
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                */

var GenericControl = function (_React$Component) {
  _inherits(GenericControl, _React$Component);

  function GenericControl(props) {
    _classCallCheck(this, GenericControl);

    var _this = _possibleConstructorReturn(this, (GenericControl.__proto__ || Object.getPrototypeOf(GenericControl)).call(this, props));

    _this.state = {
      value: props.value,
      valid: null
    };
    _this.handleChange = _this.handleChange.bind(_this);
    return _this;
  }

  _createClass(GenericControl, [{
    key: 'validationState',
    value: function validationState() {
      var valid = this.state.valid;

      if (valid) {
        return 'success';
      }
      if (valid !== null) {
        return 'error';
      }
    }
  }, {
    key: 'handleChange',
    value: function handleChange(_ref) {
      var target = _ref.target;
      var value = target.value;

      var valid = this.props.validator ? this.props.validator(value) : null;

      this.setState({
        value: value,
        valid: valid
      });

      this.props.onChange(value, valid);
    }
  }, {
    key: 'render',
    value: function render() {
      var _props = this.props,
          actions = _props.actions,
          children = _props.children,
          validator = _props.validator,
          helpText = _props.helpText,
          rest = _objectWithoutProperties(_props, ['actions', 'children', 'validator', 'helpText']);

      var value = this.state.value;

      return _react2.default.createElement(
        _FormGroup2.default,
        {
          validationState: this.validationState(),
          required: this.props.required
        },
        _react2.default.createElement(
          _rubix.ControlLabel,
          null,
          this.props.label
        ),
        _react2.default.cloneElement(children, _extends({}, rest, {
          onChange: this.handleChange,
          value: value
        })),
        _react2.default.createElement(_rubix.FormControlFeedback, null),
        helpText ? _react2.default.createElement(
          _rubix.HelpBlock,
          null,
          helpText
        ) : null
      );
    }
  }]);

  return GenericControl;
}(_react2.default.Component);

GenericControl.defaultProps = {
  onChange: function onChange() {}
};

GenericControl.propTypes = {
  actions: _propTypes2.default.objectOf(_propTypes2.default.func),
  children: _propTypes2.default.any,
  onChange: _propTypes2.default.func,
  validator: _propTypes2.default.func,
  label: _propTypes2.default.string,
  required: _propTypes2.default.bool,
  value: _propTypes2.default.any
};

exports.default = GenericControl;