'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _parse = require('parse');

var _parse2 = _interopRequireDefault(_parse);

var _rubix = require('@sketchpixy/rubix');

var _FormGroup = require('../FormGroup');

var _FormGroup2 = _interopRequireDefault(_FormGroup);

var _GenericControl2 = require('../GenericControl');

var _GenericControl3 = _interopRequireDefault(_GenericControl2);

var _NumberControl = require('../NumberControl');

var _NumberControl2 = _interopRequireDefault(_NumberControl);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _objectWithoutProperties(obj, keys) { var target = {}; for (var i in obj) { if (keys.indexOf(i) >= 0) continue; if (!Object.prototype.hasOwnProperty.call(obj, i)) continue; target[i] = obj[i]; } return target; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; } /**
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                * @Author: Ben Briggs <benbriggs>
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                * @Date:   2017-12-18T15:49:01+00:00
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                * @Email:  ben.briggs@thedistance.co.uk
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                * @Last modified by:   benbriggs
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                * @Last modified time: 2018-01-11T09:52:28+00:00
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                * @Copyright: The Distance
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                */

/*
 * We use createGeoPoint here because parse-server does not accept
 * new Parse.GeoPoint(lat, long) but it *does* support the plain
 * object form.
 */

var createGeoPoint = function createGeoPoint(latitude, longitude) {
  return {
    __type: 'GeoPoint',
    latitude: latitude,
    longitude: longitude
  };
};

var GeoPointControl = function (_GenericControl) {
  _inherits(GeoPointControl, _GenericControl);

  function GeoPointControl(props) {
    _classCallCheck(this, GeoPointControl);

    var _this = _possibleConstructorReturn(this, (GeoPointControl.__proto__ || Object.getPrototypeOf(GeoPointControl)).call(this, props));

    var value = props.value;

    if (value) {
      _this.state = {
        latitude: value.latitude,
        longitude: value.longitude,
        valid: null
      };
    } else {
      _this.state = {
        latitude: 0,
        longitude: 0,
        valid: null
      };
    }
    return _this;
  }

  _createClass(GeoPointControl, [{
    key: 'handleLatitude',
    value: function handleLatitude(latitude) {
      try {
        this.setState({ latitude: latitude });
        var longitude = this.state.longitude;
        // Here we use geoPoint to trigger validation (hence try-catch); it goes
        // unused and we build a plain object instead.

        var geoPoint = new _parse2.default.GeoPoint(latitude, longitude); // eslint-disable-line no-unused-vars
        this.setState({ valid: true });
        this.props.onChange(createGeoPoint(latitude, longitude), true);
      } catch (err) {
        this.setState({ valid: false });
        this.props.onChange(null, false);
      }
    }
  }, {
    key: 'handleLongitude',
    value: function handleLongitude(longitude) {
      try {
        this.setState({ longitude: longitude });
        var latitude = this.state.latitude;
        // Here we use geoPoint to trigger validation (hence try-catch); it goes
        // unused and we build a plain object instead.

        var geoPoint = new _parse2.default.GeoPoint(latitude, longitude); // eslint-disable-line no-unused-vars
        this.setState({ valid: true });
        this.props.onChange(createGeoPoint(latitude, longitude), true);
      } catch (err) {
        this.setState({ valid: false });
        this.props.onChange(null, false);
      }
    }
  }, {
    key: 'render',
    value: function render() {
      var _props = this.props,
          validator = _props.validator,
          rest = _objectWithoutProperties(_props, ['validator']);

      return _react2.default.createElement(
        _FormGroup2.default,
        {
          validationState: this.validationState(),
          required: this.props.required
        },
        _react2.default.createElement(
          _rubix.ControlLabel,
          null,
          this.props.label
        ),
        _react2.default.createElement(
          _rubix.Grid,
          { collapse: true },
          _react2.default.createElement(
            _rubix.Row,
            null,
            _react2.default.createElement(
              _rubix.Col,
              { sm: 5 },
              _react2.default.createElement(_NumberControl2.default, _extends({}, rest, {
                label: 'Latitude',
                onChange: this.handleLatitude.bind(this),
                value: this.state.latitude
              })),
              _react2.default.createElement(_rubix.FormControlFeedback, null)
            ),
            _react2.default.createElement(
              _rubix.Col,
              { sm: 5, smOffset: 1 },
              _react2.default.createElement(_NumberControl2.default, _extends({}, rest, {
                label: 'Longitude',
                onChange: this.handleLongitude.bind(this),
                value: this.state.longitude
              })),
              _react2.default.createElement(_rubix.FormControlFeedback, null)
            )
          )
        )
      );
    }
  }]);

  return GeoPointControl;
}(_GenericControl3.default);

exports.default = GeoPointControl;