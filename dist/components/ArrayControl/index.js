'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _rubix = require('@sketchpixy/rubix');

var _FormGroup = require('../FormGroup');

var _FormGroup2 = _interopRequireDefault(_FormGroup);

var _GenericControl2 = require('../GenericControl');

var _GenericControl3 = _interopRequireDefault(_GenericControl2);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; } /**
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                * @Author: Ben Briggs <benbriggs>
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                * @Date:   2018-02-20T16:22:59+00:00
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                * @Email:  ben.briggs@thedistance.co.uk
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                * @Last modified by:   benbriggs
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                * @Last modified time: 2018-02-28T12:29:22+00:00
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                * @Copyright: The Distance
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                */

var ArrayControl = function (_GenericControl) {
  _inherits(ArrayControl, _GenericControl);

  function ArrayControl(props) {
    _classCallCheck(this, ArrayControl);

    var _this = _possibleConstructorReturn(this, (ArrayControl.__proto__ || Object.getPrototypeOf(ArrayControl)).call(this, props));

    var value = props.value;

    _this.state = {
      displayValue: value ? value.join(_this.props.delimiter) : '',
      value: value,
      valid: null
    };
    return _this;
  }

  _createClass(ArrayControl, [{
    key: 'handleChange',
    value: function handleChange(_ref) {
      var target = _ref.target;
      var displayValue = target.value;

      var parts = displayValue.split(this.props.delimiter);
      var value = parts ? parts.filter(Boolean).map(function (p) {
        return p.trim();
      }) : null;
      var valid = this.props.validator ? this.props.validator(value) : null;

      this.setState({
        displayValue: displayValue,
        value: value,
        valid: valid
      });

      this.props.onChange(value, valid);
    }
  }, {
    key: 'render',
    value: function render() {
      return _react2.default.createElement(
        _FormGroup2.default,
        {
          validationState: this.validationState(),
          required: this.props.required
        },
        _react2.default.createElement(
          _rubix.ControlLabel,
          null,
          this.props.label
        ),
        _react2.default.createElement(_rubix.FormControl, {
          type: 'text',
          onChange: this.handleChange,
          value: this.state.displayValue
        })
      );
    }
  }]);

  return ArrayControl;
}(_GenericControl3.default);

ArrayControl.defaultProps = {
  delimiter: ','
};

exports.default = ArrayControl;