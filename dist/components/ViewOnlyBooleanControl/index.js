'use strict';

Object.defineProperty(exports, '__esModule', {
  value: true,
});

var _extends =
  Object.assign ||
  function(target) {
    for (var i = 1; i < arguments.length; i++) {
      var source = arguments[i];
      for (var key in source) {
        if (Object.prototype.hasOwnProperty.call(source, key)) {
          target[key] = source[key];
        }
      }
    }
    return target;
  };

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _propTypes = require('prop-types');

var _propTypes2 = _interopRequireDefault(_propTypes);

var _SelectControl = require('../SelectControl');

var _SelectControl2 = _interopRequireDefault(_SelectControl);

var _rubix = require('@sketchpixy/rubix');

function _interopRequireDefault(obj) {
  return obj && obj.__esModule ? obj : { default: obj };
}

function _objectWithoutProperties(obj, keys) {
  var target = {};
  for (var i in obj) {
    if (keys.indexOf(i) >= 0) continue;
    if (!Object.prototype.hasOwnProperty.call(obj, i)) continue;
    target[i] = obj[i];
  }
  return target;
} /*
                                                                                                                                                                                                                              * Created Date: Mon, 16th Apr 2018, 12:19:48 pm
                                                                                                                                                                                                                              * Author: Harry Crank
                                                                                                                                                                                                                              * Email: harry.crank@thedistance.co.uk
                                                                                                                                                                                                                              * Copyright (c) 2018 The Distance
                                                                                                                                                                                                                              */

var ViewOnlyBooleanControl = function ViewOnlyBooleanControl(_ref) {
  var choices = _ref.choices,
    value = _ref.value,
    onChange = _ref.onChange,
    rest = _objectWithoutProperties(_ref, ['choices', 'value', 'onChange']);

  return _react2.default.createElement(
    _rubix.FormControl.Static,
    _extends(
      {
        collection: choices,
      },
      rest
    ),
    choices[Number(!value)]
  );
};

ViewOnlyBooleanControl.propTypes = {
  choices: _propTypes2.default.array,
  onChange: _propTypes2.default.func,
  value: _propTypes2.default.bool,
};

ViewOnlyBooleanControl.defaultProps = { choices: ['true', 'false'] };

exports.default = ViewOnlyBooleanControl;
