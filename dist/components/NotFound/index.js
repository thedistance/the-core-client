'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _rubix = require('@sketchpixy/rubix');

var _LinkButton = require('../LinkButton');

var _LinkButton2 = _interopRequireDefault(_LinkButton);

var _Panel = require('../Panel');

var _Panel2 = _interopRequireDefault(_Panel);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/**
 * @Author: Ben Briggs <benbriggs>
 * @Date:   2017-12-20T17:09:45+00:00
 * @Email:  ben.briggs@thedistance.co.uk
 * @Last modified by:   benbriggs
 * @Last modified time: 2018-01-11T10:00:27+00:00
 * @Copyright: The Distance
 */

var NotFound = function NotFound() {
  return _react2.default.createElement(
    _Panel2.default,
    { title: 'Page not found.', className: 'bg-red' },
    _react2.default.createElement(
      _rubix.Grid,
      null,
      _react2.default.createElement(
        _rubix.Row,
        null,
        _react2.default.createElement(
          _rubix.Col,
          { xs: 12 },
          _react2.default.createElement(
            'p',
            null,
            _react2.default.createElement(
              _LinkButton2.default,
              { pathname: '/' },
              'Return to home'
            )
          )
        )
      )
    )
  );
};

exports.default = NotFound;