'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _inflected = require('inflected');

var _fieldToReadOnlyComponent = require('./fieldToReadOnlyComponent');

var _fieldToReadOnlyComponent2 = _interopRequireDefault(_fieldToReadOnlyComponent);

var _parseObjectAccessor = require('./parseObjectAccessor');

var _parseObjectAccessor2 = _interopRequireDefault(_parseObjectAccessor);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var convertSchemaToReadOnlyFields = function convertSchemaToReadOnlyFields(schema, view) {
  return Object.keys(schema).map(function (key) {
    return {
      accessor: (0, _parseObjectAccessor2.default)(schema[key].editAccessor || schema[key].accessor, key),
      Component: (0, _fieldToReadOnlyComponent2.default)(schema[key], view),
      key: key,
      label: schema[key].label || (0, _inflected.titleize)(key),
      validator: schema[key].validator
    };
  });
}; /*
    * Created Date: Fri, 13th Apr 2018, 15:21:24 pm
    * Author: Harry Crank
    * Email: harry.crank@thedistance.co.uk
    * Copyright (c) 2018 The Distance
    */

exports.default = convertSchemaToReadOnlyFields;