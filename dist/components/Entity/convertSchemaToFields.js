'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _inflected = require('inflected');

var _fieldToComponent = require('./fieldToComponent');

var _fieldToComponent2 = _interopRequireDefault(_fieldToComponent);

var _parseObjectAccessor = require('./parseObjectAccessor');

var _parseObjectAccessor2 = _interopRequireDefault(_parseObjectAccessor);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var convertSchemaToFields = function convertSchemaToFields(schema, view) {
  return Object.keys(schema).map(function (key) {
    return {
      accessor: (0, _parseObjectAccessor2.default)(schema[key].editAccessor || schema[key].accessor, key),
      Component: (0, _fieldToComponent2.default)(schema[key], view),
      key: key,
      label: schema[key].label || (0, _inflected.titleize)(key),
      validator: schema[key].validator
    };
  });
}; /**
    * @Author: Ben Briggs <benbriggs>
    * @Date:   2018-01-25T16:38:37+00:00
    * @Email:  ben.briggs@thedistance.co.uk
    * @Last modified by:   benbriggs
    * @Last modified time: 2018-04-20T14:41:05+01:00
    * @Copyright: The Distance
    */

exports.default = convertSchemaToFields;