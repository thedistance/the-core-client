'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _identity = require('ramda/src/identity');

var _identity2 = _interopRequireDefault(_identity);

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

var _slicedToArray = function () { function sliceIterator(arr, i) { var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"]) _i["return"](); } finally { if (_d) throw _e; } } return _arr; } return function (arr, i) { if (Array.isArray(arr)) { return arr; } else if (Symbol.iterator in Object(arr)) { return sliceIterator(arr, i); } else { throw new TypeError("Invalid attempt to destructure non-iterable instance"); } }; }();

var _parse = require('parse');

var _parse2 = _interopRequireDefault(_parse);

var _decamelize = require('decamelize');

var _decamelize2 = _interopRequireDefault(_decamelize);

var _escapeStringRegexp = require('escape-string-regexp');

var _escapeStringRegexp2 = _interopRequireDefault(_escapeStringRegexp);

var _asyncActions = require('./../../util/asyncActions');

var asyncActions = _interopRequireWildcard(_asyncActions);

var _createAction = require('./../../util/createAction');

var _createAction2 = _interopRequireDefault(_createAction);

var _createReducer = require('./../../util/createReducer');

var _createReducer2 = _interopRequireDefault(_createReducer);

var _deprecated = require('./../../util/deprecated');

var _deprecated2 = _interopRequireDefault(_deprecated);

var _parse3 = require('./../../selectors/parse');

var _parse4 = _interopRequireDefault(_parse3);

var _redirect = require('./redirect');

var _redirect2 = _interopRequireDefault(_redirect);

var _createObjectFromSchemaDefaults = require('./createObjectFromSchemaDefaults');

var _createObjectFromSchemaDefaults2 = _interopRequireDefault(_createObjectFromSchemaDefaults);

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj.default = obj; return newObj; } }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _toConsumableArray(arr) { if (Array.isArray(arr)) { for (var i = 0, arr2 = Array(arr.length); i < arr.length; i++) { arr2[i] = arr[i]; } return arr2; } else { return Array.from(arr); } }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; } /*
                                                                                                                                                                                                                   * Created Date: Mon, 9th Apr 2018, 11:11:20 am
                                                                                                                                                                                                                   * Author: Harry Crank
                                                                                                                                                                                                                   * Email: harry.crank@thedistance.co.uk
                                                                                                                                                                                                                   * -----
                                                                                                                                                                                                                   * Last Modified: Mon, 9th Apr 2018, 16:02:17 pm
                                                                                                                                                                                                                   * Modified By: Harry Crank
                                                                                                                                                                                                                   * -----
                                                                                                                                                                                                                   * Copyright (c) 2018 The Distance
                                                                                                                                                                                                                   */

var parseDestroy = function parseDestroy(object) {
  return new Promise(function (resolve, reject) {
    object.destroy({
      success: function success(obj) {
        return resolve(obj);
      },
      error: function error(err) {
        return reject(err);
      }
    });
  });
};

function initialiseActions(entity, _ref) {
  var _selectors, _actionCreators, _actions, _extends2;

  var _ref$parseClass = _ref.parseClass,
      parseClass = _ref$parseClass === undefined ? entity.name : _ref$parseClass,
      _ref$onLoadCollection = _ref.onLoadCollection,
      onLoadCollection = _ref$onLoadCollection === undefined ? _identity2.default : _ref$onLoadCollection,
      _ref$onLoadCurrent = _ref.onLoadCurrent,
      onLoadCurrent = _ref$onLoadCurrent === undefined ? _identity2.default : _ref$onLoadCurrent,
      _ref$reducerActions = _ref.reducerActions,
      reducerActions = _ref$reducerActions === undefined ? {} : _ref$reducerActions;
  var name = entity.name,
      schema = entity.schema;

  var createActionId = function createActionId() {
    for (var _len = arguments.length, parts = Array(_len), _key = 0; _key < _len; _key++) {
      parts[_key] = arguments[_key];
    }

    return [name].concat(parts).join(':');
  };

  var scopedSelect = function scopedSelect(state) {
    return state[entity.camelised];
  };
  var selectError = function selectError(state) {
    return scopedSelect(state).error;
  };
  var selectErrorMessage = function selectErrorMessage(state) {
    return scopedSelect(state).error ? scopedSelect(state).error.message : null;
  };
  var selectCurrent = function selectCurrent(state) {
    return scopedSelect(state).current;
  };
  var selectCollection = function selectCollection(state) {
    return scopedSelect(state).collection;
  };
  var selectPages = function selectPages(state) {
    return scopedSelect(state).pages;
  };

  var selectors = (_selectors = {
    select: scopedSelect,
    selectError: selectError,
    selectErrorMessage: selectErrorMessage,
    selectCurrent: selectCurrent,
    selectCollection: selectCollection,
    selectPages: selectPages
  }, _defineProperty(_selectors, 'select' + name, (0, _deprecated2.default)(scopedSelect, 'Please use "select" instead')), _defineProperty(_selectors, 'select' + name + 'Error', (0, _deprecated2.default)(selectError, 'Please use "selectError" instead')), _defineProperty(_selectors, 'selectCurrent' + name, (0, _deprecated2.default)(selectCurrent, 'Please use "selectCurrent" instead')), _defineProperty(_selectors, 'select' + name + 'Collection', (0, _deprecated2.default)(selectCurrent, 'Please use "selectCollection" instead')), _selectors);

  var actionKeys = ['deleting', 'deleted', 'deleteFailed', 'loadingCurrent', 'loadedCurrent', 'loadingCollection', 'loadedCollection', 'new', 'revert', 'saving', 'saved', 'saveFailed', 'unloadCollection', 'unloadCurrent', 'update'];

  var actionIds = actionKeys.reduce(function (list, key) {
    list[key + 'Action'] = createActionId.apply(undefined, _toConsumableArray((0, _decamelize2.default)(key, ' ').split(' ')));
    return list;
  }, {});

  var deletingAction = asyncActions.createStart({
    type: actionIds.deletingAction
  });

  var deletedAction = asyncActions.createEnd({
    type: actionIds.deletedAction
  });

  var deleteFailedAction = asyncActions.createError({
    type: actionIds.deleteFailedAction
  });

  var loadingCurrentAction = asyncActions.createStart({
    type: actionIds.loadingCurrentAction
  });

  var loadedCurrentAction = asyncActions.createEnd({
    type: actionIds.loadedCurrentAction
  });

  var loadedCurrentSyncAction = (0, _createAction2.default)(actionIds.loadedCurrentAction);

  var loadingCollectionAction = asyncActions.createStart({
    type: actionIds.loadingCollectionAction
  });

  var loadedCollectionAction = asyncActions.createEnd({
    type: actionIds.loadedCollectionAction
  });

  var newAction = (0, _createAction2.default)(actionIds.newAction);

  var revertAction = (0, _createAction2.default)(actionIds.revertAction);

  var savingAction = asyncActions.createStart({
    type: actionIds.savingAction
  });

  var savedAction = asyncActions.createEnd({
    type: actionIds.savedAction
  });

  var saveFailedAction = asyncActions.createEnd({
    type: actionIds.saveFailedAction
  });

  var unloadCollectionAction = (0, _createAction2.default)(actionIds.unloadCollectionAction);

  var unloadCurrentAction = (0, _createAction2.default)(actionIds.unloadCurrentAction);

  var updateAction = (0, _createAction2.default)(actionIds.updateAction);

  var actionCreators = (_actionCreators = {}, _defineProperty(_actionCreators, 'deleting' + name + 'Action', (0, _deprecated2.default)(deletingAction, 'Please use "deletingAction" instead')), _defineProperty(_actionCreators, 'deletingAction', deletingAction), _defineProperty(_actionCreators, 'deleted' + name + 'Action', (0, _deprecated2.default)(deletedAction, 'Please use "deletedAction" instead')), _defineProperty(_actionCreators, 'deletedAction', deletedAction), _defineProperty(_actionCreators, 'delete' + name + 'FailedAction', (0, _deprecated2.default)(deleteFailedAction, 'Please use "deleteFailedAction" instead')), _defineProperty(_actionCreators, 'deleteFailedAction', deleteFailedAction), _defineProperty(_actionCreators, 'loadingCurrent' + name + 'Action', (0, _deprecated2.default)(loadingCurrentAction, 'Please use "loadingCurrentAction" instead')), _defineProperty(_actionCreators, 'loadingCurrentAction', loadingCurrentAction), _defineProperty(_actionCreators, 'loadedCurrent' + name + 'Action', (0, _deprecated2.default)(loadedCurrentAction, 'Please use "loadedCurrentAction" instead')), _defineProperty(_actionCreators, 'loadedCurrentAction', loadedCurrentAction), _defineProperty(_actionCreators, 'loadedCurrent' + name + 'SyncAction', (0, _deprecated2.default)(loadedCurrentSyncAction, 'Please use "loadedCurrentSyncAction" instead')), _defineProperty(_actionCreators, 'loadedCurrentSyncAction', loadedCurrentSyncAction), _defineProperty(_actionCreators, 'loading' + name + 'CollectionAction', (0, _deprecated2.default)(loadingCollectionAction, 'Please use "loadingCollectionAction" instead')), _defineProperty(_actionCreators, 'loadingCollectionAction', loadingCollectionAction), _defineProperty(_actionCreators, 'loaded' + name + 'CollectionAction', (0, _deprecated2.default)(loadedCollectionAction, 'Please use "loadedCollectionAction" instead')), _defineProperty(_actionCreators, 'loadedCollectionAction', loadedCollectionAction), _defineProperty(_actionCreators, 'new' + name + 'Action', (0, _deprecated2.default)(newAction, 'Please use "newAction" instead')), _defineProperty(_actionCreators, 'newAction', newAction), _defineProperty(_actionCreators, 'revert' + name + 'Action', (0, _deprecated2.default)(revertAction, 'Please use "revertAction" instead')), _defineProperty(_actionCreators, 'revertAction', revertAction), _defineProperty(_actionCreators, 'saving' + name + 'Action', (0, _deprecated2.default)(savingAction, 'Please use "savingAction" instead')), _defineProperty(_actionCreators, 'savingAction', savingAction), _defineProperty(_actionCreators, 'saved' + name + 'Action', (0, _deprecated2.default)(savedAction, 'Please use "savedAction" instead')), _defineProperty(_actionCreators, 'savedAction', savedAction), _defineProperty(_actionCreators, 'saveFailedAction', saveFailedAction), _defineProperty(_actionCreators, 'unload' + name + 'CollectionAction', (0, _deprecated2.default)(unloadCollectionAction, 'Please use "unloadCollectionAction" instead')), _defineProperty(_actionCreators, 'unloadCollectionAction', unloadCollectionAction), _defineProperty(_actionCreators, 'unloadCurrent' + name + 'Action', (0, _deprecated2.default)(unloadCurrentAction, 'Please use "unloadCurrentAction" instead')), _defineProperty(_actionCreators, 'unloadCurrentAction', unloadCurrentAction), _defineProperty(_actionCreators, 'update' + name + 'Action', (0, _deprecated2.default)(updateAction, 'Please use "updateAction" instead')), _defineProperty(_actionCreators, 'updateAction', updateAction), _actionCreators);

  var destroy = function destroy() {
    var _ref2 = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {},
        pathname = _ref2.pathname,
        router = _ref2.router,
        goBack = _ref2.goBack;

    return function (dispatch, getState) {
      var result = selectors.selectCurrent(getState());
      dispatch(deletingAction({ result: result }));

      return parseDestroy(result).then(function () {
        return dispatch(deletedAction({ result: result }));
      }).then(function () {
        return Promise.resolve({ pathname: pathname, router: router, goBack: goBack });
      }).then(_redirect2.default).catch(function (error) {
        return dispatch(deleteFailedAction({ error: error }));
      });
    };
  };

  var loadCurrent = function loadCurrent() {
    var _ref3 = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {},
        id = _ref3.id;

    return function (dispatch, getState) {
      var state = getState();
      var current = selectors.selectCurrent(getState());

      if (current) {
        return Promise.resolve(dispatch(loadedCurrentSyncAction({
          result: current
        })));
      }

      if (!id) {
        return;
      }

      dispatch(loadingCurrentAction());

      var Parse = (0, _parse4.default)(state);
      return onLoadCurrent(new Parse.Query(parseClass)).get(id).then(function (result) {
        dispatch(loadedCurrentAction({ result: result }));
        return result;
      });
    };
  };

  var loadCollection = function loadCollection() {
    var _ref4 = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {},
        _ref4$pageSize = _ref4.pageSize,
        pageSize = _ref4$pageSize === undefined ? 20 : _ref4$pageSize,
        _ref4$page = _ref4.page,
        page = _ref4$page === undefined ? 0 : _ref4$page,
        sorted = _ref4.sorted,
        filtered = _ref4.filtered;

    return function (dispatch, getState) {
      dispatch(loadingCollectionAction());

      var Parse = (0, _parse4.default)(getState());

      var data = onLoadCollection(new Parse.Query(parseClass)).limit(pageSize).skip(pageSize * page);

      if (filtered && filtered.length) {
        filtered.forEach(function (_ref5) {
          var id = _ref5.id,
              value = _ref5.value;
          return data.matches(id, new RegExp((0, _escapeStringRegexp2.default)(value), 'i'));
        });
      }

      if (sorted && sorted.length) {
        sorted.forEach(function (_ref6) {
          var id = _ref6.id,
              desc = _ref6.desc;
          return desc ? data.descending(id) : data.ascending(id);
        });
      }

      return Promise.all([data.count(), data.find()]).then(function (_ref7) {
        var _ref8 = _slicedToArray(_ref7, 2),
            count = _ref8[0],
            result = _ref8[1];

        return dispatch(loadedCollectionAction({ result: result, pages: Math.ceil(count / pageSize) }));
      });
    };
  };

  var defaultObject = (0, _createObjectFromSchemaDefaults2.default)(schema);

  var create = function create(params) {
    return function (dispatch, getState) {
      var Parse = (0, _parse4.default)(getState());
      var Instance = Parse.Object.extend(parseClass);
      return dispatch(newAction({
        result: new Instance(_extends({}, defaultObject, params))
      }));
    };
  };

  var revert = function revert() {
    return function (dispatch, getState) {
      var result = selectors.selectCurrent(getState());
      if (!result) {
        return;
      }
      result.revert();
      return dispatch(revertAction({ result: result }));
    };
  };

  var save = function save(redirectOptions) {
    return function (dispatch, getState) {
      var result = selectors.selectCurrent(getState());
      dispatch(savingAction({ result: result }));

      return result.save().then(function () {
        return dispatch(savedAction({ result: result }));
      }).then(function () {
        return redirectOptions && (0, _redirect2.default)(redirectOptions);
      }).catch(function (error) {
        if (error instanceof _parse2.default.Error) {
          return dispatch(saveFailedAction({ error: error }));
        }
        // Handle large file error
        if (error.status === 413) {
          return dispatch(saveFailedAction({
            error: {
              code: error.status,
              message: 'The file you tried to upload is too large.'
            }
          }));
        }
        // Other http error
        return dispatch(saveFailedAction({
          error: { code: error.status, message: error.statusText }
        }));
      });
    };
  };

  var unloadCollection = function unloadCollection() {
    return function (dispatch, getState) {
      var result = selectors.selectCollection(getState());
      return dispatch(unloadCollectionAction({ result: result }));
    };
  };

  var unloadCurrent = function unloadCurrent() {
    return function (dispatch, getState) {
      var result = selectors.selectCurrent(getState());
      return dispatch(unloadCurrentAction({ result: result }));
    };
  };

  var update = function update(_ref9) {
    var key = _ref9.key,
        value = _ref9.value;
    return function (dispatch, getState) {
      var result = selectors.selectCurrent(getState());
      result.set(key, value);
      return dispatch(updateAction({ result: result }));
    };
  };

  var actions = (_actions = {}, _defineProperty(_actions, 'delete' + name, (0, _deprecated2.default)(destroy, 'Please use "destroy" instead')), _defineProperty(_actions, 'destroy', destroy), _defineProperty(_actions, 'loadCurrent' + name, (0, _deprecated2.default)(loadCurrent, 'Please use "loadCurrent" instead.')), _defineProperty(_actions, 'loadCurrent', loadCurrent), _defineProperty(_actions, 'load' + name + 'Collection', (0, _deprecated2.default)(loadCollection, 'Please use "loadCollection" instead')), _defineProperty(_actions, 'loadCollection', loadCollection), _defineProperty(_actions, 'new' + name, (0, _deprecated2.default)(create, 'Please use "create" instead')), _defineProperty(_actions, 'create', create), _defineProperty(_actions, 'revert' + name, (0, _deprecated2.default)(revert, 'Please use "revert" instead')), _defineProperty(_actions, 'revert', revert), _defineProperty(_actions, 'save' + name, (0, _deprecated2.default)(save, 'Please use "save" instead')), _defineProperty(_actions, 'save', save), _defineProperty(_actions, 'unload' + name + 'Collection', (0, _deprecated2.default)(unloadCollection, 'Please use "unloadCollection" instead')), _defineProperty(_actions, 'unloadCollection', unloadCollection), _defineProperty(_actions, 'unloadCurrent' + name, (0, _deprecated2.default)(unloadCurrent, 'Please use "unloadCurrent" instead')), _defineProperty(_actions, 'unloadCurrent', unloadCurrent), _defineProperty(_actions, 'update' + name, (0, _deprecated2.default)(update, 'Please use "update" instead')), _defineProperty(_actions, 'update', update), _defineProperty(_actions, 'cancel', _redirect2.default), _actions);

  var nullCurrent = function nullCurrent(state) {
    return _extends({}, state, { current: null, error: null });
  };
  var updateCurrent = function updateCurrent(state, _ref10) {
    var result = _ref10.result;
    return _extends({}, state, { current: result });
  };

  var reducer = (0, _createReducer2.default)({
    collection: null,
    current: null,
    error: null,
    pages: null
  }, _extends((_extends2 = {}, _defineProperty(_extends2, actionIds.deletedAction, nullCurrent), _defineProperty(_extends2, actionIds.deleteFailedAction, function (state, _ref11) {
    var error = _ref11.error;
    return _extends({}, state, {
      error: error
    });
  }), _defineProperty(_extends2, actionIds.loadedCurrentAction, updateCurrent), _defineProperty(_extends2, actionIds.loadedCollectionAction, function (state, _ref12) {
    var pages = _ref12.pages,
        result = _ref12.result;
    return _extends({}, state, {
      collection: result,
      pages: pages
    });
  }), _defineProperty(_extends2, actionIds.newAction, updateCurrent), _defineProperty(_extends2, actionIds.revertAction, nullCurrent), _defineProperty(_extends2, actionIds.saveFailedAction, function (state, _ref13) {
    var error = _ref13.error;
    return _extends({}, state, {
      error: error
    });
  }), _defineProperty(_extends2, actionIds.unloadCollectionAction, function (state) {
    return _extends({}, state, {
      collection: null,
      pages: null
    });
  }), _defineProperty(_extends2, actionIds.unloadCurrentAction, nullCurrent), _defineProperty(_extends2, actionIds.updateAction, updateCurrent), _extends2), reducerActions));

  return _extends({}, entity, {
    actions: actions,
    actionCreators: actionCreators,
    actionIds: actionIds,
    createActionId: createActionId,
    reducer: reducer,
    selectors: selectors
  });
}

exports.default = initialiseActions;