'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _convertSchemaToFields = require('./convertSchemaToFields');

var _convertSchemaToFields2 = _interopRequireDefault(_convertSchemaToFields);

var _convertSchemaToIndex = require('./convertSchemaToIndex');

var _convertSchemaToIndex2 = _interopRequireDefault(_convertSchemaToIndex);

var _convertSchemaToReadOnlyFields = require('./convertSchemaToReadOnlyFields');

var _convertSchemaToReadOnlyFields2 = _interopRequireDefault(_convertSchemaToReadOnlyFields);

var _filterSchemaByPropExists = require('./filterSchemaByPropExists');

var _filterSchemaByPropExists2 = _interopRequireDefault(_filterSchemaByPropExists);

var _filterSchemaByPropNotExists = require('./filterSchemaByPropNotExists');

var _filterSchemaByPropNotExists2 = _interopRequireDefault(_filterSchemaByPropNotExists);

var _inflections = require('./inflections');

var _inflections2 = _interopRequireDefault(_inflections);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/*
 * Created Date: Mon, 9th Apr 2018, 11:11:07 am
 * Author: Harry Crank
 * Email: harry.crank@thedistance.co.uk
 * -----
 * Last Modified: Wed, 25th Apr 2018, 09:42:00 am
 * Modified By: Ben Briggs
 * -----
 * Copyright (c) 2018 The Distance
 */

function initialiseEntity(_ref) {
  var name = _ref.name,
      _ref$inflections = _ref.inflections,
      inflections = _ref$inflections === undefined ? {} : _ref$inflections,
      collectionIndex = _ref.collectionIndex,
      _ref$schema = _ref.schema,
      schema = _ref$schema === undefined ? {} : _ref$schema,
      readOnly = _ref.readOnly,
      readOnlyEditView = _ref.readOnlyEditView,
      readOnlyCreateView = _ref.readOnlyCreateView;

  var _generateInflections = (0, _inflections2.default)(name, inflections),
      camelised = _generateInflections.camelised,
      singular = _generateInflections.singular,
      plural = _generateInflections.plural,
      slug = _generateInflections.slug;

  var filteredSchema = (0, _filterSchemaByPropNotExists2.default)('hideInEdit', schema);

  var _ref2 = readOnly ? {
    createSchemaFields: (0, _convertSchemaToReadOnlyFields2.default)(filteredSchema, 'create'),
    editSchemaFields: (0, _convertSchemaToReadOnlyFields2.default)(filteredSchema, 'edit')
  } : {
    createSchemaFields: readOnlyCreateView ? (0, _convertSchemaToReadOnlyFields2.default)(filteredSchema, 'create') : (0, _convertSchemaToFields2.default)(filteredSchema, 'create'),
    editSchemaFields: readOnlyEditView ? (0, _convertSchemaToReadOnlyFields2.default)(filteredSchema, 'edit') : (0, _convertSchemaToFields2.default)(filteredSchema, 'edit')
  },
      createSchemaFields = _ref2.createSchemaFields,
      editSchemaFields = _ref2.editSchemaFields;

  if (!collectionIndex) {
    collectionIndex = (0, _convertSchemaToIndex2.default)((0, _filterSchemaByPropExists2.default)('showInIndex', schema));
  }

  return {
    readOnly: readOnly,
    readOnlyEditView: readOnlyEditView,
    readOnlyCreateView: readOnlyCreateView,
    name: name,
    collectionIndex: collectionIndex,
    schema: schema,
    camelised: camelised,
    singular: singular,
    plural: plural,
    slug: slug,
    createSchemaFields: createSchemaFields,
    editSchemaFields: editSchemaFields
  };
}

exports.default = initialiseEntity;