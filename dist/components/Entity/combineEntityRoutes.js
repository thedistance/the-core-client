"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});

function _toConsumableArray(arr) { if (Array.isArray(arr)) { for (var i = 0, arr2 = Array(arr.length); i < arr.length; i++) { arr2[i] = arr[i]; } return arr2; } else { return Array.from(arr); } }

/**
 * @Author: Ben Briggs <benbriggs>
 * @Date:   2018-02-01T11:06:31+00:00
 * @Email:  ben.briggs@thedistance.co.uk
 * @Last modified by:   benbriggs
 * @Last modified time: 2018-02-01T11:07:52+00:00
 * @Copyright: The Distance
 */

var combineEntityRoutes = function combineEntityRoutes() {
  for (var _len = arguments.length, entities = Array(_len), _key = 0; _key < _len; _key++) {
    entities[_key] = arguments[_key];
  }

  return entities.reduce(function (combined, _ref) {
    var routes = _ref.routes;
    return [].concat(_toConsumableArray(combined), _toConsumableArray(routes));
  }, []);
};

exports.default = combineEntityRoutes;