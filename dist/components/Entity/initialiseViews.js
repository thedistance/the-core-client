'use strict';

Object.defineProperty(exports, '__esModule', {
  value: true,
});

var _always = require('ramda/src/always');

var _always2 = _interopRequireDefault(_always);

var _extends =
  Object.assign ||
  function(target) {
    for (var i = 1; i < arguments.length; i++) {
      var source = arguments[i];
      for (var key in source) {
        if (Object.prototype.hasOwnProperty.call(source, key)) {
          target[key] = source[key];
        }
      }
    }
    return target;
  }; /*
                                                                                                                                                                                                                                                                   * Created Date: Mon, 9th Apr 2018, 11:11:30 am
                                                                                                                                                                                                                                                                   * Author: Harry Crank
                                                                                                                                                                                                                                                                   * Email: harry.crank@thedistance.co.uk
                                                                                                                                                                                                                                                                   * -----
                                                                                                                                                                                                                                                                   * Last Modified: Mon, 16th Apr 2018, 11:56:24 am
                                                                                                                                                                                                                                                                   * Modified By: Harry Crank
                                                                                                                                                                                                                                                                   * -----
                                                                                                                                                                                                                                                                   * Copyright (c) 2018 The Distance
                                                                                                                                                                                                                                                                   */

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _reactRouter = require('react-router');

var _createListView = require('./createListView');

var _createListView2 = _interopRequireDefault(_createListView);

var _createNewView = require('./createNewView');

var _createNewView2 = _interopRequireDefault(_createNewView);

var _createEditView = require('./createEditView');

var _createEditView2 = _interopRequireDefault(_createEditView);

var _createReadOnlyView = require('./createReadOnlyView');

var _createReadOnlyView2 = _interopRequireDefault(_createReadOnlyView);

function _interopRequireDefault(obj) {
  return obj && obj.__esModule ? obj : { default: obj };
}

var alwaysObj = (0, _always2.default)({});

function initialiseViews(entity, _ref) {
  var _ref$canCreateNew = _ref.canCreateNew,
    canCreateNew = _ref$canCreateNew === undefined ? true : _ref$canCreateNew,
    createView = _ref.createView,
    editView = _ref.editView,
    editButton = _ref.editButton,
    listView = _ref.listView,
    _ref$extraActions = _ref.extraActions,
    extraActions =
      _ref$extraActions === undefined ? alwaysObj : _ref$extraActions,
    _ref$extraCreateActio = _ref.extraCreateActions,
    extraCreateActions =
      _ref$extraCreateActio === undefined ? alwaysObj : _ref$extraCreateActio,
    _ref$extraEditActions = _ref.extraEditActions,
    extraEditActions =
      _ref$extraEditActions === undefined ? alwaysObj : _ref$extraEditActions;
  var readOnly = entity.readOnly,
    readOnlyEditView = entity.readOnlyEditView,
    camelised = entity.camelised,
    collectionIndex = entity.collectionIndex,
    plural = entity.plural,
    createSchemaFields = entity.createSchemaFields,
    editSchemaFields = entity.editSchemaFields,
    singular = entity.singular,
    slug = entity.slug,
    _entity$selectors = entity.selectors,
    selectCollection = _entity$selectors.selectCollection,
    selectCurrent = _entity$selectors.selectCurrent,
    selectErrorMessage = _entity$selectors.selectErrorMessage,
    selectPages = _entity$selectors.selectPages,
    _entity$actions = entity.actions,
    create = _entity$actions.create,
    destroy = _entity$actions.destroy,
    loadCollection = _entity$actions.loadCollection,
    loadCurrent = _entity$actions.loadCurrent,
    save = _entity$actions.save,
    revert = _entity$actions.revert,
    unloadCollection = _entity$actions.unloadCollection,
    unloadCurrent = _entity$actions.unloadCurrent,
    update = _entity$actions.update,
    getTheadFilterThProps = entity.getTheadFilterThProps;

  var routeId = camelised + 'Id';

  var exposedCreateView =
    createView ||
    (0, _createNewView2.default)({
      create: create,
      save: save,
      unloadCurrent: unloadCurrent,
      update: update,
      revert: revert,
      extraActions: extraActions,
      extraCreateActions: extraCreateActions,
      selectCurrent: selectCurrent,
      selectErrorMessage: selectErrorMessage,
      schemaFields: createSchemaFields,
      singular: singular,
    });

  var exposedEditView = void 0;

  if (editView) {
    exposedEditView = editView;
  } else if (readOnly || readOnlyEditView) {
    exposedEditView = (0, _createReadOnlyView2.default)({
      loadCurrent: loadCurrent,
      unloadCurrent: unloadCurrent,
      revert: revert,
      extraActions: extraActions,
      extraEditActions: extraEditActions,
      routeId: routeId,
      selectCurrent: selectCurrent,
      selectErrorMessage: selectErrorMessage,
      schemaFields: editSchemaFields,
      singular: singular,
    });
  } else {
    exposedEditView = (0, _createEditView2.default)({
      save: save,
      destroy: destroy,
      loadCurrent: loadCurrent,
      unloadCurrent: unloadCurrent,
      update: update,
      revert: revert,
      extraActions: extraActions,
      extraEditActions: extraEditActions,
      routeId: routeId,
      selectCurrent: selectCurrent,
      selectErrorMessage: selectErrorMessage,
      schemaFields: editSchemaFields,
      singular: singular,
    });
  }

  var exposedListView =
    listView ||
    (0, _createListView2.default)({
      loadCollection: loadCollection,
      unloadCollection: unloadCollection,
      selectCollection: selectCollection,
      selectPages: selectPages,
      singular: singular,
      plural: plural,
      collectionIndex: collectionIndex,
      canCreateNew: canCreateNew,
      readOnly: readOnly || readOnlyEditView,
      editButton: editButton,
      getTheadFilterThProps: getTheadFilterThProps,
    });

  return _extends({}, entity, {
    routeId: routeId,
    createView: exposedCreateView,
    editView: exposedEditView,
    listView: exposedListView,
    routes: [
      _react2.default.createElement(_reactRouter.Route, {
        path: '/' + slug,
        component: exposedListView,
        key: camelised + '__list',
      }),
      _react2.default.createElement(_reactRouter.Route, {
        path: '/' + slug + '/new',
        component: exposedCreateView,
        key: camelised + '__create',
      }),
      _react2.default.createElement(_reactRouter.Route, {
        path: '/' + slug + '/:' + routeId,
        component: exposedEditView,
        key: camelised + '__edit',
      }),
    ],
    extraActions: extraActions,
    extraCreateActions: extraCreateActions,
    extraEditActions: extraEditActions,
  });
}

exports.default = initialiseViews;
