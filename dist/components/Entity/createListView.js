'use strict';

Object.defineProperty(exports, '__esModule', {
  value: true,
});
exports.EntityCollection = undefined;

var _reactRedux = require('react-redux');

var _listView = require('./listView');

var _listView2 = _interopRequireDefault(_listView);

function _interopRequireDefault(obj) {
  return obj && obj.__esModule ? obj : { default: obj };
}

/**
 * @Author: Ben Briggs <benbriggs>
 * @Date:   2017-11-27T14:56:42+00:00
 * @Email:  ben.briggs@thedistance.co.uk
 * @Last modified by:   benbriggs
 * @Last modified time: 2018-05-01T11:36:16+01:00
 * @Copyright: The Distance
 */

var EntityCollection = (exports.EntityCollection = _listView2.default);

function createListView(_ref) {
  var loadCollection = _ref.loadCollection,
    unloadCollection = _ref.unloadCollection,
    selectCollection = _ref.selectCollection,
    selectPages = _ref.selectPages,
    singular = _ref.singular,
    plural = _ref.plural,
    collectionIndex = _ref.collectionIndex,
    canCreateNew = _ref.canCreateNew,
    readOnly = _ref.readOnly,
    editButton = _ref.editButton,
    getTheadFilterThProps = _ref.getTheadFilterThProps;

  var mapStateToProps = function mapStateToProps(state) {
    return {
      readOnly: readOnly,
      singular: singular,
      plural: plural,
      canCreateNew: canCreateNew,
      columns: collectionIndex,
      data: selectCollection(state) || [],
      pages: selectPages(state),
      getTheadFilterThProps: getTheadFilterThProps,
      editButton: editButton,
    };
  };

  var mapDispatchToProps = function mapDispatchToProps(dispatch) {
    return {
      actions: {
        load: function load(tableState) {
          return dispatch(loadCollection(tableState));
        },
        unload: function unload() {
          return dispatch(unloadCollection());
        },
      },
    };
  };

  return (0, _reactRedux.connect)(mapStateToProps, mapDispatchToProps)(
    _listView2.default
  );
}

exports.default = createListView;
