'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; /*
                                                                                                                                                                                                                                                                   * Created Date: Fri, 13th Apr 2018, 14:41:18 pm
                                                                                                                                                                                                                                                                   * Author: Harry Crank
                                                                                                                                                                                                                                                                   * Email: harry.crank@thedistance.co.uk
                                                                                                                                                                                                                                                                   * Copyright (c) 2018 The Distance
                                                                                                                                                                                                                                                                   */

var _reactRedux = require('react-redux');

var _readOnlyView = require('./readOnlyView');

var _readOnlyView2 = _interopRequireDefault(_readOnlyView);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function createReadOnlyView(_ref) {
  var loadCurrent = _ref.loadCurrent,
      unloadCurrent = _ref.unloadCurrent,
      routeId = _ref.routeId,
      _revert = _ref.revert,
      selectCurrent = _ref.selectCurrent,
      schemaFields = _ref.schemaFields,
      singular = _ref.singular,
      _ref$goBack = _ref.goBack,
      goBack = _ref$goBack === undefined ? 1 : _ref$goBack,
      extraActions = _ref.extraActions,
      extraEditActions = _ref.extraEditActions;

  var mapStateToProps = function mapStateToProps(state) {
    return {
      model: selectCurrent(state),
      fields: schemaFields,
      title: 'View ' + singular
    };
  };

  var mapDispatchToProps = function mapDispatchToProps(dispatch, ownProps) {
    return {
      actions: _extends({
        load: function load() {
          return dispatch(loadCurrent({ id: ownProps.routeParams[routeId] }));
        },
        cancel: function cancel() {
          var routeParts = ownProps.location.pathname.split('/').slice(0, goBack * -1);
          ownProps.router.push(routeParts.join('/'));
        },
        unload: function unload() {
          return dispatch(unloadCurrent());
        },
        revert: function revert() {
          return dispatch(_revert());
        }
      }, extraActions(dispatch, ownProps), extraEditActions(dispatch, ownProps))
    };
  };

  return (0, _reactRedux.connect)(mapStateToProps, mapDispatchToProps)(_readOnlyView2.default);
}

exports.default = createReadOnlyView;