"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
/**
 * @Author: Ben Briggs <benbriggs>
 * @Date:   2018-01-25T16:33:54+00:00
 * @Email:  ben.briggs@thedistance.co.uk
 * @Last modified by:   benbriggs
 * @Last modified time: 2018-01-25T16:37:20+00:00
 * @Copyright: The Distance
 */

var parseObjectAccessor = function parseObjectAccessor(accessor, key) {
  return function (parseObject) {
    if (accessor) {
      return accessor(parseObject);
    }
    return parseObject.get(key);
  };
};

exports.default = parseObjectAccessor;