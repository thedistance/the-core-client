'use strict';

Object.defineProperty(exports, '__esModule', {
  value: true,
});

var _extends =
  Object.assign ||
  function(target) {
    for (var i = 1; i < arguments.length; i++) {
      var source = arguments[i];
      for (var key in source) {
        if (Object.prototype.hasOwnProperty.call(source, key)) {
          target[key] = source[key];
        }
      }
    }
    return target;
  }; /**
 * @Author: Ben Briggs <benbriggs>
 * @Date:   2018-01-25T16:39:35+00:00
 * @Email:  ben.briggs@thedistance.co.uk
 * @Last modified by:   benbriggs
 * @Last modified time: 2018-08-29T14:53:13+01:00
 * @Copyright: The Distance
 */

/* eslint-disable func-names */

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _propTypes = require('prop-types');

var _propTypes2 = _interopRequireDefault(_propTypes);

var _ArrayControl = require('../ArrayControl');

var _ArrayControl2 = _interopRequireDefault(_ArrayControl);

var _BooleanControl = require('../BooleanControl');

var _BooleanControl2 = _interopRequireDefault(_BooleanControl);

var _CheckboxesControl = require('../CheckboxesControl');

var _CheckboxesControl2 = _interopRequireDefault(_CheckboxesControl);

var _FileControl = require('../FileControl');

var _FileControl2 = _interopRequireDefault(_FileControl);

var _GeoPointControl = require('../GeoPointControl');

var _GeoPointControl2 = _interopRequireDefault(_GeoPointControl);

var _HtmlControl = require('../HtmlControl');

var _HtmlControl2 = _interopRequireDefault(_HtmlControl);

var _HTMLFroalaControl = require('../HTMLFroalaControl');

var _HTMLFroalaControl2 = _interopRequireDefault(_HTMLFroalaControl);

var _NumberControl = require('../NumberControl');

var _NumberControl2 = _interopRequireDefault(_NumberControl);

var _ReadOnlyNumberControl = require('../ReadOnlyNumberControl');

var _ReadOnlyNumberControl2 = _interopRequireDefault(_ReadOnlyNumberControl);

var _NumericSelectControl = require('../NumericSelectControl');

var _NumericSelectControl2 = _interopRequireDefault(_NumericSelectControl);

var _PasswordControl = require('../PasswordControl');

var _PasswordControl2 = _interopRequireDefault(_PasswordControl);

var _PointerControl = require('../PointerControl');

var _PointerControl2 = _interopRequireDefault(_PointerControl);

var _SelectControl = require('../SelectControl');

var _SelectControl2 = _interopRequireDefault(_SelectControl);

var _TextControl = require('../TextControl');

var _TextControl2 = _interopRequireDefault(_TextControl);

var _TextareaControl = require('../TextareaControl');

var _TextareaControl2 = _interopRequireDefault(_TextareaControl);

var _StaticTextControl = require('../StaticTextControl');

var _StaticTextControl2 = _interopRequireDefault(_StaticTextControl);

var _ReadOnlyDateControl = require('../ReadOnlyDateControl');

var _ReadOnlyDateControl2 = _interopRequireDefault(_ReadOnlyDateControl);

function _interopRequireDefault(obj) {
  return obj && obj.__esModule ? obj : { default: obj };
}

function prepopulatedBooleanControl(choices) {
  return function PopulatedBooleanControl(props) {
    return _react2.default.createElement(
      _BooleanControl2.default,
      _extends({ choices: choices }, props)
    );
  };
}

function prepopulatedCheckboxesControl(choices) {
  return function PopulatedCheckboxesControl(props) {
    return _react2.default.createElement(
      _CheckboxesControl2.default,
      _extends({ collection: choices }, props)
    );
  };
}

function prepopulatedPointerControl(ParseObject, loadAction) {
  function PopulatedPointerControl(props) {
    return _react2.default.createElement(
      _PointerControl2.default,
      _extends(
        {
          ParseObject: ParseObject,
          loadOptions: props.actions[loadAction],
        },
        props
      )
    );
  }

  PopulatedPointerControl.propTypes = {
    actions: _propTypes2.default.objectOf(_propTypes2.default.func),
  };

  return PopulatedPointerControl;
}

function prepopulatedSelectControl(choices, allowBlank) {
  return function PopulatedSelectControl(props) {
    return _react2.default.createElement(
      _SelectControl2.default,
      _extends({ allowBlank: allowBlank, collection: choices }, props)
    );
  };
}

function prepopulatedNumericSelectControl(choices, allowBlank) {
  return function PopulatedNumericSelectControl(props) {
    return _react2.default.createElement(
      _NumericSelectControl2.default,
      _extends(
        {
          allowBlank: allowBlank,
          collection: choices,
        },
        props
      )
    );
  };
}

var fieldToComponent = function fieldToComponent(field, view) {
  return (
    (field.EditComponent
      ? field.EditComponent
      : view === 'create' ? field.CreateComponent : field.UpdateComponent) ||
    {
      array: _ArrayControl2.default,
      boolean: prepopulatedBooleanControl(field.choices),
      checkboxes: prepopulatedCheckboxesControl(field.choices),
      enum: prepopulatedSelectControl(field.choices, field.allowBlank),
      enumNumeric: prepopulatedNumericSelectControl(
        field.choices,
        field.allowBlank
      ),
      file: _FileControl2.default,
      geopoint: _GeoPointControl2.default,
      html: _HtmlControl2.default,
      htmlFroala: _HTMLFroalaControl2.default,
      number: _NumberControl2.default,
      staticNumber: _ReadOnlyNumberControl2.default,
      password: _PasswordControl2.default,
      pointer: prepopulatedPointerControl(field.ParseObject, field.loadAction),
      string: _TextControl2.default,
      text: _TextControl2.default,
      textarea: _TextareaControl2.default,
      staticDate: _ReadOnlyDateControl2.default,
      staticText: _StaticTextControl2.default,
    }[field.type] ||
    _TextControl2.default
  );
};

exports.default = fieldToComponent;
