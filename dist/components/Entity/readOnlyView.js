'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _propTypes = require('prop-types');

var _propTypes2 = _interopRequireDefault(_propTypes);

var _rubix = require('@sketchpixy/rubix');

var _Loadable2 = require('../Loadable');

var _Loadable3 = _interopRequireDefault(_Loadable2);

var _Alert = require('../Alert');

var _Alert2 = _interopRequireDefault(_Alert);

var _Panel = require('../Panel');

var _Panel2 = _interopRequireDefault(_Panel);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; } /*
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                * Created Date: Fri, 13th Apr 2018, 14:33:17 pm
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                * Author: Harry Crank
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                * Email: harry.crank@thedistance.co.uk
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                * Copyright (c) 2018 The Distance
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                */

var ReadOnlyView = function (_Loadable) {
  _inherits(ReadOnlyView, _Loadable);

  function ReadOnlyView() {
    _classCallCheck(this, ReadOnlyView);

    return _possibleConstructorReturn(this, (ReadOnlyView.__proto__ || Object.getPrototypeOf(ReadOnlyView)).apply(this, arguments));
  }

  _createClass(ReadOnlyView, [{
    key: 'render',
    value: function render() {
      var _props = this.props,
          actions = _props.actions,
          alert = _props.alert,
          model = _props.model,
          fields = _props.fields,
          title = _props.title;

      if (!model) {
        return null;
      }
      return _react2.default.createElement(
        'div',
        null,
        alert ? _react2.default.createElement(
          _Alert2.default,
          { danger: true },
          alert
        ) : null,
        _react2.default.createElement(
          _Panel2.default,
          { title: title },
          _react2.default.createElement(
            _rubix.Grid,
            null,
            _react2.default.createElement(
              _rubix.Row,
              null,
              _react2.default.createElement(
                _rubix.Col,
                { xs: 12 },
                _react2.default.createElement(
                  _rubix.Form,
                  { autoComplete: 'off' },
                  fields.map(function (_ref, index) {
                    var label = _ref.label,
                        accessor = _ref.accessor,
                        key = _ref.key,
                        validator = _ref.validator,
                        Component = _ref.Component;
                    return _react2.default.createElement(Component, {
                      actions: actions,
                      key: index,
                      label: label,
                      value: accessor(model),
                      validator: validator
                    });
                  }),
                  _react2.default.createElement(
                    _rubix.ButtonToolbar,
                    { style: { marginBottom: '1em' } },
                    _react2.default.createElement(
                      _rubix.Grid,
                      null,
                      _react2.default.createElement(
                        _rubix.Row,
                        null,
                        _react2.default.createElement(
                          _rubix.Col,
                          { xs: 12, style: { textAlign: 'right' } },
                          _react2.default.createElement(
                            _rubix.Button,
                            { bsStyle: 'primary', onClick: actions.cancel },
                            'Back'
                          )
                        )
                      )
                    )
                  )
                )
              )
            )
          )
        )
      );
    }
  }]);

  return ReadOnlyView;
}(_Loadable3.default);

ReadOnlyView.propTypes = {
  actions: _propTypes2.default.objectOf(_propTypes2.default.func).isRequired,
  alert: _propTypes2.default.string,
  fields: _propTypes2.default.array.isRequired,
  model: _propTypes2.default.object,
  title: _propTypes2.default.string.isRequired
};

exports.default = ReadOnlyView;