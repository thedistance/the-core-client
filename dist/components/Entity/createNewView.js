'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; /**
                                                                                                                                                                                                                                                                   * @Author: Ben Briggs <benbriggs>
                                                                                                                                                                                                                                                                   * @Date:   2018-01-26T13:18:47+00:00
                                                                                                                                                                                                                                                                   * @Email:  ben.briggs@thedistance.co.uk
                                                                                                                                                                                                                                                                   * @Last modified by:   benbriggs
                                                                                                                                                                                                                                                                   * @Last modified time: 2018-04-25T11:05:08+01:00
                                                                                                                                                                                                                                                                   * @Copyright: The Distance
                                                                                                                                                                                                                                                                   */

var _reactRedux = require('react-redux');

var _editView = require('./editView');

var _editView2 = _interopRequireDefault(_editView);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function createNewView(_ref) {
  var create = _ref.create,
      _save = _ref.save,
      unloadCurrent = _ref.unloadCurrent,
      _update = _ref.update,
      _revert = _ref.revert,
      selectCurrent = _ref.selectCurrent,
      selectErrorMessage = _ref.selectErrorMessage,
      schemaFields = _ref.schemaFields,
      singular = _ref.singular,
      _ref$goBack = _ref.goBack,
      goBack = _ref$goBack === undefined ? 1 : _ref$goBack,
      extraActions = _ref.extraActions,
      extraCreateActions = _ref.extraCreateActions;

  var mapStateToProps = function mapStateToProps(state) {
    return {
      alert: selectErrorMessage(state),
      model: selectCurrent(state),
      fields: schemaFields,
      title: 'New ' + singular
    };
  };

  var mapDispatchToProps = function mapDispatchToProps(dispatch, ownProps) {
    return {
      actions: _extends({
        load: function load() {
          return dispatch(create());
        },
        save: function save() {
          return dispatch(_save({
            pathname: ownProps.location.pathname,
            router: ownProps.router,
            goBack: 1
          }));
        },
        cancel: function cancel() {
          var routeParts = ownProps.location.pathname.split('/').slice(0, goBack * -1);
          ownProps.router.push(routeParts.join('/'));
        },
        unload: function unload() {
          return dispatch(unloadCurrent());
        },
        update: function update(options) {
          return dispatch(_update(options));
        },
        revert: function revert() {
          return dispatch(_revert());
        }
      }, extraActions(dispatch, ownProps), extraCreateActions(dispatch, ownProps))
    };
  };

  return (0, _reactRedux.connect)(mapStateToProps, mapDispatchToProps)(_editView2.default);
}

exports.default = createNewView;