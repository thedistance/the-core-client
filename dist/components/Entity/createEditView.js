'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; /**
                                                                                                                                                                                                                                                                   * @Author: Ben Briggs <benbriggs>
                                                                                                                                                                                                                                                                   * @Date:   2018-01-26T13:36:27+00:00
                                                                                                                                                                                                                                                                   * @Email:  ben.briggs@thedistance.co.uk
                                                                                                                                                                                                                                                                   * @Last modified by:   benbriggs
                                                                                                                                                                                                                                                                   * @Last modified time: 2018-04-25T11:05:05+01:00
                                                                                                                                                                                                                                                                   * @Copyright: The Distance
                                                                                                                                                                                                                                                                   */

var _reactRedux = require('react-redux');

var _editView = require('./editView');

var _editView2 = _interopRequireDefault(_editView);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function createEditView(_ref) {
  var _save = _ref.save,
      destroy = _ref.destroy,
      loadCurrent = _ref.loadCurrent,
      unloadCurrent = _ref.unloadCurrent,
      _update = _ref.update,
      routeId = _ref.routeId,
      _revert = _ref.revert,
      selectCurrent = _ref.selectCurrent,
      selectErrorMessage = _ref.selectErrorMessage,
      schemaFields = _ref.schemaFields,
      singular = _ref.singular,
      _ref$goBack = _ref.goBack,
      goBack = _ref$goBack === undefined ? 1 : _ref$goBack,
      extraActions = _ref.extraActions,
      extraEditActions = _ref.extraEditActions;

  var mapStateToProps = function mapStateToProps(state) {
    return {
      alert: selectErrorMessage(state),
      model: selectCurrent(state),
      fields: schemaFields,
      title: 'Edit ' + singular
    };
  };

  var mapDispatchToProps = function mapDispatchToProps(dispatch, ownProps) {
    return {
      actions: _extends({
        load: function load() {
          return dispatch(loadCurrent({ id: ownProps.routeParams[routeId] }));
        },
        save: function save() {
          return dispatch(_save({
            pathname: ownProps.location.pathname,
            router: ownProps.router,
            goBack: goBack
          }));
        },
        delete: function _delete() {
          return dispatch(destroy({
            pathname: ownProps.location.pathname,
            router: ownProps.router,
            goBack: goBack
          }));
        },
        unload: function unload() {
          return dispatch(unloadCurrent());
        },
        update: function update(options) {
          return dispatch(_update(options));
        },
        revert: function revert() {
          return dispatch(_revert());
        }
      }, extraActions(dispatch, ownProps), extraEditActions(dispatch, ownProps))
    };
  };

  return (0, _reactRedux.connect)(mapStateToProps, mapDispatchToProps)(_editView2.default);
}

exports.default = createEditView;