'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _propTypes = require('prop-types');

var _propTypes2 = _interopRequireDefault(_propTypes);

var _rubix = require('@sketchpixy/rubix');

var _Alert = require('../Alert');

var _Alert2 = _interopRequireDefault(_Alert);

var _Panel = require('../Panel');

var _Panel2 = _interopRequireDefault(_Panel);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; } /**
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                * @Author: Ben Briggs <benbriggs>
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                * @Date:   2018-01-26T13:17:29+00:00
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                * @Email:  ben.briggs@thedistance.co.uk
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                * @Last modified by:   benbriggs
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                * @Last modified time: 2018-04-25T11:09:57+01:00
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                * @Copyright: The Distance
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                */

var EditActions = function EditActions(_ref) {
  var actions = _ref.actions;
  return actions.delete ? _react2.default.createElement(
    _rubix.Button,
    { bsStyle: 'danger', onClick: actions.delete },
    'Delete'
  ) : _react2.default.createElement(
    _rubix.Button,
    { onClick: actions.cancel },
    'Cancel'
  );
};

EditActions.propTypes = {
  actions: _propTypes2.default.objectOf(_propTypes2.default.func).isRequired
};

var EditView = function (_React$Component) {
  _inherits(EditView, _React$Component);

  function EditView(props) {
    _classCallCheck(this, EditView);

    var _this = _possibleConstructorReturn(this, (EditView.__proto__ || Object.getPrototypeOf(EditView)).call(this, props));

    _this.state = {
      saved: true
    };
    return _this;
  }

  _createClass(EditView, [{
    key: 'onChange',
    value: function onChange(key, value) {
      this.setState({ saved: false });
      this.props.actions.update({ key: key, value: value });
    }
  }, {
    key: 'componentWillMount',
    value: function componentWillMount() {
      this.props.actions.load();
    }
  }, {
    key: 'componentWillUnmount',
    value: function componentWillUnmount() {
      if (!this.state.saved) {
        this.props.actions.revert();
      }
      this.props.actions.unload();
    }
  }, {
    key: 'save',
    value: function save() {
      this.setState({ saved: true });
      this.props.actions.save();
    }
  }, {
    key: 'render',
    value: function render() {
      var _this2 = this;

      var _props = this.props,
          actions = _props.actions,
          alert = _props.alert,
          model = _props.model,
          fields = _props.fields,
          title = _props.title;

      if (!model) {
        return null;
      }
      return _react2.default.createElement(
        'div',
        null,
        alert ? _react2.default.createElement(
          _Alert2.default,
          { danger: true },
          alert
        ) : null,
        _react2.default.createElement(
          _Panel2.default,
          { title: title },
          _react2.default.createElement(
            _rubix.Grid,
            null,
            _react2.default.createElement(
              _rubix.Row,
              null,
              _react2.default.createElement(
                _rubix.Col,
                { xs: 12 },
                _react2.default.createElement(
                  _rubix.Form,
                  { autoComplete: 'off' },
                  fields.map(function (_ref2, index) {
                    var label = _ref2.label,
                        accessor = _ref2.accessor,
                        key = _ref2.key,
                        validator = _ref2.validator,
                        Component = _ref2.Component;
                    return _react2.default.createElement(Component, {
                      actions: actions,
                      key: index,
                      label: label,
                      value: accessor(model),
                      validator: validator,
                      onChange: _this2.onChange.bind(_this2, key)
                    });
                  }),
                  _react2.default.createElement(
                    _rubix.ButtonToolbar,
                    { style: { marginBottom: '1em' } },
                    _react2.default.createElement(
                      _rubix.Grid,
                      null,
                      _react2.default.createElement(
                        _rubix.Row,
                        null,
                        _react2.default.createElement(
                          _rubix.Col,
                          { xs: 6 },
                          _react2.default.createElement(EditActions, { actions: actions })
                        ),
                        _react2.default.createElement(
                          _rubix.Col,
                          { xs: 6, style: { textAlign: 'right' } },
                          _react2.default.createElement(
                            _rubix.Button,
                            {
                              bsStyle: 'primary',
                              onClick: this.save.bind(this)
                            },
                            'Save'
                          )
                        )
                      )
                    )
                  )
                )
              )
            )
          )
        )
      );
    }
  }]);

  return EditView;
}(_react2.default.Component);

EditView.propTypes = {
  actions: _propTypes2.default.objectOf(_propTypes2.default.func).isRequired,
  alert: _propTypes2.default.string,
  fields: _propTypes2.default.array.isRequired,
  model: _propTypes2.default.object,
  title: _propTypes2.default.string.isRequired
};

exports.default = EditView;