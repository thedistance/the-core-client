'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; /**
                                                                                                                                                                                                                                                                   * @Author: Ben Briggs <benbriggs>
                                                                                                                                                                                                                                                                   * @Date:   2018-01-25T16:33:39+00:00
                                                                                                                                                                                                                                                                   * @Email:  ben.briggs@thedistance.co.uk
                                                                                                                                                                                                                                                                   * @Last modified by:   benbriggs
                                                                                                                                                                                                                                                                   * @Last modified time: 2018-04-05T11:37:06+01:00
                                                                                                                                                                                                                                                                   * @Copyright: The Distance
                                                                                                                                                                                                                                                                   */

var _inflected = require('inflected');

var _parseObjectAccessor = require('./parseObjectAccessor');

var _parseObjectAccessor2 = _interopRequireDefault(_parseObjectAccessor);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var convertSchemaToIndex = function convertSchemaToIndex(schema) {
  return Object.keys(schema).map(function (key) {
    return _extends({
      accessor: (0, _parseObjectAccessor2.default)(schema[key].indexAccessor || schema[key].accessor, key),
      Header: schema[key].label || (0, _inflected.titleize)(key),
      id: key,
      Cell: schema[key].IndexComponent
    }, schema[key].Filter && { Filter: schema[key].Filter }, schema[key].filterMethod && { filterMethod: schema[key].filterMethod }, {
      filterable: schema[key].filterable,
      sortable: schema[key].sortable
    });
  });
};

exports.default = convertSchemaToIndex;