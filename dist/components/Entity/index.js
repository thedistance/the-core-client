'use strict';

Object.defineProperty(exports, '__esModule', {
  value: true,
});

var _identity = require('ramda/src/identity');

var _identity2 = _interopRequireDefault(_identity);

var _initialiseEntity = require('./initialiseEntity');

var _initialiseEntity2 = _interopRequireDefault(_initialiseEntity);

var _initialiseActions = require('./initialiseActions');

var _initialiseActions2 = _interopRequireDefault(_initialiseActions);

var _initialiseViews = require('./initialiseViews');

var _initialiseViews2 = _interopRequireDefault(_initialiseViews);

function _interopRequireDefault(obj) {
  return obj && obj.__esModule ? obj : { default: obj };
}

/**
 * @Author: Ben Briggs <benbriggs>
 * @Date:   2017-10-03T17:41:45+01:00
 * @Email:  ben.briggs@thedistance.co.uk
 * @Last modified by:   benbriggs
 * @Last modified time: 2018-04-25T09:48:05+01:00
 * @Copyright: The Distance
 */

function createEntity(_ref) {
  var name = _ref.name,
    inflections = _ref.inflections,
    collectionIndex = _ref.collectionIndex,
    _ref$parseClass = _ref.parseClass,
    parseClass = _ref$parseClass === undefined ? name : _ref$parseClass,
    _ref$canCreateNew = _ref.canCreateNew,
    canCreateNew = _ref$canCreateNew === undefined ? true : _ref$canCreateNew,
    _ref$onLoadCollection = _ref.onLoadCollection,
    onLoadCollection =
      _ref$onLoadCollection === undefined
        ? _identity2.default
        : _ref$onLoadCollection,
    _ref$onLoadCurrent = _ref.onLoadCurrent,
    onLoadCurrent =
      _ref$onLoadCurrent === undefined
        ? _identity2.default
        : _ref$onLoadCurrent,
    _ref$reducerActions = _ref.reducerActions,
    reducerActions =
      _ref$reducerActions === undefined ? {} : _ref$reducerActions,
    _ref$schema = _ref.schema,
    schema = _ref$schema === undefined ? {} : _ref$schema,
    createView = _ref.createView,
    editView = _ref.editView,
    _ref$editButton = _ref.editButton,
    editButton = _ref$editButton === undefined ? true : _ref$editButton,
    _ref$readOnly = _ref.readOnly,
    readOnly = _ref$readOnly === undefined ? false : _ref$readOnly,
    _ref$readOnlyEditView = _ref.readOnlyEditView,
    readOnlyEditView =
      _ref$readOnlyEditView === undefined ? false : _ref$readOnlyEditView,
    _ref$readOnlyCreateVi = _ref.readOnlyCreateView,
    readOnlyCreateView =
      _ref$readOnlyCreateVi === undefined ? false : _ref$readOnlyCreateVi,
    listView = _ref.listView,
    extraActions = _ref.extraActions,
    extraCreateActions = _ref.extraCreateActions,
    extraEditActions = _ref.extraEditActions,
    getTheadFilterThProps = _ref.getTheadFilterThProps;

  var entity = (0, _initialiseEntity2.default)({
    name: name,
    inflections: inflections,
    collectionIndex: collectionIndex,
    schema: schema,
    readOnly: readOnly,
    readOnlyEditView: readOnlyEditView,
    readOnlyCreateView: readOnlyCreateView,
    getTheadFilterThProps: getTheadFilterThProps,
  });
  entity = (0, _initialiseActions2.default)(entity, {
    parseClass: parseClass,
    onLoadCollection: onLoadCollection,
    onLoadCurrent: onLoadCurrent,
    reducerActions: reducerActions,
  });
  return (0, _initialiseViews2.default)(entity, {
    canCreateNew: canCreateNew,
    createView: createView,
    editView: editView,
    editButton: editButton,
    listView: listView,
    extraActions: extraActions,
    extraCreateActions: extraCreateActions,
    extraEditActions: extraEditActions,
  });
}

exports.default = createEntity;
