'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _inflected = require('inflected');

function inflections(name) {
  var overrides = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {};

  var camelised = overrides.camelised || '' + name[0].toLowerCase() + name.slice(1);
  var titleCased = overrides.titleCased || (0, _inflected.titleize)(name);
  var slug = overrides.slug || (0, _inflected.parameterize)(titleCased);
  var singular = overrides.singular || (0, _inflected.singularize)(titleCased);
  var plural = overrides.plural || (0, _inflected.pluralize)(titleCased);

  return {
    camelised: camelised,
    titleCased: titleCased,
    slug: slug,
    singular: singular,
    plural: plural
  };
} /**
   * @Author: Ben Briggs <benbriggs>
   * @Date:   2018-02-06T15:03:36+00:00
   * @Email:  ben.briggs@thedistance.co.uk
   * @Last modified by:   benbriggs
   * @Last modified time: 2018-04-25T09:39:45+01:00
   * @Copyright: The Distance
   */

exports.default = inflections;