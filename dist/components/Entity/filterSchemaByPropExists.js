'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _prop = require('ramda/src/prop');

var _prop2 = _interopRequireDefault(_prop);

var _pickBy = require('ramda/src/pickBy');

var _pickBy2 = _interopRequireDefault(_pickBy);

var _curry = require('ramda/src/curry');

var _curry2 = _interopRequireDefault(_curry);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var filterSchema = (0, _curry2.default)(function (property, schema) {
  return (0, _pickBy2.default)(function (value, key) {
    return (0, _prop2.default)(property, schema[key]);
  }, schema);
}); /**
     * @Author: Ben Briggs <benbriggs>
     * @Date:   2018-02-06T11:07:20+00:00
     * @Email:  ben.briggs@thedistance.co.uk
     * @Last modified by:   benbriggs
     * @Last modified time: 2018-02-06T11:37:05+00:00
     * @Copyright: The Distance
     */

exports.default = filterSchema;