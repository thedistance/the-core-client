"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

/**
 * @Author: Ben Briggs <benbriggs>
 * @Date:   2018-02-01T09:35:15+00:00
 * @Email:  ben.briggs@thedistance.co.uk
 * @Last modified by:   benbriggs
 * @Last modified time: 2018-02-01T09:35:36+00:00
 * @Copyright: The Distance
 */

var combineEntityReducers = function combineEntityReducers() {
  for (var _len = arguments.length, entities = Array(_len), _key = 0; _key < _len; _key++) {
    entities[_key] = arguments[_key];
  }

  return entities.reduce(function (combined, _ref) {
    var camelised = _ref.camelised,
        reducer = _ref.reducer;
    return _extends({}, combined, _defineProperty({}, camelised, reducer));
  }, {});
};

exports.default = combineEntityReducers;