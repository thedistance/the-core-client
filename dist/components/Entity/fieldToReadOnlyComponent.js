'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; /*
                                                                                                                                                                                                                                                                   * Created Date: Fri, 13th Apr 2018, 15:25:28 pm
                                                                                                                                                                                                                                                                   * Author: Harry Crank
                                                                                                                                                                                                                                                                   * Email: harry.crank@thedistance.co.uk
                                                                                                                                                                                                                                                                   * Copyright (c) 2018 The Distance
                                                                                                                                                                                                                                                                   */

/* eslint-disable func-names */

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _propTypes = require('prop-types');

var _propTypes2 = _interopRequireDefault(_propTypes);

var _CheckboxesControl = require('../CheckboxesControl');

var _CheckboxesControl2 = _interopRequireDefault(_CheckboxesControl);

var _DangerousHtmlControl = require('../DangerousHtmlControl');

var _DangerousHtmlControl2 = _interopRequireDefault(_DangerousHtmlControl);

var _ReadOnlyGeoPointControl = require('../ReadOnlyGeoPointControl');

var _ReadOnlyGeoPointControl2 = _interopRequireDefault(_ReadOnlyGeoPointControl);

var _ReadOnlyNumberControl = require('../ReadOnlyNumberControl');

var _ReadOnlyNumberControl2 = _interopRequireDefault(_ReadOnlyNumberControl);

var _ReadOnlyNumericSelectControl = require('../ReadOnlyNumericSelectControl');

var _ReadOnlyNumericSelectControl2 = _interopRequireDefault(_ReadOnlyNumericSelectControl);

var _PasswordControl = require('../PasswordControl');

var _PasswordControl2 = _interopRequireDefault(_PasswordControl);

var _ReadOnlyPointerControl = require('../ReadOnlyPointerControl');

var _ReadOnlyPointerControl2 = _interopRequireDefault(_ReadOnlyPointerControl);

var _StaticTextControl = require('../StaticTextControl');

var _StaticTextControl2 = _interopRequireDefault(_StaticTextControl);

var _ReadOnlyBooleanControl = require('../ReadOnlyBooleanControl');

var _ReadOnlyBooleanControl2 = _interopRequireDefault(_ReadOnlyBooleanControl);

var _ReadOnlyDateControl = require('../ReadOnlyDateControl');

var _ReadOnlyDateControl2 = _interopRequireDefault(_ReadOnlyDateControl);

var _ReadOnlyFileControl = require('../ReadOnlyFileControl');

var _ReadOnlyFileControl2 = _interopRequireDefault(_ReadOnlyFileControl);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function prepopulatedReadOnlyBooleanControl(choices) {
  return function PopulatedReadOnlyBooleanControl(props) {
    return _react2.default.createElement(_ReadOnlyBooleanControl2.default, _extends({ choices: choices }, props));
  };
}

function prepopulatedCheckboxesControl(choices) {
  return function PopulatedCheckboxesControl(props) {
    return _react2.default.createElement(_CheckboxesControl2.default, _extends({ collection: choices }, props));
  };
}

function prepopulatedReadOnlyPointerControl(ParseObject, loadAction) {
  function PopulatedReadOnlyPointerControl(props) {
    return _react2.default.createElement(_ReadOnlyPointerControl2.default, _extends({
      ParseObject: ParseObject,
      loadOptions: props.actions[loadAction]
    }, props));
  }

  PopulatedReadOnlyPointerControl.propTypes = {
    actions: _propTypes2.default.objectOf(_propTypes2.default.func)
  };

  return PopulatedReadOnlyPointerControl;
}

function prepopulatedReadOnlyNumericSelectControl(choices, allowBlank) {
  return function PopulatedReadOnlyNumericSelectControl(props) {
    return _react2.default.createElement(_ReadOnlyNumericSelectControl2.default, _extends({
      allowBlank: allowBlank,
      collection: choices
    }, props));
  };
}

var fieldToReadOnlyComponent = function fieldToReadOnlyComponent(field, view) {
  return (field.EditComponent ? field.EditComponent : view === 'create' ? field.CreateComponent : field.UpdateComponent) || {
    boolean: prepopulatedReadOnlyBooleanControl(field.choices),
    checkboxes: prepopulatedCheckboxesControl(field.choices),
    enumNumeric: prepopulatedReadOnlyNumericSelectControl(field.choices, field.allowBlank),
    file: _ReadOnlyFileControl2.default,
    geopoint: _ReadOnlyGeoPointControl2.default,
    htmlFroala: _DangerousHtmlControl2.default,
    number: _ReadOnlyNumberControl2.default,
    password: _PasswordControl2.default,
    pointer: prepopulatedReadOnlyPointerControl(field.ParseObject, field.loadAction),
    string: _StaticTextControl2.default,
    text: _StaticTextControl2.default,
    textarea: _StaticTextControl2.default,
    staticDate: _ReadOnlyDateControl2.default,
    staticText: _StaticTextControl2.default
  }[field.type] || _StaticTextControl2.default;
};

exports.default = fieldToReadOnlyComponent;