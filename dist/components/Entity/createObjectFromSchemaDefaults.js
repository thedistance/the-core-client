'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _keys = require('ramda/src/keys');

var _keys2 = _interopRequireDefault(_keys);

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

var createObjectFromSchemaDefaults = function createObjectFromSchemaDefaults(schema) {
  return (0, _keys2.default)(schema).reduce(function (params, key) {
    return _extends({}, params, schema[key].defaultTo && _defineProperty({}, key, schema[key].defaultTo));
  }, {});
}; /**
    * @Author: benbriggs
    * @Date:   2018-04-19T16:00:32+01:00
    * @Email:  ben.briggs@thedistance.co.uk
    * @Last modified by:   benbriggs
    * @Last modified time: 2018-04-19T16:15:14+01:00
    * @Copyright: The Distance
    */

exports.default = createObjectFromSchemaDefaults;