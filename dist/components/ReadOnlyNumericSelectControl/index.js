'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _propTypes = require('prop-types');

var _propTypes2 = _interopRequireDefault(_propTypes);

var _rubix = require('@sketchpixy/rubix');

var _GenericControl = require('../GenericControl');

var _GenericControl2 = _interopRequireDefault(_GenericControl);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _objectWithoutProperties(obj, keys) { var target = {}; for (var i in obj) { if (keys.indexOf(i) >= 0) continue; if (!Object.prototype.hasOwnProperty.call(obj, i)) continue; target[i] = obj[i]; } return target; } /*
                                                                                                                                                                                                                              * Created Date: Mon, 16th Apr 2018, 16:26:53 pm
                                                                                                                                                                                                                              * Author: Harry Crank
                                                                                                                                                                                                                              * Email: harry.crank@thedistance.co.uk
                                                                                                                                                                                                                              * Copyright (c) 2018 The Distance
                                                                                                                                                                                                                              */

var ReadOnlyNumericSelectControl = function ReadOnlyNumericSelectControl(_ref) {
  var collection = _ref.collection,
      allowBlank = _ref.allowBlank,
      props = _objectWithoutProperties(_ref, ['collection', 'allowBlank']);

  var entry = collection.find(function (obj) {
    return parseInt(obj.value, 10) === props.value;
  });

  return _react2.default.createElement(
    _GenericControl2.default,
    props,
    _react2.default.createElement(
      _rubix.FormControl.Static,
      null,
      entry ? entry.label : '-'
    )
  );
};

ReadOnlyNumericSelectControl.propTypes = {
  allowBlank: _propTypes2.default.bool,
  collection: _propTypes2.default.array,
  value: _propTypes2.default.oneOfType([_propTypes2.default.number, _propTypes2.default.string])
};

exports.default = ReadOnlyNumericSelectControl;