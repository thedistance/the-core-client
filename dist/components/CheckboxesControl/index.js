'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _contains = require('ramda/src/contains');

var _contains2 = _interopRequireDefault(_contains);

var _reject = require('ramda/src/reject');

var _reject2 = _interopRequireDefault(_reject);

var _append = require('ramda/src/append');

var _append2 = _interopRequireDefault(_append);

var _any = require('ramda/src/any');

var _any2 = _interopRequireDefault(_any);

var _unless = require('ramda/src/unless');

var _unless2 = _interopRequireDefault(_unless);

var _always = require('ramda/src/always');

var _always2 = _interopRequireDefault(_always);

var _ifElse = require('ramda/src/ifElse');

var _ifElse2 = _interopRequireDefault(_ifElse);

var _equals = require('ramda/src/equals');

var _equals2 = _interopRequireDefault(_equals);

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _rubix = require('@sketchpixy/rubix');

var _propTypes = require('prop-types');

var _propTypes2 = _interopRequireDefault(_propTypes);

var _FormGroup = require('../FormGroup');

var _FormGroup2 = _interopRequireDefault(_FormGroup);

var _GenericControl2 = require('../GenericControl');

var _GenericControl3 = _interopRequireDefault(_GenericControl2);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; } /**
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                * @Author: benbriggs
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                * @Date:   2018-08-29T09:23:12+01:00
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                * @Email:  ben.briggs@thedistance.co.uk
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                * @Last modified by:   benbriggs
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                * @Last modified time: 2018-08-29T17:12:48+01:00
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                * @Copyright: The Distance
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                */

var CheckboxesControl = function (_GenericControl) {
  _inherits(CheckboxesControl, _GenericControl);

  function CheckboxesControl() {
    _classCallCheck(this, CheckboxesControl);

    return _possibleConstructorReturn(this, (CheckboxesControl.__proto__ || Object.getPrototypeOf(CheckboxesControl)).apply(this, arguments));
  }

  _createClass(CheckboxesControl, [{
    key: 'handleChange',
    value: function handleChange(_ref) {
      var target = _ref.target;
      var checked = target.checked,
          id = target.name;

      var stateValue = this.state.value;

      var eqId = (0, _equals2.default)(id);

      var value = (0, _ifElse2.default)((0, _always2.default)(checked), (0, _unless2.default)((0, _any2.default)(eqId), (0, _append2.default)(id)), (0, _reject2.default)(eqId))(stateValue);

      var valid = this.props.validator ? this.props.validator(value) : null;

      this.setState({
        value: value,
        valid: valid
      });

      this.props.onChange(value ? value : null, valid);
    }
  }, {
    key: 'render',
    value: function render() {
      var _this2 = this;

      var value = this.state.value;
      var collection = this.props.collection;

      return _react2.default.createElement(
        _FormGroup2.default,
        {
          validationState: this.validationState(),
          required: this.props.required
        },
        _react2.default.createElement(
          _rubix.ControlLabel,
          null,
          this.props.label
        ),
        collection.map(function (c) {
          return _react2.default.createElement(
            _rubix.Checkbox,
            {
              key: c.id,
              name: c.id,
              checked: (0, _contains2.default)(c.id, value),
              onChange: _this2.handleChange
            },
            c.name
          );
        }),
        _react2.default.createElement(_rubix.FormControlFeedback, null)
      );
    }
  }]);

  return CheckboxesControl;
}(_GenericControl3.default);

CheckboxesControl.defaultProps = {
  value: []
};

CheckboxesControl.propTypes = {
  collection: _propTypes2.default.array.isRequired,
  value: _propTypes2.default.array
};

exports.default = CheckboxesControl;