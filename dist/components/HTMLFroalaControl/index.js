'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _propTypes = require('prop-types');

var _propTypes2 = _interopRequireDefault(_propTypes);

var _rubix = require('@sketchpixy/rubix');

var _reactFroalaWysiwyg = require('react-froala-wysiwyg');

var _reactFroalaWysiwyg2 = _interopRequireDefault(_reactFroalaWysiwyg);

var _FormGroup = require('../FormGroup');

var _FormGroup2 = _interopRequireDefault(_FormGroup);

var _GenericControl2 = require('../GenericControl');

var _GenericControl3 = _interopRequireDefault(_GenericControl2);

require('froala-editor/js/froala_editor.pkgd.min.js');

require('froala-editor/css/froala_style.min.css');

require('froala-editor/css/froala_editor.pkgd.min.css');

require('font-awesome/css/font-awesome.css');

var _jquery = require('jquery');

var _jquery2 = _interopRequireDefault(_jquery);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; } /**
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                * @Author: Ben Briggs <benbriggs>
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                * @Date:   2017-11-27T16:18:54+00:00
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                * @Email:  ben.briggs@thedistance.co.uk
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                * @Last modified by:   benbriggs
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                * @Last modified time: 2018-09-19T09:31:05+01:00
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                * @Copyright: The Distance
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                */

window.$ = _jquery2.default;

var HtmlFroalaControl = function (_GenericControl) {
  _inherits(HtmlFroalaControl, _GenericControl);

  function HtmlFroalaControl(props) {
    _classCallCheck(this, HtmlFroalaControl);

    var _this = _possibleConstructorReturn(this, (HtmlFroalaControl.__proto__ || Object.getPrototypeOf(HtmlFroalaControl)).call(this, props));

    var config = {
      // https://www.froala.com/wysiwyg-editor/docs/options
      // All the options can be added here to configure the editor
      key: process.env.FROALA_KEY,
      placeholderText: 'Edit Your Content Here!',
      charCounterCount: false,
      imageUploadURL: process.env.UPLOAD_URL,
      fileUploadURL: process.env.UPLOAD_URL,
      // https://www.froala.com/wysiwyg-editor/docs/options#linkInsertButtons
      linkInsertButtons: [],
      // https://www.froala.com/wysiwyg-editor/docs/options#linkList
      linkList: [],
      requestHeaders: {
        'X-Parse-Application-Id': process.env.APP_ID
      },
      paragraphStyles: {
        'fr-text-gray': 'Gray',
        'fr-text-bordered': 'Bordered',
        'fr-text-spaced': 'Spaced',
        'fr-text-uppercase': 'Uppercase',
        highlightBox: 'Highlight box',
        quoteBox: 'Quote box',
        blockquote: 'Blockquote'
      },
      videoInsertButtons: ['videoBack', '|', 'videoByURL', 'videoEmbed'],
      // https://www.froala.com/wysiwyg-editor/docs/options#pluginsEnabled
      pluginsEnabled: ['align', 'charCounter', 'codeBeautifier', 'codeView', 'colors', 'draggable', 'embedly', 'emoticons',
      //'entities', - removing entities plugin may have security implications
      // Froala also entity encodes HTML tags separately though, so this seems pretty hard to exploit
      'file', 'fontFamily', 'fontSize', 'fullscreen', 'image', 'inlineStyle', 'lineBreaker', 'link', 'lists', 'paragraphFormat', 'paragraphStyle', 'quickInsert', 'save', 'table', 'url', 'video', 'wordPaste'],
      // https://www.froala.com/wysiwyg-editor/docs/options#toolbarButtons
      toolbarButtons: ['fullscreen', 'bold', 'italic', 'underline', 'strikeThrough', 'subscript', 'superscript', '|', 'color', 'paragraphStyle', '|', 'paragraphFormat', 'align', 'formatOL', 'formatUL', 'outdent', 'indent', '-', 'insertLink', 'insertImage', 'insertVideo', 'insertFile', 'embedly', 'insertTable', '|', 'emoticons', 'specialCharacters', 'insertHR', 'clearFormatting', '|', 'spellChecker', 'help', 'html', '|', 'undo', 'redo']
    };

    if (process.env.FROALA_TOOLBAR_OFFSET) {
      config.toolbarStickyOffset = process.env.FROALA_TOOLBAR_OFFSET;
    }

    _this.state = {
      config: config,
      text: _this.props.value
    };
    return _this;
  }

  _createClass(HtmlFroalaControl, [{
    key: 'handleChange',
    value: function handleChange(text) {
      var valid = this.props.validator ? this.props.validator(text) : null;

      this.setState({
        text: text,
        valid: valid
      });

      this.props.onChange(text, valid);
    }
  }, {
    key: 'render',
    value: function render() {
      return _react2.default.createElement(
        _FormGroup2.default,
        {
          validationState: this.validationState(),
          required: this.props.required
        },
        _react2.default.createElement(
          _rubix.ControlLabel,
          null,
          this.props.label
        ),
        _react2.default.createElement(_reactFroalaWysiwyg2.default, {
          tag: 'textarea',
          config: this.state.config,
          model: this.state.text,
          onModelChange: this.handleChange
        })
      );
    }
  }]);

  return HtmlFroalaControl;
}(_GenericControl3.default);

HtmlFroalaControl.propTypes = {
  value: _propTypes2.default.string,
  onChange: _propTypes2.default.func.isRequired,
  validator: _propTypes2.default.func
};

HtmlFroalaControl.defaultProps = {
  value: ''
};

exports.default = HtmlFroalaControl;