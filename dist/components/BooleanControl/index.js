'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _propTypes = require('prop-types');

var _propTypes2 = _interopRequireDefault(_propTypes);

var _SelectControl = require('../SelectControl');

var _SelectControl2 = _interopRequireDefault(_SelectControl);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _objectWithoutProperties(obj, keys) { var target = {}; for (var i in obj) { if (keys.indexOf(i) >= 0) continue; if (!Object.prototype.hasOwnProperty.call(obj, i)) continue; target[i] = obj[i]; } return target; } /**
                                                                                                                                                                                                                              * @Author: Ben Briggs <benbriggs>
                                                                                                                                                                                                                              * @Date:   2018-01-03T17:17:25+00:00
                                                                                                                                                                                                                              * @Email:  ben.briggs@thedistance.co.uk
                                                                                                                                                                                                                              * @Last modified by:   benbriggs
                                                                                                                                                                                                                              * @Last modified time: 2018-01-11T09:47:15+00:00
                                                                                                                                                                                                                              * @Copyright: The Distance
                                                                                                                                                                                                                              */

var BooleanControl = function BooleanControl(_ref) {
  var choices = _ref.choices,
      value = _ref.value,
      _onChange = _ref.onChange,
      rest = _objectWithoutProperties(_ref, ['choices', 'value', 'onChange']);

  return _react2.default.createElement(_SelectControl2.default, _extends({
    collection: choices
  }, rest, {
    onChange: function onChange(value, valid) {
      return _onChange(value === choices[0], valid);
    },
    value: choices[Number(!value)]
  }));
};

BooleanControl.propTypes = {
  choices: _propTypes2.default.array,
  onChange: _propTypes2.default.func,
  value: _propTypes2.default.bool
};

BooleanControl.defaultProps = {
  choices: ['true', 'false']
};

exports.default = BooleanControl;