'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _propTypes = require('prop-types');

var _propTypes2 = _interopRequireDefault(_propTypes);

var _rubix = require('@sketchpixy/rubix');

var _LinkButton = require('../LinkButton');

var _LinkButton2 = _interopRequireDefault(_LinkButton);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; } /**
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                * @Author: Ben Briggs <benbriggs>
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                * @Date:   2017-09-26T13:54:19+01:00
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                * @Email:  ben.briggs@thedistance.co.uk
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                * @Last modified by:   benbriggs
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                * @Last modified time: 2018-01-11T09:30:02+00:00
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                * @Copyright: The Distance
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                */

var PanelComponent = function (_React$Component) {
  _inherits(PanelComponent, _React$Component);

  function PanelComponent() {
    _classCallCheck(this, PanelComponent);

    return _possibleConstructorReturn(this, (PanelComponent.__proto__ || Object.getPrototypeOf(PanelComponent)).apply(this, arguments));
  }

  _createClass(PanelComponent, [{
    key: 'renderButton',
    value: function renderButton() {
      var _props = this.props,
          buttonTo = _props.buttonTo,
          buttonTitle = _props.buttonTitle;


      if (!buttonTo || !buttonTitle) {
        return null;
      }

      return _react2.default.createElement(
        _LinkButton2.default,
        { pathname: buttonTo, bsStyle: 'primary' },
        buttonTitle
      );
    }
  }, {
    key: 'render',
    value: function render() {
      var _props2 = this.props,
          className = _props2.className,
          children = _props2.children,
          title = _props2.title;

      var button = this.renderButton();

      var header = button ? _react2.default.createElement(
        _rubix.Row,
        null,
        _react2.default.createElement(
          _rubix.Col,
          { xs: 6, className: 'fg-white' },
          _react2.default.createElement(
            'h3',
            null,
            title
          )
        ),
        _react2.default.createElement(
          _rubix.Col,
          { xs: 6, className: 'fg-white', style: { textAlign: 'right' } },
          button
        )
      ) : _react2.default.createElement(
        _rubix.Row,
        null,
        _react2.default.createElement(
          _rubix.Col,
          { xs: 12, className: 'fg-white' },
          _react2.default.createElement(
            'h3',
            null,
            title
          )
        )
      );

      return _react2.default.createElement(
        _rubix.PanelContainer,
        null,
        _react2.default.createElement(
          _rubix.Panel,
          null,
          _react2.default.createElement(
            _rubix.PanelHeader,
            { className: className },
            _react2.default.createElement(
              _rubix.Grid,
              null,
              header
            )
          ),
          _react2.default.createElement(
            _rubix.PanelBody,
            null,
            children
          )
        )
      );
    }
  }]);

  return PanelComponent;
}(_react2.default.Component);

PanelComponent.propTypes = {
  buttonTo: _propTypes2.default.string,
  buttonTitle: _propTypes2.default.string,
  children: _propTypes2.default.any,
  className: _propTypes2.default.string,
  title: _propTypes2.default.string
};

PanelComponent.defaultProps = {
  className: 'bg-blue'
};

exports.default = PanelComponent;