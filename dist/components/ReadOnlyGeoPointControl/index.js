'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _propTypes = require('prop-types');

var _propTypes2 = _interopRequireDefault(_propTypes);

var _rubix = require('@sketchpixy/rubix');

var _FormGroup = require('../FormGroup');

var _FormGroup2 = _interopRequireDefault(_FormGroup);

var _GenericControl2 = require('../GenericControl');

var _GenericControl3 = _interopRequireDefault(_GenericControl2);

var _StaticTextControl = require('../StaticTextControl');

var _StaticTextControl2 = _interopRequireDefault(_StaticTextControl);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _objectWithoutProperties(obj, keys) { var target = {}; for (var i in obj) { if (keys.indexOf(i) >= 0) continue; if (!Object.prototype.hasOwnProperty.call(obj, i)) continue; target[i] = obj[i]; } return target; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; } /*
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                * Created Date: Tue, 17th Apr 2018, 15:18:57 pm
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                * Author: Harry Crank
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                * Email: harry.crank@thedistance.co.uk
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                * Copyright (c) 2018 The Distance
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                */

var ReadOnlyGeoPointControl = function (_GenericControl) {
  _inherits(ReadOnlyGeoPointControl, _GenericControl);

  function ReadOnlyGeoPointControl(props) {
    _classCallCheck(this, ReadOnlyGeoPointControl);

    var _this = _possibleConstructorReturn(this, (ReadOnlyGeoPointControl.__proto__ || Object.getPrototypeOf(ReadOnlyGeoPointControl)).call(this, props));

    var value = props.value;

    if (value) {
      _this.state = {
        latitude: value.latitude,
        longitude: value.longitude,
        valid: null
      };
    } else {
      _this.state = { latitude: 0, longitude: 0, valid: null };
    }
    return _this;
  }

  _createClass(ReadOnlyGeoPointControl, [{
    key: 'render',
    value: function render() {
      var _props = this.props,
          label = _props.label,
          required = _props.required,
          rest = _objectWithoutProperties(_props, ['label', 'required']);

      var _state = this.state,
          latitude = _state.latitude,
          longitude = _state.longitude;

      return _react2.default.createElement(
        _FormGroup2.default,
        { validationState: this.validationState(), required: required },
        _react2.default.createElement(
          _rubix.ControlLabel,
          null,
          label
        ),
        _react2.default.createElement(
          _rubix.Grid,
          { collapse: true },
          _react2.default.createElement(
            _rubix.Row,
            null,
            _react2.default.createElement(
              _rubix.Col,
              { sm: 5 },
              _react2.default.createElement(_StaticTextControl2.default, _extends({}, rest, {
                label: 'Latitude',
                value: latitude
              })),
              _react2.default.createElement(_rubix.FormControlFeedback, null)
            ),
            _react2.default.createElement(
              _rubix.Col,
              { sm: 5, smOffset: 1 },
              _react2.default.createElement(_StaticTextControl2.default, _extends({}, rest, {
                label: 'Longitude',
                value: longitude
              })),
              _react2.default.createElement(_rubix.FormControlFeedback, null)
            )
          )
        )
      );
    }
  }]);

  return ReadOnlyGeoPointControl;
}(_GenericControl3.default);

ReadOnlyGeoPointControl.propTypes = {
  value: _propTypes2.default.object,
  label: _propTypes2.default.string.isRequired
};

exports.default = ReadOnlyGeoPointControl;