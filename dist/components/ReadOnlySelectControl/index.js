'use strict';

Object.defineProperty(exports, '__esModule', {
  value: true,
});

var _is = require('ramda/src/is');

var _is2 = _interopRequireDefault(_is);

var _createClass = (function() {
  function defineProperties(target, props) {
    for (var i = 0; i < props.length; i++) {
      var descriptor = props[i];
      descriptor.enumerable = descriptor.enumerable || false;
      descriptor.configurable = true;
      if ('value' in descriptor) descriptor.writable = true;
      Object.defineProperty(target, descriptor.key, descriptor);
    }
  }
  return function(Constructor, protoProps, staticProps) {
    if (protoProps) defineProperties(Constructor.prototype, protoProps);
    if (staticProps) defineProperties(Constructor, staticProps);
    return Constructor;
  };
})();

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _propTypes = require('prop-types');

var _propTypes2 = _interopRequireDefault(_propTypes);

var _rubix = require('@sketchpixy/rubix');

var _FormGroup = require('../FormGroup');

var _FormGroup2 = _interopRequireDefault(_FormGroup);

var _GenericControl2 = require('../GenericControl');

var _GenericControl3 = _interopRequireDefault(_GenericControl2);

function _interopRequireDefault(obj) {
  return obj && obj.__esModule ? obj : { default: obj };
}

function _classCallCheck(instance, Constructor) {
  if (!(instance instanceof Constructor)) {
    throw new TypeError('Cannot call a class as a function');
  }
}

function _possibleConstructorReturn(self, call) {
  if (!self) {
    throw new ReferenceError(
      "this hasn't been initialised - super() hasn't been called"
    );
  }
  return call && (typeof call === 'object' || typeof call === 'function')
    ? call
    : self;
}

function _inherits(subClass, superClass) {
  if (typeof superClass !== 'function' && superClass !== null) {
    throw new TypeError(
      'Super expression must either be null or a function, not ' +
        typeof superClass
    );
  }
  subClass.prototype = Object.create(superClass && superClass.prototype, {
    constructor: {
      value: subClass,
      enumerable: false,
      writable: true,
      configurable: true,
    },
  });
  if (superClass)
    Object.setPrototypeOf
      ? Object.setPrototypeOf(subClass, superClass)
      : (subClass.__proto__ = superClass);
} /*
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                * Created Date: Mon, 16th Apr 2018, 14:33:39 pm
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                * Author: Harry Crank
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                * Email: harry.crank@thedistance.co.uk
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                * Copyright (c) 2018 The Distance
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                */

var Option = function Option(item) {
  return (0, _is2.default)(Object, item)
    ? _react2.default.createElement(
        'option',
        { key: item.value, value: item.value },
        item.label
      )
    : _react2.default.createElement('option', { key: item, value: item }, item);
};

var ReadOnlySelectControl = (function(_GenericControl) {
  _inherits(ReadOnlySelectControl, _GenericControl);

  function ReadOnlySelectControl() {
    _classCallCheck(this, ReadOnlySelectControl);

    return _possibleConstructorReturn(
      this,
      (
        ReadOnlySelectControl.__proto__ ||
        Object.getPrototypeOf(ReadOnlySelectControl)
      ).apply(this, arguments)
    );
  }

  _createClass(ReadOnlySelectControl, [
    {
      key: 'handleChange',
      value: function handleChange(_ref) {
        var target = _ref.target;
        var value = target.value;

        var valid = this.props.validator ? this.props.validator(value) : null;

        this.setState({
          value: value,
          valid: valid,
        });

        this.props.onChange(value ? value : null, valid);
      },
    },
    {
      key: 'render',
      value: function render() {
        var _props = this.props,
          allowBlank = _props.allowBlank,
          collection = _props.collection;
        var value = this.state.value;

        return _react2.default.createElement(
          _FormGroup2.default,
          {
            validationState: this.validationState(),
            required: this.props.required,
          },
          _react2.default.createElement(
            _rubix.ControlLabel,
            null,
            this.props.label
          ),
          _react2.default.createElement(
            _rubix.FormControl.Static,
            {
              componentClass: 'select',
              value: value,
              onChange: this.handleChange.bind(this),
            },
            value
          ),
          _react2.default.createElement(_rubix.FormControlFeedback, null)
        );
      },
    },
  ]);

  return ReadOnlySelectControl;
})(_GenericControl3.default);

ReadOnlySelectControl.propTypes = {
  allowBlank: _propTypes2.default.bool,
  collection: _propTypes2.default.array,
  value: _propTypes2.default.oneOfType([
    _propTypes2.default.number,
    _propTypes2.default.string,
  ]),
};

exports.default = ReadOnlySelectControl;
