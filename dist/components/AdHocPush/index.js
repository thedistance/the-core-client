'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.mapDispatchToProps = exports.mapStateToProps = exports.PushMessage = undefined;

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _propTypes = require('prop-types');

var _propTypes2 = _interopRequireDefault(_propTypes);

var _rubix = require('@sketchpixy/rubix');

var _reactRedux = require('react-redux');

var _Alert = require('../Alert');

var _Alert2 = _interopRequireDefault(_Alert);

var _Panel = require('../Panel');

var _Panel2 = _interopRequireDefault(_Panel);

var _TextControl = require('../TextControl');

var _TextControl2 = _interopRequireDefault(_TextControl);

var _actions = require('./actions');

var actions = _interopRequireWildcard(_actions);

var _reducer = require('./reducer');

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj.default = obj; return newObj; } }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; } /**
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                * @Author: Ben Briggs <benbriggs>
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                * @Date:   2017-10-19T10:14:09+01:00
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                * @Email:  ben.briggs@thedistance.co.uk
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                * @Last modified by:   benbriggs
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                * @Last modified time: 2018-01-11T12:04:34+00:00
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                * @Copyright: The Distance
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                */

var PushMessage = exports.PushMessage = function (_React$Component) {
  _inherits(PushMessage, _React$Component);

  function PushMessage(props) {
    _classCallCheck(this, PushMessage);

    var _this = _possibleConstructorReturn(this, (PushMessage.__proto__ || Object.getPrototypeOf(PushMessage)).call(this, props));

    _this.state = {
      message: ''
    };
    return _this;
  }

  _createClass(PushMessage, [{
    key: 'handleChange',
    value: function handleChange(message) {
      this.setState({ message: message });
    }
  }, {
    key: 'sendMessage',
    value: function sendMessage() {
      this.props.actions.send(this.state.message);
      this.setState({ message: '' });
    }
  }, {
    key: 'renderMessage',
    value: function renderMessage() {
      var _props = this.props,
          error = _props.error,
          sent = _props.sent;

      if (error) {
        return _react2.default.createElement(
          _Alert2.default,
          { danger: true },
          error.message
        );
      }
      if (sent) {
        return _react2.default.createElement(
          _Alert2.default,
          { success: true },
          'Your message has been sent!'
        );
      }
      return null;
    }
  }, {
    key: 'render',
    value: function render() {
      return _react2.default.createElement(
        'div',
        null,
        this.renderMessage(),
        _react2.default.createElement(
          _Panel2.default,
          { title: 'Send a Push Message' },
          _react2.default.createElement(
            _rubix.Grid,
            null,
            _react2.default.createElement(
              _rubix.Row,
              null,
              _react2.default.createElement(
                _rubix.Col,
                { xs: 12 },
                _react2.default.createElement(
                  _rubix.Form,
                  { autoComplete: false, onSubmit: function onSubmit(e) {
                      return e.preventDefault();
                    } },
                  _react2.default.createElement(_TextControl2.default, {
                    label: 'Message',
                    onChange: this.handleChange.bind(this),
                    value: this.state.message
                  }),
                  _react2.default.createElement(
                    _rubix.ButtonToolbar,
                    { style: { marginBottom: '1em' } },
                    _react2.default.createElement(
                      _rubix.Button,
                      { onClick: this.sendMessage.bind(this) },
                      'Send'
                    )
                  )
                )
              )
            )
          )
        )
      );
    }
  }]);

  return PushMessage;
}(_react2.default.Component);

PushMessage.propTypes = {
  actions: _propTypes2.default.objectOf(_propTypes2.default.func).isRequired,
  error: _propTypes2.default.any,
  sent: _propTypes2.default.any
};

var mapStateToProps = exports.mapStateToProps = function mapStateToProps(state) {
  return {
    error: (0, _reducer.selectError)(state),
    sent: (0, _reducer.selectSent)(state)
  };
};

var mapDispatchToProps = exports.mapDispatchToProps = function mapDispatchToProps(dispatch) {
  return {
    actions: {
      send: function send(message) {
        return dispatch(actions.sendPushMessage({ message: message }));
      }
    }
  };
};

exports.default = (0, _reactRedux.connect)(mapStateToProps, mapDispatchToProps)(PushMessage);