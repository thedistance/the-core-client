'use strict';

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _propTypes = require('prop-types');

var _propTypes2 = _interopRequireDefault(_propTypes);

var _reactReduxSpinner = require('react-redux-spinner');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var LoadProgress = function LoadProgress(_ref) {
  var children = _ref.children;
  return _react2.default.createElement(
    'div',
    null,
    _react2.default.createElement(_reactReduxSpinner.Spinner, null),
    children
  );
}; /**
    * @Author: Ben Briggs <benbriggs>
    * @Date:   2018-01-30T09:49:41+00:00
    * @Email:  ben.briggs@thedistance.co.uk
    * @Last modified by:   benbriggs
    * @Last modified time: 2018-02-01T09:43:41+00:00
    * @Copyright: The Distance
    */

LoadProgress.propTypes = {
  children: _propTypes2.default.node
};

// We are intentionally using module.exports here as export.default fails
// with an invariant violation "The root route must render a single element".

module.exports = LoadProgress;