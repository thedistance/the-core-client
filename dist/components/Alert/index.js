'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _propTypes = require('prop-types');

var _propTypes2 = _interopRequireDefault(_propTypes);

var _rubix = require('@sketchpixy/rubix');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _objectWithoutProperties(obj, keys) { var target = {}; for (var i in obj) { if (keys.indexOf(i) >= 0) continue; if (!Object.prototype.hasOwnProperty.call(obj, i)) continue; target[i] = obj[i]; } return target; } /**
                                                                                                                                                                                                                              * @Author: Ben Briggs <benbriggs>
                                                                                                                                                                                                                              * @Date:   2017-09-26T13:54:19+01:00
                                                                                                                                                                                                                              * @Email:  ben.briggs@thedistance.co.uk
                                                                                                                                                                                                                              * @Last modified by:   benbriggs
                                                                                                                                                                                                                              * @Last modified time: 2018-01-11T09:34:11+00:00
                                                                                                                                                                                                                              * @Copyright: The Distance
                                                                                                                                                                                                                              */

var AlertComponent = function AlertComponent(_ref) {
  var children = _ref.children,
      props = _objectWithoutProperties(_ref, ['children']);

  return _react2.default.createElement(
    _rubix.Alert,
    _extends({ dismissible: true }, props),
    children
  );
};

AlertComponent.propTypes = {
  children: _propTypes2.default.any
};

exports.default = AlertComponent;