'use strict';

Object.defineProperty(exports, '__esModule', {
  value: true,
});

var _createClass = (function() {
  function defineProperties(target, props) {
    for (var i = 0; i < props.length; i++) {
      var descriptor = props[i];
      descriptor.enumerable = descriptor.enumerable || false;
      descriptor.configurable = true;
      if ('value' in descriptor) descriptor.writable = true;
      Object.defineProperty(target, descriptor.key, descriptor);
    }
  }
  return function(Constructor, protoProps, staticProps) {
    if (protoProps) defineProperties(Constructor.prototype, protoProps);
    if (staticProps) defineProperties(Constructor, staticProps);
    return Constructor;
  };
})();

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _parse = require('parse');

var _parse2 = _interopRequireDefault(_parse);

var _rubix = require('@sketchpixy/rubix');

var _FormGroup = require('../FormGroup');

var _FormGroup2 = _interopRequireDefault(_FormGroup);

var _GenericControl2 = require('../GenericControl');

var _GenericControl3 = _interopRequireDefault(_GenericControl2);

function _interopRequireDefault(obj) {
  return obj && obj.__esModule ? obj : { default: obj };
}

function _classCallCheck(instance, Constructor) {
  if (!(instance instanceof Constructor)) {
    throw new TypeError('Cannot call a class as a function');
  }
}

function _possibleConstructorReturn(self, call) {
  if (!self) {
    throw new ReferenceError(
      "this hasn't been initialised - super() hasn't been called"
    );
  }
  return call && (typeof call === 'object' || typeof call === 'function')
    ? call
    : self;
}

function _inherits(subClass, superClass) {
  if (typeof superClass !== 'function' && superClass !== null) {
    throw new TypeError(
      'Super expression must either be null or a function, not ' +
        typeof superClass
    );
  }
  subClass.prototype = Object.create(superClass && superClass.prototype, {
    constructor: {
      value: subClass,
      enumerable: false,
      writable: true,
      configurable: true,
    },
  });
  if (superClass)
    Object.setPrototypeOf
      ? Object.setPrototypeOf(subClass, superClass)
      : (subClass.__proto__ = superClass);
} /**
 * @Author: Ben Briggs <benbriggs>
 * @Date:   2017-12-18T15:49:01+00:00
 * @Email:  ben.briggs@thedistance.co.uk
 * @Last modified by:   benbriggs
 * @Last modified time: 2018-01-11T09:27:55+00:00
 * @Copyright: The Distance
 */

var divStyle = {
  marginBottom: '10px',
};

var FileControl = (function(_GenericControl) {
  _inherits(FileControl, _GenericControl);

  function FileControl() {
    _classCallCheck(this, FileControl);

    return _possibleConstructorReturn(
      this,
      (FileControl.__proto__ || Object.getPrototypeOf(FileControl)).apply(
        this,
        arguments
      )
    );
  }

  _createClass(FileControl, [
    {
      key: 'handleChange',
      value: function handleChange(_ref) {
        var target = _ref.target;

        if (target.files.length > 0) {
          var file = target.files[0];

          var value = new _parse2.default.File(file.name, file);
          this.setState({ value: value });
          this.props.onChange(value, true);
        }
      },
    },
    {
      key: 'deleteFile',
      value: function deleteFile() {
        var value = null;
        this.setState({ value: value });
        this.props.onChange(value, true);
      },
    },
    {
      key: 'render',
      value: function render() {
        return _react2.default.createElement(
          'div',
          null,
          _react2.default.createElement(
            _FormGroup2.default,
            {
              validationState: this.validationState(),
              required: this.props.required,
            },
            _react2.default.createElement(
              _rubix.ControlLabel,
              null,
              this.props.label
            ),
            _react2.default.createElement(_rubix.FormControl, {
              type: 'file',
              onChange: this.handleChange,
            })
          ),
          this.state.value &&
            _react2.default.createElement(
              'div',
              { style: divStyle },
              _react2.default.createElement(
                'a',
                { href: this.state.value.url(), target: '_blank' },
                _react2.default.createElement(
                  _rubix.Button,
                  { bsStyle: 'info', outlined: true },
                  'Open ',
                  this.state.value.name()
                )
              ),
              ' ',
              _react2.default.createElement(
                _rubix.Button,
                {
                  bsStyle: 'danger',
                  outlined: true,
                  onClick: this.deleteFile.bind(this),
                },
                'Delete file'
              )
            )
        );
      },
    },
  ]);

  return FileControl;
})(_GenericControl3.default);

exports.default = FileControl;
