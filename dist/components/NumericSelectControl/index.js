'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _propTypes = require('prop-types');

var _propTypes2 = _interopRequireDefault(_propTypes);

var _SelectControl = require('../SelectControl');

var _SelectControl2 = _interopRequireDefault(_SelectControl);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _objectWithoutProperties(obj, keys) { var target = {}; for (var i in obj) { if (keys.indexOf(i) >= 0) continue; if (!Object.prototype.hasOwnProperty.call(obj, i)) continue; target[i] = obj[i]; } return target; } /**
                                                                                                                                                                                                                              * @Author: Ben Briggs <benbriggs>
                                                                                                                                                                                                                              * @Date:   2018-02-20T11:32:30+00:00
                                                                                                                                                                                                                              * @Email:  ben.briggs@thedistance.co.uk
                                                                                                                                                                                                                              * @Last modified by:   benbriggs
                                                                                                                                                                                                                              * @Last modified time: 2018-02-20T11:40:44+00:00
                                                                                                                                                                                                                              * @Copyright: The Distance
                                                                                                                                                                                                                              */

var NumericSelectControl = function NumericSelectControl(_ref) {
  var _onChange = _ref.onChange,
      rest = _objectWithoutProperties(_ref, ['onChange']);

  return _react2.default.createElement(_SelectControl2.default, _extends({}, rest, {
    onChange: function onChange(value, valid) {
      return _onChange(parseInt(value, 10), valid);
    }
  }));
};

NumericSelectControl.propTypes = {
  onChange: _propTypes2.default.func
};

exports.default = NumericSelectControl;