'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _propTypes = require('prop-types');

var _propTypes2 = _interopRequireDefault(_propTypes);

var _rubix = require('@sketchpixy/rubix');

var _Panel = require('../Panel');

var _Panel2 = _interopRequireDefault(_Panel);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; } /**
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                * @Author: benbriggs
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                * @Date:   2018-04-23T13:05:45+01:00
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                * @Email:  ben.briggs@thedistance.co.uk
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                * @Last modified by:   benbriggs
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                * @Last modified time: 2018-04-23T13:13:42+01:00
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                * @Copyright: The Distance
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                */

var LoginForm = function (_React$Component) {
  _inherits(LoginForm, _React$Component);

  function LoginForm(props) {
    _classCallCheck(this, LoginForm);

    var _this = _possibleConstructorReturn(this, (LoginForm.__proto__ || Object.getPrototypeOf(LoginForm)).call(this, props));

    _this.state = {
      username: props.username,
      password: ''
    };
    return _this;
  }

  _createClass(LoginForm, [{
    key: 'componentDidMount',
    value: function componentDidMount() {
      var el = document.querySelector('html');
      if (el.classList) el.classList.add('authentication');else el.className += ' authentication';
    }
  }, {
    key: 'componentWillUnmount',
    value: function componentWillUnmount() {
      var el = document.querySelector('html');
      if (el.classList) el.classList.remove('authentication');else el.className = el.className.replace(new RegExp('(^|\\b)authentication(\\b|$)', 'gi'), ' ');
    }
  }, {
    key: 'handleUsername',
    value: function handleUsername(_ref) {
      var target = _ref.target;

      this.setState({ username: target.value });
    }
  }, {
    key: 'handlePassword',
    value: function handlePassword(_ref2) {
      var target = _ref2.target;

      this.setState({ password: target.value });
    }
  }, {
    key: 'handleSubmit',
    value: function handleSubmit(_ref3) {
      var key = _ref3.key;

      if (key && key === 'Enter' || !key) {
        this.props.onSubmit(this.state.username, this.state.password);
      }
    }
  }, {
    key: 'render',
    value: function render() {
      return _react2.default.createElement(
        'div',
        { id: 'auth-container', className: 'login' },
        _react2.default.createElement(
          'div',
          { id: 'auth-row' },
          _react2.default.createElement(
            'div',
            { id: 'auth-cell' },
            _react2.default.createElement(
              _rubix.Grid,
              null,
              _react2.default.createElement(
                _rubix.Row,
                null,
                _react2.default.createElement(
                  _rubix.Col,
                  {
                    sm: 4,
                    smOffset: 4,
                    xs: 10,
                    xsOffset: 1,
                    collapseLeft: true,
                    collapseRight: true
                  },
                  _react2.default.createElement(
                    _Panel2.default,
                    { title: this.props.titleMessage },
                    _react2.default.createElement(
                      'div',
                      null,
                      this.props.flash && _react2.default.createElement(
                        _rubix.Alert,
                        { bsStyle: 'warning' },
                        this.props.flash
                      ),
                      _react2.default.createElement(
                        'div',
                        {
                          style: {
                            padding: 25,
                            paddingTop: 0,
                            paddingBottom: 0,
                            margin: 'auto',
                            marginBottom: 25,
                            marginTop: 25
                          }
                        },
                        _react2.default.createElement(
                          _rubix.Form,
                          null,
                          _react2.default.createElement(
                            _rubix.FormGroup,
                            { controlId: 'username' },
                            _react2.default.createElement(
                              _rubix.InputGroup,
                              { bsSize: 'large' },
                              _react2.default.createElement(
                                _rubix.InputGroup.Addon,
                                null,
                                _react2.default.createElement(_rubix.Icon, { glyph: 'icon-fontello-mail' })
                              ),
                              _react2.default.createElement(_rubix.FormControl, {
                                autoFocus: true,
                                type: 'text',
                                value: this.state.username,
                                onChange: this.handleUsername.bind(this),
                                className: 'border-focus-blue',
                                placeholder: 'username'
                              })
                            )
                          ),
                          _react2.default.createElement(
                            _rubix.FormGroup,
                            { controlId: 'password' },
                            _react2.default.createElement(
                              _rubix.InputGroup,
                              { bsSize: 'large' },
                              _react2.default.createElement(
                                _rubix.InputGroup.Addon,
                                null,
                                _react2.default.createElement(_rubix.Icon, { glyph: 'icon-fontello-key' })
                              ),
                              _react2.default.createElement(_rubix.FormControl, {
                                type: 'password',
                                value: this.state.password,
                                onChange: this.handlePassword.bind(this),
                                onKeyPress: this.handleSubmit.bind(this),
                                className: 'border-focus-blue',
                                placeholder: 'password'
                              })
                            )
                          ),
                          _react2.default.createElement(
                            _rubix.FormGroup,
                            null,
                            _react2.default.createElement(
                              _rubix.Grid,
                              null,
                              _react2.default.createElement(
                                _rubix.Row,
                                null,
                                _react2.default.createElement(
                                  _rubix.Col,
                                  {
                                    xs: 12,
                                    collapseLeft: true,
                                    collapseRight: true,
                                    className: 'text-right'
                                  },
                                  _react2.default.createElement(
                                    _rubix.Button,
                                    {
                                      outlined: true,
                                      lg: true,
                                      bsStyle: 'blue',
                                      onClick: this.handleSubmit.bind(this)
                                    },
                                    'Login'
                                  )
                                )
                              )
                            )
                          )
                        )
                      )
                    )
                  )
                )
              )
            )
          )
        )
      );
    }
  }]);

  return LoginForm;
}(_react2.default.Component);

LoginForm.propTypes = {
  onSubmit: _propTypes2.default.func.isRequired,
  flash: _propTypes2.default.string,
  username: _propTypes2.default.string,
  titleMessage: _propTypes2.default.string
};

LoginForm.defaultProps = {
  username: '',
  titleMessage: 'Sign in to The Core v3'
};

exports.default = LoginForm;