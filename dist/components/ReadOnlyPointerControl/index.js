'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _propTypes = require('prop-types');

var _propTypes2 = _interopRequireDefault(_propTypes);

var _reactSelect = require('react-select');

var _rubix = require('@sketchpixy/rubix');

var _FormGroup = require('../FormGroup');

var _FormGroup2 = _interopRequireDefault(_FormGroup);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; } /*
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                * Created Date: Tue, 17th Apr 2018, 13:27:02 pm
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                * Author: Harry Crank
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                * Email: harry.crank@thedistance.co.uk
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                * Copyright (c) 2018 The Distance
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                */

var ReadOnlyPointerControl = function (_React$Component) {
  _inherits(ReadOnlyPointerControl, _React$Component);

  function ReadOnlyPointerControl(props) {
    _classCallCheck(this, ReadOnlyPointerControl);

    var _this = _possibleConstructorReturn(this, (ReadOnlyPointerControl.__proto__ || Object.getPrototypeOf(ReadOnlyPointerControl)).call(this, props));

    _this.state = {
      displayValue: props.value
    };
    return _this;
  }

  _createClass(ReadOnlyPointerControl, [{
    key: 'render',
    value: function render() {
      var displayValue = this.state.displayValue;
      var _props = this.props,
          label = _props.label,
          loadOptions = _props.loadOptions;

      return _react2.default.createElement(
        'div',
        null,
        _react2.default.createElement(
          _FormGroup2.default,
          null,
          _react2.default.createElement(
            _rubix.ControlLabel,
            null,
            label
          ),
          _react2.default.createElement(_reactSelect.Async, {
            disabled: true,
            value: displayValue,
            loadOptions: loadOptions
          })
        )
      );
    }
  }]);

  return ReadOnlyPointerControl;
}(_react2.default.Component);

ReadOnlyPointerControl.propTypes = {
  value: _propTypes2.default.oneOfType([_propTypes2.default.number, _propTypes2.default.string]),
  label: _propTypes2.default.string.isRequired,
  loadOptions: _propTypes2.default.func.isRequired
};

exports.default = ReadOnlyPointerControl;