'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _propTypes = require('prop-types');

var _propTypes2 = _interopRequireDefault(_propTypes);

var _rubix = require('@sketchpixy/rubix');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var Footer = function Footer(_ref) {
  var version = _ref.version;
  return _react2.default.createElement(
    'div',
    { id: 'footer-container' },
    _react2.default.createElement(
      _rubix.Grid,
      { id: 'footer', className: 'text-center' },
      _react2.default.createElement(
        _rubix.Row,
        null,
        _react2.default.createElement(
          _rubix.Col,
          { xs: 12 },
          _react2.default.createElement(
            'p',
            null,
            '\xA9 2018 The Distance - ',
            version
          )
        )
      )
    )
  );
}; /**
    * @Author: Ben Briggs <benbriggs>
    * @Date:   2017-12-19T15:05:58+00:00
    * @Email:  ben.briggs@thedistance.co.uk
    * @Last modified by:   benbriggs
    * @Last modified time: 2018-01-11T09:33:39+00:00
    * @Copyright: The Distance
    */

Footer.propTypes = {
  version: _propTypes2.default.string
};

Footer.defaultProps = {
  version: 'v1.0.0'
};

exports.default = Footer;