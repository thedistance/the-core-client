'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.mapDispatchToProps = exports.mapStateToProps = exports.PushMessages = undefined;

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _reactTable = require('react-table');

var _reactTable2 = _interopRequireDefault(_reactTable);

var _reactRedux = require('react-redux');

var _rubix = require('@sketchpixy/rubix');

var _propTypes = require('prop-types');

var _propTypes2 = _interopRequireDefault(_propTypes);

var _Panel = require('../Panel');

var _Panel2 = _interopRequireDefault(_Panel);

var _Timeago = require('../Timeago');

var _Timeago2 = _interopRequireDefault(_Timeago);

var _actions = require('./actions');

var actions = _interopRequireWildcard(_actions);

var _reducer = require('./reducer');

require('react-table/react-table.css');

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj.default = obj; return newObj; } }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; } /**
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                * @Author: Ben Briggs <benbriggs>
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                * @Date:   2018-01-10T09:35:09+00:00
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                * @Email:  ben.briggs@thedistance.co.uk
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                * @Last modified by:   benbriggs
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                * @Last modified time: 2018-03-19T09:24:11+00:00
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                * @Copyright: The Distance
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                */

var PushMessage = function PushMessage(_ref) {
  var value = _ref.value;
  return _react2.default.createElement(
    'span',
    null,
    JSON.parse(value).alert
  );
};

PushMessage.propTypes = {
  value: _propTypes2.default.any
};

var PushMessages = exports.PushMessages = function (_React$Component) {
  _inherits(PushMessages, _React$Component);

  function PushMessages(props) {
    _classCallCheck(this, PushMessages);

    var _this = _possibleConstructorReturn(this, (PushMessages.__proto__ || Object.getPrototypeOf(PushMessages)).call(this, props));

    _this.state = {
      loading: false
    };
    return _this;
  }

  _createClass(PushMessages, [{
    key: 'fetchData',
    value: function fetchData(tableState) {
      var _this2 = this;

      this.setState({ loading: true });

      this.props.actions.load(tableState).then(function () {
        _this2.setState({
          loading: false
        });
      });
    }
  }, {
    key: 'componentWillUnmount',
    value: function componentWillUnmount() {
      this.props.actions.unload();
    }
  }, {
    key: 'render',
    value: function render() {
      var _props = this.props,
          data = _props.data,
          pages = _props.pages;

      return _react2.default.createElement(
        _Panel2.default,
        { title: 'Recent Messages' },
        _react2.default.createElement(
          _rubix.Grid,
          null,
          _react2.default.createElement(
            _rubix.Row,
            null,
            _react2.default.createElement(
              _rubix.Col,
              { xs: 12 },
              _react2.default.createElement(_reactTable2.default, {
                data: data,
                loading: this.state.loading,
                pages: pages,
                onFetchData: this.fetchData.bind(this),
                columns: [{
                  Header: 'Message',
                  accessor: function accessor(obj) {
                    return obj.get('payload');
                  },
                  Cell: PushMessage,
                  id: 'payload'
                }, {
                  Header: 'Sent',
                  accessor: function accessor(obj) {
                    return obj.get('numSent');
                  },
                  id: 'numSent'
                }, {
                  Header: 'Status',
                  accessor: function accessor(obj) {
                    return obj.get('status');
                  },
                  id: 'status'
                }, {
                  Header: 'When',
                  accessor: function accessor(obj) {
                    return obj.get('createdAt');
                  },
                  Cell: _Timeago2.default,
                  id: 'createdAt'
                }],
                manual: true,
                filterable: true,
                style: { marginTop: '10px', marginBottom: '40px' }
              })
            )
          )
        )
      );
    }
  }]);

  return PushMessages;
}(_react2.default.Component);

PushMessages.propTypes = {
  actions: _propTypes2.default.objectOf(_propTypes2.default.func).isRequired,
  data: _propTypes2.default.array,
  pages: _propTypes2.default.number
};

var mapStateToProps = exports.mapStateToProps = function mapStateToProps(state) {
  return {
    data: (0, _reducer.selectCollection)(state),
    pages: (0, _reducer.selectPages)(state)
  };
};

var mapDispatchToProps = exports.mapDispatchToProps = function mapDispatchToProps(dispatch) {
  return {
    actions: {
      send: function send(params) {
        return dispatch(actions.sendPushMessage(params));
      },
      load: function load(tableState) {
        return dispatch(actions.loadPushMessageCollection(tableState));
      },
      unload: function unload() {
        return dispatch(actions.unloadPushMessage());
      }
    }
  };
};

exports.default = (0, _reactRedux.connect)(mapStateToProps, mapDispatchToProps)(PushMessages);