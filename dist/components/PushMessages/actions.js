'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.unloadPushMessage = exports.sendPushMessage = exports.loadPushMessageCollection = exports.unloadPushMessageAction = exports.sendPushMessageFailedAction = exports.sentPushMessageAction = exports.sendingPushMessageAction = exports.loadedPushMessageAction = exports.loadingPushMessageAction = exports.unloadAll = exports.sendPushMessageFailed = exports.sentPushMessage = exports.sendingPushMessage = exports.loadedPushMessage = exports.loadingPushMessage = undefined;

var _parse = require('../../selectors/parse');

var _parse2 = _interopRequireDefault(_parse);

var _asyncActions = require('../../util/asyncActions');

var asyncActions = _interopRequireWildcard(_asyncActions);

var _createAction = require('../../util/createAction');

var _createAction2 = _interopRequireDefault(_createAction);

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj.default = obj; return newObj; } }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var loadingPushMessage = exports.loadingPushMessage = 'pushMessage:collection:loading'; /**
                                                                                         * @Author: Ben Briggs <benbriggs>
                                                                                         * @Date:   2018-01-10T13:17:24+00:00
                                                                                         * @Email:  ben.briggs@thedistance.co.uk
                                                                                         * @Last modified by:   benbriggs
                                                                                         * @Last modified time: 2018-01-11T14:37:33+00:00
                                                                                         * @Copyright: The Distance
                                                                                         */

var loadedPushMessage = exports.loadedPushMessage = 'pushMessage:collection:loaded';
var sendingPushMessage = exports.sendingPushMessage = 'pushMessage:sending';
var sentPushMessage = exports.sentPushMessage = 'pushMessage:sent';
var sendPushMessageFailed = exports.sendPushMessageFailed = 'pushMessage:send:failed';
var unloadAll = exports.unloadAll = 'pushMessage:unload';

var loadingPushMessageAction = exports.loadingPushMessageAction = asyncActions.createStart({
  type: loadingPushMessage
});
var loadedPushMessageAction = exports.loadedPushMessageAction = asyncActions.createEnd({
  type: loadedPushMessage
});
var sendingPushMessageAction = exports.sendingPushMessageAction = asyncActions.createStart({
  type: sendingPushMessage
});
var sentPushMessageAction = exports.sentPushMessageAction = asyncActions.createEnd({
  type: sentPushMessage
});
var sendPushMessageFailedAction = exports.sendPushMessageFailedAction = asyncActions.createError({
  type: sendPushMessageFailed
});
var unloadPushMessageAction = exports.unloadPushMessageAction = (0, _createAction2.default)(unloadAll);

var loadPushMessageCollection = exports.loadPushMessageCollection = function loadPushMessageCollection(_ref) {
  var page = _ref.page,
      pageSize = _ref.pageSize,
      sorted = _ref.sorted,
      filtered = _ref.filtered;
  return function (dispatch, getState) {
    var Parse = (0, _parse2.default)(getState());

    dispatch(loadingPushMessageAction());

    return Parse.Cloud.run('list-push-messages', {
      page: page,
      pageSize: pageSize,
      sorted: sorted,
      filtered: filtered
    }).then(function (result) {
      return dispatch(loadedPushMessageAction(result));
    });
  };
};

var sendPushMessage = exports.sendPushMessage = function sendPushMessage(_ref2) {
  var from = _ref2.from,
      until = _ref2.until,
      message = _ref2.message;
  return function (dispatch, getState) {
    var Parse = (0, _parse2.default)(getState());

    dispatch(sendingPushMessageAction());

    return Parse.Cloud.run('create-push-message', { from: from, until: until, message: message }).then(function () {
      return dispatch(sentPushMessageAction());
    }).catch(function (error) {
      return dispatch(sendPushMessageFailedAction({ error: error }));
    });
  };
};

var unloadPushMessage = exports.unloadPushMessage = function unloadPushMessage() {
  return function (dispatch) {
    dispatch(unloadPushMessageAction());
  };
};