/**
 * @Author: Ben Briggs <benbriggs>
 * @Date:   2018-02-20T16:28:41+00:00
 * @Email:  ben.briggs@thedistance.co.uk
 * @Last modified by:   benbriggs
 * @Last modified time: 2018-02-20T16:32:13+00:00
 * @Copyright: The Distance
 */

import React from 'react';
import { action } from '@storybook/addon-actions';
import { storiesOf } from '@storybook/react';
import backdrop from '../../../.storybook/backdrop';
import ArrayControl from './';

storiesOf('ArrayControl', module)
  .addDecorator(backdrop)
  .add('component', () => (
    <ArrayControl
      label={`Your Name`}
      placeholder={`John Smith`}
      onChange={action('ArrayControl:onChange')}
      value={['one', 'two', 'three']}
    />
  ));
