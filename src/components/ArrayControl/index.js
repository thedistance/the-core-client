/**
 * @Author: Ben Briggs <benbriggs>
 * @Date:   2018-02-20T16:22:59+00:00
 * @Email:  ben.briggs@thedistance.co.uk
 * @Last modified by:   benbriggs
 * @Last modified time: 2018-02-28T12:29:22+00:00
 * @Copyright: The Distance
 */

import React from 'react';
import { FormControl, ControlLabel } from '@sketchpixy/rubix';
import FormGroup from '../FormGroup';
import GenericControl from '../GenericControl';

class ArrayControl extends GenericControl {
  constructor(props) {
    super(props);
    const { value } = props;
    this.state = {
      displayValue: value ? value.join(this.props.delimiter) : '',
      value,
      valid: null,
    };
  }

  handleChange({ target }) {
    const { value: displayValue } = target;
    const parts = displayValue.split(this.props.delimiter);
    const value = parts ? parts.filter(Boolean).map(p => p.trim()) : null;
    const valid = this.props.validator ? this.props.validator(value) : null;

    this.setState({
      displayValue,
      value,
      valid,
    });

    this.props.onChange(value, valid);
  }

  render() {
    return (
      <FormGroup
        validationState={this.validationState()}
        required={this.props.required}
      >
        <ControlLabel>{this.props.label}</ControlLabel>
        <FormControl
          type={`text`}
          onChange={this.handleChange}
          value={this.state.displayValue}
        />
      </FormGroup>
    );
  }
}

ArrayControl.defaultProps = {
  delimiter: ',',
};

export default ArrayControl;
