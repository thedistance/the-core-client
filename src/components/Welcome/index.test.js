/**
 * @Author: Ben Briggs <benbriggs>
 * @Date:   2018-01-22T10:53:26+00:00
 * @Email:  ben.briggs@thedistance.co.uk
 * @Last modified by:   benbriggs
 * @Last modified time: 2018-01-22T10:53:40+00:00
 * @Copyright: The Distance
 */

import React from 'react';
import { shallow } from 'enzyme';
import toJson from 'enzyme-to-json';
import Welcome from './';

test('should render', () => {
  const component = shallow(<Welcome />);

  expect(toJson(component.shallow())).toMatchSnapshot();
});
