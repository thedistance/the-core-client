/**
 * @Author: Ben Briggs <benbriggs>
 * @Date:   2017-12-19T17:06:29+00:00
 * @Email:  ben.briggs@thedistance.co.uk
 * @Last modified by:   benbriggs
 * @Last modified time: 2018-01-22T10:55:35+00:00
 * @Copyright: The Distance
 */

import React from 'react';
import { Grid, Row, Col } from '@sketchpixy/rubix';
import Panel from '../Panel';

const Welcome = () => (
  <Panel title={`Welcome!`}>
    <Grid>
      <Row>
        <Col xs={12}>
          <p>Welcome to The Core!</p>
        </Col>
      </Row>
    </Grid>
  </Panel>
);

export default Welcome;
