/**
 * @Author: Ben Briggs <benbriggs>
 * @Date:   2018-01-22T10:52:41+00:00
 * @Email:  ben.briggs@thedistance.co.uk
 * @Last modified by:   benbriggs
 * @Last modified time: 2018-01-22T10:53:02+00:00
 * @Copyright: The Distance
 */

import React from 'react';
import { storiesOf } from '@storybook/react';
import { withInfo } from '@storybook/addon-info';
import backdrop from '../../../.storybook/backdrop';
import Welcome from './';

storiesOf('Welcome', module)
  .addDecorator(backdrop)
  .add('component', withInfo()(() => <Welcome />));
