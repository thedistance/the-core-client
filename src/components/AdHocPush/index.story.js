/**
 * @Author: Ben Briggs <benbriggs>
 * @Date:   2017-12-08T17:26:25+00:00
 * @Email:  ben.briggs@thedistance.co.uk
 * @Last modified by:   benbriggs
 * @Last modified time: 2018-01-11T09:42:58+00:00
 * @Copyright: The Distance
 */

import React from 'react';
import { action } from '@storybook/addon-actions';
import { storiesOf } from '@storybook/react';
import backdrop from '../../../.storybook/backdrop';
import { PushMessage } from './';

storiesOf('AdHocPush', module)
  .addDecorator(backdrop)
  .add('No messages', () => (
    <PushMessage
      actions={{
        load: () => Promise.resolve(action('load')),
        unload: action('unload'),
        send: action('send'),
      }}
      collection={[]}
    />
  ))
  .add('Successful send', () => (
    <PushMessage
      actions={{
        load: () => Promise.resolve(action('load')),
        unload: action('unload'),
        send: action('send'),
      }}
      collection={[
        {
          get(key) {
            switch (key) {
              case 'payload':
                return JSON.stringify({ alert: 'Hello, world' });
              case 'numSent':
                return 1;
              case 'status':
                return 'succeeded';
              case 'createdAt':
                return new Date();
              default:
                break;
            }
          },
        },
      ]}
      sent={true}
    />
  ))
  .add('Failed send', () => (
    <PushMessage
      actions={{
        load: () => Promise.resolve(action('load')),
        unload: action('unload'),
        send: action('send'),
      }}
      collection={[
        {
          get(key) {
            switch (key) {
              case 'payload':
                return JSON.stringify({ alert: 'Hello, world' });
              case 'numSent':
                return 0;
              case 'status':
                return 'failed';
              case 'createdAt':
                return new Date();
              default:
                break;
            }
          },
        },
      ]}
      error={{
        message: `Could not send the push message. Please try again.`,
      }}
    />
  ));
