/**
 * @Author: Ben Briggs <benbriggs>
 * @Date:   2017-10-20T09:36:35+01:00
 * @Email:  ben.briggs@thedistance.co.uk
 * @Last modified by:   benbriggs
 * @Last modified time: 2018-01-11T12:06:55+00:00
 * @Copyright: The Distance
 */

import React from 'react';
import { shallow } from 'enzyme';
import toJson from 'enzyme-to-json';
import { PushMessage, mapStateToProps, mapDispatchToProps } from './';

describe('component', () => {
  it('should render a push message view', () => {
    const mock = jest.fn();
    const component = shallow(
      <PushMessage
        actions={{
          load: mock,
        }}
        collection={[
          {
            get(key) {
              switch (key) {
                case 'payload':
                  return JSON.stringify({ alert: 'Hello, world' });
                case 'numSent':
                  return 1;
                case 'status':
                  return 'succeeded';
                case 'createdAt':
                  return new Date();
                default:
                  break;
              }
            },
          },
        ]}
      />
    );

    expect(toJson(component.shallow())).toMatchSnapshot();
  });

  it('should render a push message view with an empty collection', () => {
    const mock = jest.fn();
    const component = shallow(
      <PushMessage
        actions={{
          load: mock,
        }}
        collection={[]}
      />
    );

    expect(toJson(component.shallow())).toMatchSnapshot();
  });

  it('should update the message state when the form is changed', () => {
    const mock = jest.fn();

    const component = shallow(
      <PushMessage
        actions={{
          load: mock,
          unload: mock,
        }}
        collection={[]}
      />
    );

    component.find('TextControl').simulate('change', 'Hello, world');
    expect(component.state('message')).toEqual('Hello, world');
  });
});

describe('mapStateToProps', () => {
  it('should expose the expected props', () => {
    const state = {
      pushMessage: {
        error: null,
        sent: null,
      },
    };

    const props = mapStateToProps(state);

    expect(props.error).toEqual(null);
    expect(props.sent).toEqual(null);
  });
});

describe('mapDispatchToProps', () => {
  it('should expose the expected actions', () => {
    const dispatch = jest.fn();
    const props = mapDispatchToProps(dispatch);

    const actions = {
      send: expect.any(Function),
    };

    expect(props.actions).toEqual(expect.objectContaining(actions));
    expect(Object.keys(props.actions).length).toEqual(
      Object.keys(actions).length
    );
  });

  it('should call dispatch for send', () => {
    const dispatch = jest.fn();
    const props = mapDispatchToProps(dispatch);

    props.actions.send();
    expect(dispatch).toHaveBeenCalledTimes(1);
  });
});
