/**
 * @Author: Ben Briggs <benbriggs>
 * @Date:   2017-10-19T10:14:09+01:00
 * @Email:  ben.briggs@thedistance.co.uk
 * @Last modified by:   benbriggs
 * @Last modified time: 2018-01-11T12:04:34+00:00
 * @Copyright: The Distance
 */

import React from 'react';
import PropTypes from 'prop-types';
import { Grid, Row, Col, Form, Button, ButtonToolbar } from '@sketchpixy/rubix';
import { connect } from 'react-redux';
import Alert from '../Alert';
import Panel from '../Panel';
import TextControl from '../TextControl';
import * as actions from './actions';
import { selectSent, selectError } from './reducer';

export class PushMessage extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      message: '',
    };
  }

  handleChange(message) {
    this.setState({ message });
  }

  sendMessage() {
    this.props.actions.send(this.state.message);
    this.setState({ message: '' });
  }

  renderMessage() {
    const { error, sent } = this.props;
    if (error) {
      return <Alert danger>{error.message}</Alert>;
    }
    if (sent) {
      return <Alert success>Your message has been sent!</Alert>;
    }
    return null;
  }

  render() {
    return (
      <div>
        {this.renderMessage()}
        <Panel title={`Send a Push Message`}>
          <Grid>
            <Row>
              <Col xs={12}>
                <Form autoComplete={false} onSubmit={e => e.preventDefault()}>
                  <TextControl
                    label={`Message`}
                    onChange={this.handleChange.bind(this)}
                    value={this.state.message}
                  />
                  <ButtonToolbar style={{ marginBottom: '1em' }}>
                    <Button onClick={this.sendMessage.bind(this)}>Send</Button>
                  </ButtonToolbar>
                </Form>
              </Col>
            </Row>
          </Grid>
        </Panel>
      </div>
    );
  }
}

PushMessage.propTypes = {
  actions: PropTypes.objectOf(PropTypes.func).isRequired,
  error: PropTypes.any,
  sent: PropTypes.any,
};

export const mapStateToProps = state => ({
  error: selectError(state),
  sent: selectSent(state),
});

export const mapDispatchToProps = dispatch => ({
  actions: {
    send: message => dispatch(actions.sendPushMessage({ message })),
  },
});

export default connect(mapStateToProps, mapDispatchToProps)(PushMessage);
