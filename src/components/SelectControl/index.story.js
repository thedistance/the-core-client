/**
 * @Author: Ben Briggs <benbriggs>
 * @Date:   2017-12-15T12:07:36+00:00
 * @Email:  ben.briggs@thedistance.co.uk
 * @Last modified by:   benbriggs
 * @Last modified time: 2018-02-12T15:55:31+00:00
 * @Copyright: The Distance
 */

import React from 'react';
import { action } from '@storybook/addon-actions';
import { storiesOf } from '@storybook/react';
import backdrop from '../../../.storybook/backdrop';
import SelectControl from './';

storiesOf('SelectControl', module)
  .addDecorator(backdrop)
  .add('component', () => (
    <SelectControl
      label={`Fruit`}
      collection={['Apples', 'Bananas', 'Pears']}
      value={`Pears`}
      onChange={action('SelectControl:onChange')}
    />
  ))
  .add('with blank option', () => (
    <SelectControl
      allowBlank={true}
      label={`Fruit`}
      collection={['Apples', 'Bananas', 'Pears']}
      value={`Pears`}
      onChange={action('SelectControl:onChange')}
    />
  ))
  .add('with label support', () => (
    <SelectControl
      label={`Fruit`}
      collection={[
        { label: 'Apples', value: 'a' },
        { label: 'Bananas', value: 'b' },
        { label: 'Pears', value: 'c' },
      ]}
      value={`Pears`}
      onChange={action('SelectControl:onChange')}
    />
  ));
