/**
 * @Author: Ben Briggs <benbriggs>
 * @Date:   2017-12-15T12:05:26+00:00
 * @Email:  ben.briggs@thedistance.co.uk
 * @Last modified by:   benbriggs
 * @Last modified time: 2018-02-20T11:41:00+00:00
 * @Copyright: The Distance
 */

import React from 'react';
import * as R from 'ramda';
import PropTypes from 'prop-types';
import {
  FormControl,
  ControlLabel,
  FormControlFeedback,
} from '@sketchpixy/rubix';
import FormGroup from '../FormGroup';
import GenericControl from '../GenericControl';

const Option = item =>
  R.is(Object, item) ? (
    <option key={item.value} value={item.value}>
      {item.label}
    </option>
  ) : (
    <option key={item} value={item}>
      {item}
    </option>
  );

class SelectControl extends GenericControl {
  handleChange({ target }) {
    const { value } = target;
    const valid = this.props.validator ? this.props.validator(value) : null;

    this.setState({
      value,
      valid,
    });

    this.props.onChange(value ? value : null, valid);
  }

  render() {
    const { allowBlank, collection } = this.props;
    const { value } = this.state;
    return (
      <FormGroup
        validationState={this.validationState()}
        required={this.props.required}
      >
        <ControlLabel>{this.props.label}</ControlLabel>
        <FormControl
          componentClass={`select`}
          value={value}
          onChange={this.handleChange.bind(this)}
        >
          {allowBlank && <option key={`null`} />}
          {collection.map(Option)}
        </FormControl>
        <FormControlFeedback />
      </FormGroup>
    );
  }
}

SelectControl.propTypes = {
  allowBlank: PropTypes.bool,
  collection: PropTypes.array,
  value: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
};

export default SelectControl;
