/**
 * @Author: Ben Briggs <benbriggs>
 * @Date:   2017-12-15T13:46:03+00:00
 * @Email:  ben.briggs@thedistance.co.uk
 * @Last modified by:   benbriggs
 * @Last modified time: 2018-02-12T15:56:27+00:00
 * @Copyright: The Distance
 */

import React from 'react';
import { shallow } from 'enzyme';
import toJson from 'enzyme-to-json';
import SelectControl from './';

test('should render', () => {
  const component = shallow(
    <SelectControl
      label={`Fruit`}
      collection={['Apples', 'Bananas', 'Pears']}
      onChange={jest.fn()}
    />
  );

  expect(toJson(component.shallow())).toMatchSnapshot();
});

test('should render with a value', () => {
  const component = shallow(
    <SelectControl
      label={`Fruit`}
      collection={['Apples', 'Bananas', 'Pears']}
      value={`Pears`}
      onChange={jest.fn()}
    />
  );

  expect(toJson(component.shallow())).toMatchSnapshot();
});

test('should render with labels and values', () => {
  const component = shallow(
    <SelectControl
      label={`Fruit`}
      collection={[
        { label: 'Apples', value: 'a' },
        { label: 'Bananas', value: 'b' },
        { label: 'Pears', value: 'c' },
      ]}
      value={`a`}
      onChange={jest.fn()}
    />
  );

  expect(toJson(component.shallow())).toMatchSnapshot();
});
