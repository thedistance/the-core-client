/**
 * @Author: Ben Briggs <benbriggs>
 * @Date:   2017-11-02T12:50:25+00:00
 * @Email:  ben.briggs@thedistance.co.uk
 * @Last modified by:   benbriggs
 * @Last modified time: 2018-01-11T09:34:35+00:00
 * @Copyright: The Distance
 */

import React from 'react';
import { shallow } from 'enzyme';
import Loadable from './';

/*
 * Loadable does not implement render, so define
 * a dummy component that will call load/unload.
 */

class LoadableComponent extends Loadable {
  render() {
    return <p>Hello!</p>;
  }
}

it('should load and unload the data on component lifecycle methods', () => {
  const mock = jest.fn();

  const component = shallow(
    <LoadableComponent
      actions={{
        load: mock,
        unload: mock,
      }}
    />
  );

  component.unmount();
  expect(mock).toHaveBeenCalledTimes(2);
});
