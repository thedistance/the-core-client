/**
 * @Author: Ben Briggs <benbriggs>
 * @Date:   2017-11-02T12:46:30+00:00
 * @Email:  ben.briggs@thedistance.co.uk
 * @Last modified by:   benbriggs
 * @Last modified time: 2018-01-11T09:32:35+00:00
 * @Copyright: The Distance
 */

import React from 'react';
import PropTypes from 'prop-types';

class Loadable extends React.Component {
  componentWillMount() {
    this.props.actions.load();
  }

  componentWillUnmount() {
    this.props.actions.unload();
  }
}

Loadable.propTypes = {
  actions: PropTypes.objectOf(PropTypes.func).isRequired,
};

export default Loadable;
