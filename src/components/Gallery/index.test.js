/**
 * @Author: Ben Briggs <benbriggs>
 * @Date:   2018-01-04T13:45:06+00:00
 * @Email:  ben.briggs@thedistance.co.uk
 * @Last modified by:   benbriggs
 * @Last modified time: 2018-01-04T14:00:22+00:00
 * @Copyright: The Distance
 */

import React from 'react';
import { shallow } from 'enzyme';
import toJson from 'enzyme-to-json';
import Gallery from './';

test('should render', () => {
  const component = shallow(
    <Gallery
      images={[
        {
          thumbnail: 'http://via.placeholder.com/350x150',
          original: 'http://via.placeholder.com/350x150',
        },
      ]}
    />
  );

  expect(toJson(component.shallow())).toMatchSnapshot();
});

test('should render without images', () => {
  const component = shallow(<Gallery />);

  expect(toJson(component.shallow())).toMatchSnapshot();
});
