/**
 * @Author: Ben Briggs <benbriggs>
 * @Date:   2018-01-04T13:22:29+00:00
 * @Email:  ben.briggs@thedistance.co.uk
 * @Last modified by:   benbriggs
 * @Last modified time: 2018-01-11T09:49:42+00:00
 * @Copyright: The Distance
 */

import React from 'react';
import PropTypes from 'prop-types';
import { Grid, Row, Col } from '@sketchpixy/rubix';
import ImageGallery from 'react-image-gallery';
import Panel from '../Panel';
import 'react-image-gallery/styles/css/image-gallery.css';

class Gallery extends React.Component {
  handleDelete() {
    const index = this._gallery.getCurrentIndex();
    this.props.onDelete(index);
    this._gallery.slideToIndex(index === 0 ? 0 : index - 1);
  }

  render() {
    const { images, onDelete, title } = this.props;
    const renderCustomControls = onDelete
      ? () => (
          <button
            type={`button`}
            className={`icon-outlined-delete image-gallery-icon`}
            onClick={this.handleDelete.bind(this)}
          />
        )
      : null;
    return (
      <Panel title={title}>
        {images ? (
          <ImageGallery
            ref={i => {
              this._gallery = i;
            }}
            items={images}
            renderCustomControls={renderCustomControls}
          />
        ) : (
          <Grid>
            <Row>
              <Col xs={12}>
                <p>No images to display.</p>
              </Col>
            </Row>
          </Grid>
        )}
      </Panel>
    );
  }
}

Gallery.propTypes = {
  title: PropTypes.string.isRequired,
  onDelete: PropTypes.func,
  images: PropTypes.array,
};

Gallery.defaultProps = {
  title: 'Images',
};

export default Gallery;
