/**
 * @Author: Ben Briggs <benbriggs>
 * @Date:   2018-01-04T13:26:54+00:00
 * @Email:  ben.briggs@thedistance.co.uk
 * @Last modified by:   benbriggs
 * @Last modified time: 2018-01-08T14:33:59+00:00
 * @Copyright: The Distance
 */

import React from 'react';
import { action } from '@storybook/addon-actions';
import { storiesOf } from '@storybook/react';
import { withInfo } from '@storybook/addon-info';
import backdrop from '../../../.storybook/backdrop';
import Gallery from './';

storiesOf('Gallery', module)
  .addDecorator(backdrop)
  .add(
    'component',
    withInfo()(() => (
      <Gallery
        images={[
          {
            thumbnail: 'http://via.placeholder.com/350x150',
            original: 'http://via.placeholder.com/350x150',
          },
        ]}
        onDelete={action('Gallery:onDelete')}
      />
    ))
  )
  .add(
    'without delete action',
    withInfo()(() => (
      <Gallery
        images={[
          {
            thumbnail: 'http://via.placeholder.com/350x150',
            original: 'http://via.placeholder.com/350x150',
          },
        ]}
      />
    ))
  )
  .add('empty', withInfo()(() => <Gallery />));
