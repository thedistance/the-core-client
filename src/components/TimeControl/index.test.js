/**
 * @Author: Ben Briggs <benbriggs>
 * @Date:   2018-01-10T10:34:09+00:00
 * @Email:  ben.briggs@thedistance.co.uk
 * @Last modified by:   benbriggs
 * @Last modified time: 2018-01-10T10:34:49+00:00
 * @Copyright: The Distance
 */

import React from 'react';
import { shallow } from 'enzyme';
import toJson from 'enzyme-to-json';
import TimeControl from './';

test('should render', () => {
  const component = shallow(
    <TimeControl label={`Start time`} onChange={jest.fn()} />
  );

  expect(toJson(component.shallow())).toMatchSnapshot();
});

test('should render with a value', () => {
  const component = shallow(
    <TimeControl label={`Start time`} onChange={jest.fn()} value={`09:00`} />
  );

  expect(toJson(component.shallow())).toMatchSnapshot();
});
