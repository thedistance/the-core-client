/**
 * @Author: Ben Briggs <benbriggs>
 * @Date:   2018-01-09T16:06:54+00:00
 * @Email:  ben.briggs@thedistance.co.uk
 * @Last modified by:   benbriggs
 * @Last modified time: 2018-01-09T16:54:48+00:00
 * @Copyright: The Distance
 */

import React from 'react';
import { action } from '@storybook/addon-actions';
import { storiesOf } from '@storybook/react';
import { withInfo } from '@storybook/addon-info';
import backdrop from '../../../.storybook/backdrop';
import TimeControl from './';

storiesOf('TimeControl', module)
  .addDecorator(backdrop)
  .add(
    'component',
    withInfo()(() => (
      <TimeControl
        label={`Start time`}
        onChange={action('TimeControl:onChange')}
      />
    ))
  );
