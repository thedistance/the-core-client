/**
 * @Author: Ben Briggs <benbriggs>
 * @Date:   2018-01-09T16:06:09+00:00
 * @Email:  ben.briggs@thedistance.co.uk
 * @Last modified by:   benbriggs
 * @Last modified time: 2018-01-09T16:55:22+00:00
 * @Copyright: The Distance
 */

import React from 'react';
import PropTypes from 'prop-types';
import { FormControl } from '@sketchpixy/rubix';
import GenericControl from '../GenericControl';

const TimeControl = props => (
  <GenericControl {...props}>
    <FormControl type={`time`} />
  </GenericControl>
);

TimeControl.defaultProps = {
  value: '',
};

TimeControl.propTypes = {
  value: PropTypes.string,
};

export default TimeControl;
