/**
 * @Author: benbriggs
 * @Date:   2018-09-25T13:09:53+01:00
 * @Email:  ben.briggs@thedistance.co.uk
 * @Last modified by:   benbriggs
 * @Last modified time: 2018-09-25T13:14:01+01:00
 * @Copyright: The Distance
 */

import React from 'react';
import PropTypes from 'prop-types';
import { FormControl } from '@sketchpixy/rubix';
import GenericControl from '../GenericControl';

const DangerousHtmlControl = props => (
  <GenericControl {...props}>
    <FormControl.Static>
      <div dangerouslySetInnerHTML={{ __html: props.value }} />
    </FormControl.Static>
  </GenericControl>
);

DangerousHtmlControl.propTypes = {
  value: PropTypes.string,
};

export default DangerousHtmlControl;
