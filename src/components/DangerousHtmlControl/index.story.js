/**
 * @Author: benbriggs
 * @Date:   2018-09-25T13:12:26+01:00
 * @Email:  ben.briggs@thedistance.co.uk
 * @Last modified by:   benbriggs
 * @Last modified time: 2018-09-25T13:14:23+01:00
 * @Copyright: The Distance
 */

import React from 'react';
import { storiesOf } from '@storybook/react';
import backdrop from '../../../.storybook/backdrop';
import DangerousHtmlControl from './';

storiesOf('DangerousHtmlControl', module)
  .addDecorator(backdrop)
  .add('component', () => (
    <DangerousHtmlControl label={'HTML'} value={'<h1>Hello world</h1>'} />
  ));
