/*
 * Created Date: Fri, 13th Apr 2018, 11:28:33 am
 * Author: Harry Crank
 * Email: harry.crank@thedistance.co.uk
 * Copyright (c) 2018 The Distance
 */

import React from 'react';
import { shallow } from 'enzyme';
import toJson from 'enzyme-to-json';
import StaticTextControl from './';

test('should render', () => {
  const component = shallow(
    <StaticTextControl label={'Your Name'} onChange={jest.fn()} />
  );
  expect(toJson(component.shallow())).toMatchSnapshot();
});

test('should render with a value', () => {
  const component = shallow(
    <StaticTextControl
      label={'Your Name'}
      onChange={jest.fn()}
      value={'Betty Boo'}
    />
  );
  expect(toJson(component.shallow())).toMatchSnapshot();
});
