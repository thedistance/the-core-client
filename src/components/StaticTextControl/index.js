/*
 * Created Date: Fri, 13th Apr 2018, 11:28:19 am
 * Author: Harry Crank
 * Email: harry.crank@thedistance.co.uk
 * Copyright (c) 2018 The Distance
 */

import React from 'react';
import PropTypes from 'prop-types';
import { FormControl } from '@sketchpixy/rubix';
import GenericControl from '../GenericControl';

const StaticTextControl = props => (
  <GenericControl {...props}>
    <FormControl.Static>{props.value}</FormControl.Static>
  </GenericControl>
);

StaticTextControl.propTypes = {
  value: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
};

export default StaticTextControl;
