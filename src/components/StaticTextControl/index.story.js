/*
 * Created Date: Fri, 13th Apr 2018, 11:28:40 am
 * Author: Harry Crank
 * Email: harry.crank@thedistance.co.uk
 * Copyright (c) 2018 The Distance
 */

import React from 'react';
import { storiesOf } from '@storybook/react';
import backdrop from '../../../.storybook/backdrop';
import StaticTextControl from './';

storiesOf('StaticTextControl', module)
  .addDecorator(backdrop)
  .add('component', () => (
    <StaticTextControl label={'Your Name'} value={'Hello you!'} />
  ));
