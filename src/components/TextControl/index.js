/**
 * @Author: Ben Briggs <benbriggs>
 * @Date:   2017-12-15T12:05:26+00:00
 * @Email:  ben.briggs@thedistance.co.uk
 * @Last modified by:   benbriggs
 * @Last modified time: 2017-12-19T09:40:31+00:00
 * @Copyright: The Distance
 */

import React from 'react';
import PropTypes from 'prop-types';
import { FormControl } from '@sketchpixy/rubix';
import GenericControl from '../GenericControl';

const TextControl = props => (
  <GenericControl {...props}>
    <FormControl type={`text`} />
  </GenericControl>
);

TextControl.propTypes = {
  value: PropTypes.string,
};

export default TextControl;
