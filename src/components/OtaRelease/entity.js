/**
 * @Author: benbriggs
 * @Date:   2018-04-20T11:06:19+01:00
 * @Email:  ben.briggs@thedistance.co.uk
 * @Last modified by:   benbriggs
 * @Last modified time: 2018-04-25T13:49:10+01:00
 * @Copyright: The Distance
 */

import React from 'react';
import * as R from 'ramda';
import PropTypes from 'prop-types';
import { ControlLabel, FormControl, FormGroup } from '@sketchpixy/rubix';
import createEntity from '../Entity';

const propOf = R.flip(R.prop);
const parseNumber = R.flip(parseInt);
const parseDecimal = parseNumber(10);

const platforms = {
  1: 'iOS',
  2: 'Android',
};

const Platform = R.compose(propOf(platforms), parseDecimal, R.prop('value'));

const ReleaseDate = R.compose(
  R.invoker(2, 'toLocaleString')('en-GB', {
    weekday: 'long',
    year: 'numeric',
    month: 'long',
    day: 'numeric',
    hour: 'numeric',
    minute: 'numeric',
    second: 'numeric',
    timeZone: 'UTC',
  }),
  R.prop('value')
);

const ReadFile = props => (
  <FormGroup>
    <ControlLabel>{props.label}</ControlLabel>
    <FormControl.Static>
      <a href={R.path(['value', '_url'], props)}>
        {R.path(['value', '_name'], props)}
      </a>
    </FormControl.Static>
  </FormGroup>
);

ReadFile.propTypes = {
  label: PropTypes.string.isRequired,
};

const OtaRelease = createEntity({
  name: 'OtaRelease',
  inflections: {
    singular: 'OTA Release',
    plural: 'OTA Releases',
  },
  readOnlyEditView: true,
  schema: {
    version: {
      type: 'string',
      showInIndex: true,
    },
    releaseNotes: {
      type: 'textarea',
    },
    platform: {
      type: 'enumNumeric',
      choices: R.keys(platforms).map(key => ({
        value: key,
        label: platforms[key],
      })),
      IndexComponent: Platform,
      showInIndex: true,
      defaultTo: 1,
    },
    release: {
      type: 'file',
      UpdateComponent: ReadFile,
    },
    createdAt: {
      label: 'Release Date',
      type: 'staticDate',
      showInIndex: true,
      IndexComponent: ReleaseDate,
    },
  },
  onLoadCollection: query => query.descending('createdAt'),
});

export default OtaRelease;
