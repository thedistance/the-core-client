/**
 * @Author: Ben Briggs <benbriggs>
 * @Date:   2017-12-15T13:46:03+00:00
 * @Email:  ben.briggs@thedistance.co.uk
 * @Last modified by:   benbriggs
 * @Last modified time: 2017-12-19T12:31:31+00:00
 * @Copyright: The Distance
 */

import React from 'react';
import { shallow } from 'enzyme';
import toJson from 'enzyme-to-json';
import NumberControl from './';

test('should render', () => {
  const component = shallow(
    <NumberControl label={`Over`} onChange={jest.fn()} />
  );

  expect(toJson(component.shallow())).toMatchSnapshot();
});

test('should render with a value', () => {
  const component = shallow(
    <NumberControl label={`Over`} onChange={jest.fn()} value={9000} />
  );

  expect(toJson(component.shallow())).toMatchSnapshot();
});

test('should handle change', () => {
  const mock = jest.fn();
  const component = shallow(
    <NumberControl label={`Latitude`} onChange={mock} />
  );

  component.simulate('change', 90, true);
  expect(mock).toHaveBeenCalledWith(90, true);
});
