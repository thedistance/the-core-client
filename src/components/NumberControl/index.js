/**
 * @Author: Ben Briggs <benbriggs>
 * @Date:   2017-12-15T12:05:26+00:00
 * @Email:  ben.briggs@thedistance.co.uk
 * @Last modified by:   benbriggs
 * @Last modified time: 2018-01-11T09:37:08+00:00
 * @Copyright: The Distance
 */

import React from 'react';
import PropTypes from 'prop-types';
import { FormControl } from '@sketchpixy/rubix';
import GenericControl from '../GenericControl';

const NumberControl = ({ onChange, ...rest }) => (
  <GenericControl
    {...rest}
    onChange={(value, valid) => onChange(parseFloat(value), valid)}
  >
    <FormControl type={`number`} />
  </GenericControl>
);

NumberControl.propTypes = {
  onChange: PropTypes.func,
  value: PropTypes.number,
};

export default NumberControl;
