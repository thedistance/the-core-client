/**
 * @Author: Ben Briggs <benbriggs>
 * @Date:   2017-12-15T12:07:36+00:00
 * @Email:  ben.briggs@thedistance.co.uk
 * @Last modified by:   benbriggs
 * @Last modified time: 2018-01-11T09:57:28+00:00
 * @Copyright: The Distance
 */

import React from 'react';
import { action } from '@storybook/addon-actions';
import { storiesOf } from '@storybook/react';
import backdrop from '../../../.storybook/backdrop';
import NumberControl from './';

storiesOf('NumberControl', module)
  .addDecorator(backdrop)
  .add('component', () => (
    <NumberControl
      label={`Order`}
      onChange={action('NumberControl:onChange')}
    />
  ));
