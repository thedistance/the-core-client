/**
 * @Author: Ben Briggs <benbriggs>
 * @Date:   2018-01-11T14:54:37+00:00
 * @Email:  ben.briggs@thedistance.co.uk
 * @Last modified by:   benbriggs
 * @Last modified time: 2018-02-06T12:22:59+00:00
 * @Copyright: The Distance
 */

import TimeAgo from 'react-timeago';
import React from 'react';
import PropTypes from 'prop-types';

const Timeago = ({ value }) => <TimeAgo date={value} />;

Timeago.propTypes = {
  value: PropTypes.instanceOf(Date),
};

export default Timeago;
