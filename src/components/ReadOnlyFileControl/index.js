/**
 * @Author: benbriggs
 * @Date:   2018-08-06T16:35:40+01:00
 * @Email:  ben.briggs@thedistance.co.uk
 * @Last modified by:   benbriggs
 * @Last modified time: 2018-08-06T16:39:54+01:00
 * @Copyright: The Distance
 */

import React from 'react';
import { ControlLabel, Button } from '@sketchpixy/rubix';
import FormGroup from '../FormGroup';
import GenericControl from '../GenericControl';

const divStyle = {
  marginBottom: '10px',
};

class ReadOnlyFileControl extends GenericControl {
  render() {
    return (
      <div style={divStyle}>
        <FormGroup
          validationState={this.validationState()}
          required={this.props.required}
        >
          <ControlLabel>{this.props.label}</ControlLabel>
        </FormGroup>
        {this.state.value && (
          <a href={this.state.value.url()} target="_blank">
            <Button bsStyle={`info`} outlined>
              Open {this.state.value.name()}
            </Button>
          </a>
        )}
      </div>
    );
  }
}

export default ReadOnlyFileControl;
