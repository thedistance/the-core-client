/**
 * @Author: Ben Briggs <benbriggs>
 * @Date:   2017-12-15T13:46:03+00:00
 * @Email:  ben.briggs@thedistance.co.uk
 * @Last modified by:   benbriggs
 * @Last modified time: 2017-12-19T16:13:28+00:00
 * @Copyright: The Distance
 */

import React from 'react';
import { shallow } from 'enzyme';
import toJson from 'enzyme-to-json';
import PasswordControl from './';

test('should render', () => {
  const component = shallow(
    <PasswordControl
      label={`Your Password`}
      placeholder={`John Smith`}
      onChange={jest.fn()}
    />
  );

  expect(toJson(component.shallow())).toMatchSnapshot();
});

test('should render with a value', () => {
  const component = shallow(
    <PasswordControl
      label={`Your Password`}
      onChange={jest.fn()}
      value={`Chuck Norris`}
    />
  );

  expect(toJson(component.shallow())).toMatchSnapshot();
});
