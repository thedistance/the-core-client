/**
 * @Author: Ben Briggs <benbriggs>
 * @Date:   2017-09-26T13:54:19+01:00
 * @Email:  ben.briggs@thedistance.co.uk
 * @Last modified by:   benbriggs
 * @Last modified time: 2018-01-11T09:34:11+00:00
 * @Copyright: The Distance
 */

import React from 'react';
import PropTypes from 'prop-types';

import { Alert } from '@sketchpixy/rubix';

const AlertComponent = ({ children, ...props }) => (
  <Alert dismissible {...props}>
    {children}
  </Alert>
);

AlertComponent.propTypes = {
  children: PropTypes.any,
};

export default AlertComponent;
