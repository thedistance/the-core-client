/**
 * @Author: Ben Briggs <benbriggs>
 * @Date:   2017-12-08T15:09:12+00:00
 * @Email:  ben.briggs@thedistance.co.uk
 * @Last modified by:   benbriggs
 * @Last modified time: 2018-01-02T15:52:34+00:00
 * @Copyright: The Distance
 */

import React from 'react';
import { shallow } from 'enzyme';
import toJson from 'enzyme-to-json';
import Alert from './';

test('should render', () => {
  const component = shallow(
    <Alert success>
      <p>Success!</p>
    </Alert>
  );

  expect(toJson(component.shallow())).toMatchSnapshot();
});
