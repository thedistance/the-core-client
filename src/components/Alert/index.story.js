/**
 * @Author: Ben Briggs <benbriggs>
 * @Date:   2017-12-08T15:24:50+00:00
 * @Email:  ben.briggs@thedistance.co.uk
 * @Last modified by:   benbriggs
 * @Last modified time: 2018-01-11T09:43:16+00:00
 * @Copyright: The Distance
 */

import React from 'react';
import { storiesOf } from '@storybook/react';
import { withInfo } from '@storybook/addon-info';
import backdrop from '../../../.storybook/backdrop';
import Alert from './';

storiesOf('Alert', module)
  .addDecorator(backdrop)
  .add(
    'Successful state',
    withInfo()(() => (
      <Alert success>
        <p>The request completed successfully!</p>
      </Alert>
    ))
  )
  .add(
    'Error state',
    withInfo()(() => (
      <Alert danger>
        <p>The request failed.</p>
      </Alert>
    ))
  );
