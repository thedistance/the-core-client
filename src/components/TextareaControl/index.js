/**
 * @Author: Ben Briggs <benbriggs>
 * @Date:   2018-01-22T12:27:16+00:00
 * @Email:  ben.briggs@thedistance.co.uk
 * @Last modified by:   benbriggs
 * @Last modified time: 2018-01-22T12:27:45+00:00
 * @Copyright: The Distance
 */

import React from 'react';
import PropTypes from 'prop-types';
import { FormControl } from '@sketchpixy/rubix';
import GenericControl from '../GenericControl';

const TextareaControl = props => (
  <GenericControl {...props}>
    <FormControl componentClass={`textarea`} />
  </GenericControl>
);

TextareaControl.propTypes = {
  value: PropTypes.string,
};

export default TextareaControl;
