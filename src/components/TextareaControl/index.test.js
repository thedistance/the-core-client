/**
 * @Author: Ben Briggs <benbriggs>
 * @Date:   2018-01-22T12:28:41+00:00
 * @Email:  ben.briggs@thedistance.co.uk
 * @Last modified by:   benbriggs
 * @Last modified time: 2018-01-22T12:28:55+00:00
 * @Copyright: The Distance
 */

import React from 'react';
import { shallow } from 'enzyme';
import toJson from 'enzyme-to-json';
import TextareaControl from './';

test('should render', () => {
  const component = shallow(
    <TextareaControl
      label={`Fruit`}
      collection={['Apples', 'Bananas', 'Pears']}
      onChange={jest.fn()}
    />
  );

  expect(toJson(component.shallow())).toMatchSnapshot();
});
