/**
 * @Author: Ben Briggs <benbriggs>
 * @Date:   2018-01-22T12:27:57+00:00
 * @Email:  ben.briggs@thedistance.co.uk
 * @Last modified by:   benbriggs
 * @Last modified time: 2018-01-22T12:28:30+00:00
 * @Copyright: The Distance
 */

import React from 'react';
import { action } from '@storybook/addon-actions';
import { storiesOf } from '@storybook/react';
import backdrop from '../../../.storybook/backdrop';
import TextareaControl from './';

storiesOf('TextareaControl', module)
  .addDecorator(backdrop)
  .add('component', () => (
    <TextareaControl
      label={`Comments`}
      onChange={action('TextareaControl:onChange')}
    />
  ));
