/*
 * Created Date: Tue, 17th Apr 2018, 15:18:57 pm
 * Author: Harry Crank
 * Email: harry.crank@thedistance.co.uk
 * Copyright (c) 2018 The Distance
 */

import React from 'react';
import PropTypes from 'prop-types';
import {
  Grid,
  Row,
  Col,
  FormControlFeedback,
  ControlLabel,
} from '@sketchpixy/rubix';
import FormGroup from '../FormGroup';
import GenericControl from '../GenericControl';
import StaticTextControl from '../StaticTextControl';

class ReadOnlyGeoPointControl extends GenericControl {
  constructor(props) {
    super(props);
    const { value } = props;
    if (value) {
      this.state = {
        latitude: value.latitude,
        longitude: value.longitude,
        valid: null,
      };
    } else {
      this.state = { latitude: 0, longitude: 0, valid: null };
    }
  }

  render() {
    const { label, required, ...rest } = this.props;
    const { latitude, longitude } = this.state;
    return (
      <FormGroup validationState={this.validationState()} required={required}>
        <ControlLabel>{label}</ControlLabel>
        <Grid collapse>
          <Row>
            <Col sm={5}>
              <StaticTextControl
                {...rest}
                label={'Latitude'}
                value={latitude}
              />
              <FormControlFeedback />
            </Col>
            <Col sm={5} smOffset={1}>
              <StaticTextControl
                {...rest}
                label={'Longitude'}
                value={longitude}
              />
              <FormControlFeedback />
            </Col>
          </Row>
        </Grid>
      </FormGroup>
    );
  }
}

ReadOnlyGeoPointControl.propTypes = {
  value: PropTypes.object,
  label: PropTypes.string.isRequired,
};

export default ReadOnlyGeoPointControl;
