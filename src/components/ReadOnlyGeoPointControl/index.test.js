/*
 * Created Date: Thu, 19th Apr 2018, 09:21:44 am
 * Author: Harry Crank
 * Email: harry.crank@thedistance.co.uk
 * Copyright (c) 2018 The Distance
 */

import React from 'react';
import { shallow } from 'enzyme';
import toJson from 'enzyme-to-json';
import ReadOnlyGeoPointControl from './';

describe('ReadOnlyGeoPointControl', () => {
  test('should render', () => {
    const component = shallow(
      <ReadOnlyGeoPointControl label={`Location`} onChange={jest.fn()} />
    );

    expect(toJson(component.shallow())).toMatchSnapshot();
  });
});
