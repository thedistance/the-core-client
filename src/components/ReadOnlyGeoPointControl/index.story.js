/*
 * Created Date: Thu, 19th Apr 2018, 08:41:59 am
 * Author: Harry Crank
 * Email: harry.crank@thedistance.co.uk
 * Copyright (c) 2018 The Distance
 */

import React from 'react';
import { storiesOf } from '@storybook/react';
import backdrop from '../../../.storybook/backdrop';
import ReadOnlyGeoPointControl from './';

storiesOf('ReadOnlyGeoPointControl', module)
  .addDecorator(backdrop)
  .add('component', () => <ReadOnlyGeoPointControl label={`Location`} />);
