/**
 * @Author: Ben Briggs <benbriggs>
 * @Date:   2017-09-27T15:29:10+01:00
 * @Email:  ben.briggs@thedistance.co.uk
 * @Last modified by:   benbriggs
 * @Last modified time: 2018-01-11T09:58:16+00:00
 * @Copyright: The Distance
 */

import React from 'react';
import PropTypes from 'prop-types';
import { Button } from '@sketchpixy/rubix';
import { LinkContainer } from 'react-router-bootstrap';

const LinkButton = ({ pathname, children, ...rest }) => (
  <LinkContainer to={{ pathname }}>
    <Button {...rest}>{children}</Button>
  </LinkContainer>
);

LinkButton.propTypes = {
  pathname: PropTypes.string,
  children: PropTypes.any,
};

export default LinkButton;
