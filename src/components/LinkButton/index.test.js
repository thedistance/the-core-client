/**
 * @Author: Ben Briggs <benbriggs>
 * @Date:   2017-09-27T15:31:17+01:00
 * @Email:  ben.briggs@thedistance.co.uk
 * @Last modified by:   benbriggs
 * @Last modified time: 2017-12-08T16:17:27+00:00
 * @Copyright: The Distance
 */

import React from 'react';
import { shallow } from 'enzyme';
import toJson from 'enzyme-to-json';
import LinkButton from './';

test('should render a basic link', () => {
  const component = shallow(
    <LinkButton pathname={`/path/to/somewhere`}>Somewhere</LinkButton>
  );

  expect(toJson(component.shallow())).toMatchSnapshot();
});

test('should render extra props', () => {
  const component = shallow(
    <LinkButton pathname={`/path/to/somewhere`} bsStyle={`primary`}>
      Somewhere (primary style)
    </LinkButton>
  );

  expect(toJson(component.shallow())).toMatchSnapshot();
});
