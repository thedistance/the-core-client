/**
 * @Author: benbriggs
 * @Date:   2018-05-22T15:15:01+01:00
 * @Email:  ben.briggs@thedistance.co.uk
 * @Last modified by:   benbriggs
 * @Last modified time: 2018-09-10T10:48:56+01:00
 * @Copyright: The Distance
 */

import React from 'react';
import { Route } from 'react-router';
import createListView from './containers/list';
import createNewView from './containers/create';
import createDetailView from './containers/detail';

function createViews(
  entity,
  {
    createConnector,
    editConnector,
    listConnector,
    createView,
    editView,
    listView,
    extraActions,
    extraCreateActions,
    extraEditActions,
    warnOnDelete,
  }
) {
  const {
    camelised,
    collectionIndex,
    plural,
    createSchemaFields,
    editSchemaFields,
    singular,
    slug,
    selectors: {
      selectCurrent,
      selectErrorMessage,
      selectPermissions,
      selectPages,
      selectCollection,
      selectSaved,
    },
    actions,
    actions: {
      create,
      destroy,
      loadCollection,
      loadCurrent,
      save,
      revert,
      unloadCollection,
      unloadCurrent,
      update,
    },
  } = entity;

  const routeId = `${camelised}Id`;

  const exposedCreateView = createNewView(createView)({
    actions,
    createConnector,
    create,
    save,
    unloadCurrent,
    update,
    plural,
    revert,
    extraActions,
    extraCreateActions,
    selectCurrent,
    selectErrorMessage,
    selectSaved,
    schemaFields: createSchemaFields,
    slug,
    singular,
  });

  const exposedEditView = createDetailView(editView)({
    actions,
    editConnector,
    save,
    destroy,
    loadCurrent,
    unloadCurrent,
    update,
    plural,
    revert,
    extraActions,
    extraEditActions,
    routeId,
    selectCurrent,
    selectErrorMessage,
    selectSaved,
    schemaFields: editSchemaFields,
    slug,
    singular,
    warnOnDelete,
  });

  const exposedListView = createListView(listView)({
    actions,
    listConnector,
    loadCollection,
    unloadCollection,
    selectPermissions,
    selectPages,
    selectCollection,
    selectErrorMessage,
    singular,
    slug,
    plural,
    collectionIndex,
  });

  return {
    ...entity,
    routeId,
    createView: exposedCreateView,
    editView: exposedEditView,
    listView: exposedListView,
    routes: [
      <Route
        path={`/${slug}`}
        component={exposedListView}
        key={`${camelised}__list`}
      />,
      <Route
        path={`/${slug}/new`}
        component={exposedCreateView}
        key={`${camelised}__create`}
      />,
      <Route
        path={`/${slug}/:${routeId}`}
        component={exposedEditView}
        key={`${camelised}__edit`}
      />,
    ],
  };
}

export default createViews;
