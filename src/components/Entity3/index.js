/**
 * @Author: benbriggs
 * @Date:   2018-05-21T12:16:44+01:00
 * @Email:  ben.briggs@thedistance.co.uk
 * @Last modified by:   benbriggs
 * @Last modified time: 2018-09-10T10:47:41+01:00
 * @Copyright: The Distance
 */

import * as R from 'ramda';
import createReducer from './../../util/createReducer';
import createInflections from './../Entity/inflections';
import createObjectFromSchemaDefaults from './../Entity/createObjectFromSchemaDefaults';
import convertSchemaToFields from './convertSchemaToFields';
import convertSchemaToIndex from './convertSchemaToIndex';
import filterSchemaByPropExists from './../Entity/filterSchemaByPropExists';
import filterSchemaByPropNotExists from './../Entity/filterSchemaByPropNotExists';
import create from './actions/create';
import destroy from './actions/destroy';
import loadCollection from './actions/loadCollection';
import loadCurrent from './actions/loadCurrent';
import revert from './actions/revert';
import save from './actions/save';
import unloadCollection from './../Entity2/actions/unloadCollection';
import unloadCurrent from './../Entity2/actions/unloadCurrent';
import update from './actions/update';
import createActionCreators from './../Entity2/createActionCreators';
import createActionIds from './../Entity2/createActionIds';
import createReducerHandlers from './createReducerHandlers';
import createSelectors from './../Entity2/createSelectors';
import createViews from './createViews';
import placeholder from './../Entity2/placeholder';

const callWith = R.flip(R.binary(R.call));

const _startActions = {
  deletingAction: `deleting`,
  loadingCollectionAction: `loading:collection`,
  loadingCurrentAction: `loading:current`,
  savingAction: `saving`,
};

const _endActions = {
  deletedAction: `deleted`,
  deleteFailedAction: `delete:failed`,
  loadedCollectionAction: `loaded:collection`,
  loadCollectionFailedAction: `load:collection:failed`,
  loadedCurrentAction: `loaded:current`,
  savedAction: `saved`,
  saveFailedAction: `save:failed`,
};

const _syncActions = {
  loadedCurrentSyncAction: `loaded:current`,
  newAction: `new`,
  revertAction: `revert`,
  unloadCollectionAction: `unload:collection`,
  unloadCurrentAction: `unload:current`,
  updateAction: `update`,
};

const createEntity = ({
  actions = {},
  actionCreators = {},
  findAll,
  findOne,
  name,
  className = name,
  inflections = {},
  readOnly,
  reducerHandlers = {},
  reducerState = {
    collection: null,
    current: null,
    pages: null,
    error: null,
    permissions: null,
    saved: null,
  },
  schema = {},
  selectors,
  createConnector = placeholder,
  editConnector = placeholder,
  listConnector = placeholder,
  createView,
  editView,
  listView,
  extraActions = placeholder,
  extraCreateActions = placeholder,
  extraEditActions = placeholder,
  warnOnDelete = false,
}) => {
  // 1. Inflections
  const { camelised, singular, plural, slug } = createInflections(
    name,
    inflections
  );

  // 2. Schema
  const filteredSchema = filterSchemaByPropNotExists('hideInEdit', schema);
  const createSchemaFields = convertSchemaToFields(filteredSchema, 'create');
  const editSchemaFields = convertSchemaToFields(filteredSchema, 'edit');

  const collectionIndex = convertSchemaToIndex(
    filterSchemaByPropExists('showInIndex', schema)
  );

  // 3. Selectors
  const mergedSelectors = createSelectors(
    [camelised],
    R.merge(
      {
        selectCollection: ['collection'],
        selectCurrent: ['current'],
        selectError: ['error'],
        selectErrorMessage: ['error', 'message'],
        selectPages: ['pages'],
        selectPermissions: ['permissions'],
        selectSaved: ['saved'],
      },
      selectors
    )
  );

  const { selectCollection, selectCurrent } = mergedSelectors;

  // 4. Actions
  const namespacedActionIds = createActionIds(
    R.mergeDeepRight(
      {
        className,
        startActions: _startActions,
        endActions: _endActions,
        syncActions: _syncActions,
      },
      actionCreators
    )
  );

  const actionIds = R.compose(R.mergeAll, R.values)(namespacedActionIds);

  const mergedActionCreators = createActionCreators(namespacedActionIds);

  const mergedActions = R.compose(
    R.mapObjIndexed((value, key) =>
      R.ifElse(
        R.contains(key),
        R.always(R.identity(value)),
        R.always(
          callWith(
            {
              actionCreators: mergedActionCreators,
              selectors: mergedSelectors,
            },
            value
          )
        )
      )([
        'create',
        'destroy',
        'loadCollection',
        'loadCurrent',
        'revert',
        'save',
        'unloadCollection',
        'unloadCurrent',
        'update',
      ])
    ),
    R.evolve({
      create: callWith({
        action: mergedActionCreators.newAction,
        className,
        defaultValues: createObjectFromSchemaDefaults(schema),
      }),
      destroy: callWith({
        startedAction: mergedActionCreators.deletingAction,
        completedAction: mergedActionCreators.deletedAction,
        failedAction: mergedActionCreators.deleteFailedAction,
        selectCurrent,
        actionCreators: mergedActionCreators,
        selectors: mergedSelectors,
      }),
      loadCollection: callWith({
        findAll,
        startedAction: mergedActionCreators.loadingCollectionAction,
        completedAction: mergedActionCreators.loadedCollectionAction,
        failedAction: mergedActionCreators.loadCollectionFailedAction,
        actionCreators: mergedActionCreators,
        selectors: mergedSelectors,
      }),
      loadCurrent: callWith({
        findOne,
        startedAction: mergedActionCreators.loadingCurrentAction,
        startedSyncAction: mergedActionCreators.loadedCurrentSyncAction,
        completedAction: mergedActionCreators.loadedCurrentAction,
        selectCurrent,
        actionCreators: mergedActionCreators,
        selectors: mergedSelectors,
      }),
      revert: callWith({
        action: mergedActionCreators.revertAction,
        selectCurrent,
        actionCreators: mergedActionCreators,
        selectors: mergedSelectors,
      }),
      save: callWith({
        selectCurrent,
        startedAction: mergedActionCreators.savingAction,
        completedAction: mergedActionCreators.savedAction,
        failedAction: mergedActionCreators.saveFailedAction,
        actionCreators: mergedActionCreators,
        selectors: mergedSelectors,
      }),
      unloadCollection: callWith({
        selectCollection,
        action: mergedActionCreators.unloadCollectionAction,
        actionCreators: mergedActionCreators,
        selectors: mergedSelectors,
      }),
      unloadCurrent: callWith({
        selectCurrent,
        action: mergedActionCreators.unloadCurrentAction,
        actionCreators: mergedActionCreators,
        selectors: mergedSelectors,
      }),
      update: callWith({
        selectCurrent,
        action: mergedActionCreators.updateAction,
        actionCreators: mergedActionCreators,
        selectors: mergedSelectors,
      }),
    }),
    R.merge({
      create,
      destroy,
      loadCollection,
      loadCurrent,
      revert,
      save,
      unloadCollection,
      unloadCurrent,
      update,
    })
  )(actions);

  // 5. Reducer
  const reducer = createReducer(
    reducerState,
    R.merge(createReducerHandlers(actionIds), reducerHandlers)
  );

  // 6. Views

  return createViews(
    {
      // Actions
      actions: mergedActions,
      actionCreators: mergedActionCreators,
      actionIds,
      // Inflections
      camelised,
      plural,
      singular,
      slug,
      // Read only?
      readOnly,
      // Reducer
      reducer,
      // Schema
      collectionIndex,
      createSchemaFields,
      editSchemaFields,
      // Selectors
      selectors: mergedSelectors,
    },
    {
      createConnector,
      editConnector,
      listConnector,
      createView,
      editView,
      listView,
      extraActions: callWith(mergedActions, extraActions),
      extraCreateActions: callWith(mergedActions, extraCreateActions),
      extraEditActions: callWith(mergedActions, extraEditActions),
      warnOnDelete,
    }
  );
};

export default createEntity;
