/**
 * @Author: benbriggs
 * @Date:   2018-05-22T15:53:29+01:00
 * @Email:  ben.briggs@thedistance.co.uk
 * @Last modified by:   benbriggs
 * @Last modified time: 2018-05-22T15:54:31+01:00
 * @Copyright: The Distance
 */

import Parse from 'parse';

const loadCurrent = ({
  findOne,
  startedSyncAction,
  startedAction,
  completedAction,
  selectCurrent,
}) => ({ id } = {}) => (dispatch, getState) => {
  const current = selectCurrent(getState());

  if (current) {
    return Promise.resolve(
      dispatch(
        startedSyncAction({
          result: current,
        })
      )
    );
  }

  if (!id) {
    return;
  }

  dispatch(startedAction());

  return Parse.Cloud.run(findOne, { id }).then(result => {
    dispatch(completedAction({ result }));
    return result;
  });
};

export default loadCurrent;
