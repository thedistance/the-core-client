/**
 * @Author: benbriggs
 * @Date:   2018-04-30T11:46:26+01:00
 * @Email:  ben.briggs@thedistance.co.uk
 * @Last modified by:   benbriggs
 * @Last modified time: 2018-05-23T08:50:35+01:00
 * @Copyright: The Distance
 */

import Parse from 'parse';
import redirect from '../../Entity/redirect';

const save = ({
  selectCurrent,
  startedAction,
  completedAction,
  failedAction,
}) => redirectOptions => (dispatch, getState) => {
  const result = selectCurrent(getState());
  dispatch(startedAction({ result }));

  return result.object
    .save()
    .then(() => dispatch(completedAction({ result })))
    .then(() => redirectOptions && redirect(redirectOptions))
    .catch(error => {
      if (error instanceof Parse.Error) {
        return dispatch(failedAction({ error }));
      }
      // Handle large file error
      if (error.status === 413) {
        return dispatch(
          failedAction({
            error: {
              code: error.status,
              message: 'The file you tried to upload is too large.',
            },
          })
        );
      }
      // Other http error
      return dispatch(
        failedAction({
          error: { code: error.status, message: error.statusText },
        })
      );
    });
};

export default save;
