/**
 * @Author: benbriggs
 * @Date:   2018-04-30T11:50:47+01:00
 * @Email:  ben.briggs@thedistance.co.uk
 * @Last modified by:   benbriggs
 * @Last modified time: 2018-05-23T08:50:44+01:00
 * @Copyright: The Distance
 */

const update = ({ selectCurrent, action }) => ({ key, value }) => (
  dispatch,
  getState
) => {
  const result = selectCurrent(getState());
  result.object.set(key, value);
  return dispatch(action({ result }));
};

export default update;
