/**
 * @Author: benbriggs
 * @Date:   2018-04-30T11:21:46+01:00
 * @Email:  ben.briggs@thedistance.co.uk
 * @Last modified by:   benbriggs
 * @Last modified time: 2018-05-23T08:50:09+01:00
 * @Copyright: The Distance
 */

import * as R from 'ramda';
import redirect from '../../Entity/redirect';

const parseDestroy = object =>
  new Promise((resolve, reject) => {
    object.destroy({
      success: obj => resolve(obj),
      error: err => reject(err),
    });
  });

const destroy = ({
  startedAction,
  completedAction,
  failedAction,
  selectCurrent,
}) => redirectOptions => (dispatch, getState) => {
  const result = selectCurrent(getState());
  dispatch(startedAction({ result }));

  return parseDestroy(result.object)
    .then(() => dispatch(completedAction({ result })))
    .then(R.always(redirectOptions))
    .then(redirect)
    .catch(error => dispatch(failedAction({ error })));
};

export default destroy;
