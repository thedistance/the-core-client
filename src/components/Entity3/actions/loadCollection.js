/**
 * @Author: benbriggs
 * @Date:   2018-05-22T15:45:35+01:00
 * @Email:  ben.briggs@thedistance.co.uk
 * @Last modified by:   benbriggs
 * @Last modified time: 2018-05-24T12:05:03+01:00
 * @Copyright: The Distance
 */

import Parse from 'parse';

const loadCollection = ({
  findAll,
  startedAction,
  completedAction,
  failedAction,
}) => ({ pageSize = 20, page = 0, sorted, filtered } = {}) => dispatch => {
  dispatch(startedAction());

  return Parse.Cloud.run(findAll, { pageSize, page, sorted, filtered })
    .then(result => dispatch(completedAction(result)))
    .catch(error => dispatch(failedAction({ error })));
};

export default loadCollection;
