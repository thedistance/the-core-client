/**
 * @Author: benbriggs
 * @Date:   2018-04-30T10:57:31+01:00
 * @Email:  ben.briggs@thedistance.co.uk
 * @Last modified by:   benbriggs
 * @Last modified time: 2018-05-23T15:54:46+01:00
 * @Copyright: The Distance
 */

import Parse from 'parse';
import * as R from 'ramda';

const create = ({ action, className, defaultValues = {} }) => (
  params = {}
) => dispatch => {
  const Instance = Parse.Object.extend(className);
  return dispatch(
    action({
      result: {
        object: new Instance(R.merge(defaultValues, params)),
        permissions: {
          create: true,
          update: true,
          delete: true,
        },
      },
    })
  );
};

export default create;
