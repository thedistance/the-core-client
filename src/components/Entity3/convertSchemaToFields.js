/**
 * @Author: benbriggs
 * @Date:   2018-05-22T15:09:32+01:00
 * @Email:  ben.briggs@thedistance.co.uk
 * @Last modified by:   benbriggs
 * @Last modified time: 2018-05-22T16:45:12+01:00
 * @Copyright: The Distance
 */

import { titleize } from 'inflected';
import fieldToComponent from '../Entity/fieldToComponent';
import fieldToReadOnlyComponent from '../Entity/fieldToReadOnlyComponent';
import parseObjectAccessor from './parseObjectAccessor';

const convertSchemaToFields = (schema, view) =>
  Object.keys(schema).map(key => ({
    accessor: parseObjectAccessor(
      schema[key].editAccessor || schema[key].accessor,
      key
    ),
    EditComponent: fieldToComponent(schema[key], view),
    ReadComponent: fieldToReadOnlyComponent(schema[key], view),
    key,
    label: schema[key].label || titleize(key),
    validator: schema[key].validator,
    helpText: schema[key].helpText,
  }));

export default convertSchemaToFields;
