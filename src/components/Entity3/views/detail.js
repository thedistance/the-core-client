/**
 * @Author: benbriggs
 * @Date:   2018-05-22T14:30:19+01:00
 * @Email:  ben.briggs@thedistance.co.uk
 * @Last modified by:   benbriggs
 * @Last modified time: 2018-09-10T10:53:58+01:00
 * @Copyright: The Distance
 */

import React from 'react';
import * as R from 'ramda';
import PropTypes from 'prop-types';
import {
  Grid,
  Row,
  Col,
  Button,
  ButtonToolbar,
  Form,
  Modal,
} from '@sketchpixy/rubix';
import Alert from '../../Alert';
import Panel from '../../Panel';

const EditActions = ({ actions }) =>
  actions.delete ? (
    <Button bsStyle="danger" onClick={actions.delete}>
      Delete
    </Button>
  ) : (
    <Button onClick={actions.cancel}>Cancel</Button>
  );

EditActions.propTypes = {
  actions: PropTypes.objectOf(PropTypes.func).isRequired,
};

class EditView extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      saved: true,
      showModal: false,
    };

    this.closeModal = this.closeModal.bind(this);
    this.openModal = this.openModal.bind(this);
    this.deleteFromModal = this.deleteFromModal.bind(this);
  }

  onChange(key, value) {
    this.setState({ saved: false });
    this.props.actions.update({ key, value });
  }

  componentWillMount() {
    this.props.actions.load();
  }

  componentWillUnmount() {
    if (!this.state.saved) {
      this.props.actions.revert();
    }
    this.props.actions.unload();
  }

  closeModal() {
    this.setState({ showModal: false });
  }

  openModal() {
    this.setState({ showModal: true });
  }

  deleteFromModal() {
    this.props.actions.delete();
    this.closeModal();
  }

  save() {
    this.setState({ saved: true });
    this.props.actions.save();
  }

  render() {
    const {
      actions,
      alert,
      model,
      fields,
      saved,
      singular,
      warnOnDelete,
      hideDelete,
    } = this.props;
    const savedOverride = R.path(
      ['location', 'state', 'savedOverride'],
      this.props
    );
    if (!model) {
      return null;
    }
    const { object } = model;
    return (
      <div>
        {warnOnDelete ? (
          <Modal show={this.state.showModal} onHide={this.closeModal}>
            <Modal.Body>
              <p>
                Are you sure that you want to delete this record? This cannot be
                undone.
              </p>
            </Modal.Body>
            <Modal.Footer>
              <ButtonToolbar>
                <Button onClick={this.closeModal}>Cancel</Button>
                <Button bsStyle="danger" onClick={this.deleteFromModal}>
                  Delete
                </Button>
              </ButtonToolbar>
            </Modal.Footer>
          </Modal>
        ) : null}
        {alert ? <Alert danger>{alert}</Alert> : null}
        {saved || savedOverride ? (
          <Alert success>Saved successfully.</Alert>
        ) : null}
        <Panel
          title={object.isNew() ? `New ${singular}` : `${singular} Details`}
        >
          <Grid>
            <Row>
              <Col xs={12}>
                <Form autoComplete="off">
                  {fields.map(
                    (
                      {
                        label,
                        accessor,
                        key,
                        validator,
                        helpText,
                        EditComponent,
                        ReadComponent,
                      },
                      index
                    ) => {
                      const Component = R.path(['permissions', 'update'], model)
                        ? EditComponent
                        : ReadComponent;

                      return (
                        <Component
                          actions={actions}
                          key={index}
                          label={label}
                          value={accessor(model)}
                          validator={validator}
                          helpText={helpText}
                          onChange={this.onChange.bind(this, key)}
                        />
                      );
                    }
                  )}
                  <Grid style={{ marginBottom: '1em' }}>
                    <Row>
                      <Col xs={6}>
                        <ButtonToolbar>
                          <Button onClick={actions.cancel}>Cancel</Button>
                          {R.path(['permissions', 'delete'], model) &&
                            !hideDelete && (
                              <Button
                                bsStyle="danger"
                                onClick={
                                  warnOnDelete ? this.openModal : actions.delete
                                }
                              >
                                Delete
                              </Button>
                            )}
                        </ButtonToolbar>
                      </Col>
                      {R.path(['permissions', 'update'], model) && (
                        <Col xs={6} style={{ textAlign: 'right' }}>
                          <Button
                            bsStyle="primary"
                            onClick={this.save.bind(this)}
                          >
                            Save
                          </Button>
                        </Col>
                      )}
                    </Row>
                  </Grid>
                </Form>
              </Col>
            </Row>
          </Grid>
        </Panel>
      </div>
    );
  }
}

EditView.propTypes = {
  actions: PropTypes.objectOf(PropTypes.func).isRequired,
  alert: PropTypes.string,
  fields: PropTypes.array.isRequired,
  model: PropTypes.object,
  saved: PropTypes.bool,
  singular: PropTypes.string.isRequired,
  warnOnDelete: PropTypes.bool,
};

export default EditView;
