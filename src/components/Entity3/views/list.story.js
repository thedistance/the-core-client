/**
 * @Author: benbriggs
 * @Date:   2018-05-21T12:27:26+01:00
 * @Email:  ben.briggs@thedistance.co.uk
 * @Last modified by:   benbriggs
 * @Last modified time: 2018-05-23T14:48:27+01:00
 * @Copyright: The Distance
 */

import React from 'react';
import { action } from '@storybook/addon-actions';
import { storiesOf } from '@storybook/react';
import { withInfo } from '@storybook/addon-info';
import backdrop from '../../../../.storybook/backdrop';
import ListView from './list';

storiesOf('Entity3->ListView', module)
  .addDecorator(backdrop)
  .add(
    'create permissions',
    withInfo()(() => (
      <ListView
        actions={{
          load: () => Promise.resolve(),
          unload: action('Entity3->ListView:unload'),
        }}
        columns={[
          {
            Header: 'Name',
            accessor: 'object.name',
          },
          {
            Header: 'Age',
            accessor: 'object.age',
          },
        ]}
        data={[
          {
            object: {
              name: 'John Smith',
              age: 27,
              id: '1',
            },
            permissions: {
              create: true,
              update: true,
              delete: true,
            },
          },
          {
            object: {
              name: 'Joe Bloggs',
              age: 25,
              id: '2',
            },
            permissions: {
              create: true,
              update: true,
              delete: true,
            },
          },
        ]}
        pages={1}
        manual={false}
        route={{ path: '/collection' }}
        singular={`Collection`}
        plural={`Collection`}
        permissions={{
          create: true,
          update: true,
          delete: true,
        }}
      />
    ))
  )
  .add(
    'read permissions',
    withInfo()(() => (
      <ListView
        actions={{
          load: () => Promise.resolve(),
          unload: action('Entity3->ListView:unload'),
        }}
        columns={[
          {
            Header: 'Name',
            accessor: 'object.name',
          },
          {
            Header: 'Age',
            accessor: 'object.age',
          },
        ]}
        data={[
          {
            object: {
              name: 'John Smith',
              age: 27,
              id: '1',
            },
            permissions: {},
          },
          {
            object: {
              name: 'Joe Bloggs',
              age: 25,
              id: '2',
            },
            permissions: {},
          },
        ]}
        pages={1}
        manual={false}
        route={{ path: '/collection' }}
        singular={`Collection`}
        plural={`Collection`}
      />
    ))
  )
  .add(
    'custom filter header props',
    withInfo()(() => (
      <ListView
        actions={{
          load: () => Promise.resolve(),
          unload: action('Entity3->ListView:unload'),
        }}
        getTheadFilterThProps={() => ({
          style: { backgroundColor: 'yellow' },
        })}
        columns={[
          {
            Header: 'Name',
            accessor: 'object.name',
          },
          {
            Header: 'Age',
            accessor: 'object.age',
          },
        ]}
        data={[
          {
            object: {
              name: 'John Smith',
              age: 27,
              id: '1',
            },
            permissions: {},
          },
          {
            object: {
              name: 'Joe Bloggs',
              age: 25,
              id: '2',
            },
            permissions: {},
          },
        ]}
        pages={1}
        manual={false}
        route={{ path: '/collection' }}
        singular={`Collection`}
        plural={`Collection`}
      />
    ))
  );
