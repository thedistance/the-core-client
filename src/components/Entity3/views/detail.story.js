/**
 * @Author: benbriggs
 * @Date:   2018-05-22T14:36:53+01:00
 * @Email:  ben.briggs@thedistance.co.uk
 * @Last modified by:   benbriggs
 * @Last modified time: 2018-09-10T10:53:18+01:00
 * @Copyright: The Distance
 */

import React from 'react';
import * as R from 'ramda';
import { action } from '@storybook/addon-actions';
import { storiesOf } from '@storybook/react';
import { withInfo } from '@storybook/addon-info';
import backdrop from '../../../../.storybook/backdrop';
import TextControl from '../../TextControl';
import StaticTextControl from '../../StaticTextControl';
import DetailView from './detail';

const accessor = R.path(['object', 'name']);

storiesOf('Entity3->DetailView', module)
  .addDecorator(backdrop)
  .add(
    'write permissions (new)',
    withInfo()(() => (
      <DetailView
        actions={{
          load: action('Entity3->DetailView:load'),
          unload: action('Entity3->DetailView:unload'),
          delete: action('Entity3->DetailView:delete'),
        }}
        model={{
          object: {
            name: 'John Smith',
            age: 27,
            id: '1',
            isNew: R.T,
          },
          permissions: {
            create: true,
            delete: true,
            update: true,
          },
        }}
        fields={[
          {
            accessor,
            key: 'name',
            label: 'Name',
            EditComponent: TextControl,
            ReadComponent: StaticTextControl,
          },
        ]}
        manual={false}
        route={{ path: '/collection' }}
        singular={`User`}
      />
    ))
  )
  .add(
    'read permissions (new)',
    withInfo()(() => (
      <DetailView
        actions={{
          load: action('Entity3->DetailView:load'),
          unload: action('Entity3->DetailView:unload'),
        }}
        model={{
          object: {
            name: 'John Smith',
            age: 27,
            id: '1',
            isNew: R.T,
          },
          permissions: {},
        }}
        fields={[
          {
            accessor,
            key: 'name',
            label: 'Name',
            EditComponent: TextControl,
            ReadComponent: StaticTextControl,
          },
        ]}
        title={`Model`}
        manual={false}
        route={{ path: '/collection' }}
        singular={`User`}
      />
    ))
  )
  .add(
    'delete confirmation',
    withInfo()(() => (
      <DetailView
        actions={{
          load: action('Entity3->DetailView:load'),
          unload: action('Entity3->DetailView:unload'),
          delete: action('Entity3->DetailView:delete'),
        }}
        model={{
          object: {
            name: 'John Smith',
            age: 27,
            id: '1',
            isNew: R.F,
          },
          permissions: {
            create: true,
            delete: true,
            update: true,
          },
        }}
        fields={[
          {
            accessor,
            key: 'name',
            label: 'Name',
            EditComponent: TextControl,
            ReadComponent: StaticTextControl,
          },
        ]}
        manual={false}
        route={{ path: '/collection' }}
        singular={`User`}
        warnOnDelete={true}
      />
    ))
  )
  .add(
    'write permissions (exists)',
    withInfo()(() => (
      <DetailView
        actions={{
          load: action('Entity3->DetailView:load'),
          unload: action('Entity3->DetailView:unload'),
          delete: action('Entity3->DetailView:delete'),
        }}
        model={{
          object: {
            name: 'John Smith',
            age: 27,
            id: '1',
            isNew: R.F,
          },
          permissions: {
            create: true,
            delete: true,
            update: true,
          },
        }}
        fields={[
          {
            accessor,
            key: 'name',
            label: 'Name',
            EditComponent: TextControl,
            ReadComponent: StaticTextControl,
          },
        ]}
        manual={false}
        route={{ path: '/collection' }}
        singular={`User`}
      />
    ))
  )
  .add(
    'read permissions (exists)',
    withInfo()(() => (
      <DetailView
        actions={{
          load: action('Entity3->DetailView:load'),
          unload: action('Entity3->DetailView:unload'),
        }}
        model={{
          object: {
            name: 'John Smith',
            age: 27,
            id: '1',
            isNew: R.F,
          },
          permissions: {},
        }}
        fields={[
          {
            accessor,
            key: 'name',
            label: 'Name',
            EditComponent: TextControl,
            ReadComponent: StaticTextControl,
          },
        ]}
        title={`Model`}
        manual={false}
        route={{ path: '/collection' }}
        singular={`User`}
      />
    ))
  )
  .add(
    'with alert',
    withInfo()(() => (
      <DetailView
        alert={'Something went wrong!'}
        actions={{
          load: action('Entity3->DetailView:load'),
          unload: action('Entity3->DetailView:unload'),
        }}
        model={{
          object: {
            name: 'John Smith',
            age: 27,
            id: '1',
            isNew: R.F,
          },
          permissions: {},
        }}
        fields={[
          {
            accessor,
            key: 'name',
            label: 'Name',
            EditComponent: TextControl,
            ReadComponent: StaticTextControl,
          },
        ]}
        title={`Model`}
        manual={false}
        route={{ path: '/collection' }}
        singular={`User`}
      />
    ))
  )
  .add(
    'with success message',
    withInfo()(() => (
      <DetailView
        saved={true}
        actions={{
          load: action('Entity3->DetailView:load'),
          unload: action('Entity3->DetailView:unload'),
        }}
        model={{
          object: {
            name: 'John Smith',
            age: 27,
            id: '1',
            isNew: R.F,
          },
          permissions: {},
        }}
        fields={[
          {
            accessor,
            key: 'name',
            label: 'Name',
            EditComponent: TextControl,
            ReadComponent: StaticTextControl,
          },
        ]}
        title={`Model`}
        manual={false}
        route={{ path: '/collection' }}
        singular={`User`}
      />
    ))
  );
