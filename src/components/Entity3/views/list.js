/**
 * @Author: benbriggs
 * @Date:   2018-05-21T12:22:37+01:00
 * @Email:  ben.briggs@thedistance.co.uk
 * @Last modified by:   benbriggs
 * @Last modified time: 2018-09-10T10:50:56+01:00
 * @Copyright: The Distance
 */

import React from 'react';
import * as R from 'ramda';
import PropTypes from 'prop-types';
import ReactTable from 'react-table';
import { Grid, Row, Col } from '@sketchpixy/rubix';
import getOrDotPath from 'the-core-utils/src/getOrDotPath';
import Alert from '../../Alert';
import Panel from '../../Panel';
import LinkButton from '../../LinkButton';
import 'react-table/react-table.css';

class ListView extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      loading: true,
    };
    this.fetchData = this.fetchData.bind(this);
  }

  componentWillUnmount() {
    this.props.actions.unload();
  }

  fetchData(tableState) {
    this.setState({ loading: true });

    this.props.actions.load(tableState).then(() => {
      this.setState({
        loading: false,
      });
    });
  }

  render() {
    const { loading } = this.state;
    const {
      alert,
      data,
      pages,
      columns,
      singular,
      plural,
      route,
      getTheadFilterThProps,
    } = this.props;
    const create = R.path(['permissions', 'create'], this.props);
    const createPath = this.props.createPath
      ? this.props.createPath(route)
      : `${route.path}/new`;
    return (
      <div>
        {alert && <Alert danger>{alert}</Alert>}
        <Panel
          title={plural}
          buttonTo={create ? createPath : null}
          buttonTitle={create ? `New ${singular}` : null}
        >
          <Grid>
            <Row>
              <Col xs={12}>
                <ReactTable
                  data={data}
                  pages={pages}
                  loading={loading}
                  getTheadFilterThProps={getTheadFilterThProps}
                  onFetchData={this.fetchData}
                  columns={R.concat(columns, [
                    {
                      accessor: getOrDotPath(['object', 'id']),
                      id: 'id',
                      Cell: props => {
                        const editPath = this.props.editPath
                          ? this.props.editPath(route, props)
                          : `${route.path}/${props.value}`;

                        return (
                          <LinkButton pathname={editPath} outlined>
                            {R.path(
                              ['original', 'permissions', 'update'],
                              props
                            )
                              ? 'Edit'
                              : 'View'}
                          </LinkButton>
                        );
                      },
                      filterable: false,
                      sortable: false,
                    },
                  ])}
                  style={{ marginTop: '10px', marginBottom: '40px' }}
                  filterable
                  manual
                />
              </Col>
            </Row>
          </Grid>
        </Panel>
      </div>
    );
  }
}

ListView.propTypes = {
  actions: PropTypes.objectOf(PropTypes.func).isRequired,
  alert: PropTypes.string,
  columns: PropTypes.array.isRequired,
  createPath: PropTypes.func,
  editPath: PropTypes.func,
  data: PropTypes.array,
  pages: PropTypes.number,
  route: PropTypes.any,
  saved: PropTypes.bool,
  singular: PropTypes.string.isRequired,
  plural: PropTypes.string.isRequired,
  getTheadFilterThProps: PropTypes.func,
};

export default ListView;
