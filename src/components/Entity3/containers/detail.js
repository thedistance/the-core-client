/**
 * @Author: benbriggs
 * @Date:   2018-05-22T15:34:36+01:00
 * @Email:  ben.briggs@thedistance.co.uk
 * @Last modified by:   benbriggs
 * @Last modified time: 2018-09-10T10:50:02+01:00
 * @Copyright: The Distance
 */

import * as R from 'ramda';
import { connect } from 'react-redux';
import { mapDispatch, mapState } from '../../Entity2/views/mapConnectors';
import DetailView from './../views/detail';

const editView = (View = DetailView) => ({
  actions,
  editConnector,
  save,
  destroy,
  loadCurrent,
  unloadCurrent,
  update,
  plural,
  routeId,
  revert,
  selectCurrent,
  selectErrorMessage,
  selectSaved,
  schemaFields,
  slug,
  singular,
  goBack = 1,
  extraActions,
  extraEditActions,
  warnOnDelete,
}) => {
  const mapStateToProps = state =>
    R.mergeDeepRight(
      {
        alert: selectErrorMessage(state),
        model: selectCurrent(state),
        fields: schemaFields,
        saved: selectSaved(state),
        plural,
        singular,
        slug,
        warnOnDelete,
      },
      mapState(editConnector)(state)
    );

  const mapDispatchToProps = (dispatch, ownProps) =>
    R.mergeDeepRight(
      {
        actions: {
          load: () =>
            dispatch(loadCurrent({ id: ownProps.routeParams[routeId] })),
          save: () =>
            dispatch(
              save({
                pathname: ownProps.location.pathname,
                router: ownProps.router,
                goBack,
              })
            ),
          delete: () =>
            dispatch(
              destroy({
                pathname: ownProps.location.pathname,
                router: ownProps.router,
                goBack,
              })
            ),
          cancel: () => {
            const routeParts = ownProps.location.pathname
              .split('/')
              .slice(0, goBack * -1);
            ownProps.router.push(routeParts.join('/'));
          },
          unload: () => dispatch(unloadCurrent()),
          update: options => dispatch(update(options)),
          revert: () => dispatch(revert()),
          ...extraActions(dispatch, ownProps),
          ...extraEditActions(dispatch, ownProps),
        },
      },
      mapDispatch(editConnector)(actions)(dispatch, ownProps)
    );

  return connect(mapStateToProps, mapDispatchToProps)(View);
};

export default editView;
