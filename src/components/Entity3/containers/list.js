/**
 * @Author: benbriggs
 * @Date:   2018-05-22T15:22:32+01:00
 * @Email:  ben.briggs@thedistance.co.uk
 * @Last modified by:   benbriggs
 * @Last modified time: 2018-09-03T12:59:52+01:00
 * @Copyright: The Distance
 */

import * as R from 'ramda';
import { connect } from 'react-redux';
import { mapDispatch, mapState } from '../../Entity2/views/mapConnectors';
import _ListView from '../views/list';

const listView = (View = _ListView) => ({
  actions,
  listConnector,
  loadCollection,
  unloadCollection,
  selectPermissions,
  selectCollection,
  selectPages,
  selectErrorMessage,
  slug,
  singular,
  plural,
  collectionIndex,
}) => {
  const mapStateToProps = state =>
    R.mergeDeepRight(
      {
        slug,
        singular,
        plural,
        columns: collectionIndex,
        permissions: selectPermissions(state),
        data: selectCollection(state) || [],
        pages: selectPages(state),
        alert: selectErrorMessage(state),
      },
      mapState(listConnector)(state)
    );

  const mapDispatchToProps = (dispatch, ownProps) =>
    R.mergeDeepRight(
      {
        actions: {
          load: tableState => dispatch(loadCollection(tableState)),
          unload: () => dispatch(unloadCollection()),
        },
      },
      mapDispatch(listConnector)(actions)(dispatch, ownProps)
    );

  return connect(mapStateToProps, mapDispatchToProps)(View);
};

export default listView;
