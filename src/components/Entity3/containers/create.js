/**
 * @Author: benbriggs
 * @Date:   2018-05-22T15:31:28+01:00
 * @Email:  ben.briggs@thedistance.co.uk
 * @Last modified by:   benbriggs
 * @Last modified time: 2018-09-10T10:49:48+01:00
 * @Copyright: The Distance
 */

import * as R from 'ramda';
import { connect } from 'react-redux';
import { mapDispatch, mapState } from '../../Entity2/views/mapConnectors';
import DetailView from './../views/detail';

const createView = (View = DetailView) => ({
  actions,
  createConnector,
  create,
  save,
  unloadCurrent,
  update,
  plural,
  revert,
  selectCurrent,
  selectErrorMessage,
  selectSaved,
  schemaFields,
  slug,
  singular,
  goBack = 1,
  extraActions,
  extraCreateActions,
}) => {
  const mapStateToProps = state =>
    R.mergeDeepRight(
      {
        alert: selectErrorMessage(state),
        model: selectCurrent(state),
        fields: schemaFields,
        saved: selectSaved(state),
        plural,
        singular,
        slug,
      },
      mapState(createConnector)(state)
    );

  const mapDispatchToProps = (dispatch, ownProps) =>
    R.mergeDeepRight(
      {
        actions: {
          load: () => dispatch(create()),
          save: () =>
            dispatch(
              save({
                pathname: ownProps.location.pathname,
                router: ownProps.router,
                goBack,
              })
            ),
          cancel: () => {
            const routeParts = ownProps.location.pathname
              .split('/')
              .slice(0, goBack * -1);
            ownProps.router.push(routeParts.join('/'));
          },
          unload: () => dispatch(unloadCurrent()),
          update: options => dispatch(update(options)),
          revert: () => dispatch(revert()),
          ...extraActions(dispatch, ownProps),
          ...extraCreateActions(dispatch, ownProps),
        },
      },
      mapDispatch(createConnector)(actions)(dispatch, ownProps)
    );

  return connect(mapStateToProps, mapDispatchToProps)(View);
};

export default createView;
