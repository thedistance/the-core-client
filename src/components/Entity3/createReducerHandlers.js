/**
 * @Author: benbriggs
 * @Date:   2018-05-22T16:09:02+01:00
 * @Email:  ben.briggs@thedistance.co.uk
 * @Last modified by:   benbriggs
 * @Last modified time: 2018-09-10T11:03:46+01:00
 * @Copyright: The Distance
 */

import * as R from 'ramda';

const hasError = (state, { error }) => R.merge(state, { error, saved: null });
const nullCurrent = R.mergeDeepLeft({
  current: null,
  error: null,
  saved: null,
});
const updateCurrent = (state, { result }) =>
  R.merge(state, { current: result, saved: null });

const createReducerHandlers = ({
  deletedAction,
  deleteFailedAction,
  loadedCurrentAction,
  loadedCollectionAction,
  loadCollectionFailedAction,
  newAction,
  revertAction,
  savingAction,
  savedAction,
  saveFailedAction,
  unloadCollectionAction,
  unloadCurrentAction,
  updateAction,
}) => ({
  [deletedAction]: nullCurrent,
  [deleteFailedAction]: hasError,
  [loadedCurrentAction]: updateCurrent,
  [loadedCollectionAction]: (state, { data, pages, permissions }) =>
    R.merge(state, { collection: data, pages, permissions }),
  [loadCollectionFailedAction]: hasError,
  [newAction]: updateCurrent,
  [revertAction]: nullCurrent,
  [savingAction]: R.mergeDeepLeft({ saved: null }),
  [savedAction]: R.mergeDeepLeft({ saved: true }),
  [saveFailedAction]: hasError,
  [unloadCollectionAction]: R.mergeDeepLeft({
    collection: null,
    pages: null,
    error: null,
  }),
  [unloadCurrentAction]: nullCurrent,
  [updateAction]: updateCurrent,
});

export default createReducerHandlers;
