/**
 * @Author: benbriggs
 * @Date:   2018-08-29T09:23:12+01:00
 * @Email:  ben.briggs@thedistance.co.uk
 * @Last modified by:   benbriggs
 * @Last modified time: 2018-08-29T17:12:48+01:00
 * @Copyright: The Distance
 */

import React from 'react';
import * as R from 'ramda';
import { Checkbox, ControlLabel, FormControlFeedback } from '@sketchpixy/rubix';
import PropTypes from 'prop-types';
import FormGroup from '../FormGroup';
import GenericControl from '../GenericControl';

class CheckboxesControl extends GenericControl {
  handleChange({ target }) {
    const { checked, name: id } = target;
    const stateValue = this.state.value;

    const eqId = R.equals(id);

    const value = R.ifElse(
      R.always(checked),
      R.unless(R.any(eqId), R.append(id)),
      R.reject(eqId)
    )(stateValue);

    const valid = this.props.validator ? this.props.validator(value) : null;

    this.setState({
      value,
      valid,
    });

    this.props.onChange(value ? value : null, valid);
  }

  render() {
    const { value } = this.state;
    const { collection } = this.props;
    return (
      <FormGroup
        validationState={this.validationState()}
        required={this.props.required}
      >
        <ControlLabel>{this.props.label}</ControlLabel>
        {collection.map(c => (
          <Checkbox
            key={c.id}
            name={c.id}
            checked={R.contains(c.id, value)}
            onChange={this.handleChange}
          >
            {c.name}
          </Checkbox>
        ))}
        <FormControlFeedback />
      </FormGroup>
    );
  }
}

CheckboxesControl.defaultProps = {
  value: [],
};

CheckboxesControl.propTypes = {
  collection: PropTypes.array.isRequired,
  value: PropTypes.array,
};

export default CheckboxesControl;
