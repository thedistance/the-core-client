/**
 * @Author: benbriggs
 * @Date:   2018-08-29T09:43:23+01:00
 * @Email:  ben.briggs@thedistance.co.uk
 * @Last modified by:   benbriggs
 * @Last modified time: 2018-08-29T17:03:04+01:00
 * @Copyright: The Distance
 */

import React from 'react';
import { action } from '@storybook/addon-actions';
import { storiesOf } from '@storybook/react';
import { withInfo } from '@storybook/addon-info';
import backdrop from '../../../.storybook/backdrop';
import CheckboxesControl from './';

storiesOf('CheckboxesControl', module)
  .addDecorator(backdrop)
  .add(
    'component',
    withInfo()(() => (
      <CheckboxesControl
        collection={[
          {
            id: 'abc',
            name: 'Foo',
          },
          {
            id: 'xyz',
            name: 'Bar',
          },
        ]}
        onChange={action('CheckboxesControl:onChange')}
        value={['abc']}
      />
    ))
  )
  .add(
    'empty',
    withInfo()(() => (
      <CheckboxesControl
        collection={[
          {
            id: 'abc',
            name: 'Foo',
          },
          {
            id: 'xyz',
            name: 'Bar',
          },
        ]}
        onChange={action('CheckboxesControl:onChange')}
      />
    ))
  );
