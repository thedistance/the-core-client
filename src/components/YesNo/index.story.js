/**
 * @Author: Ben Briggs <benbriggs>
 * @Date:   2018-02-23T12:02:29+00:00
 * @Email:  ben.briggs@thedistance.co.uk
 * @Last modified by:   benbriggs
 * @Last modified time: 2018-02-23T12:04:29+00:00
 * @Copyright: The Distance
 */

import React from 'react';
import { storiesOf } from '@storybook/react';
import { withInfo } from '@storybook/addon-info';
import backdrop from '../../../.storybook/backdrop';
import YesNo from './';

storiesOf('YesNo', module)
  .addDecorator(backdrop)
  .add('yes', withInfo()(() => <YesNo value />))
  .add('no', withInfo()(() => <YesNo value={false} />));
