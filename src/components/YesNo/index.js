/**
 * @Author: Ben Briggs <benbriggs>
 * @Date:   2018-02-23T12:00:18+00:00
 * @Email:  ben.briggs@thedistance.co.uk
 * @Last modified by:   benbriggs
 * @Last modified time: 2018-02-23T12:01:07+00:00
 * @Copyright: The Distance
 */

import React from 'react';
import PropTypes from 'prop-types';

const YesNo = ({ value }) => <span>{value ? 'Yes' : 'No'}</span>;

YesNo.propTypes = {
  value: PropTypes.bool.isRequired,
};

export default YesNo;
