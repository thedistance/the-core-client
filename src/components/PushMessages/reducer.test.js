/**
 * @Author: Ben Briggs <benbriggs>
 * @Date:   2017-10-20T10:17:20+01:00
 * @Email:  ben.briggs@thedistance.co.uk
 * @Last modified by:   benbriggs
 * @Last modified time: 2018-01-11T14:13:54+00:00
 * @Copyright: The Distance
 */

import deepFreeze from 'deep-freeze';
import * as actions from './actions';
import reducer from './reducer';

const initialState = (overrides = {}) => ({
  collection: [],
  sent: null,
  error: null,
  pages: null,
  ...overrides,
});

test('should set initial state', () => {
  expect(reducer(undefined, {})).toEqual(initialState());
});

test('should handle collection load', () => {
  const before = initialState();
  const action = actions.loadedPushMessageAction({
    data: [{ payload: 'foo' }],
    pages: 1,
  });

  deepFreeze(before);
  deepFreeze(action);

  expect(reducer(before, action)).toEqual({
    ...before,
    collection: action.data,
    pages: 1,
  });
});

test('should handle sent push message', () => {
  const before = initialState();
  const action = actions.sentPushMessageAction({
    sent: true,
  });

  deepFreeze(before);
  deepFreeze(action);

  expect(reducer(before, action)).toEqual({
    ...before,
    sent: true,
  });
});

test('should handle errored push message', () => {
  const before = initialState();
  const action = actions.sendPushMessageFailedAction({
    error: {
      message: 'Unauthorised',
    },
  });

  deepFreeze(before);
  deepFreeze(action);

  expect(reducer(before, action)).toEqual({
    ...before,
    error: action.error,
    sent: false,
  });
});

test('should handle unload', () => {
  const before = initialState();
  const action = actions.unloadPushMessageAction();

  deepFreeze(before);
  deepFreeze(action);

  expect(reducer(before, action)).toEqual(initialState());
});
