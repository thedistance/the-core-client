/**
 * @Author: Ben Briggs <benbriggs>
 * @Date:   2018-01-10T10:00:00+00:00
 * @Email:  ben.briggs@thedistance.co.uk
 * @Last modified by:   benbriggs
 * @Last modified time: 2018-01-11T14:14:55+00:00
 * @Copyright: The Distance
 */

import React from 'react';
import { shallow } from 'enzyme';
import toJson from 'enzyme-to-json';
import { PushMessages } from './';

it('should render a push message view', () => {
  const mock = jest.fn();
  const component = shallow(
    <PushMessages
      actions={{ load: mock, unload: mock }}
      data={[
        {
          get(key) {
            switch (key) {
              case 'payload':
                return JSON.stringify({ alert: 'Hello, world' });
              case 'numSent':
                return 1;
              case 'status':
                return 'succeeded';
              case 'createdAt':
                return new Date();
              default:
                break;
            }
          },
        },
      ]}
      pages={1}
    />
  );

  expect(toJson(component.shallow())).toMatchSnapshot();
});

it('should render a push message view with an empty dataset', () => {
  const mock = jest.fn();
  const component = shallow(
    <PushMessages actions={{ load: mock, unload: mock }} data={[]} pages={0} />
  );

  expect(toJson(component.shallow())).toMatchSnapshot();
});
