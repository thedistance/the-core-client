/**
 * @Author: Ben Briggs <benbriggs>
 * @Date:   2018-01-10T09:35:09+00:00
 * @Email:  ben.briggs@thedistance.co.uk
 * @Last modified by:   benbriggs
 * @Last modified time: 2018-03-19T09:24:11+00:00
 * @Copyright: The Distance
 */

import React from 'react';
import ReactTable from 'react-table';
import { connect } from 'react-redux';
import { Grid, Row, Col } from '@sketchpixy/rubix';
import PropTypes from 'prop-types';
import Panel from '../Panel';
import Timeago from '../Timeago';
import * as actions from './actions';
import { selectCollection, selectPages } from './reducer';
import 'react-table/react-table.css';

const PushMessage = ({ value }) => <span>{JSON.parse(value).alert}</span>;

PushMessage.propTypes = {
  value: PropTypes.any,
};

export class PushMessages extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      loading: false,
    };
  }

  fetchData(tableState) {
    this.setState({ loading: true });

    this.props.actions.load(tableState).then(() => {
      this.setState({
        loading: false,
      });
    });
  }

  componentWillUnmount() {
    this.props.actions.unload();
  }

  render() {
    const { data, pages } = this.props;
    return (
      <Panel title={`Recent Messages`}>
        <Grid>
          <Row>
            <Col xs={12}>
              <ReactTable
                data={data}
                loading={this.state.loading}
                pages={pages}
                onFetchData={this.fetchData.bind(this)}
                columns={[
                  {
                    Header: 'Message',
                    accessor: obj => obj.get('payload'),
                    Cell: PushMessage,
                    id: 'payload',
                  },
                  {
                    Header: 'Sent',
                    accessor: obj => obj.get('numSent'),
                    id: 'numSent',
                  },
                  {
                    Header: 'Status',
                    accessor: obj => obj.get('status'),
                    id: 'status',
                  },
                  {
                    Header: 'When',
                    accessor: obj => obj.get('createdAt'),
                    Cell: Timeago,
                    id: 'createdAt',
                  },
                ]}
                manual
                filterable
                style={{ marginTop: '10px', marginBottom: '40px' }}
              />
            </Col>
          </Row>
        </Grid>
      </Panel>
    );
  }
}

PushMessages.propTypes = {
  actions: PropTypes.objectOf(PropTypes.func).isRequired,
  data: PropTypes.array,
  pages: PropTypes.number,
};

export const mapStateToProps = state => ({
  data: selectCollection(state),
  pages: selectPages(state),
});

export const mapDispatchToProps = dispatch => ({
  actions: {
    send: params => dispatch(actions.sendPushMessage(params)),
    load: tableState => dispatch(actions.loadPushMessageCollection(tableState)),
    unload: () => dispatch(actions.unloadPushMessage()),
  },
});

export default connect(mapStateToProps, mapDispatchToProps)(PushMessages);
