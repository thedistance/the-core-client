/**
 * @Author: Ben Briggs <benbriggs>
 * @Date:   2018-01-10T09:35:23+00:00
 * @Email:  ben.briggs@thedistance.co.uk
 * @Last modified by:   benbriggs
 * @Last modified time: 2018-01-11T11:57:41+00:00
 * @Copyright: The Distance
 */

import React from 'react';
import { action } from '@storybook/addon-actions';
import { storiesOf } from '@storybook/react';
import { withInfo } from '@storybook/addon-info';
import backdrop from '../../../.storybook/backdrop';
import { PushMessages } from './';

const actions = {
  load: () => Promise.resolve(action('PushMessages:load')),
  unload: action('PushMessages:unload'),
};

storiesOf('PushMessages', module)
  .addDecorator(backdrop)
  .add('No messages', withInfo()(() => <PushMessages actions={actions} />))
  .add(
    'Successful send',
    withInfo()(() => (
      <PushMessages
        actions={actions}
        data={[
          {
            get(key) {
              switch (key) {
                case 'payload':
                  return JSON.stringify({ alert: 'Hello, world' });
                case 'numSent':
                  return 1;
                case 'status':
                  return 'succeeded';
                case 'createdAt':
                  return new Date();
                default:
                  break;
              }
            },
          },
        ]}
        pages={1}
      />
    ))
  )
  .add(
    'Failed send',
    withInfo()(() => (
      <PushMessages
        actions={actions}
        data={[
          {
            get(key) {
              switch (key) {
                case 'payload':
                  return JSON.stringify({ alert: 'Hello, world' });
                case 'numSent':
                  return 0;
                case 'status':
                  return 'failed';
                case 'createdAt':
                  return new Date();
                default:
                  break;
              }
            },
          },
        ]}
        pages={1}
      />
    ))
  );
