/**
 * @Author: Ben Briggs <benbriggs>
 * @Date:   2017-10-19T15:30:54+01:00
 * @Email:  ben.briggs@thedistance.co.uk
 * @Last modified by:   benbriggs
 * @Last modified time: 2018-01-11T14:13:50+00:00
 * @Copyright: The Distance
 */

import createReducer from '../../util/createReducer';
import * as actions from './actions';

export default createReducer(
  { collection: [], sent: null, error: null, pages: null },
  {
    [actions.loadedPushMessage]: (state, { data, pages }) => ({
      ...state,
      collection: data,
      pages,
    }),
    [actions.sentPushMessage]: state => ({
      ...state,
      sent: true,
      error: null,
    }),
    [actions.sendPushMessageFailed]: (state, { error }) => ({
      ...state,
      sent: false,
      error,
    }),
    [actions.unloadAll]: () => ({
      collection: [],
      sent: null,
      error: null,
      pages: null,
    }),
  }
);

export const selectSent = state => state.pushMessage.sent;
export const selectError = state => state.pushMessage.error;
export const selectCollection = state => state.pushMessage.collection;
export const selectPages = state => state.pushMessage.pages;
