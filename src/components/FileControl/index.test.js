/**
 * @Author: Ben Briggs <benbriggs>
 * @Date:   2017-12-18T17:10:52+00:00
 * @Email:  ben.briggs@thedistance.co.uk
 * @Last modified by:   benbriggs
 * @Last modified time: 2018-01-11T09:46:46+00:00
 * @Copyright: The Distance
 */

import React from 'react';
import { shallow } from 'enzyme';
import toJson from 'enzyme-to-json';
import FileControl from './';

describe('FileControl', () => {
  test('should render', () => {
    const component = shallow(
      <FileControl label={`Upload file`} onChange={jest.fn()} />
    );

    expect(toJson(component.shallow())).toMatchSnapshot();
  });

  test('should render with an attached file', () => {
    const component = shallow(
      <FileControl
        label={`Upload file`}
        onChange={jest.fn()}
        value={{
          name: () => 'no_file_here.mp3',
          url: () => 'no_file_here.mp3',
        }}
      />
    );

    expect(toJson(component.shallow())).toMatchSnapshot();
  });
});
