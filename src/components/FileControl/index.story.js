/**
 * @Author: Ben Briggs <benbriggs>
 * @Date:   2017-12-18T15:49:44+00:00
 * @Email:  ben.briggs@thedistance.co.uk
 * @Last modified by:   benbriggs
 * @Last modified time: 2018-01-11T09:46:39+00:00
 * @Copyright: The Distance
 */

import React from 'react';
import { action } from '@storybook/addon-actions';
import { storiesOf } from '@storybook/react';
import backdrop from '../../../.storybook/backdrop';
import FileControl from './';

storiesOf('FileControl', module)
  .addDecorator(backdrop)
  .add('component', () => (
    <FileControl
      label={`Upload file`}
      onChange={action('FileControl:onChange')}
    />
  ))
  .add('with attached file', () => (
    <FileControl
      label={`Upload file`}
      onChange={action('FileControl:onChange')}
      value={{
        name: () => 'no_file_here.mp3',
        url: () => 'no_file_here.mp3',
      }}
    />
  ));
