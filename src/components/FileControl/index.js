/**
 * @Author: Ben Briggs <benbriggs>
 * @Date:   2017-12-18T15:49:01+00:00
 * @Email:  ben.briggs@thedistance.co.uk
 * @Last modified by:   benbriggs
 * @Last modified time: 2018-01-11T09:27:55+00:00
 * @Copyright: The Distance
 */

import React from 'react';
import Parse from 'parse';
import { FormControl, ControlLabel, Button } from '@sketchpixy/rubix';
import FormGroup from '../FormGroup';
import GenericControl from '../GenericControl';

const divStyle = {
  marginBottom: '10px',
};

class FileControl extends GenericControl {
  handleChange({ target }) {
    if (target.files.length > 0) {
      const file = target.files[0];

      const value = new Parse.File(file.name, file);
      this.setState({ value });
      this.props.onChange(value, true);
    }
  }

  deleteFile() {
    const value = null;
    this.setState({ value });
    this.props.onChange(value, true);
  }

  render() {
    return (
      <div>
        <FormGroup
          validationState={this.validationState()}
          required={this.props.required}
        >
          <ControlLabel>{this.props.label}</ControlLabel>
          <FormControl type="file" onChange={this.handleChange} />
        </FormGroup>
        {this.state.value && (
          <div style={divStyle}>
            <a href={this.state.value.url()} target="_blank">
              <Button bsStyle={`info`} outlined>
                Open {this.state.value.name()}
              </Button>
            </a>{' '}
            <Button
              bsStyle={`danger`}
              outlined
              onClick={this.deleteFile.bind(this)}
            >
              Delete file
            </Button>
          </div>
        )}
      </div>
    );
  }
}

export default FileControl;
