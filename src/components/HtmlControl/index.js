/**
 * @Author: Ben Briggs <benbriggs>
 * @Date:   2017-11-27T16:18:54+00:00
 * @Email:  ben.briggs@thedistance.co.uk
 * @Last modified by:   benbriggs
 * @Last modified time: 2018-01-11T09:53:22+00:00
 * @Copyright: The Distance
 */

import React from 'react';
import PropTypes from 'prop-types';
import { ControlLabel } from '@sketchpixy/rubix';
import { Editor } from 'react-draft-wysiwyg';
import 'react-draft-wysiwyg/dist/react-draft-wysiwyg.css';
import { ContentState, EditorState, convertToRaw } from 'draft-js';
import draftToHtml from 'draftjs-to-html';
import htmlToDraft from 'html-to-draftjs';
import FormGroup from '../FormGroup';
import GenericControl from '../GenericControl';

class HtmlControl extends GenericControl {
  constructor(props) {
    super(props);

    const { contentBlocks, entityMap } = htmlToDraft(props.value);
    const editor = EditorState.createWithContent(
      ContentState.createFromBlockArray(contentBlocks, entityMap)
    );

    this.state.editor = editor;
  }

  handleChange(editorState) {
    const value = draftToHtml(convertToRaw(editorState.getCurrentContent()));
    const valid = this.props.validator ? this.props.validator(value) : null;

    this.setState({
      editor: editorState,
      value,
      valid,
    });

    this.props.onChange(value, valid);
  }

  render() {
    return (
      <FormGroup
        validationState={this.validationState()}
        required={this.props.required}
      >
        <ControlLabel>{this.props.label}</ControlLabel>
        <Editor
          editorState={this.state.editor}
          onEditorStateChange={this.handleChange}
          toolbar={{
            options: [
              'inline',
              'blockType',
              'list',
              'textAlign',
              'colorPicker',
              'link',
              'emoji',
              'image',
              'remove',
              'history',
            ],
          }}
          editorClassName={`draftJs-wrapper`}
          toolbarClassName={`draftJs-wrapper`}
        />
      </FormGroup>
    );
  }
}

HtmlControl.propTypes = {
  value: PropTypes.string,
  onChange: PropTypes.func.isRequired,
};

HtmlControl.defaultProps = {
  value: '',
};

export default HtmlControl;
