/**
 * @Author: Ben Briggs <benbriggs>
 * @Date:   2017-11-27T16:18:54+00:00
 * @Email:  ben.briggs@thedistance.co.uk
 * @Last modified by:   benbriggs
 * @Last modified time: 2018-09-19T09:31:05+01:00
 * @Copyright: The Distance
 */

import React from 'react';
import PropTypes from 'prop-types';
import { ControlLabel } from '@sketchpixy/rubix';
import FroalaEditor from 'react-froala-wysiwyg';
import FormGroup from '../FormGroup';
import GenericControl from '../GenericControl';

import 'froala-editor/js/froala_editor.pkgd.min.js';
import 'froala-editor/css/froala_style.min.css';
import 'froala-editor/css/froala_editor.pkgd.min.css';
import 'font-awesome/css/font-awesome.css';
import $ from 'jquery';

window.$ = $;

class HtmlFroalaControl extends GenericControl {
  constructor(props) {
    super(props);

    const config = {
      // https://www.froala.com/wysiwyg-editor/docs/options
      // All the options can be added here to configure the editor
      key: process.env.FROALA_KEY,
      placeholderText: 'Edit Your Content Here!',
      charCounterCount: false,
      imageUploadURL: process.env.UPLOAD_URL,
      fileUploadURL: process.env.UPLOAD_URL,
      // https://www.froala.com/wysiwyg-editor/docs/options#linkInsertButtons
      linkInsertButtons: [],
      // https://www.froala.com/wysiwyg-editor/docs/options#linkList
      linkList: [],
      requestHeaders: {
        'X-Parse-Application-Id': process.env.APP_ID,
      },
      paragraphStyles: {
        'fr-text-gray': 'Gray',
        'fr-text-bordered': 'Bordered',
        'fr-text-spaced': 'Spaced',
        'fr-text-uppercase': 'Uppercase',
        highlightBox: 'Highlight box',
        quoteBox: 'Quote box',
        blockquote: 'Blockquote',
      },
      videoInsertButtons: ['videoBack', '|', 'videoByURL', 'videoEmbed'],
      // https://www.froala.com/wysiwyg-editor/docs/options#pluginsEnabled
      pluginsEnabled: [
        'align',
        'charCounter',
        'codeBeautifier',
        'codeView',
        'colors',
        'draggable',
        'embedly',
        'emoticons',
        //'entities', - removing entities plugin may have security implications
        // Froala also entity encodes HTML tags separately though, so this seems pretty hard to exploit
        'file',
        'fontFamily',
        'fontSize',
        'fullscreen',
        'image',
        'inlineStyle',
        'lineBreaker',
        'link',
        'lists',
        'paragraphFormat',
        'paragraphStyle',
        'quickInsert',
        'save',
        'table',
        'url',
        'video',
        'wordPaste',
      ],
      // https://www.froala.com/wysiwyg-editor/docs/options#toolbarButtons
      toolbarButtons: [
        'fullscreen',
        'bold',
        'italic',
        'underline',
        'strikeThrough',
        'subscript',
        'superscript',
        '|',
        'color',
        'paragraphStyle',
        '|',
        'paragraphFormat',
        'align',
        'formatOL',
        'formatUL',
        'outdent',
        'indent',
        '-',
        'insertLink',
        'insertImage',
        'insertVideo',
        'insertFile',
        'embedly',
        'insertTable',
        '|',
        'emoticons',
        'specialCharacters',
        'insertHR',
        'clearFormatting',
        '|',
        'spellChecker',
        'help',
        'html',
        '|',
        'undo',
        'redo',
      ],
    };

    if (process.env.FROALA_TOOLBAR_OFFSET) {
      config.toolbarStickyOffset = process.env.FROALA_TOOLBAR_OFFSET;
    }

    this.state = {
      config,
      text: this.props.value,
    };
  }

  handleChange(text) {
    const valid = this.props.validator ? this.props.validator(text) : null;

    this.setState({
      text,
      valid,
    });

    this.props.onChange(text, valid);
  }

  render() {
    return (
      <FormGroup
        validationState={this.validationState()}
        required={this.props.required}
      >
        <ControlLabel>{this.props.label}</ControlLabel>
        <FroalaEditor
          tag="textarea"
          config={this.state.config}
          model={this.state.text}
          onModelChange={this.handleChange}
        />
      </FormGroup>
    );
  }
}

HtmlFroalaControl.propTypes = {
  value: PropTypes.string,
  onChange: PropTypes.func.isRequired,
  validator: PropTypes.func,
};

HtmlFroalaControl.defaultProps = {
  value: '',
};

export default HtmlFroalaControl;
