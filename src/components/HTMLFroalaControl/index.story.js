import React from 'react';
import { action } from '@storybook/addon-actions';
import { storiesOf } from '@storybook/react';
import backdrop from '../../../.storybook/backdrop';
import HtmlFroalaControl from './';

storiesOf('HtmlFroalaControl', module)
  .addDecorator(backdrop)
  .add('component', () => (
    <HtmlFroalaControl
      label={`Description`}
      onChange={action('HtmlFroalaControl:onChange')}
    />
  ));
