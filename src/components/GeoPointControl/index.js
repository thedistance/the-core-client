/**
 * @Author: Ben Briggs <benbriggs>
 * @Date:   2017-12-18T15:49:01+00:00
 * @Email:  ben.briggs@thedistance.co.uk
 * @Last modified by:   benbriggs
 * @Last modified time: 2018-01-11T09:52:28+00:00
 * @Copyright: The Distance
 */

import React from 'react';
import Parse from 'parse';
import {
  Grid,
  Row,
  Col,
  FormControlFeedback,
  ControlLabel,
} from '@sketchpixy/rubix';
import FormGroup from '../FormGroup';
import GenericControl from '../GenericControl';
import NumberControl from '../NumberControl';

/*
 * We use createGeoPoint here because parse-server does not accept
 * new Parse.GeoPoint(lat, long) but it *does* support the plain
 * object form.
 */

const createGeoPoint = (latitude, longitude) => ({
  __type: 'GeoPoint',
  latitude,
  longitude,
});

class GeoPointControl extends GenericControl {
  constructor(props) {
    super(props);
    const { value } = props;
    if (value) {
      this.state = {
        latitude: value.latitude,
        longitude: value.longitude,
        valid: null,
      };
    } else {
      this.state = {
        latitude: 0,
        longitude: 0,
        valid: null,
      };
    }
  }

  handleLatitude(latitude) {
    try {
      this.setState({ latitude });
      const { longitude } = this.state;
      // Here we use geoPoint to trigger validation (hence try-catch); it goes
      // unused and we build a plain object instead.
      const geoPoint = new Parse.GeoPoint(latitude, longitude); // eslint-disable-line no-unused-vars
      this.setState({ valid: true });
      this.props.onChange(createGeoPoint(latitude, longitude), true);
    } catch (err) {
      this.setState({ valid: false });
      this.props.onChange(null, false);
    }
  }

  handleLongitude(longitude) {
    try {
      this.setState({ longitude });
      const { latitude } = this.state;
      // Here we use geoPoint to trigger validation (hence try-catch); it goes
      // unused and we build a plain object instead.
      const geoPoint = new Parse.GeoPoint(latitude, longitude); // eslint-disable-line no-unused-vars
      this.setState({ valid: true });
      this.props.onChange(createGeoPoint(latitude, longitude), true);
    } catch (err) {
      this.setState({ valid: false });
      this.props.onChange(null, false);
    }
  }

  render() {
    const { validator, ...rest } = this.props;
    return (
      <FormGroup
        validationState={this.validationState()}
        required={this.props.required}
      >
        <ControlLabel>{this.props.label}</ControlLabel>
        <Grid collapse>
          <Row>
            <Col sm={5}>
              <NumberControl
                {...rest}
                label={'Latitude'}
                onChange={this.handleLatitude.bind(this)}
                value={this.state.latitude}
              />
              <FormControlFeedback />
            </Col>
            <Col sm={5} smOffset={1}>
              <NumberControl
                {...rest}
                label={'Longitude'}
                onChange={this.handleLongitude.bind(this)}
                value={this.state.longitude}
              />
              <FormControlFeedback />
            </Col>
          </Row>
        </Grid>
      </FormGroup>
    );
  }
}

export default GeoPointControl;
