/**
 * @Author: Ben Briggs <benbriggs>
 * @Date:   2017-12-18T17:10:52+00:00
 * @Email:  ben.briggs@thedistance.co.uk
 * @Last modified by:   benbriggs
 * @Last modified time: 2018-01-11T09:52:43+00:00
 * @Copyright: The Distance
 */

import React from 'react';
import { shallow } from 'enzyme';
import toJson from 'enzyme-to-json';
import GeoPointControl from './';

describe('GeoPointControl', () => {
  test('should render', () => {
    const component = shallow(
      <GeoPointControl label={`Location`} onChange={jest.fn()} />
    );

    expect(toJson(component.shallow())).toMatchSnapshot();
  });

  test('should update', () => {
    const handler = jest.fn();
    const component = shallow(
      <GeoPointControl label={`Location`} onChange={handler} />
    );

    component
      .find('NumberControl')
      .at(0)
      .simulate('change', 90);
    expect(handler).toHaveBeenCalledWith(
      { __type: 'GeoPoint', latitude: 90, longitude: 0 },
      true
    );

    component
      .find('NumberControl')
      .at(1)
      .simulate('change', 91);
    expect(handler).toHaveBeenCalledWith(
      { __type: 'GeoPoint', latitude: 90, longitude: 91 },
      true
    );

    expect(toJson(component.shallow())).toMatchSnapshot();
  });
});
