/**
 * @Author: Ben Briggs <benbriggs>
 * @Date:   2017-12-18T15:49:44+00:00
 * @Email:  ben.briggs@thedistance.co.uk
 * @Last modified by:   benbriggs
 * @Last modified time: 2018-01-11T09:52:34+00:00
 * @Copyright: The Distance
 */

import React from 'react';
import { action } from '@storybook/addon-actions';
import { storiesOf } from '@storybook/react';
import backdrop from '../../../.storybook/backdrop';
import GeoPointControl from './';

storiesOf('GeoPointControl', module)
  .addDecorator(backdrop)
  .add('component', () => (
    <GeoPointControl
      label={`Location`}
      onChange={action('GeoPointControl:onChange')}
    />
  ));
