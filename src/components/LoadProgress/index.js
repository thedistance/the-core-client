/**
 * @Author: Ben Briggs <benbriggs>
 * @Date:   2018-01-30T09:49:41+00:00
 * @Email:  ben.briggs@thedistance.co.uk
 * @Last modified by:   benbriggs
 * @Last modified time: 2018-02-01T09:43:41+00:00
 * @Copyright: The Distance
 */

import React from 'react';
import PropTypes from 'prop-types';
import { Spinner } from 'react-redux-spinner';

const LoadProgress = ({ children }) => (
  <div>
    <Spinner />
    {children}
  </div>
);

LoadProgress.propTypes = {
  children: PropTypes.node,
};

// We are intentionally using module.exports here as export.default fails
// with an invariant violation "The root route must render a single element".

module.exports = LoadProgress;
