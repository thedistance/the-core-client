/*
 * Created Date: Thu, 19th Apr 2018, 09:06:20 am
 * Author: Harry Crank
 * Email: harry.crank@thedistance.co.uk
 * Copyright (c) 2018 The Distance
 */

import React from 'react';
import Parse from 'parse';
import { storiesOf } from '@storybook/react';
import { withInfo } from '@storybook/addon-info';
import backdrop from '../../../.storybook/backdrop';
import ReadOnlyPointerControl from './';

storiesOf('ReadOnlyPointerControl', module)
  .addDecorator(backdrop)
  .add(
    'component',
    withInfo()(() => (
      <ReadOnlyPointerControl
        label={`Parse Object`}
        loadOptions={(input, callback) => {
          callback(null, {
            options: [{ value: 1, label: 'Foo' }, { value: 2, label: 'Bar' }],
          });
        }}
        ParseObject={Parse.Object.extend('ParseSubclass')}
        value={1}
      />
    ))
  );
