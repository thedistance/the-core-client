/*
 * Created Date: Tue, 17th Apr 2018, 13:27:02 pm
 * Author: Harry Crank
 * Email: harry.crank@thedistance.co.uk
 * Copyright (c) 2018 The Distance
 */

import React from 'react';
import PropTypes from 'prop-types';
import { Async as Select } from 'react-select';
import { ControlLabel } from '@sketchpixy/rubix';
import FormGroup from '../FormGroup';

class ReadOnlyPointerControl extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      displayValue: props.value,
    };
  }

  render() {
    const { displayValue } = this.state;
    const { label, loadOptions } = this.props;
    return (
      <div>
        <FormGroup>
          <ControlLabel>{label}</ControlLabel>
          <Select
            disabled={true}
            value={displayValue}
            loadOptions={loadOptions}
          />
        </FormGroup>
      </div>
    );
  }
}

ReadOnlyPointerControl.propTypes = {
  value: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
  label: PropTypes.string.isRequired,
  loadOptions: PropTypes.func.isRequired,
};

export default ReadOnlyPointerControl;
