/*
 * Created Date: Thu, 19th Apr 2018, 08:58:01 am
 * Author: Harry Crank
 * Email: harry.crank@thedistance.co.uk
 * Copyright (c) 2018 The Distance
 */

import React from 'react';
import { storiesOf } from '@storybook/react';
import backdrop from '../../../.storybook/backdrop';
import ReadOnlyNumericSelectControl from './';

storiesOf('ReadOnlyNumericSelectControl', module)
  .addDecorator(backdrop)
  .add('with label support', () => (
    <ReadOnlyNumericSelectControl
      label={`Fruit`}
      collection={[
        { label: 'Apples', value: 1 },
        { label: 'Bananas', value: 2 },
        { label: 'Pears', value: 3 },
      ]}
      value={2}
    />
  ));
