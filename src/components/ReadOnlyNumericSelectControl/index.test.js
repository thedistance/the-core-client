/*
 * Created Date: Thu, 19th Apr 2018, 09:16:35 am
 * Author: Harry Crank
 * Email: harry.crank@thedistance.co.uk
 * Copyright (c) 2018 The Distance
 */

import React from 'react';
import { shallow } from 'enzyme';
import toJson from 'enzyme-to-json';
import ReadOnlyNumericSelectControl from './';

test('should render', () => {
  const component = shallow(
    <ReadOnlyNumericSelectControl
      label={`Numbers`}
      collection={[1, 2, 3]}
      onChange={jest.fn()}
    />
  );

  expect(toJson(component.shallow())).toMatchSnapshot();
});

test('should render - on an empty collection', () => {
  const component = shallow(
    <ReadOnlyNumericSelectControl
      label={`Fruit`}
      collection={[]}
      value={0}
      onChange={jest.fn()}
    />
  );

  expect(toJson(component.shallow())).toMatchSnapshot();
});

test('should render with a value', () => {
  const component = shallow(
    <ReadOnlyNumericSelectControl
      label={`Numbers`}
      collection={[1, 2, 3]}
      value={1}
      onChange={jest.fn()}
    />
  );

  expect(toJson(component.shallow())).toMatchSnapshot();
});

test('should render with labels and values', () => {
  const component = shallow(
    <ReadOnlyNumericSelectControl
      label={`Fruit`}
      collection={[
        { label: 'Apples', value: 1 },
        { label: 'Bananas', value: 2 },
        { label: 'Pears', value: 3 },
      ]}
      value={3}
      onChange={jest.fn()}
    />
  );

  expect(toJson(component.shallow())).toMatchSnapshot();
});
