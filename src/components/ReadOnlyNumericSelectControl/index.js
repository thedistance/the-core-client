/*
 * Created Date: Mon, 16th Apr 2018, 16:26:53 pm
 * Author: Harry Crank
 * Email: harry.crank@thedistance.co.uk
 * Copyright (c) 2018 The Distance
 */

import React from 'react';
import PropTypes from 'prop-types';
import { FormControl } from '@sketchpixy/rubix';
import GenericControl from '../GenericControl';

const ReadOnlyNumericSelectControl = ({ collection, allowBlank, ...props }) => {
  const entry = collection.find(obj => parseInt(obj.value, 10) === props.value);

  return (
    <GenericControl {...props}>
      <FormControl.Static>{entry ? entry.label : '-'}</FormControl.Static>
    </GenericControl>
  );
};

ReadOnlyNumericSelectControl.propTypes = {
  allowBlank: PropTypes.bool,
  collection: PropTypes.array,
  value: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
};

export default ReadOnlyNumericSelectControl;
