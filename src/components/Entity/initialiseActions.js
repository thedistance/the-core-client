/*
 * Created Date: Mon, 9th Apr 2018, 11:11:20 am
 * Author: Harry Crank
 * Email: harry.crank@thedistance.co.uk
 * -----
 * Last Modified: Mon, 9th Apr 2018, 16:02:17 pm
 * Modified By: Harry Crank
 * -----
 * Copyright (c) 2018 The Distance
 */

import Parse from 'parse';
import decamelize from 'decamelize';
import escape from 'escape-string-regexp';
import * as R from 'ramda';
import * as asyncActions from './../../util/asyncActions';
import createAction from './../../util/createAction';
import createReducer from './../../util/createReducer';
import deprecated from './../../util/deprecated';
import selectParse from './../../selectors/parse';
import redirect from './redirect';
import createObjectFromSchemaDefaults from './createObjectFromSchemaDefaults';

const parseDestroy = object =>
  new Promise((resolve, reject) => {
    object.destroy({
      success: obj => resolve(obj),
      error: err => reject(err),
    });
  });

function initialiseActions(
  entity,
  {
    parseClass = entity.name,
    onLoadCollection = R.identity,
    onLoadCurrent = R.identity,
    reducerActions = {},
  }
) {
  const { name, schema } = entity;
  const createActionId = (...parts) => [name, ...parts].join(':');

  const scopedSelect = state => state[entity.camelised];
  const selectError = state => scopedSelect(state).error;
  const selectErrorMessage = state =>
    scopedSelect(state).error ? scopedSelect(state).error.message : null;
  const selectCurrent = state => scopedSelect(state).current;
  const selectCollection = state => scopedSelect(state).collection;
  const selectPages = state => scopedSelect(state).pages;

  const selectors = {
    select: scopedSelect,
    selectError,
    selectErrorMessage,
    selectCurrent,
    selectCollection,
    selectPages,
    [`select${name}`]: deprecated(scopedSelect, 'Please use "select" instead'),
    [`select${name}Error`]: deprecated(
      selectError,
      'Please use "selectError" instead'
    ),
    [`selectCurrent${name}`]: deprecated(
      selectCurrent,
      'Please use "selectCurrent" instead'
    ),
    [`select${name}Collection`]: deprecated(
      selectCurrent,
      'Please use "selectCollection" instead'
    ),
  };

  const actionKeys = [
    'deleting',
    'deleted',
    'deleteFailed',
    'loadingCurrent',
    'loadedCurrent',
    'loadingCollection',
    'loadedCollection',
    'new',
    'revert',
    'saving',
    'saved',
    'saveFailed',
    'unloadCollection',
    'unloadCurrent',
    'update',
  ];

  const actionIds = actionKeys.reduce((list, key) => {
    list[`${key}Action`] = createActionId(...decamelize(key, ' ').split(' '));
    return list;
  }, {});

  const deletingAction = asyncActions.createStart({
    type: actionIds.deletingAction,
  });

  const deletedAction = asyncActions.createEnd({
    type: actionIds.deletedAction,
  });

  const deleteFailedAction = asyncActions.createError({
    type: actionIds.deleteFailedAction,
  });

  const loadingCurrentAction = asyncActions.createStart({
    type: actionIds.loadingCurrentAction,
  });

  const loadedCurrentAction = asyncActions.createEnd({
    type: actionIds.loadedCurrentAction,
  });

  const loadedCurrentSyncAction = createAction(actionIds.loadedCurrentAction);

  const loadingCollectionAction = asyncActions.createStart({
    type: actionIds.loadingCollectionAction,
  });

  const loadedCollectionAction = asyncActions.createEnd({
    type: actionIds.loadedCollectionAction,
  });

  const newAction = createAction(actionIds.newAction);

  const revertAction = createAction(actionIds.revertAction);

  const savingAction = asyncActions.createStart({
    type: actionIds.savingAction,
  });

  const savedAction = asyncActions.createEnd({
    type: actionIds.savedAction,
  });

  const saveFailedAction = asyncActions.createEnd({
    type: actionIds.saveFailedAction,
  });

  const unloadCollectionAction = createAction(actionIds.unloadCollectionAction);

  const unloadCurrentAction = createAction(actionIds.unloadCurrentAction);

  const updateAction = createAction(actionIds.updateAction);

  const actionCreators = {
    [`deleting${name}Action`]: deprecated(
      deletingAction,
      'Please use "deletingAction" instead'
    ),
    deletingAction,
    [`deleted${name}Action`]: deprecated(
      deletedAction,
      'Please use "deletedAction" instead'
    ),
    deletedAction,
    [`delete${name}FailedAction`]: deprecated(
      deleteFailedAction,
      'Please use "deleteFailedAction" instead'
    ),
    deleteFailedAction,
    [`loadingCurrent${name}Action`]: deprecated(
      loadingCurrentAction,
      'Please use "loadingCurrentAction" instead'
    ),
    loadingCurrentAction,
    [`loadedCurrent${name}Action`]: deprecated(
      loadedCurrentAction,
      'Please use "loadedCurrentAction" instead'
    ),
    loadedCurrentAction,
    [`loadedCurrent${name}SyncAction`]: deprecated(
      loadedCurrentSyncAction,
      'Please use "loadedCurrentSyncAction" instead'
    ),
    loadedCurrentSyncAction,
    [`loading${name}CollectionAction`]: deprecated(
      loadingCollectionAction,
      'Please use "loadingCollectionAction" instead'
    ),
    loadingCollectionAction,
    [`loaded${name}CollectionAction`]: deprecated(
      loadedCollectionAction,
      'Please use "loadedCollectionAction" instead'
    ),
    loadedCollectionAction,
    [`new${name}Action`]: deprecated(
      newAction,
      'Please use "newAction" instead'
    ),
    newAction,
    [`revert${name}Action`]: deprecated(
      revertAction,
      'Please use "revertAction" instead'
    ),
    revertAction,
    [`saving${name}Action`]: deprecated(
      savingAction,
      'Please use "savingAction" instead'
    ),
    savingAction,
    [`saved${name}Action`]: deprecated(
      savedAction,
      'Please use "savedAction" instead'
    ),
    savedAction,
    saveFailedAction,
    [`unload${name}CollectionAction`]: deprecated(
      unloadCollectionAction,
      'Please use "unloadCollectionAction" instead'
    ),
    unloadCollectionAction,
    [`unloadCurrent${name}Action`]: deprecated(
      unloadCurrentAction,
      'Please use "unloadCurrentAction" instead'
    ),
    unloadCurrentAction,
    [`update${name}Action`]: deprecated(
      updateAction,
      'Please use "updateAction" instead'
    ),
    updateAction,
  };

  const destroy = ({ pathname, router, goBack } = {}) => (
    dispatch,
    getState
  ) => {
    const result = selectors.selectCurrent(getState());
    dispatch(deletingAction({ result }));

    return parseDestroy(result)
      .then(() => dispatch(deletedAction({ result })))
      .then(() => Promise.resolve({ pathname, router, goBack }))
      .then(redirect)
      .catch(error => dispatch(deleteFailedAction({ error })));
  };

  const loadCurrent = ({ id } = {}) => (dispatch, getState) => {
    const state = getState();
    const current = selectors.selectCurrent(getState());

    if (current) {
      return Promise.resolve(
        dispatch(
          loadedCurrentSyncAction({
            result: current,
          })
        )
      );
    }

    if (!id) {
      return;
    }

    dispatch(loadingCurrentAction());

    const Parse = selectParse(state);
    return onLoadCurrent(new Parse.Query(parseClass))
      .get(id)
      .then(result => {
        dispatch(loadedCurrentAction({ result }));
        return result;
      });
  };

  const loadCollection = ({
    pageSize = 20,
    page = 0,
    sorted,
    filtered,
  } = {}) => (dispatch, getState) => {
    dispatch(loadingCollectionAction());

    const Parse = selectParse(getState());

    const data = onLoadCollection(new Parse.Query(parseClass))
      .limit(pageSize)
      .skip(pageSize * page);

    if (filtered && filtered.length) {
      filtered.forEach(({ id, value }) =>
        data.matches(id, new RegExp(escape(value), 'i'))
      );
    }

    if (sorted && sorted.length) {
      sorted.forEach(
        ({ id, desc }) => (desc ? data.descending(id) : data.ascending(id))
      );
    }

    return Promise.all([data.count(), data.find()]).then(([count, result]) =>
      dispatch(
        loadedCollectionAction({ result, pages: Math.ceil(count / pageSize) })
      )
    );
  };

  const defaultObject = createObjectFromSchemaDefaults(schema);

  const create = params => (dispatch, getState) => {
    const Parse = selectParse(getState());
    const Instance = Parse.Object.extend(parseClass);
    return dispatch(
      newAction({
        result: new Instance({ ...defaultObject, ...params }),
      })
    );
  };

  const revert = () => (dispatch, getState) => {
    const result = selectors.selectCurrent(getState());
    if (!result) {
      return;
    }
    result.revert();
    return dispatch(revertAction({ result }));
  };

  const save = redirectOptions => (dispatch, getState) => {
    const result = selectors.selectCurrent(getState());
    dispatch(savingAction({ result }));

    return result
      .save()
      .then(() => dispatch(savedAction({ result })))
      .then(() => redirectOptions && redirect(redirectOptions))
      .catch(error => {
        if (error instanceof Parse.Error) {
          return dispatch(saveFailedAction({ error }));
        }
        // Handle large file error
        if (error.status === 413) {
          return dispatch(
            saveFailedAction({
              error: {
                code: error.status,
                message: 'The file you tried to upload is too large.',
              },
            })
          );
        }
        // Other http error
        return dispatch(
          saveFailedAction({
            error: { code: error.status, message: error.statusText },
          })
        );
      });
  };

  const unloadCollection = () => (dispatch, getState) => {
    const result = selectors.selectCollection(getState());
    return dispatch(unloadCollectionAction({ result }));
  };

  const unloadCurrent = () => (dispatch, getState) => {
    const result = selectors.selectCurrent(getState());
    return dispatch(unloadCurrentAction({ result }));
  };

  const update = ({ key, value }) => (dispatch, getState) => {
    const result = selectors.selectCurrent(getState());
    result.set(key, value);
    return dispatch(updateAction({ result }));
  };

  const actions = {
    [`delete${name}`]: deprecated(destroy, 'Please use "destroy" instead'),
    destroy,
    [`loadCurrent${name}`]: deprecated(
      loadCurrent,
      'Please use "loadCurrent" instead.'
    ),
    loadCurrent,
    [`load${name}Collection`]: deprecated(
      loadCollection,
      'Please use "loadCollection" instead'
    ),
    loadCollection,
    [`new${name}`]: deprecated(create, 'Please use "create" instead'),
    create,
    [`revert${name}`]: deprecated(revert, 'Please use "revert" instead'),
    revert,
    [`save${name}`]: deprecated(save, 'Please use "save" instead'),
    save,
    [`unload${name}Collection`]: deprecated(
      unloadCollection,
      'Please use "unloadCollection" instead'
    ),
    unloadCollection,
    [`unloadCurrent${name}`]: deprecated(
      unloadCurrent,
      'Please use "unloadCurrent" instead'
    ),
    unloadCurrent,
    [`update${name}`]: deprecated(update, 'Please use "update" instead'),
    update,
    cancel: redirect,
  };

  const nullCurrent = state => ({ ...state, current: null, error: null });
  const updateCurrent = (state, { result }) => ({ ...state, current: result });

  const reducer = createReducer(
    {
      collection: null,
      current: null,
      error: null,
      pages: null,
    },
    {
      [actionIds.deletedAction]: nullCurrent,
      [actionIds.deleteFailedAction]: (state, { error }) => ({
        ...state,
        error,
      }),
      [actionIds.loadedCurrentAction]: updateCurrent,
      [actionIds.loadedCollectionAction]: (state, { pages, result }) => ({
        ...state,
        collection: result,
        pages,
      }),
      [actionIds.newAction]: updateCurrent,
      [actionIds.revertAction]: nullCurrent,
      [actionIds.saveFailedAction]: (state, { error }) => ({
        ...state,
        error,
      }),
      [actionIds.unloadCollectionAction]: state => ({
        ...state,
        collection: null,
        pages: null,
      }),
      [actionIds.unloadCurrentAction]: nullCurrent,
      [actionIds.updateAction]: updateCurrent,
      ...reducerActions,
    }
  );

  return {
    ...entity,
    actions,
    actionCreators,
    actionIds,
    createActionId,
    reducer,
    selectors,
  };
}

export default initialiseActions;
