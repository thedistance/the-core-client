/**
 * @Author: Ben Briggs <benbriggs>
 * @Date:   2018-01-26T13:28:36+00:00
 * @Email:  ben.briggs@thedistance.co.uk
 * @Last modified by:   benbriggs
 * @Last modified time: 2018-01-26T13:33:29+00:00
 * @Copyright: The Distance
 */

import React from 'react';
import { action } from '@storybook/addon-actions';
import { storiesOf } from '@storybook/react';
import { withInfo } from '@storybook/addon-info';
import backdrop from '../../../.storybook/backdrop';
import TextControl from '../TextControl';
import EditView from './editView';

storiesOf('EditView', module)
  .addDecorator(backdrop)
  .add(
    'component',
    withInfo()(() => (
      <EditView
        actions={{
          load: () => Promise.resolve(action('EditView:load')),
          unload: action('EditView:unload'),
        }}
        model={{ title: 'Hi there' }}
        fields={[
          {
            accessor: model => model.title,
            key: 'title',
            label: 'Title',
            Component: TextControl,
          },
        ]}
        title={`Edit Model`}
      />
    ))
  );
