/**
 * @Author: Ben Briggs <benbriggs>
 * @Date:   2018-01-25T16:33:54+00:00
 * @Email:  ben.briggs@thedistance.co.uk
 * @Last modified by:   benbriggs
 * @Last modified time: 2018-01-25T16:37:20+00:00
 * @Copyright: The Distance
 */

const parseObjectAccessor = (accessor, key) => parseObject => {
  if (accessor) {
    return accessor(parseObject);
  }
  return parseObject.get(key);
};

export default parseObjectAccessor;
