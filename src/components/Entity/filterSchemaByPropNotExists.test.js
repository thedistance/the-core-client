/**
 * @Author: Ben Briggs <benbriggs>
 * @Date:   2018-02-06T11:28:24+00:00
 * @Email:  ben.briggs@thedistance.co.uk
 * @Last modified by:   benbriggs
 * @Last modified time: 2018-02-06T11:37:29+00:00
 * @Copyright: The Distance
 */

import filterSchema from './filterSchemaByPropNotExists';

test('should filter a schema', () => {
  const schema = {
    foo: {
      hideInEdit: true,
      type: 'string',
    },
    bar: {
      type: 'string',
    },
  };

  expect(filterSchema('hideInEdit', schema)).toMatchSnapshot();
});
