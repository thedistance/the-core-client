/**
 * @Author: Ben Briggs <benbriggs>
 * @Date:   2018-02-06T15:03:36+00:00
 * @Email:  ben.briggs@thedistance.co.uk
 * @Last modified by:   benbriggs
 * @Last modified time: 2018-04-25T09:39:45+01:00
 * @Copyright: The Distance
 */

import { parameterize, pluralize, singularize, titleize } from 'inflected';

function inflections(name, overrides = {}) {
  const camelised =
    overrides.camelised || `${name[0].toLowerCase()}${name.slice(1)}`;
  const titleCased = overrides.titleCased || titleize(name);
  const slug = overrides.slug || parameterize(titleCased);
  const singular = overrides.singular || singularize(titleCased);
  const plural = overrides.plural || pluralize(titleCased);

  return {
    camelised,
    titleCased,
    slug,
    singular,
    plural,
  };
}

export default inflections;
