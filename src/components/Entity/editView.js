/**
 * @Author: Ben Briggs <benbriggs>
 * @Date:   2018-01-26T13:17:29+00:00
 * @Email:  ben.briggs@thedistance.co.uk
 * @Last modified by:   benbriggs
 * @Last modified time: 2018-04-25T11:09:57+01:00
 * @Copyright: The Distance
 */

import React from 'react';
import PropTypes from 'prop-types';
import { Grid, Row, Col, Button, ButtonToolbar, Form } from '@sketchpixy/rubix';
import Alert from '../Alert';
import Panel from '../Panel';

const EditActions = ({ actions }) =>
  actions.delete ? (
    <Button bsStyle="danger" onClick={actions.delete}>
      Delete
    </Button>
  ) : (
    <Button onClick={actions.cancel}>Cancel</Button>
  );

EditActions.propTypes = {
  actions: PropTypes.objectOf(PropTypes.func).isRequired,
};

class EditView extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      saved: true,
    };
  }

  onChange(key, value) {
    this.setState({ saved: false });
    this.props.actions.update({ key, value });
  }

  componentWillMount() {
    this.props.actions.load();
  }

  componentWillUnmount() {
    if (!this.state.saved) {
      this.props.actions.revert();
    }
    this.props.actions.unload();
  }

  save() {
    this.setState({ saved: true });
    this.props.actions.save();
  }

  render() {
    const { actions, alert, model, fields, title } = this.props;
    if (!model) {
      return null;
    }
    return (
      <div>
        {alert ? <Alert danger>{alert}</Alert> : null}
        <Panel title={title}>
          <Grid>
            <Row>
              <Col xs={12}>
                <Form autoComplete="off">
                  {fields.map(
                    ({ label, accessor, key, validator, Component }, index) => (
                      <Component
                        actions={actions}
                        key={index}
                        label={label}
                        value={accessor(model)}
                        validator={validator}
                        onChange={this.onChange.bind(this, key)}
                      />
                    )
                  )}
                  <ButtonToolbar style={{ marginBottom: '1em' }}>
                    <Grid>
                      <Row>
                        <Col xs={6}>{<EditActions actions={actions} />}</Col>
                        <Col xs={6} style={{ textAlign: 'right' }}>
                          <Button
                            bsStyle="primary"
                            onClick={this.save.bind(this)}
                          >
                            Save
                          </Button>
                        </Col>
                      </Row>
                    </Grid>
                  </ButtonToolbar>
                </Form>
              </Col>
            </Row>
          </Grid>
        </Panel>
      </div>
    );
  }
}

EditView.propTypes = {
  actions: PropTypes.objectOf(PropTypes.func).isRequired,
  alert: PropTypes.string,
  fields: PropTypes.array.isRequired,
  model: PropTypes.object,
  title: PropTypes.string.isRequired,
};

export default EditView;
