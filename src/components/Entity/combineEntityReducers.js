/**
 * @Author: Ben Briggs <benbriggs>
 * @Date:   2018-02-01T09:35:15+00:00
 * @Email:  ben.briggs@thedistance.co.uk
 * @Last modified by:   benbriggs
 * @Last modified time: 2018-02-01T09:35:36+00:00
 * @Copyright: The Distance
 */

const combineEntityReducers = (...entities) =>
  entities.reduce(
    (combined, { camelised, reducer }) => ({
      ...combined,
      [camelised]: reducer,
    }),
    {}
  );

export default combineEntityReducers;
