/**
 * @Author: Ben Briggs <benbriggs>
 * @Date:   2018-02-06T11:08:45+00:00
 * @Email:  ben.briggs@thedistance.co.uk
 * @Last modified by:   benbriggs
 * @Last modified time: 2018-02-06T11:28:58+00:00
 * @Copyright: The Distance
 */

import filterSchema from './filterSchemaByPropExists';

test('should filter a schema', () => {
  const schema = {
    foo: {
      showInIndex: true,
      type: 'string',
    },
    bar: {
      type: 'string',
    },
  };

  expect(filterSchema('showInIndex', schema)).toMatchSnapshot();
});
