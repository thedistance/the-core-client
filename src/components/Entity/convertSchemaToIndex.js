/**
 * @Author: Ben Briggs <benbriggs>
 * @Date:   2018-01-25T16:33:39+00:00
 * @Email:  ben.briggs@thedistance.co.uk
 * @Last modified by:   benbriggs
 * @Last modified time: 2018-04-05T11:37:06+01:00
 * @Copyright: The Distance
 */

import { titleize } from 'inflected';
import parseObjectAccessor from './parseObjectAccessor';

const convertSchemaToIndex = schema =>
  Object.keys(schema).map(key => ({
    accessor: parseObjectAccessor(
      schema[key].indexAccessor || schema[key].accessor,
      key
    ),
    Header: schema[key].label || titleize(key),
    id: key,
    Cell: schema[key].IndexComponent,
    ...(schema[key].Filter && { Filter: schema[key].Filter }),
    ...(schema[key].filterMethod && { filterMethod: schema[key].filterMethod }),
    filterable: schema[key].filterable,
    sortable: schema[key].sortable,
  }));

export default convertSchemaToIndex;
