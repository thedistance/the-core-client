/*
 * Created Date: Fri, 13th Apr 2018, 14:33:17 pm
 * Author: Harry Crank
 * Email: harry.crank@thedistance.co.uk
 * Copyright (c) 2018 The Distance
 */

import React from 'react';
import PropTypes from 'prop-types';
import { Grid, Row, Col, Button, ButtonToolbar, Form } from '@sketchpixy/rubix';
import Loadable from '../Loadable';
import Alert from '../Alert';
import Panel from '../Panel';

class ReadOnlyView extends Loadable {
  render() {
    const { actions, alert, model, fields, title } = this.props;
    if (!model) {
      return null;
    }
    return (
      <div>
        {alert ? <Alert danger>{alert}</Alert> : null}
        <Panel title={title}>
          <Grid>
            <Row>
              <Col xs={12}>
                <Form autoComplete="off">
                  {fields.map(
                    ({ label, accessor, key, validator, Component }, index) => (
                      <Component
                        actions={actions}
                        key={index}
                        label={label}
                        value={accessor(model)}
                        validator={validator}
                      />
                    )
                  )}
                  <ButtonToolbar style={{ marginBottom: '1em' }}>
                    <Grid>
                      <Row>
                        <Col xs={12} style={{ textAlign: 'right' }}>
                          <Button bsStyle="primary" onClick={actions.cancel}>
                            Back
                          </Button>
                        </Col>
                      </Row>
                    </Grid>
                  </ButtonToolbar>
                </Form>
              </Col>
            </Row>
          </Grid>
        </Panel>
      </div>
    );
  }
}

ReadOnlyView.propTypes = {
  actions: PropTypes.objectOf(PropTypes.func).isRequired,
  alert: PropTypes.string,
  fields: PropTypes.array.isRequired,
  model: PropTypes.object,
  title: PropTypes.string.isRequired,
};

export default ReadOnlyView;
