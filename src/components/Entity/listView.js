/**
 * @Author: benbriggs
 * @Date:   2018-05-01T11:31:30+01:00
 * @Email:  ben.briggs@thedistance.co.uk
 * @Last modified by:   benbriggs
 * @Last modified time: 2018-05-01T11:36:45+01:00
 * @Copyright: The Distance
 */

import React from 'react';
import PropTypes from 'prop-types';
import ReactTable from 'react-table';
import { Grid, Row, Col } from '@sketchpixy/rubix';
import Panel from '../Panel';
import LinkButton from '../LinkButton';
import 'react-table/react-table.css';

export class ListView extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      loading: true,
    };
    this.fetchData = this.fetchData.bind(this);
  }

  componentWillUnmount() {
    this.props.actions.unload();
  }

  fetchData(tableState) {
    this.setState({ loading: true });

    this.props.actions.load(tableState).then(() => {
      this.setState({
        loading: false,
      });
    });
  }

  render() {
    const { loading } = this.state;
    const {
      data,
      canCreateNew,
      columns,
      pages,
      route,
      singular,
      plural,
      readOnly,
      editButton,
      getTheadFilterThProps,
    } = this.props;
    const createPath = this.props.createPath
      ? this.props.createPath(route)
      : `${route.path}/new`;
    const viewOrEdit = readOnly ? 'View' : 'Edit';
    const cols = editButton
      ? [
          ...columns,
          {
            accessor: 'id',
            Cell: props => {
              const editPath = this.props.editPath
                ? this.props.editPath(route, props)
                : `${route.path}/${props.value}`;

              return (
                <LinkButton pathname={editPath} outlined>
                  {viewOrEdit}
                </LinkButton>
              );
            },
            filterable: false,
            sortable: false,
          },
        ]
      : columns;
    return (
      <Panel
        title={plural}
        buttonTo={canCreateNew ? createPath : null}
        buttonTitle={canCreateNew ? `New ${singular}` : null}
      >
        <Grid>
          <Row>
            <Col xs={12}>
              <ReactTable
                data={data}
                pages={pages}
                loading={loading}
                onFetchData={this.fetchData}
                getTheadFilterThProps={getTheadFilterThProps}
                columns={cols}
                filterable
                manual
                style={{ marginTop: '10px', marginBottom: '40px' }}
              />
            </Col>
          </Row>
        </Grid>
      </Panel>
    );
  }
}

ListView.propTypes = {
  actions: PropTypes.objectOf(PropTypes.func).isRequired,
  canCreateNew: PropTypes.bool,
  columns: PropTypes.array,
  createPath: PropTypes.func,
  data: PropTypes.array,
  editPath: PropTypes.func,
  pages: PropTypes.number,
  route: PropTypes.any,
  singular: PropTypes.string.isRequired,
  plural: PropTypes.string.isRequired,
  readOnly: PropTypes.bool,
  editButton: PropTypes.bool,
  getTheadFilterThProps: PropTypes.func,
};

ListView.defaultProps = {
  canCreateNew: true,
  editButton: true,
};

export default ListView;
