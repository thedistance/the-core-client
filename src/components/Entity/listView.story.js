/**
 * @Author: Ben Briggs <benbriggs>
 * @Date:   2018-01-02T13:53:16+00:00
 * @Email:  ben.briggs@thedistance.co.uk
 * @Last modified by:   benbriggs
 * @Last modified time: 2018-05-01T11:32:47+01:00
 * @Copyright: The Distance
 */

import React from 'react';
import PropTypes from 'prop-types';
import { action } from '@storybook/addon-actions';
import { storiesOf } from '@storybook/react';
import { withInfo } from '@storybook/addon-info';
import backdrop from '../../../.storybook/backdrop';
import ListView from './listView';

const NumberRenderer = ({ value }) => <span className="number">{value}</span>;

NumberRenderer.propTypes = {
  value: PropTypes.number,
};

storiesOf('ListView', module)
  .addDecorator(backdrop)
  .add(
    'component',
    withInfo()(() => (
      <ListView
        actions={{
          load: () => Promise.resolve(action('ListView:load')),
          unload: action('ListView:unload'),
        }}
        data={[
          {
            name: 'John Smith',
            age: 27,
          },
        ]}
        columns={[
          {
            Header: 'Name',
            accessor: 'name',
          },
          {
            Header: 'Age',
            accessor: 'age',
            Cell: NumberRenderer,
          },
        ]}
        manual={false}
        route={{ path: '/collection' }}
        singular={`Collection`}
        plural={`Collection`}
      />
    ))
  )
  .add(
    'without new button',
    withInfo()(() => (
      <ListView
        actions={{
          load: () => Promise.resolve(action('ListView:load')),
          unload: action('ListView:unload'),
        }}
        canCreateNew={false}
        data={[
          {
            name: 'John Smith',
            age: 27,
          },
        ]}
        columns={[
          {
            Header: 'Name',
            accessor: 'name',
          },
          {
            Header: 'Age',
            accessor: 'age',
            Cell: NumberRenderer,
          },
        ]}
        manual={false}
        route={{ path: '/collection' }}
        singular={`Collection`}
        plural={`Collection`}
      />
    ))
  )
  .add(
    'with custom filter headers',
    withInfo()(() => (
      <ListView
        actions={{
          load: () => Promise.resolve(action('ListView:load')),
          unload: action('ListView:unload'),
        }}
        getTheadFilterThProps={() => ({
          style: { backgroundColor: 'rebeccapurple' },
        })}
        canCreateNew={false}
        data={[
          {
            name: 'John Smith',
            age: 27,
          },
        ]}
        columns={[
          {
            Header: 'Name',
            accessor: 'name',
            filter: true,
          },
          {
            Header: 'Age',
            accessor: 'age',
            Cell: NumberRenderer,
            filter: true,
          },
        ]}
        manual={false}
        route={{ path: '/collection' }}
        singular={`Collection`}
        plural={`Collection`}
      />
    ))
  );
