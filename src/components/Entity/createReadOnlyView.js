/*
 * Created Date: Fri, 13th Apr 2018, 14:41:18 pm
 * Author: Harry Crank
 * Email: harry.crank@thedistance.co.uk
 * Copyright (c) 2018 The Distance
 */

import { connect } from 'react-redux';
import ReadOnlyView from './readOnlyView';

function createReadOnlyView({
  loadCurrent,
  unloadCurrent,
  routeId,
  revert,
  selectCurrent,
  schemaFields,
  singular,
  goBack = 1,
  extraActions,
  extraEditActions,
}) {
  const mapStateToProps = state => ({
    model: selectCurrent(state),
    fields: schemaFields,
    title: `View ${singular}`,
  });

  const mapDispatchToProps = (dispatch, ownProps) => ({
    actions: {
      load: () => dispatch(loadCurrent({ id: ownProps.routeParams[routeId] })),
      cancel: () => {
        const routeParts = ownProps.location.pathname
          .split('/')
          .slice(0, goBack * -1);
        ownProps.router.push(routeParts.join('/'));
      },
      unload: () => dispatch(unloadCurrent()),
      revert: () => dispatch(revert()),
      ...extraActions(dispatch, ownProps),
      ...extraEditActions(dispatch, ownProps),
    },
  });

  return connect(mapStateToProps, mapDispatchToProps)(ReadOnlyView);
}

export default createReadOnlyView;
