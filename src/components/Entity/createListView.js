/**
 * @Author: Ben Briggs <benbriggs>
 * @Date:   2017-11-27T14:56:42+00:00
 * @Email:  ben.briggs@thedistance.co.uk
 * @Last modified by:   benbriggs
 * @Last modified time: 2018-05-01T11:36:16+01:00
 * @Copyright: The Distance
 */

import { connect } from 'react-redux';
import ListView from './listView';

export const EntityCollection = ListView;

function createListView({
  loadCollection,
  unloadCollection,
  selectCollection,
  selectPages,
  singular,
  plural,
  collectionIndex,
  canCreateNew,
  readOnly,
  editButton,
  getTheadFilterThProps,
}) {
  const mapStateToProps = state => ({
    readOnly,
    singular,
    plural,
    canCreateNew,
    columns: collectionIndex,
    data: selectCollection(state) || [],
    pages: selectPages(state),
    getTheadFilterThProps,
    editButton,
  });

  const mapDispatchToProps = dispatch => ({
    actions: {
      load: tableState => dispatch(loadCollection(tableState)),
      unload: () => dispatch(unloadCollection()),
    },
  });

  return connect(mapStateToProps, mapDispatchToProps)(ListView);
}

export default createListView;
