/**
 * @Author: Ben Briggs <benbriggs>
 * @Date:   2018-02-01T09:35:43+00:00
 * @Email:  ben.briggs@thedistance.co.uk
 * @Last modified by:   benbriggs
 * @Last modified time: 2018-02-01T09:38:42+00:00
 * @Copyright: The Distance
 */

import combineEntityReducers from './combineEntityReducers';

test('should combine entity reducers into a single reducer object', () => {
  // No need for the full entity here, just the keys that
  // we need for combineEntityReducers.
  const topicEntity = { camelised: 'topic', reducer: jest.fn() };
  const gameModeEntity = { camelised: 'gameMode', reducer: jest.fn() };

  expect(combineEntityReducers(topicEntity, gameModeEntity)).toMatchSnapshot();
});
