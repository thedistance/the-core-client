/**
 * @Author: Ben Briggs <benbriggs>
 * @Date:   2018-01-26T13:18:47+00:00
 * @Email:  ben.briggs@thedistance.co.uk
 * @Last modified by:   benbriggs
 * @Last modified time: 2018-04-25T11:05:08+01:00
 * @Copyright: The Distance
 */

import { connect } from 'react-redux';
import EditView from './editView';

function createNewView({
  create,
  save,
  unloadCurrent,
  update,
  revert,
  selectCurrent,
  selectErrorMessage,
  schemaFields,
  singular,
  goBack = 1,
  extraActions,
  extraCreateActions,
}) {
  const mapStateToProps = state => ({
    alert: selectErrorMessage(state),
    model: selectCurrent(state),
    fields: schemaFields,
    title: `New ${singular}`,
  });

  const mapDispatchToProps = (dispatch, ownProps) => ({
    actions: {
      load: () => dispatch(create()),
      save: () =>
        dispatch(
          save({
            pathname: ownProps.location.pathname,
            router: ownProps.router,
            goBack: 1,
          })
        ),
      cancel: () => {
        const routeParts = ownProps.location.pathname
          .split('/')
          .slice(0, goBack * -1);
        ownProps.router.push(routeParts.join('/'));
      },
      unload: () => dispatch(unloadCurrent()),
      update: options => dispatch(update(options)),
      revert: () => dispatch(revert()),
      ...extraActions(dispatch, ownProps),
      ...extraCreateActions(dispatch, ownProps),
    },
  });

  return connect(mapStateToProps, mapDispatchToProps)(EditView);
}

export default createNewView;
