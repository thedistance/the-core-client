/**
 * @Author: Ben Briggs <benbriggs>
 * @Date:   2018-02-01T11:06:31+00:00
 * @Email:  ben.briggs@thedistance.co.uk
 * @Last modified by:   benbriggs
 * @Last modified time: 2018-02-01T11:07:52+00:00
 * @Copyright: The Distance
 */

const combineEntityRoutes = (...entities) =>
  entities.reduce((combined, { routes }) => [...combined, ...routes], []);

export default combineEntityRoutes;
