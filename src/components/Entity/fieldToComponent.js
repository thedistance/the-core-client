/**
 * @Author: Ben Briggs <benbriggs>
 * @Date:   2018-01-25T16:39:35+00:00
 * @Email:  ben.briggs@thedistance.co.uk
 * @Last modified by:   benbriggs
 * @Last modified time: 2018-08-29T14:53:13+01:00
 * @Copyright: The Distance
 */

/* eslint-disable func-names */

import React from 'react';
import PropTypes from 'prop-types';
import ArrayControl from '../ArrayControl';
import BooleanControl from '../BooleanControl';
import CheckboxesControl from '../CheckboxesControl';
import FileControl from '../FileControl';
import GeoPointControl from '../GeoPointControl';
import HtmlControl from '../HtmlControl';
import HtmlFroalaControl from '../HTMLFroalaControl';
import NumberControl from '../NumberControl';
import ReadOnlyNumberControl from '../ReadOnlyNumberControl';
import NumericSelectControl from '../NumericSelectControl';
import PasswordControl from '../PasswordControl';
import PointerControl from '../PointerControl';
import SelectControl from '../SelectControl';
import TextControl from '../TextControl';
import TextareaControl from '../TextareaControl';
import StaticTextControl from '../StaticTextControl';
import ReadOnlyDateControl from '../ReadOnlyDateControl';

function prepopulatedBooleanControl(choices) {
  return function PopulatedBooleanControl(props) {
    return <BooleanControl choices={choices} {...props} />;
  };
}

function prepopulatedCheckboxesControl(choices) {
  return function PopulatedCheckboxesControl(props) {
    return <CheckboxesControl collection={choices} {...props} />;
  };
}

function prepopulatedPointerControl(ParseObject, loadAction) {
  function PopulatedPointerControl(props) {
    return (
      <PointerControl
        ParseObject={ParseObject}
        loadOptions={props.actions[loadAction]}
        {...props}
      />
    );
  }

  PopulatedPointerControl.propTypes = {
    actions: PropTypes.objectOf(PropTypes.func),
  };

  return PopulatedPointerControl;
}

function prepopulatedSelectControl(choices, allowBlank) {
  return function PopulatedSelectControl(props) {
    return (
      <SelectControl allowBlank={allowBlank} collection={choices} {...props} />
    );
  };
}

function prepopulatedNumericSelectControl(choices, allowBlank) {
  return function PopulatedNumericSelectControl(props) {
    return (
      <NumericSelectControl
        allowBlank={allowBlank}
        collection={choices}
        {...props}
      />
    );
  };
}

const fieldToComponent = (field, view) =>
  (field.EditComponent
    ? field.EditComponent
    : view === 'create' ? field.CreateComponent : field.UpdateComponent) ||
  {
    array: ArrayControl,
    boolean: prepopulatedBooleanControl(field.choices),
    checkboxes: prepopulatedCheckboxesControl(field.choices),
    enum: prepopulatedSelectControl(field.choices, field.allowBlank),
    enumNumeric: prepopulatedNumericSelectControl(
      field.choices,
      field.allowBlank
    ),
    file: FileControl,
    geopoint: GeoPointControl,
    html: HtmlControl,
    htmlFroala: HtmlFroalaControl,
    number: NumberControl,
    staticNumber: ReadOnlyNumberControl,
    password: PasswordControl,
    pointer: prepopulatedPointerControl(field.ParseObject, field.loadAction),
    string: TextControl,
    text: TextControl,
    textarea: TextareaControl,
    staticDate: ReadOnlyDateControl,
    staticText: StaticTextControl,
  }[field.type] ||
  TextControl;

export default fieldToComponent;
