/**
 * @Author: Ben Briggs <benbriggs>
 * @Date:   2018-02-06T14:52:31+00:00
 * @Email:  ben.briggs@thedistance.co.uk
 * @Last modified by:   benbriggs
 * @Last modified time: 2018-02-06T14:52:55+00:00
 * @Copyright: The Distance
 */

const redirect = ({ pathname, router, goBack }) =>
  router.push(
    pathname
      .split('/')
      .slice(0, -1 * goBack)
      .join('/')
  );

module.exports = redirect;
