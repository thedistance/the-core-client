/**
 * @Author: Ben Briggs <benbriggs>
 * @Date:   2018-01-25T16:38:37+00:00
 * @Email:  ben.briggs@thedistance.co.uk
 * @Last modified by:   benbriggs
 * @Last modified time: 2018-04-20T14:41:05+01:00
 * @Copyright: The Distance
 */

import { titleize } from 'inflected';
import fieldToComponent from './fieldToComponent';
import parseObjectAccessor from './parseObjectAccessor';

const convertSchemaToFields = (schema, view) =>
  Object.keys(schema).map(key => ({
    accessor: parseObjectAccessor(
      schema[key].editAccessor || schema[key].accessor,
      key
    ),
    Component: fieldToComponent(schema[key], view),
    key,
    label: schema[key].label || titleize(key),
    validator: schema[key].validator,
  }));

export default convertSchemaToFields;
