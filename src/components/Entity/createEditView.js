/**
 * @Author: Ben Briggs <benbriggs>
 * @Date:   2018-01-26T13:36:27+00:00
 * @Email:  ben.briggs@thedistance.co.uk
 * @Last modified by:   benbriggs
 * @Last modified time: 2018-04-25T11:05:05+01:00
 * @Copyright: The Distance
 */

import { connect } from 'react-redux';
import EditView from './editView';

function createEditView({
  save,
  destroy,
  loadCurrent,
  unloadCurrent,
  update,
  routeId,
  revert,
  selectCurrent,
  selectErrorMessage,
  schemaFields,
  singular,
  goBack = 1,
  extraActions,
  extraEditActions,
}) {
  const mapStateToProps = state => ({
    alert: selectErrorMessage(state),
    model: selectCurrent(state),
    fields: schemaFields,
    title: `Edit ${singular}`,
  });

  const mapDispatchToProps = (dispatch, ownProps) => ({
    actions: {
      load: () => dispatch(loadCurrent({ id: ownProps.routeParams[routeId] })),
      save: () =>
        dispatch(
          save({
            pathname: ownProps.location.pathname,
            router: ownProps.router,
            goBack,
          })
        ),
      delete: () =>
        dispatch(
          destroy({
            pathname: ownProps.location.pathname,
            router: ownProps.router,
            goBack,
          })
        ),
      unload: () => dispatch(unloadCurrent()),
      update: options => dispatch(update(options)),
      revert: () => dispatch(revert()),
      ...extraActions(dispatch, ownProps),
      ...extraEditActions(dispatch, ownProps),
    },
  });

  return connect(mapStateToProps, mapDispatchToProps)(EditView);
}

export default createEditView;
