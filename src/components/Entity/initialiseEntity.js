/*
 * Created Date: Mon, 9th Apr 2018, 11:11:07 am
 * Author: Harry Crank
 * Email: harry.crank@thedistance.co.uk
 * -----
 * Last Modified: Wed, 25th Apr 2018, 09:42:00 am
 * Modified By: Ben Briggs
 * -----
 * Copyright (c) 2018 The Distance
 */

import convertSchemaToFields from './convertSchemaToFields';
import convertSchemaToIndex from './convertSchemaToIndex';
import convertSchemaToReadOnlyFields from './convertSchemaToReadOnlyFields';
import filterSchemaByPropExists from './filterSchemaByPropExists';
import filterSchemaByPropNotExists from './filterSchemaByPropNotExists';
import generateInflections from './inflections';

function initialiseEntity({
  name,
  inflections = {},
  collectionIndex,
  schema = {},
  readOnly,
  readOnlyEditView,
  readOnlyCreateView,
}) {
  const { camelised, singular, plural, slug } = generateInflections(
    name,
    inflections
  );
  const filteredSchema = filterSchemaByPropNotExists('hideInEdit', schema);

  const { createSchemaFields, editSchemaFields } = readOnly
    ? {
        createSchemaFields: convertSchemaToReadOnlyFields(
          filteredSchema,
          'create'
        ),
        editSchemaFields: convertSchemaToReadOnlyFields(filteredSchema, 'edit'),
      }
    : {
        createSchemaFields: readOnlyCreateView
          ? convertSchemaToReadOnlyFields(filteredSchema, 'create')
          : convertSchemaToFields(filteredSchema, 'create'),
        editSchemaFields: readOnlyEditView
          ? convertSchemaToReadOnlyFields(filteredSchema, 'edit')
          : convertSchemaToFields(filteredSchema, 'edit'),
      };

  if (!collectionIndex) {
    collectionIndex = convertSchemaToIndex(
      filterSchemaByPropExists('showInIndex', schema)
    );
  }

  return {
    readOnly,
    readOnlyEditView,
    readOnlyCreateView,
    name,
    collectionIndex,
    schema,
    camelised,
    singular,
    plural,
    slug,
    createSchemaFields,
    editSchemaFields,
  };
}

export default initialiseEntity;
