/**
 * @Author: Ben Briggs <benbriggs>
 * @Date:   2017-12-22T11:55:08+00:00
 * @Email:  ben.briggs@thedistance.co.uk
 * @Last modified by:   benbriggs
 * @Last modified time: 2018-01-11T09:56:23+00:00
 * @Copyright: The Distance
 */

import Parse from 'parse';
import ParseMockDB from 'parse-mockdb';
import configureMockStore from 'redux-mock-store';
import thunk from 'redux-thunk';
import deepFreeze from 'deep-freeze';
import createEntity from './';

const Entity = Parse.Object.extend('Entity');
const ChildEntity = Parse.Object.extend('ChildEntity');
const mockStore = configureMockStore([thunk]);

beforeEach(() => {
  ParseMockDB.mockDB();
});

afterEach(() => {
  ParseMockDB.cleanUp();
  ParseMockDB.unMockDB();
});

test('should load a single entity', () => {
  const { actions, actionIds } = createEntity({
    name: 'Entity',
  });

  const store = mockStore({
    Parse,
    entity: { current: null, collection: null },
  });
  const entity = new Entity({ number: 1 });

  return entity
    .save()
    .then(entity => store.dispatch(actions.loadCurrent({ id: entity.id })))
    .then(() => {
      const payload = store.getActions();
      expect(payload[0].type).toEqual(actionIds.loadingCurrentAction);
      expect(payload[1].type).toEqual(actionIds.loadedCurrentAction);
      expect(payload[1].result).toBeInstanceOf(Entity);
    });
});

test('should load a single entity, with a custom query', () => {
  const { actions, actionIds } = createEntity({
    name: 'Entity',
    onLoadCurrent: query => query.include('child'),
  });

  const store = mockStore({
    Parse,
    entity: { current: null, collection: null },
  });

  const child = new ChildEntity({ foo: 'bar' });
  const entity = new Entity({ number: 1 });

  return Promise.all([child.save(), entity.save()])
    .then(([child, entity]) => entity.save({ child }))
    .then(entity => store.dispatch(actions.loadCurrent({ id: entity.id })))
    .then(() => {
      const payload = store.getActions();
      expect(payload[0].type).toEqual(actionIds.loadingCurrentAction);
      expect(payload[1].type).toEqual(actionIds.loadedCurrentAction);
      expect(payload[1].result).toBeInstanceOf(Entity);
      expect(payload[1].result.get('child').get('foo')).toEqual('bar');
    });
});

test('should load a single entity from the store if already defined', () => {
  const { actions, actionIds } = createEntity({
    name: 'Entity',
  });

  const entity = new Entity({ number: 1 });
  const store = mockStore({
    Parse,
    entity: { current: entity, collection: null },
  });

  return entity
    .save()
    .then(() => store.dispatch(actions.loadCurrent()))
    .then(() => {
      const payload = store.getActions();
      expect(payload[0].type).toEqual(actionIds.loadedCurrentAction);
      expect(payload[0].result).toBeInstanceOf(Entity);
    });
});

test('should load a collection of entities', () => {
  const { actions, actionIds } = createEntity({
    name: 'Entity',
  });

  const store = mockStore({
    Parse,
    entity: { current: null, collection: null },
  });

  const entity1 = new Entity({ number: 1 });
  const entity2 = new Entity({ number: 2 });

  return Promise.all([entity1.save(), entity2.save()])
    .then(() => store.dispatch(actions.loadCollection()))
    .then(() => {
      const payload = store.getActions();
      expect(payload[0].type).toEqual(actionIds.loadingCollectionAction);
      expect(payload[1].type).toEqual(actionIds.loadedCollectionAction);
      expect(payload[1].result).toHaveLength(2);
      expect(payload[1].result[0]).toBeInstanceOf(Entity);
      expect(payload[1].result[1]).toBeInstanceOf(Entity);
    });
});

test('should save the current entity', () => {
  const { actions, actionIds } = createEntity({
    name: 'Entity',
  });

  const entity = new Entity({ number: 1 });
  const store = mockStore({
    Parse,
    entity: { current: entity, collection: null },
  });

  expect(entity.id).toBeFalsy();

  return store.dispatch(actions.save()).then(() => {
    const payload = store.getActions();
    expect(payload[0].type).toEqual(actionIds.savingAction);
    expect(payload[1].type).toEqual(actionIds.savedAction);
    expect(payload[1].result.id).toBeTruthy();
  });
});

test('should update the current entity', () => {
  const { actions, actionIds } = createEntity({
    name: 'Entity',
  });

  const entity = new Entity({ number: 1 });
  const store = mockStore({
    Parse,
    entity: { current: entity, collection: null },
  });

  store.dispatch(actions.update({ key: 'number', value: 2 }));

  const payload = store.getActions();
  expect(payload[0].type).toEqual(actionIds.updateAction);
  expect(entity.get('number')).toEqual(2);
});

test('should revert changed keys on the current entity', () => {
  const { actions, actionIds } = createEntity({
    name: 'Entity',
  });

  const entity = new Entity({ number: 1 });
  const store = mockStore({
    Parse,
    entity: { current: entity, collection: null },
  });

  return entity
    .save()
    .then(() => store.dispatch(actions.update({ key: 'number', value: 2 })))
    .then(() => store.dispatch(actions.revert()))
    .then(() => {
      const payload = store.getActions();
      expect(payload[0].type).toEqual(actionIds.updateAction);
      expect(payload[1].type).toEqual(actionIds.revertAction);
      expect(payload[1].result.get('number')).toEqual(1);
    });
});

test('should not revert anything when a entity is not defined', () => {
  const { actions } = createEntity({
    name: 'Entity',
  });
  const store = mockStore({
    Parse,
    entity: { current: null, collection: null },
  });

  store.dispatch(actions.revert());

  const payload = store.getActions();
  expect(payload).toHaveLength(0);
});

test('should unload the current entity', () => {
  const { actions, actionIds } = createEntity({
    name: 'Entity',
  });

  const entity = new Entity({ number: 1 });
  const store = mockStore({
    Parse,
    entity: { current: entity, collection: null },
  });

  store.dispatch(actions.unloadCurrent());

  const payload = store.getActions();
  expect(payload[0].type).toEqual(actionIds.unloadCurrentAction);
});

test('should unload the current entitys', () => {
  const { actions, actionIds } = createEntity({
    name: 'Entity',
  });

  const entities = [new Entity({ number: 1 }), new Entity({ number: 2 })];
  const store = mockStore({
    Parse,
    entity: { current: null, collection: entities },
  });

  store.dispatch(actions.unloadCollection());

  const payload = store.getActions();
  expect(payload[0].type).toEqual(actionIds.unloadCollectionAction);
});

test('should create a new entity', () => {
  const { actions, actionIds } = createEntity({
    name: 'Entity',
  });

  const store = mockStore({ Parse });

  return Promise.resolve(true)
    .then(() => store.dispatch(actions.create()))
    .then(() => {
      const payload = store.getActions();
      expect(payload[0].type).toEqual(actionIds.newAction);
      expect(payload[0].result).toBeInstanceOf(Entity);
    });
});

test('should create a new entity with params', () => {
  const { actions, actionIds } = createEntity({
    name: 'Entity',
  });

  const store = mockStore({ Parse });

  return Promise.resolve(true)
    .then(() => store.dispatch(actions.create({ name: 'Chuck' })))
    .then(() => {
      const payload = store.getActions();
      expect(payload[0].type).toEqual(actionIds.newAction);
      expect(payload[0].result).toBeInstanceOf(Entity);
      expect(payload[0].result.get('name')).toEqual('Chuck');
    });
});

test('should delete the current entity', () => {
  const { actions, actionIds } = createEntity({
    name: 'Entity',
  });

  const entity = new Entity({ number: 1 });
  const store = mockStore({
    Parse,
    entity: { current: entity, collection: null },
  });

  return entity
    .save()
    .then(() => store.dispatch(actions.destroy()))
    .then(() => {
      const payload = store.getActions();
      expect(payload[0].type).toEqual(actionIds.deletingAction);
      expect(payload[1].type).toEqual(actionIds.deletedAction);
      return new Parse.Query('Entity').find();
    })
    .then(results => {
      expect(results).toHaveLength(0);
    });
});

/*
 * Reducer tests
 */

test('should set initial state', () => {
  const { reducer } = createEntity({
    name: 'Entity',
  });
  expect(reducer(undefined, {})).toEqual({
    collection: null,
    current: null,
    error: null,
    pages: null,
  });
});

test('should fetch a single entity', () => {
  const { actionCreators, reducer } = createEntity({
    name: 'Entity',
  });

  const entity = new Entity({ number: 5 });
  const before = { current: null, collection: null, pages: null };

  deepFreeze(before);

  return entity
    .save()
    .then(saved => actionCreators.loadedCurrentAction({ result: saved }))
    .then(action => {
      deepFreeze(action);

      const after = { current: action.result, collection: null, pages: null };

      expect(reducer(before, action)).toEqual(after);
    });
});

test('should fetch a list of entities', () => {
  const { actionCreators, reducer } = createEntity({
    name: 'Entity',
  });

  const entity5 = new Entity({ number: 5 });
  const entity6 = new Entity({ number: 6 });
  const before = { current: null, collection: [entity5, entity6], pages: null };

  return Promise.all([entity5.save(), entity6.save()])
    .then(result => actionCreators.loadedCollectionAction({ pages: 1, result }))
    .then(action => {
      deepFreeze(action);

      const after = {
        current: null,
        collection: action.result,
        pages: action.pages,
      };

      expect(reducer(before, action)).toEqual(after);
    });
});

test('should handle a entity update', () => {
  const { actionCreators, reducer } = createEntity({
    name: 'Entity',
  });

  const entity = new Entity({ number: 5 });
  const before = { current: entity, collection: null };

  return entity
    .save()
    .then(saved => actionCreators.updateAction({ result: saved }))
    .then(action => {
      deepFreeze(action);

      const after = { current: action.result, collection: null };

      expect(reducer(before, action)).toEqual(after);
    });
});

test('should handle a new entity', () => {
  const { actionCreators, reducer } = createEntity({
    name: 'Entity',
  });

  const entity = new Entity({ number: 5 });
  const before = { current: null, collection: null };

  return entity
    .save()
    .then(saved => actionCreators.newAction({ result: saved }))
    .then(action => {
      deepFreeze(action);

      const after = { current: action.result, collection: null };

      expect(reducer(before, action)).toEqual(after);
    });
});

test('should null the current entity on delete', () => {
  const { actionCreators, reducer } = createEntity({
    name: 'Entity',
  });

  const entity = new Entity({ number: 5 });
  const before = { current: entity, collection: null, error: null };

  return entity
    .save()
    .then(() => actionCreators.deletedAction({ result: entity }))
    .then(action => {
      deepFreeze(action);

      const after = { current: null, collection: null, error: null };

      expect(reducer(before, action)).toEqual(after);
    });
});

test('should null the current entity on revert', () => {
  const { actionCreators, reducer } = createEntity({
    name: 'Entity',
  });
  const entity = new Entity({ number: 5 });
  const before = { current: entity, collection: null, error: null };

  return entity
    .save()
    .then(() => actionCreators.revertAction({ result: entity }))
    .then(action => {
      deepFreeze(action);

      const after = { current: null, collection: null, error: null };

      expect(reducer(before, action)).toEqual(after);
    });
});

test('should null the current entity on unload', () => {
  const { actionCreators, reducer } = createEntity({
    name: 'Entity',
  });
  const entity = new Entity({ number: 5 });
  const before = {
    current: entity,
    collection: null,
    error: null,
    pages: null,
  };

  return entity
    .save()
    .then(() => actionCreators.unloadCurrentAction({ result: entity }))
    .then(action => {
      deepFreeze(action);

      const after = {
        current: null,
        collection: null,
        error: null,
        pages: null,
      };

      expect(reducer(before, action)).toEqual(after);
    });
});

test('should null the current entity collection on unload', () => {
  const { actionCreators, reducer } = createEntity({
    name: 'Entity',
  });
  return Promise.all([
    new Entity({ number: 5 }).save(),
    new Entity({ number: 6 }).save(),
  ])
    .then(saved => ({
      current: null,
      collection: saved,
    }))
    .then(before => {
      deepFreeze(before);
      return before;
    })
    .then(before =>
      Promise.all([
        before,
        actionCreators.unloadCollectionAction({ result: before.collection }),
      ])
    )
    .then(([before, action]) => {
      deepFreeze(action);

      const after = { current: null, collection: null, pages: null };

      expect(reducer(before, action)).toEqual(after);
    });
});
