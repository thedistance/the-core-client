/**
 * @Author: Ben Briggs <benbriggs>
 * @Date:   2018-02-06T11:27:43+00:00
 * @Email:  ben.briggs@thedistance.co.uk
 * @Last modified by:   benbriggs
 * @Last modified time: 2018-02-06T11:37:23+00:00
 * @Copyright: The Distance
 */

import * as R from 'ramda';

const filterSchema = R.curry((property, schema) =>
  R.pickBy((value, key) => R.not(R.prop(property, schema[key])), schema)
);

export default filterSchema;
