/**
 * @Author: Ben Briggs <benbriggs>
 * @Date:   2018-02-01T11:08:02+00:00
 * @Email:  ben.briggs@thedistance.co.uk
 * @Last modified by:   benbriggs
 * @Last modified time: 2018-02-01T11:10:30+00:00
 * @Copyright: The Distance
 */

import React from 'react';
import { Route } from 'react-router';
import combineEntityRoutes from './combineEntityRoutes';

test('should combine entity routes', () => {
  const entity1 = {
    routes: [<Route path={`foo/bar`} component={jest.fn()} key={1} />],
  };
  const entity2 = {
    routes: [<Route path={`baz/quux`} component={jest.fn()} key={2} />],
  };

  expect(combineEntityRoutes(entity1, entity2)).toMatchSnapshot();
});
