/*
 * Created Date: Mon, 9th Apr 2018, 11:11:30 am
 * Author: Harry Crank
 * Email: harry.crank@thedistance.co.uk
 * -----
 * Last Modified: Mon, 16th Apr 2018, 11:56:24 am
 * Modified By: Harry Crank
 * -----
 * Copyright (c) 2018 The Distance
 */

import React from 'react';
import { Route } from 'react-router';
import * as R from 'ramda';
import createListView from './createListView';
import createNewView from './createNewView';
import createEditView from './createEditView';
import createReadOnlyView from './createReadOnlyView';

const alwaysObj = R.always({});

function initialiseViews(
  entity,
  {
    canCreateNew = true,
    createView,
    editView,
    editButton,
    listView,
    extraActions = alwaysObj,
    extraCreateActions = alwaysObj,
    extraEditActions = alwaysObj,
  }
) {
  const {
    readOnly,
    readOnlyEditView,
    camelised,
    collectionIndex,
    plural,
    createSchemaFields,
    editSchemaFields,
    singular,
    slug,
    selectors: {
      selectCollection,
      selectCurrent,
      selectErrorMessage,
      selectPages,
    },
    actions: {
      create,
      destroy,
      loadCollection,
      loadCurrent,
      save,
      revert,
      unloadCollection,
      unloadCurrent,
      update,
    },
    getTheadFilterThProps,
  } = entity;

  const routeId = `${camelised}Id`;

  const exposedCreateView =
    createView ||
    createNewView({
      create,
      save,
      unloadCurrent,
      update,
      revert,
      extraActions,
      extraCreateActions,
      selectCurrent,
      selectErrorMessage,
      schemaFields: createSchemaFields,
      singular,
    });

  let exposedEditView;

  if (editView) {
    exposedEditView = editView;
  } else if (readOnly || readOnlyEditView) {
    exposedEditView = createReadOnlyView({
      loadCurrent,
      unloadCurrent,
      revert,
      extraActions,
      extraEditActions,
      routeId,
      selectCurrent,
      selectErrorMessage,
      schemaFields: editSchemaFields,
      singular,
    });
  } else {
    exposedEditView = createEditView({
      save,
      destroy,
      loadCurrent,
      unloadCurrent,
      update,
      revert,
      extraActions,
      extraEditActions,
      routeId,
      selectCurrent,
      selectErrorMessage,
      schemaFields: editSchemaFields,
      singular,
    });
  }

  const exposedListView =
    listView ||
    createListView({
      loadCollection,
      unloadCollection,
      selectCollection,
      selectPages,
      singular,
      plural,
      collectionIndex,
      canCreateNew,
      readOnly: readOnly || readOnlyEditView,
      editButton,
      getTheadFilterThProps,
    });

  return {
    ...entity,
    routeId,
    createView: exposedCreateView,
    editView: exposedEditView,
    listView: exposedListView,
    routes: [
      <Route
        path={`/${slug}`}
        component={exposedListView}
        key={`${camelised}__list`}
      />,
      <Route
        path={`/${slug}/new`}
        component={exposedCreateView}
        key={`${camelised}__create`}
      />,
      <Route
        path={`/${slug}/:${routeId}`}
        component={exposedEditView}
        key={`${camelised}__edit`}
      />,
    ],
    extraActions,
    extraCreateActions,
    extraEditActions,
  };
}

export default initialiseViews;
