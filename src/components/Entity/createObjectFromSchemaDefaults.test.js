/**
 * @Author: benbriggs
 * @Date:   2018-04-19T16:03:34+01:00
 * @Email:  ben.briggs@thedistance.co.uk
 * @Last modified by:   benbriggs
 * @Last modified time: 2018-04-19T16:06:03+01:00
 * @Copyright: The Distance
 */

import createObjectFromSchemaDefaults from './createObjectFromSchemaDefaults';

test('should create an object from the schema defaults', () =>
  expect(
    createObjectFromSchemaDefaults({
      title: {
        defaultTo: 'wow',
        type: 'string',
      },
      content: {
        type: 'html',
      },
    })
  ).toEqual({ title: 'wow' }));
