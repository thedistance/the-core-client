/*
 * Created Date: Fri, 13th Apr 2018, 15:25:28 pm
 * Author: Harry Crank
 * Email: harry.crank@thedistance.co.uk
 * Copyright (c) 2018 The Distance
 */

/* eslint-disable func-names */

import React from 'react';
import PropTypes from 'prop-types';
import CheckboxesControl from '../CheckboxesControl';
import DangerousHtmlControl from '../DangerousHtmlControl';
import ReadOnlyGeoPointControl from '../ReadOnlyGeoPointControl';
import ReadOnlyNumberControl from '../ReadOnlyNumberControl';
import ReadOnlyNumericSelectControl from '../ReadOnlyNumericSelectControl';
import PasswordControl from '../PasswordControl';
import ReadOnlyPointerControl from '../ReadOnlyPointerControl';
import StaticTextControl from '../StaticTextControl';
import ReadOnlyBooleanControl from '../ReadOnlyBooleanControl';
import ReadOnlyDateControl from '../ReadOnlyDateControl';
import ReadOnlyFileControl from '../ReadOnlyFileControl';

function prepopulatedReadOnlyBooleanControl(choices) {
  return function PopulatedReadOnlyBooleanControl(props) {
    return <ReadOnlyBooleanControl choices={choices} {...props} />;
  };
}

function prepopulatedCheckboxesControl(choices) {
  return function PopulatedCheckboxesControl(props) {
    return <CheckboxesControl collection={choices} {...props} />;
  };
}

function prepopulatedReadOnlyPointerControl(ParseObject, loadAction) {
  function PopulatedReadOnlyPointerControl(props) {
    return (
      <ReadOnlyPointerControl
        ParseObject={ParseObject}
        loadOptions={props.actions[loadAction]}
        {...props}
      />
    );
  }

  PopulatedReadOnlyPointerControl.propTypes = {
    actions: PropTypes.objectOf(PropTypes.func),
  };

  return PopulatedReadOnlyPointerControl;
}

function prepopulatedReadOnlyNumericSelectControl(choices, allowBlank) {
  return function PopulatedReadOnlyNumericSelectControl(props) {
    return (
      <ReadOnlyNumericSelectControl
        allowBlank={allowBlank}
        collection={choices}
        {...props}
      />
    );
  };
}

const fieldToReadOnlyComponent = (field, view) =>
  (field.EditComponent
    ? field.EditComponent
    : view === 'create' ? field.CreateComponent : field.UpdateComponent) ||
  {
    boolean: prepopulatedReadOnlyBooleanControl(field.choices),
    checkboxes: prepopulatedCheckboxesControl(field.choices),
    enumNumeric: prepopulatedReadOnlyNumericSelectControl(
      field.choices,
      field.allowBlank
    ),
    file: ReadOnlyFileControl,
    geopoint: ReadOnlyGeoPointControl,
    htmlFroala: DangerousHtmlControl,
    number: ReadOnlyNumberControl,
    password: PasswordControl,
    pointer: prepopulatedReadOnlyPointerControl(
      field.ParseObject,
      field.loadAction
    ),
    string: StaticTextControl,
    text: StaticTextControl,
    textarea: StaticTextControl,
    staticDate: ReadOnlyDateControl,
    staticText: StaticTextControl,
  }[field.type] ||
  StaticTextControl;

export default fieldToReadOnlyComponent;
