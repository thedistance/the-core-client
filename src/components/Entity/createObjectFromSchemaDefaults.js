/**
 * @Author: benbriggs
 * @Date:   2018-04-19T16:00:32+01:00
 * @Email:  ben.briggs@thedistance.co.uk
 * @Last modified by:   benbriggs
 * @Last modified time: 2018-04-19T16:15:14+01:00
 * @Copyright: The Distance
 */

import * as R from 'ramda';

const createObjectFromSchemaDefaults = schema =>
  R.keys(schema).reduce(
    (params, key) => ({
      ...params,
      ...(schema[key].defaultTo && { [key]: schema[key].defaultTo }),
    }),
    {}
  );

export default createObjectFromSchemaDefaults;
