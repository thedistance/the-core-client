/*
 * Created Date: Fri, 13th Apr 2018, 15:21:24 pm
 * Author: Harry Crank
 * Email: harry.crank@thedistance.co.uk
 * Copyright (c) 2018 The Distance
 */

import { titleize } from 'inflected';
import fieldToReadOnlyComponent from './fieldToReadOnlyComponent';
import parseObjectAccessor from './parseObjectAccessor';

const convertSchemaToReadOnlyFields = (schema, view) =>
  Object.keys(schema).map(key => ({
    accessor: parseObjectAccessor(
      schema[key].editAccessor || schema[key].accessor,
      key
    ),
    Component: fieldToReadOnlyComponent(schema[key], view),
    key,
    label: schema[key].label || titleize(key),
    validator: schema[key].validator,
  }));

export default convertSchemaToReadOnlyFields;
