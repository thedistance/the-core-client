# Entities

## Schema

The schema is considered the single source of truth for the various Entity
fields. It is an object, with keys that either correspond to Parse fields, or
relate to another Entity in the system. The chosen behaviour depends on the
`type` of the field. A basic schema for a class with a string field may look
something like this:

```js
{
  title: {
    type: 'string',
  }
}
```

### Field globals

All fields support some shared functionality.

#### accessor

Type: `Function`

This function is passed the current Parse object for the Entity. You should not
need to override this unless you need to access nested Parse objects.

```js
{
  nestedObj: {
    accessor: obj => obj.get('nested').get('value')
  }
}
```

#### filterable

Type: `Boolean`  
Default: `true`

This is passed straight through to React Table - it controls whether the list
of entities can be searched by this field.

```js
{
  searchField: {
    filterable: true
  }
}
```

#### hideInEdit

Type: `Boolean`  
Default: `false`

By default, all schema fields will appear in the edit view. Use this to hide
the field from the editor.

```js
{
  hidden: {
    hideInEdit: true
  }
}
```

#### label

Type: `String`

Allows you to set a custom label for the field, in case the default behaviour
of titlecasing the field name is not suitable.

```js
{
  fieldOne: {
    label: 'First ever field' // If this were not set, it would be "Field One"
  }
}
```

#### showInIndex

Type: `Boolean`

If this is set then the field will be added to the list view.

```js
{
  title: {
    showInIndex: true
  }
}
```

#### sortable

Type: `Boolean`  
Default: `true`

This is passed straight through to React Table - it controls whether the list
of entities can be ordered by this field or not.

```js
{
  sortedField: {
    sortable: true
  }
}
```

#### validator

Type: `Function`

This function will be used by the edit views in order to display some validation
of the field. You are passed the value of the field and are expected to return
a boolean.

```js
{
  validatedField: {
    validator: R.identity // Use Ramda's identity function as a presence validator  
  }
}
```

### Field types

#### boolean

Shows a true or false selection.

```js
{
  booleanField: {
    type: 'boolean'
  }
}
```

##### options

###### choices

Type: `Array<String>`

You can use this to change the true/false labels.

```js
{
  booleanField: {
    type: 'boolean',
    choices: ['Yes', 'No']
  }
}
```

#### enum

Shows a select box.

##### options

###### choices

Type: `Array<String>`

The choices to pick from.

```js
{
  enumField: {
    type: 'enum',
    choices: ['Coke', '7-up', 'Fanta']
  }
}
```

#### file

Shows a file upload field.

```js
{
  fileField: {
    type: 'file'
  }
}
```

#### geopoint

Shows a pair of number fields for entering latitude and longitude.

```js
{
  geopointField: {
    type: 'geopoint'
  }
}
```

#### html

Shows a WYSIWYG editor field.

```js
{
  htmlField: {
    type: 'html'
  }
}
```

#### number

Shows a number entry field.

```js
{
  numberField: {
    type: 'number'
  }
}
```

#### password

Shows a password field.

```js
{
  passwordField: {
    type: 'password'
  }
}
```

#### string

> Also aliased as `text`.

Shows a text field. Note that if a field does not declare a `type`, or if the
type is unknown then this component will be shown instead.

```js
{
  stringField: {
    type: 'string'
  }
}
```

#### textarea

Shows a larger text field.

```js
{
  textareaField: {
    type: 'textarea'
  }
}
```

## API

### createEntity([options])

#### options

##### schema

Type: `Object`

See the *Schema* section.
