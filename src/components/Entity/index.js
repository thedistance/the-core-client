/**
 * @Author: Ben Briggs <benbriggs>
 * @Date:   2017-10-03T17:41:45+01:00
 * @Email:  ben.briggs@thedistance.co.uk
 * @Last modified by:   benbriggs
 * @Last modified time: 2018-04-25T09:48:05+01:00
 * @Copyright: The Distance
 */

import * as R from 'ramda';
import initialiseEntity from './initialiseEntity';
import initialiseActions from './initialiseActions';
import initialiseViews from './initialiseViews';

function createEntity({
  name,
  inflections,
  collectionIndex,
  parseClass = name,
  canCreateNew = true,
  onLoadCollection = R.identity,
  onLoadCurrent = R.identity,
  reducerActions = {},
  schema = {},
  createView,
  editView,
  editButton = true,
  readOnly = false,
  readOnlyEditView = false,
  readOnlyCreateView = false,
  listView,
  extraActions,
  extraCreateActions,
  extraEditActions,
  getTheadFilterThProps,
}) {
  let entity = initialiseEntity({
    name,
    inflections,
    collectionIndex,
    schema,
    readOnly,
    readOnlyEditView,
    readOnlyCreateView,
    getTheadFilterThProps,
  });
  entity = initialiseActions(entity, {
    parseClass,
    onLoadCollection,
    onLoadCurrent,
    reducerActions,
  });
  return initialiseViews(entity, {
    canCreateNew,
    createView,
    editView,
    editButton,
    listView,
    extraActions,
    extraCreateActions,
    extraEditActions,
  });
}

export default createEntity;
