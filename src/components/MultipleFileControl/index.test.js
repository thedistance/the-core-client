/**
 * @Author: Ben Briggs <benbriggs>
 * @Date:   2017-12-18T17:10:52+00:00
 * @Email:  ben.briggs@thedistance.co.uk
 * @Last modified by:   benbriggs
 * @Last modified time: 2018-01-12T14:56:08+00:00
 * @Copyright: The Distance
 */

import React from 'react';
import { shallow } from 'enzyme';
import toJson from 'enzyme-to-json';
import MultipleFileControl from './';

describe('MultipleFileControl', () => {
  test('should render', () => {
    const component = shallow(
      <MultipleFileControl label={`Upload file`} onChange={jest.fn()} />
    );

    expect(toJson(component.shallow())).toMatchSnapshot();
  });
});
