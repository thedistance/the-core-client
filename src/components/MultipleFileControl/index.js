/**
 * @Author: Ben Briggs <benbriggs>
 * @Date:   2017-12-18T15:49:01+00:00
 * @Email:  ben.briggs@thedistance.co.uk
 * @Last modified by:   benbriggs
 * @Last modified time: 2018-01-12T14:49:49+00:00
 * @Copyright: The Distance
 */

import React from 'react';
import Parse from 'parse';
import { FormControl, ControlLabel } from '@sketchpixy/rubix';
import FormGroup from '../FormGroup';
import GenericControl from '../GenericControl';

class MultipleFileControl extends GenericControl {
  handleChange({ target }) {
    if (target.files.length > 0) {
      const { files } = target;

      const value = Array.from(files).map(
        file => new Parse.File(file.name, file)
      );

      this.setState({ value });
      this.props.onChange(value, true);
    }
  }

  render() {
    return (
      <FormGroup
        validationState={this.validationState()}
        required={this.props.required}
      >
        <ControlLabel>{this.props.label}</ControlLabel>
        <FormControl type="file" onChange={this.handleChange} multiple />
      </FormGroup>
    );
  }
}

export default MultipleFileControl;
