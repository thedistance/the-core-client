/**
 * @Author: Ben Briggs <benbriggs>
 * @Date:   2018-02-20T11:34:04+00:00
 * @Email:  ben.briggs@thedistance.co.uk
 * @Last modified by:   benbriggs
 * @Last modified time: 2018-02-20T11:34:52+00:00
 * @Copyright: The Distance
 */

import React from 'react';
import { action } from '@storybook/addon-actions';
import { storiesOf } from '@storybook/react';
import backdrop from '../../../.storybook/backdrop';
import NumericSelectControl from './';

storiesOf('NumericSelectControl', module)
  .addDecorator(backdrop)
  .add('component', () => (
    <NumericSelectControl
      label={`Fruit`}
      collection={[1, 2, 3]}
      value={2}
      onChange={action('NumericSelectControl:onChange')}
    />
  ))
  .add('with blank option', () => (
    <NumericSelectControl
      allowBlank={true}
      label={`Fruit`}
      collection={[1, 2, 3]}
      value={2}
      onChange={action('NumericSelectControl:onChange')}
    />
  ))
  .add('with label support', () => (
    <NumericSelectControl
      label={`Fruit`}
      collection={[
        { label: 'Apples', value: 1 },
        { label: 'Bananas', value: 2 },
        { label: 'Pears', value: 3 },
      ]}
      value={2}
      onChange={action('NumericSelectControl:onChange')}
    />
  ));
