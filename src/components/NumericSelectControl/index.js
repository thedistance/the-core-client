/**
 * @Author: Ben Briggs <benbriggs>
 * @Date:   2018-02-20T11:32:30+00:00
 * @Email:  ben.briggs@thedistance.co.uk
 * @Last modified by:   benbriggs
 * @Last modified time: 2018-02-20T11:40:44+00:00
 * @Copyright: The Distance
 */

import React from 'react';
import PropTypes from 'prop-types';
import SelectControl from '../SelectControl';

const NumericSelectControl = ({ onChange, ...rest }) => (
  <SelectControl
    {...rest}
    onChange={(value, valid) => onChange(parseInt(value, 10), valid)}
  />
);

NumericSelectControl.propTypes = {
  onChange: PropTypes.func,
};

export default NumericSelectControl;
