/**
 * @Author: Ben Briggs <benbriggs>
 * @Date:   2018-02-20T11:36:52+00:00
 * @Email:  ben.briggs@thedistance.co.uk
 * @Last modified by:   benbriggs
 * @Last modified time: 2018-02-20T11:37:39+00:00
 * @Copyright: The Distance
 */

import React from 'react';
import { shallow } from 'enzyme';
import toJson from 'enzyme-to-json';
import NumericSelectControl from './';

test('should render', () => {
  const component = shallow(
    <NumericSelectControl
      label={`Numbers`}
      collection={[1, 2, 3]}
      onChange={jest.fn()}
    />
  );

  expect(toJson(component.shallow())).toMatchSnapshot();
});

test('should render with a value', () => {
  const component = shallow(
    <NumericSelectControl
      label={`Numbers`}
      collection={[1, 2, 3]}
      value={1}
      onChange={jest.fn()}
    />
  );

  expect(toJson(component.shallow())).toMatchSnapshot();
});

test('should render with labels and values', () => {
  const component = shallow(
    <NumericSelectControl
      label={`Fruit`}
      collection={[
        { label: 'Apples', value: 1 },
        { label: 'Bananas', value: 2 },
        { label: 'Pears', value: 3 },
      ]}
      value={3}
      onChange={jest.fn()}
    />
  );

  expect(toJson(component.shallow())).toMatchSnapshot();
});
