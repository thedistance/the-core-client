/**
 * @Author: Ben Briggs <benbriggs>
 * @Date:   2017-12-20T17:11:52+00:00
 * @Email:  ben.briggs@thedistance.co.uk
 * @Last modified by:   benbriggs
 * @Last modified time: 2018-01-11T09:19:06+00:00
 * @Copyright: The Distance
 */

import React from 'react';
import { storiesOf } from '@storybook/react';
import { withInfo } from '@storybook/addon-info';
import backdrop from '../../../.storybook/backdrop';
import NotFound from './';

storiesOf('NotFound', module)
  .addDecorator(backdrop)
  .add('component', withInfo()(() => <NotFound />));
