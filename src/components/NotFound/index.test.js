/**
 * @Author: Ben Briggs <benbriggs>
 * @Date:   2017-12-20T17:13:06+00:00
 * @Email:  ben.briggs@thedistance.co.uk
 * @Last modified by:   benbriggs
 * @Last modified time: 2017-12-20T17:13:13+00:00
 * @Copyright: The Distance
 */

import React from 'react';
import { shallow } from 'enzyme';
import toJson from 'enzyme-to-json';
import NotFound from './';

test('should render (defaults)', () => {
  const component = shallow(<NotFound />);

  expect(toJson(component.shallow())).toMatchSnapshot();
});
