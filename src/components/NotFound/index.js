/**
 * @Author: Ben Briggs <benbriggs>
 * @Date:   2017-12-20T17:09:45+00:00
 * @Email:  ben.briggs@thedistance.co.uk
 * @Last modified by:   benbriggs
 * @Last modified time: 2018-01-11T10:00:27+00:00
 * @Copyright: The Distance
 */

import React from 'react';
import { Grid, Row, Col } from '@sketchpixy/rubix';
import LinkButton from '../LinkButton';
import Panel from '../Panel';

const NotFound = () => (
  <Panel title={`Page not found.`} className={`bg-red`}>
    <Grid>
      <Row>
        <Col xs={12}>
          <p>
            <LinkButton pathname={`/`}>Return to home</LinkButton>
          </p>
        </Col>
      </Row>
    </Grid>
  </Panel>
);

export default NotFound;
