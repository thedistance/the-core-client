/**
 * @Author: Ben Briggs <benbriggs>
 * @Date:   2018-01-03T17:17:25+00:00
 * @Email:  ben.briggs@thedistance.co.uk
 * @Last modified by:   benbriggs
 * @Last modified time: 2018-01-11T09:47:15+00:00
 * @Copyright: The Distance
 */

import React from 'react';
import PropTypes from 'prop-types';
import SelectControl from '../SelectControl';

const BooleanControl = ({ choices, value, onChange, ...rest }) => (
  <SelectControl
    collection={choices}
    {...rest}
    onChange={(value, valid) => onChange(value === choices[0], valid)}
    value={choices[Number(!value)]}
  />
);

BooleanControl.propTypes = {
  choices: PropTypes.array,
  onChange: PropTypes.func,
  value: PropTypes.bool,
};

BooleanControl.defaultProps = {
  choices: ['true', 'false'],
};

export default BooleanControl;
