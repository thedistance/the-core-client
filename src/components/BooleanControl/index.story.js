/**
 * @Author: Ben Briggs <benbriggs>
 * @Date:   2018-01-03T17:18:47+00:00
 * @Email:  ben.briggs@thedistance.co.uk
 * @Last modified by:   benbriggs
 * @Last modified time: 2018-01-04T13:11:52+00:00
 * @Copyright: The Distance
 */

import React from 'react';
import { action } from '@storybook/addon-actions';
import { storiesOf } from '@storybook/react';
import { withInfo } from '@storybook/addon-info';
import backdrop from '../../../.storybook/backdrop';
import BooleanControl from './';

storiesOf('BooleanControl', module)
  .addDecorator(backdrop)
  .add(
    'component',
    withInfo()(() => (
      <BooleanControl
        label={`Promoted`}
        onChange={action('BooleanControl:onChange')}
        value={true}
      />
    ))
  )
  .add(
    'with custom choices',
    withInfo()(() => (
      <BooleanControl
        label={`Promoted`}
        choices={[`Yes`, `No`]}
        onChange={action('BooleanControl:onChange')}
        value={true}
      />
    ))
  );
