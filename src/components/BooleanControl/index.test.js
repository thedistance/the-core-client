/**
 * @Author: Ben Briggs <benbriggs>
 * @Date:   2018-01-04T13:01:55+00:00
 * @Email:  ben.briggs@thedistance.co.uk
 * @Last modified by:   benbriggs
 * @Last modified time: 2018-01-04T13:10:38+00:00
 * @Copyright: The Distance
 */

import React from 'react';
import { shallow } from 'enzyme';
import toJson from 'enzyme-to-json';
import BooleanControl from './';

test('should render', () => {
  const component = shallow(
    <BooleanControl label={`Over`} onChange={jest.fn()} />
  );

  expect(toJson(component.shallow())).toMatchSnapshot();
});

test('should render with a value', () => {
  const component = shallow(
    <BooleanControl onChange={jest.fn()} value={true} />
  );

  expect(toJson(component.shallow())).toMatchSnapshot();
});

test('should render with custom choices (true case)', () => {
  const component = shallow(
    <BooleanControl choices={['yes', 'no']} onChange={jest.fn()} value={true} />
  );

  expect(toJson(component.shallow())).toMatchSnapshot();
});

test('should render with custom choices (false case)', () => {
  const component = shallow(
    <BooleanControl
      choices={['yes', 'no']}
      onChange={jest.fn()}
      value={false}
    />
  );

  expect(toJson(component.shallow())).toMatchSnapshot();
});

test('should handle change', () => {
  const mock = jest.fn();
  const component = shallow(<BooleanControl onChange={mock} />);

  component.simulate('change', 'true', true);
  expect(mock).toHaveBeenCalledWith(true, true);
});

test('should handle change with custom choices', () => {
  const mock = jest.fn();
  const component = shallow(
    <BooleanControl onChange={mock} choices={['yes', 'no']} />
  );

  component.simulate('change', 'yes', true);
  expect(mock).toHaveBeenCalledWith(true, true);
});
