/*
 * Created Date: Thu, 19th Apr 2018, 09:19:39 am
 * Author: Harry Crank
 * Email: harry.crank@thedistance.co.uk
 * Copyright (c) 2018 The Distance
 */

import React from 'react';
import { shallow } from 'enzyme';
import toJson from 'enzyme-to-json';
import ReadOnlyNumberControl from './';

test('should render', () => {
  const component = shallow(
    <ReadOnlyNumberControl label={`Over`} onChange={jest.fn()} />
  );

  expect(toJson(component.shallow())).toMatchSnapshot();
});

test('should render with a value', () => {
  const component = shallow(
    <ReadOnlyNumberControl label={`Over`} onChange={jest.fn()} value={9000} />
  );

  expect(toJson(component.shallow())).toMatchSnapshot();
});
