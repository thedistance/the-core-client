/*
 * Created Date: Wed, 18th Apr 2018, 12:47:11 pm
 * Author: Harry Crank
 * Email: harry.crank@thedistance.co.uk
 * Copyright (c) 2018 The Distance
 */

import React from 'react';
import StaticTextControl from '../StaticTextControl';

const ReadOnlyNumberControl = rest => <StaticTextControl {...rest} />;

export default ReadOnlyNumberControl;
