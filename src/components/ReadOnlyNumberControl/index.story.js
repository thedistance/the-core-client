/*
 * Created Date: Thu, 19th Apr 2018, 08:56:24 am
 * Author: Harry Crank
 * Email: harry.crank@thedistance.co.uk
 * Copyright (c) 2018 The Distance
 */

import React from 'react';
import { storiesOf } from '@storybook/react';
import backdrop from '../../../.storybook/backdrop';
import ReadOnlyNumberControl from './';

storiesOf('ReadOnlyNumberControl', module)
  .addDecorator(backdrop)
  .add('component', () => <ReadOnlyNumberControl label={`Order`} value={2} />);
