/**
 * @Author: Ben Briggs <benbriggs>
 * @Date:   2017-12-15T14:09:40+00:00
 * @Email:  ben.briggs@thedistance.co.uk
 * @Last modified by:   benbriggs
 * @Last modified time: 2018-04-20T12:21:53+01:00
 * @Copyright: The Distance
 */

import React from 'react';
import PropTypes from 'prop-types';
import {
  ControlLabel,
  FormControlFeedback,
  HelpBlock,
} from '@sketchpixy/rubix';
import FormGroup from '../FormGroup';

class GenericControl extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      value: props.value,
      valid: null,
    };
    this.handleChange = this.handleChange.bind(this);
  }

  validationState() {
    const { valid } = this.state;
    if (valid) {
      return 'success';
    }
    if (valid !== null) {
      return 'error';
    }
  }

  handleChange({ target }) {
    const { value } = target;
    const valid = this.props.validator ? this.props.validator(value) : null;

    this.setState({
      value,
      valid,
    });

    this.props.onChange(value, valid);
  }

  render() {
    const { actions, children, validator, helpText, ...rest } = this.props;
    const { value } = this.state;
    return (
      <FormGroup
        validationState={this.validationState()}
        required={this.props.required}
      >
        <ControlLabel>{this.props.label}</ControlLabel>
        {React.cloneElement(children, {
          ...rest,
          onChange: this.handleChange,
          value,
        })}
        <FormControlFeedback />
        {helpText ? <HelpBlock>{helpText}</HelpBlock> : null}
      </FormGroup>
    );
  }
}

GenericControl.defaultProps = {
  onChange: () => {},
};

GenericControl.propTypes = {
  actions: PropTypes.objectOf(PropTypes.func),
  children: PropTypes.any,
  onChange: PropTypes.func,
  validator: PropTypes.func,
  label: PropTypes.string,
  required: PropTypes.bool,
  value: PropTypes.any,
};

export default GenericControl;
