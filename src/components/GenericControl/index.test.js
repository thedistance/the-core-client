/**
 * @Author: Ben Briggs <benbriggs>
 * @Date:   2017-12-18T15:24:21+00:00
 * @Email:  ben.briggs@thedistance.co.uk
 * @Last modified by:   benbriggs
 * @Last modified time: 2017-12-18T16:02:13+00:00
 * @Copyright: The Distance
 */

import React from 'react';
import { shallow } from 'enzyme';
import toJson from 'enzyme-to-json';
import GenericControl from './';

test('should not validate without a validation function', () => {
  const onChange = jest.fn();
  const component = shallow(
    <GenericControl onChange={onChange}>
      <input type={`text`} />
    </GenericControl>
  );

  component.find('input').simulate('change', { target: { value: 'Changed' } });
  expect(component.state().valid).toEqual(null);
  expect(toJson(component.shallow())).toMatchSnapshot();
});

test('should validate with a validation function (error case)', () => {
  const onChange = jest.fn();
  const component = shallow(
    <GenericControl
      onChange={onChange}
      validator={value => Boolean(value.length > 10)}
    >
      <input type={`text`} />
    </GenericControl>
  );

  component.find('input').simulate('change', { target: { value: 'Error' } });
  expect(component.state().valid).toEqual(false);
  expect(toJson(component.shallow())).toMatchSnapshot();
});

test('should validate with a validation function (success case)', () => {
  const onChange = jest.fn();
  const component = shallow(
    <GenericControl
      onChange={onChange}
      validator={value => Boolean(value.length > 10)}
    >
      <input type={`text`} />
    </GenericControl>
  );

  component
    .find('input')
    .simulate('change', { target: { value: 'This should succeed!' } });
  expect(component.state().valid).toEqual(true);
  expect(toJson(component.shallow())).toMatchSnapshot();
});

test('should show the correct class when required', () => {
  const onChange = jest.fn();
  const component = shallow(
    <GenericControl onChange={onChange} required={true}>
      <input type={`text`} />
    </GenericControl>
  );

  expect(toJson(component.shallow())).toMatchSnapshot();
});
