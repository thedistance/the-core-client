/**
 * @Author: Ben Briggs <benbriggs>
 * @Date:   2017-12-19T15:11:39+00:00
 * @Email:  ben.briggs@thedistance.co.uk
 * @Last modified by:   benbriggs
 * @Last modified time: 2018-01-11T09:47:02+00:00
 * @Copyright: The Distance
 */

import React from 'react';
import { storiesOf } from '@storybook/react';
import { withInfo } from '@storybook/addon-info';
import backdrop from '../../../.storybook/backdrop';
import Footer from './';

storiesOf('Footer', module)
  .addDecorator(backdrop)
  .add('component', withInfo()(() => <Footer />))
  .add('custom version', withInfo()(() => <Footer version={`v20.0.0`} />));
