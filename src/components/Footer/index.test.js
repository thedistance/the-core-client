/**
 * @Author: Ben Briggs <benbriggs>
 * @Date:   2017-12-19T15:10:52+00:00
 * @Email:  ben.briggs@thedistance.co.uk
 * @Last modified by:   benbriggs
 * @Last modified time: 2017-12-19T16:11:03+00:00
 * @Copyright: The Distance
 */

import React from 'react';
import { shallow } from 'enzyme';
import toJson from 'enzyme-to-json';
import Footer from './';

test('should render (defaults)', () => {
  const component = shallow(<Footer />);

  expect(toJson(component.shallow())).toMatchSnapshot();
});

test('should render (custom version)', () => {
  const component = shallow(<Footer version={`v20.0.0`} />);

  expect(toJson(component.shallow())).toMatchSnapshot();
});
