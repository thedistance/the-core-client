/**
 * @Author: Ben Briggs <benbriggs>
 * @Date:   2017-12-19T15:05:58+00:00
 * @Email:  ben.briggs@thedistance.co.uk
 * @Last modified by:   benbriggs
 * @Last modified time: 2018-01-11T09:33:39+00:00
 * @Copyright: The Distance
 */

import React from 'react';
import PropTypes from 'prop-types';
import { Row, Col, Grid } from '@sketchpixy/rubix';

const Footer = ({ version }) => (
  <div id="footer-container">
    <Grid id="footer" className="text-center">
      <Row>
        <Col xs={12}>
          <p>© 2018 The Distance - {version}</p>
        </Col>
      </Row>
    </Grid>
  </div>
);

Footer.propTypes = {
  version: PropTypes.string,
};

Footer.defaultProps = {
  version: 'v1.0.0',
};

export default Footer;
