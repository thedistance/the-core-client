/*
 * Created Date: Thu, 19th Apr 2018, 08:49:56 am
 * Author: Harry Crank
 * Email: harry.crank@thedistance.co.uk
 * Copyright (c) 2018 The Distance
 */

import React from 'react';
import { storiesOf } from '@storybook/react';
import { withInfo } from '@storybook/addon-info';
import backdrop from '../../../.storybook/backdrop';
import ReadOnlyBooleanControl from './';

storiesOf('ReadOnlyBooleanControl', module)
  .addDecorator(backdrop)
  .add(
    'component',
    withInfo()(() => (
      <ReadOnlyBooleanControl
        label={`Promoted`}
        choices={[`True`, `False`]}
        value={true}
      />
    ))
  )
  .add(
    'with custom choices',
    withInfo()(() => (
      <ReadOnlyBooleanControl
        label={`Promoted`}
        choices={[`Yes`, `No`]}
        value={true}
      />
    ))
  );
