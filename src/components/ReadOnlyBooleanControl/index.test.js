/*
 * Created Date: Thu, 19th Apr 2018, 09:24:06 am
 * Author: Harry Crank
 * Email: harry.crank@thedistance.co.uk
 * Copyright (c) 2018 The Distance
 */

import React from 'react';
import { shallow } from 'enzyme';
import toJson from 'enzyme-to-json';
import ReadOnlyBooleanControl from './';

test('should render with custom choices (true case)', () => {
  const component = shallow(
    <ReadOnlyBooleanControl
      choices={['yes', 'no']}
      onChange={jest.fn()}
      value={true}
    />
  );

  expect(toJson(component.shallow())).toMatchSnapshot();
});

test('should render with custom choices (false case)', () => {
  const component = shallow(
    <ReadOnlyBooleanControl
      choices={['yes', 'no']}
      onChange={jest.fn()}
      value={false}
    />
  );

  expect(toJson(component.shallow())).toMatchSnapshot();
});
