/*
 * Created Date: Mon, 16th Apr 2018, 12:19:48 pm
 * Author: Harry Crank
 * Email: harry.crank@thedistance.co.uk
 * Copyright (c) 2018 The Distance
 */

import React from 'react';
import PropTypes from 'prop-types';
import { FormControl } from '@sketchpixy/rubix';
import GenericControl from '../GenericControl';

const ReadOnlyBooleanControl = ({ choices, ...props }) => (
  <GenericControl {...props}>
    <FormControl.Static>{choices[Number(!props.value)]}</FormControl.Static>
  </GenericControl>
);

ReadOnlyBooleanControl.propTypes = {
  value: PropTypes.bool,
  choices: PropTypes.any,
};

export default ReadOnlyBooleanControl;
