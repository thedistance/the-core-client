/**
 * @Author: Ben Briggs <benbriggs>
 * @Date:   2018-01-29T15:51:32+00:00
 * @Email:  ben.briggs@thedistance.co.uk
 * @Last modified by:   benbriggs
 * @Last modified time: 2018-01-30T15:15:28+00:00
 * @Copyright: The Distance
 */

import React from 'react';
import Parse from 'parse';
import { action } from '@storybook/addon-actions';
import { storiesOf } from '@storybook/react';
import { withInfo } from '@storybook/addon-info';
import backdrop from '../../../.storybook/backdrop';
import PointerControl from './';

storiesOf('PointerControl', module)
  .addDecorator(backdrop)
  .add(
    'component',
    withInfo()(() => (
      <PointerControl
        label={`Parse Object`}
        onChange={action('PointerControl:onChange')}
        loadOptions={(input, callback) => {
          callback(null, {
            options: [{ value: 1, label: 'Foo' }, { value: 2, label: 'Bar' }],
          });
        }}
        ParseObject={Parse.Object.extend('ParseSubclass')}
        value={1}
      />
    ))
  );
