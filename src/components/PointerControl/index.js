/**
 * @Author: Ben Briggs <benbriggs>
 * @Date:   2018-01-29T15:50:50+00:00
 * @Email:  ben.briggs@thedistance.co.uk
 * @Last modified by:   benbriggs
 * @Last modified time: 2018-02-12T15:35:06+00:00
 * @Copyright: The Distance
 */

import React from 'react';
import PropTypes from 'prop-types';
import * as R from 'ramda';
import { Async as Select } from 'react-select';
import { ControlLabel } from '@sketchpixy/rubix';
import FormGroup from '../FormGroup';
import GenericControl from '../GenericControl';
import 'react-select/dist/react-select.css';

class PointerControl extends GenericControl {
  constructor(props) {
    super(props);

    this.state = {
      displayValue: props.value,
      value: props.value,
      valid: null,
    };
  }

  handleChange(opts) {
    const { ParseObject, validator } = this.props;
    const valid = validator
      ? validator(opts === null ? opts : opts.value)
      : null;
    const value =
      opts === null
        ? opts
        : ParseObject.createWithoutData(opts.value).toPointer();

    this.setState({
      displayValue: opts,
      value,
      valid,
    });

    this.props.onChange(value, valid);
  }

  render() {
    const displayValue = this.state.displayValue;
    return (
      <div>
        <FormGroup
          validationState={this.validationState()}
          required={this.props.required}
        >
          <ControlLabel>{this.props.label}</ControlLabel>
          <Select
            value={displayValue}
            loadOptions={this.props.loadOptions}
            options={this.props.options}
            onChange={this.handleChange}
            filterOptions={R.identity}
          />
        </FormGroup>
      </div>
    );
  }
}

PointerControl.propTypes = {
  ParseObject: PropTypes.func.isRequired,
  loadOptions: PropTypes.func.isRequired,
};

export default PointerControl;
