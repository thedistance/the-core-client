/**
 * @Author: benbriggs
 * @Date:   2018-04-30T14:03:23+01:00
 * @Email:  ben.briggs@thedistance.co.uk
 * @Last modified by:   benbriggs
 * @Last modified time: 2018-05-01T09:50:10+01:00
 * @Copyright: The Distance
 */

import * as R from 'ramda';

const hasError = (state, { error }) => R.merge(state, { error });
const nullCurrent = R.mergeDeepLeft({ current: null, error: null });
const updateCurrent = (state, { result }) =>
  R.merge(state, { current: result });

const createReducerHandlers = ({
  deletedAction,
  deleteFailedAction,
  loadedCurrentAction,
  loadedCollectionAction,
  newAction,
  revertAction,
  saveFailedAction,
  unloadCollectionAction,
  unloadCurrentAction,
  updateAction,
}) => ({
  [deletedAction]: nullCurrent,
  [deleteFailedAction]: hasError,
  [loadedCurrentAction]: updateCurrent,
  [loadedCollectionAction]: (state, { result, pages }) =>
    R.merge(state, { collection: result, pages }),
  [newAction]: updateCurrent,
  [revertAction]: nullCurrent,
  [saveFailedAction]: hasError,
  [unloadCollectionAction]: R.mergeDeepLeft({ collection: null, pages: null }),
  [unloadCurrentAction]: nullCurrent,
  [updateAction]: updateCurrent,
});

export default createReducerHandlers;
