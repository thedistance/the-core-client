/**
 * @Author: benbriggs
 * @Date:   2018-04-30T10:32:19+01:00
 * @Email:  ben.briggs@thedistance.co.uk
 * @Last modified by:   benbriggs
 * @Last modified time: 2018-05-14T13:31:39+01:00
 * @Copyright: The Distance
 */

import createSelectors from './createSelectors';

test('should select collections', () =>
  expect(
    createSelectors(['entity']).selectCollection({
      entity: { collection: ['x'] },
    })
  ).toEqual(['x']));

test('should select current objects', () =>
  expect(
    createSelectors(['entity']).selectCurrent({ entity: { current: 'x' } })
  ).toEqual('x'));

test('should select errors', () =>
  expect(
    createSelectors(['entity']).selectError({
      entity: { error: { message: 'x', code: 1 } },
    })
  ).toEqual({ message: 'x', code: 1 }));

test('should select error messages', () =>
  expect(
    createSelectors(['entity']).selectErrorMessage({
      entity: { error: { message: 'x', code: 1 } },
    })
  ).toEqual('x'));

test('should select error messages when undefined', () =>
  expect(
    createSelectors(['entity']).selectErrorMessage({ entity: {} })
  ).toBeUndefined());

test('should select pages', () =>
  expect(
    createSelectors(['entity']).selectPages({ entity: { pages: 5 } })
  ).toEqual(5));

test('should select custom areas of the state', () =>
  expect(
    createSelectors(['entity'], { selectBar: ['bar'] }).selectBar({
      entity: { bar: 'x' },
    })
  ).toEqual('x'));
