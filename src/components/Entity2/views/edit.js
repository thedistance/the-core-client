/**
 * @Author: benbriggs
 * @Date:   2018-05-01T11:40:16+01:00
 * @Email:  ben.briggs@thedistance.co.uk
 * @Last modified by:   benbriggs
 * @Last modified time: 2018-05-08T15:50:47+01:00
 * @Copyright: The Distance
 */

import * as R from 'ramda';
import { connect } from 'react-redux';
import EditView from './../../Entity/editView';
import { mapDispatch, mapState } from './mapConnectors';

const editView = (View = EditView) => ({
  actions,
  editConnector,
  save,
  destroy,
  loadCurrent,
  unloadCurrent,
  update,
  routeId,
  revert,
  selectCurrent,
  selectErrorMessage,
  schemaFields,
  singular,
  goBack = 1,
  extraActions,
  extraEditActions,
}) => {
  const mapStateToProps = state =>
    R.mergeDeepRight(
      {
        alert: selectErrorMessage(state),
        model: selectCurrent(state),
        fields: schemaFields,
        title: `Edit ${singular}`,
      },
      mapState(editConnector)(state)
    );

  const mapDispatchToProps = (dispatch, ownProps) =>
    R.mergeDeepRight(
      {
        actions: {
          load: () =>
            dispatch(loadCurrent({ id: ownProps.routeParams[routeId] })),
          save: () =>
            dispatch(
              save({
                pathname: ownProps.location.pathname,
                router: ownProps.router,
                goBack,
              })
            ),
          delete: () =>
            dispatch(
              destroy({
                pathname: ownProps.location.pathname,
                router: ownProps.router,
                goBack,
              })
            ),
          unload: () => dispatch(unloadCurrent()),
          update: options => dispatch(update(options)),
          revert: () => dispatch(revert()),
          ...extraActions(dispatch, ownProps),
          ...extraEditActions(dispatch, ownProps),
        },
      },
      mapDispatch(editConnector)(actions)(dispatch, ownProps)
    );

  return connect(mapStateToProps, mapDispatchToProps)(View);
};

export default editView;
