/**
 * @Author: benbriggs
 * @Date:   2018-05-01T11:40:16+01:00
 * @Email:  ben.briggs@thedistance.co.uk
 * @Last modified by:   benbriggs
 * @Last modified time: 2018-05-08T15:50:52+01:00
 * @Copyright: The Distance
 */

import * as R from 'ramda';

import { connect } from 'react-redux';
import EditView from './../../Entity/editView';
import { mapDispatch, mapState } from './mapConnectors';

const createView = (View = EditView) => ({
  actions,
  createConnector,
  create,
  save,
  unloadCurrent,
  update,
  revert,
  selectCurrent,
  selectErrorMessage,
  schemaFields,
  singular,
  goBack = 1,
  extraActions,
  extraCreateActions,
}) => {
  const mapStateToProps = state =>
    R.mergeDeepRight(
      {
        alert: selectErrorMessage(state),
        model: selectCurrent(state),
        fields: schemaFields,
        title: `New ${singular}`,
      },
      mapState(createConnector)(state)
    );

  const mapDispatchToProps = (dispatch, ownProps) =>
    R.mergeDeepRight(
      {
        actions: {
          load: () => dispatch(create()),
          save: () =>
            dispatch(
              save({
                pathname: ownProps.location.pathname,
                router: ownProps.router,
                goBack: 1,
              })
            ),
          cancel: () => {
            const routeParts = ownProps.location.pathname
              .split('/')
              .slice(0, goBack * -1);
            ownProps.router.push(routeParts.join('/'));
          },
          unload: () => dispatch(unloadCurrent()),
          update: options => dispatch(update(options)),
          revert: () => dispatch(revert()),
          ...extraActions(dispatch, ownProps),
          ...extraCreateActions(dispatch, ownProps),
        },
      },
      mapDispatch(createConnector)(actions)(dispatch, ownProps)
    );

  return connect(mapStateToProps, mapDispatchToProps)(View);
};

export default createView;
