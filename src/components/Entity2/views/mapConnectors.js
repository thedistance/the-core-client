/**
 * @Author: benbriggs
 * @Date:   2018-05-08T15:12:56+01:00
 * @Email:  ben.briggs@thedistance.co.uk
 * @Last modified by:   benbriggs
 * @Last modified time: 2018-05-08T15:49:08+01:00
 * @Copyright: The Distance
 */

import * as R from 'ramda';
import placeholder from './../placeholder';

export const mapDispatch = R.compose(
  R.defaultTo(placeholder),
  R.prop('mapDispatchToProps')
);

export const mapState = R.compose(
  R.defaultTo(placeholder),
  R.prop('mapStateToProps')
);
