/**
 * @Author: benbriggs
 * @Date:   2018-05-01T11:30:51+01:00
 * @Email:  ben.briggs@thedistance.co.uk
 * @Last modified by:   benbriggs
 * @Last modified time: 2018-05-08T15:50:42+01:00
 * @Copyright: The Distance
 */

import * as R from 'ramda';
import { connect } from 'react-redux';
import _ListView from './../../Entity/listView';
import { mapDispatch, mapState } from './mapConnectors';

const listView = (View = _ListView) => ({
  actions,
  listConnector,
  loadCollection,
  unloadCollection,
  selectCollection,
  selectPages,
  singular,
  plural,
  collectionIndex,
  canCreateNew,
  readOnly,
  getTheadFilterThProps,
}) => {
  const mapStateToProps = state =>
    R.mergeDeepRight(
      {
        readOnly,
        singular,
        plural,
        canCreateNew,
        columns: collectionIndex,
        data: selectCollection(state) || [],
        pages: selectPages(state),
        getTheadFilterThProps,
      },
      mapState(listConnector)(state)
    );

  const mapDispatchToProps = (dispatch, ownProps) =>
    R.mergeDeepRight(
      {
        actions: {
          load: tableState => dispatch(loadCollection(tableState)),
          unload: () => dispatch(unloadCollection()),
        },
      },
      mapDispatch(listConnector)(actions)(dispatch, ownProps)
    );

  return connect(mapStateToProps, mapDispatchToProps)(View);
};

export default listView;
