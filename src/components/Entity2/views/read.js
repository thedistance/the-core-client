/**
 * @Author: benbriggs
 * @Date:   2018-05-01T12:22:49+01:00
 * @Email:  ben.briggs@thedistance.co.uk
 * @Last modified by:   benbriggs
 * @Last modified time: 2018-05-08T15:50:36+01:00
 * @Copyright: The Distance
 */

import * as R from 'ramda';
import { connect } from 'react-redux';
import ReadOnlyView from './../../Entity/readOnlyView';
import { mapDispatch, mapState } from './mapConnectors';

const readView = (View = ReadOnlyView) => ({
  actions,
  editConnector,
  loadCurrent,
  unloadCurrent,
  routeId,
  revert,
  selectCurrent,
  schemaFields,
  singular,
  goBack = 1,
  extraActions,
  extraEditActions,
}) => {
  const mapStateToProps = state =>
    R.mergeDeepRight(
      {
        model: selectCurrent(state),
        fields: schemaFields,
        title: `View ${singular}`,
      },
      mapState(editConnector)(state)
    );

  const mapDispatchToProps = (dispatch, ownProps) =>
    R.mergeDeepRight(
      {
        actions: {
          load: () =>
            dispatch(loadCurrent({ id: ownProps.routeParams[routeId] })),
          cancel: () => {
            const routeParts = ownProps.location.pathname
              .split('/')
              .slice(0, goBack * -1);
            ownProps.router.push(routeParts.join('/'));
          },
          unload: () => dispatch(unloadCurrent()),
          revert: () => dispatch(revert()),
          ...extraActions(dispatch, ownProps),
          ...extraEditActions(dispatch, ownProps),
        },
      },
      mapDispatch(editConnector)(actions)(dispatch, ownProps)
    );

  return connect(mapStateToProps, mapDispatchToProps)(View);
};

export default readView;
