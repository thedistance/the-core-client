/**
 * @Author: benbriggs
 * @Date:   2018-04-30T13:14:55+01:00
 * @Email:  ben.briggs@thedistance.co.uk
 * @Last modified by:   benbriggs
 * @Last modified time: 2018-05-01T15:06:10+01:00
 * @Copyright: The Distance
 */

import * as R from 'ramda';
import * as asyncActions from './../../util/asyncActions';
import createAction from './../../util/createAction';

const createActionCreators = ({ startActions, endActions, syncActions }) =>
  R.compose(
    R.mergeAll,
    R.juxt([
      R.compose(
        R.map(R.compose(asyncActions.createStart, R.objOf('type'))),
        R.head
      ),
      R.compose(
        R.map(R.compose(asyncActions.createEnd, R.objOf('type'))),
        R.prop(1)
      ),
      R.compose(R.map(createAction), R.last),
    ])
  )([startActions, endActions, syncActions]);

export default createActionCreators;
