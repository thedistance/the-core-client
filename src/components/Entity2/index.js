/**
 * @Author: benbriggs
 * @Date:   2018-04-30T08:32:16+01:00
 * @Email:  ben.briggs@thedistance.co.uk
 * @Last modified by:   benbriggs
 * @Last modified time: 2018-05-14T13:34:26+01:00
 * @Copyright: The Distance
 */

import * as R from 'ramda';
import createReducer from './../../util/createReducer';
import createInflections from './../Entity/inflections';
import createObjectFromSchemaDefaults from './../Entity/createObjectFromSchemaDefaults';
import convertSchemaToFields from './../Entity/convertSchemaToFields';
import convertSchemaToIndex from './../Entity/convertSchemaToIndex';
import convertSchemaToReadOnlyFields from './../Entity/convertSchemaToReadOnlyFields';
import filterSchemaByPropExists from './../Entity/filterSchemaByPropExists';
import filterSchemaByPropNotExists from './../Entity/filterSchemaByPropNotExists';
import create from './actions/create';
import destroy from './actions/destroy';
import loadCollection from './actions/loadCollection';
import loadCurrent from './actions/loadCurrent';
import revert from './actions/revert';
import save from './actions/save';
import unloadCollection from './actions/unloadCollection';
import unloadCurrent from './actions/unloadCurrent';
import update from './actions/update';
import createActionCreators from './createActionCreators';
import createActionIds from './createActionIds';
import createReducerHandlers from './createReducerHandlers';
import createSelectors from './createSelectors';
import createViews from './createViews';
import placeholder from './placeholder';

const callWith = R.flip(R.binary(R.call));

const _startActions = {
  deletingAction: `deleting`,
  loadingCollectionAction: `loading:collection`,
  loadingCurrentAction: `loading:current`,
  savingAction: `saving`,
};

const _endActions = {
  deletedAction: `deleted`,
  deleteFailedAction: `delete:failed`,
  loadedCollectionAction: `loaded:collection`,
  loadedCurrentAction: `loaded:current`,
  savedAction: `saved`,
  saveFailedAction: `save:failed`,
};

const _syncActions = {
  loadedCurrentSyncAction: `loaded:current`,
  newAction: `new`,
  revertAction: `revert`,
  unloadCollectionAction: `unload:collection`,
  unloadCurrentAction: `unload:current`,
  updateAction: `update`,
};

const createEntity = ({
  actions = {},
  actionCreators = {},
  name,
  className = name,
  inflections = {},
  readOnly,
  reducerHandlers = {},
  reducerState = {
    collection: null,
    current: null,
    pages: null,
    error: null,
  },
  schema = {},
  selectors,
  onLoadCollection,
  onLoadCurrent,
  canCreateNew = true,
  createConnector = placeholder,
  editConnector = placeholder,
  listConnector = placeholder,
  createView,
  editView,
  listView,
  extraActions = placeholder,
  extraCreateActions = placeholder,
  extraEditActions = placeholder,
}) => {
  // 1. Inflections
  const { camelised, singular, plural, slug } = createInflections(
    name,
    inflections
  );

  // 2. Schema
  const filteredSchema = filterSchemaByPropNotExists('hideInEdit', schema);
  const createSchemaFields = convertSchemaToFields(filteredSchema, 'create');
  const editSchemaFields = readOnly
    ? convertSchemaToReadOnlyFields(filteredSchema, 'edit')
    : convertSchemaToFields(filteredSchema, 'edit');

  const collectionIndex = convertSchemaToIndex(
    filterSchemaByPropExists('showInIndex', schema)
  );

  // 3. Selectors
  const mergedSelectors = createSelectors([camelised], selectors);
  const { selectCollection, selectCurrent } = mergedSelectors;

  // 4. Actions
  const namespacedActionIds = createActionIds(
    R.mergeDeepRight(
      {
        className,
        startActions: _startActions,
        endActions: _endActions,
        syncActions: _syncActions,
      },
      actionCreators
    )
  );

  const actionIds = R.compose(R.mergeAll, R.values)(namespacedActionIds);

  const mergedActionCreators = createActionCreators(namespacedActionIds);

  const mergedActions = R.compose(
    R.mapObjIndexed((value, key) =>
      R.ifElse(
        R.contains(key),
        R.always(R.identity(value)),
        R.always(
          callWith(
            {
              actionCreators: mergedActionCreators,
              selectors: mergedSelectors,
            },
            value
          )
        )
      )([
        'create',
        'destroy',
        'loadCollection',
        'loadCurrent',
        'revert',
        'save',
        'unloadCollection',
        'unloadCurrent',
        'update',
      ])
    ),
    R.evolve({
      create: callWith({
        action: mergedActionCreators.newAction,
        className,
        defaultValues: createObjectFromSchemaDefaults(schema),
      }),
      destroy: callWith({
        startedAction: mergedActionCreators.deletingAction,
        completedAction: mergedActionCreators.deletedAction,
        failedAction: mergedActionCreators.deleteFailedAction,
        selectCurrent,
      }),
      loadCollection: callWith({
        className,
        queryTransform: onLoadCollection,
        startedAction: mergedActionCreators.loadingCollectionAction,
        completedAction: mergedActionCreators.loadedCollectionAction,
      }),
      loadCurrent: callWith({
        className,
        queryTransform: onLoadCurrent,
        startedAction: mergedActionCreators.loadingCurrentAction,
        startedSyncAction: mergedActionCreators.loadedCurrentSyncAction,
        completedAction: mergedActionCreators.loadedCurrentAction,
        selectCurrent,
      }),
      revert: callWith({
        action: mergedActionCreators.revertAction,
        selectCurrent,
      }),
      save: callWith({
        selectCurrent,
        startedAction: mergedActionCreators.savingAction,
        completedAction: mergedActionCreators.savedAction,
        failedAction: mergedActionCreators.saveFailedAction,
      }),
      unloadCollection: callWith({
        selectCollection,
        action: mergedActionCreators.unloadCollectionAction,
      }),
      unloadCurrent: callWith({
        selectCurrent,
        action: mergedActionCreators.unloadCurrentAction,
      }),
      update: callWith({
        selectCurrent,
        action: mergedActionCreators.updateAction,
      }),
    }),
    R.merge({
      create,
      destroy,
      loadCollection,
      loadCurrent,
      revert,
      save,
      unloadCollection,
      unloadCurrent,
      update,
    })
  )(actions);

  // 5. Reducer
  const reducer = createReducer(
    reducerState,
    R.merge(createReducerHandlers(actionIds), reducerHandlers)
  );

  // 6. Views

  return createViews(
    {
      // Actions
      actions: mergedActions,
      actionCreators: mergedActionCreators,
      actionIds,
      // Inflections
      camelised,
      plural,
      singular,
      slug,
      // Read only?
      readOnly,
      // Reducer
      reducer,
      // Schema
      collectionIndex,
      createSchemaFields,
      editSchemaFields,
      // Selectors
      selectors: mergedSelectors,
    },
    {
      canCreateNew,
      createConnector,
      editConnector,
      listConnector,
      createView,
      editView,
      listView,
      extraActions: callWith(mergedActions, extraActions),
      extraCreateActions: callWith(mergedActions, extraCreateActions),
      extraEditActions: callWith(mergedActions, extraEditActions),
    }
  );
};

export default createEntity;
