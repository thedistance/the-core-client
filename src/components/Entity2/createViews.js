/**
 * @Author: benbriggs
 * @Date:   2018-05-01T11:24:48+01:00
 * @Email:  ben.briggs@thedistance.co.uk
 * @Last modified by:   benbriggs
 * @Last modified time: 2018-09-03T08:22:13+01:00
 * @Copyright: The Distance
 */

import React from 'react';
import { Route } from 'react-router';
import createListView from './views/list';
import createNewView from './views/create';
import createEditView from './views/edit';
import createReadOnlyView from './views/read';

function createViews(
  entity,
  {
    canCreateNew = true,
    createConnector,
    editConnector,
    listConnector,
    createView,
    editView,
    listView,
    extraActions,
    extraCreateActions,
    extraEditActions,
  }
) {
  const {
    readOnly,
    camelised,
    collectionIndex,
    plural,
    createSchemaFields,
    editSchemaFields,
    singular,
    slug,
    selectors: {
      selectCollection,
      selectCurrent,
      selectErrorMessage,
      selectPages,
    },
    actions,
    actions: {
      create,
      destroy,
      loadCollection,
      loadCurrent,
      save,
      revert,
      unloadCollection,
      unloadCurrent,
      update,
    },
  } = entity;

  const routeId = `${camelised}Id`;

  const exposedCreateView = createNewView(createView)({
    actions,
    createConnector,
    create,
    save,
    unloadCurrent,
    update,
    revert,
    extraActions,
    extraCreateActions,
    selectCurrent,
    selectErrorMessage,
    schemaFields: createSchemaFields,
    singular,
  });

  const exposedEditView = readOnly
    ? createReadOnlyView(editView)({
        actions,
        editConnector,
        loadCurrent,
        unloadCurrent,
        revert,
        extraActions,
        extraEditActions,
        routeId,
        selectCurrent,
        selectErrorMessage,
        schemaFields: editSchemaFields,
        singular,
      })
    : createEditView(editView)({
        actions,
        editConnector,
        save,
        destroy,
        loadCurrent,
        unloadCurrent,
        update,
        revert,
        extraActions,
        extraEditActions,
        routeId,
        selectCurrent,
        selectErrorMessage,
        schemaFields: editSchemaFields,
        singular,
      });

  const exposedListView = createListView(listView)({
    actions,
    listConnector,
    loadCollection,
    unloadCollection,
    selectCollection,
    selectPages,
    singular,
    plural,
    collectionIndex,
    canCreateNew,
    readOnly,
  });

  return {
    ...entity,
    routeId,
    createView: exposedCreateView,
    editView: exposedEditView,
    listView: exposedListView,
    routes: [
      <Route
        path={`/${slug}`}
        component={exposedListView}
        key={`${camelised}__list`}
      />,
      <Route
        path={`/${slug}/new`}
        component={exposedCreateView}
        key={`${camelised}__create`}
      />,
      <Route
        path={`/${slug}/:${routeId}`}
        component={exposedEditView}
        key={`${camelised}__edit`}
      />,
    ],
  };
}

export default createViews;
