# Entity guide

## Getting started

Entities provide a way to generate common functionality for working with Parse
Objects through The Core. For instance, to connect to a Parse class named
`Article`, we would write the following;

```js
import createEntity from 'the-core-client/dist/components/Entity2';

const Article = createEntity({
  name: 'Article',
});

export default Article;
```

Now that we have created our entity, we need to register it with the router;

```js
/* => /router.js */
import React from 'react';
import { Router, Route, browserHistory } from 'react-router';
import combineEntityRoutes from 'the-core-client/dist/components/Entity/combineEntityRoutes';
import LoadProgress from 'the-core-client/dist/components/LoadProgress';
import ApplicationLayout from './components/applicationLayout';
import Article from './Article/entity';
import Category from './Category/entity';

const entityRoutes = combineEntityRoutes(Article, Category);

export default (
  <Router history={browserHistory}>
    <Route component={LoadProgress}>
      <Route component={Login}>
        <Route component={ApplicationLayout}>
          {entityRoutes}
          {/* Insert the rest of the application routing */}
        </Route>
      </Route>
    </Route>
  </Router>
);
```

And the redux root reducer;

```js
/* => /reducers/index.js */
import { combineReducers } from 'redux';
import { pendingTasksReducer as pendingTasks } from 'react-redux-spinner';
import combineEntityReducers from 'the-core-client/dist/components/Entity/combineEntityReducers';
import Article from '../Article/entity';
import Category from '../Category/entity';

const reducers = combineReducers({
  pendingTasks,
  ...combineEntityReducers(Article, Category),
  /* Insert the rest of the application reducers */
});

export default reducers;
```

Now, if you browse to `/article` you will see an index of the available
articles in your database!

## Differences from Entity

* `actions` has been added as a top level option, so that you can override the
  default actions and/or provide new ones.
* `collectionIndex` has been removed as a top level option. Instead, use
  the schema.
* `extraActions`, `extraCreateActions` and `extraEditActions` now have an
  additional function wrapper which is called with an object of all of the
  entity actions. Also, these keys are no longer exposed from the entity
  after it is created.
* `readOnlyCreateView` and `readOnlyEditView` have been removed as top level
  options. `readOnly` will now only affect the edit views.

## Configuration options

### `className`

* Type: `string`

The name to use for all of the database queries; if it is not defined then
the `name` option will be used instead. This value should equal the name of the
Parse class that it should query.

```js
const Article = createEntity({
  className: 'Article',
});
```


### `inflections`

* Type: `object`

By default the inflections are automatically created for you, but there may be
some cases in which the inflector may not provide suitable pluralisms for your
class. In these cases you may override any of the inflections by supplying an
object with the following shape:

```js
const SOS = createEntity({
  name: 'SOS',
  inflections: {
    camelised: 'sos',    // Used by the redux store
    plural: 'S.O.S',     // Used in the index view
    singular: 'S.O.S',   // Used in the new/edit view
    slug: 'sos',         // Used by the router
  },
});
```

Note that any keys that are missing here will be automatically filled in by
the inflection library.


### `name`

* Type: `string`

The name to use for all of the inflections of the class, and if `className` is
not defined it will be used for all of the database queries.

When `className` is not defined, this value should equal the name of the Parse
class that should be queried. But when `className` is defined then you may wish
to present a different name to the client, such as when using prefixed classes.

```js
const Article = createEntity({
  name: 'Article',
  className: 'TDArticle',
});
```


### `onLoadCollection`

* Type: `function`

This function is called when a collection is requested; it allows you to
transform the query in cases where you may want to change the default sort
order, or include relational data.

```js
const Article = createEntity({
  name: 'Article',
  onLoadCollection: query => query.descending('createdAt'),
});
```

This function is called with the query and expects you to return
the transformed query.


### `onLoadCurrent`

* Type: `function`

This function is called when a single record is requested; it allows you to
transform the query in cases where you may want to include relational data.

```js
const Article = createEntity({
  name: 'Article',
  onLoadCurrent: query => query.include('comments'),
});
```

This function is called with the query and expects you to return
the transformed query.


### `schema`

* Type: `object`

The model definition for the Parse object, which is an object of objects, keyed
by field name. Each sub-object takes a few different parameters depending on
the type of data that they accept, but there are some parameters that can be
defined for all fields.

#### Global field configuration options

##### `accessor`

* Type: `function`

This function is used to access the field's value. For top level properties it
is not necessary to define this, but it is useful if you need to access a value
that is nested in some other object.

```js
const Article = createEntity({
  name: 'Article',
  schema: {
    featuredContent: {
      accessor: obj => obj.get('featuredContent').get('title'),
      type: 'pointer',
    },
  },
  onLoadCollection: query => query.include('featuredContent'),
});
```

The function is called with the entity's Parse object and expects you to return
an appropriate display value.

##### `hideInEdit`

* Type: `boolean`

Whether to hide the field in the edit view; the default is `false`.

```js
const Article = createEntity({
  name: 'Article',
  schema: {
    title: {
      type: 'string',
      showInIndex: true,
      hideInEdit: true,
    },
  },
});
```


##### `label`

* Type: `string`

By default the entity will convert the camel cased field name and use it as
the label for the form field/column name. Sometimes you may want to make
this more descriptive, in cases where the database field is an abbreviation.

```js
const Article = createEntity({
  name: 'Article',
  schema: {
    appleIap: {
      label: 'In app purchase ID (iOS)'
      type: 'string',
    },
  },
});
```


##### `showInIndex`

* Type: `boolean`

Whether to show the field in the index view; the default is `false`.

```js
const Article = createEntity({
  name: 'Article',
  schema: {
    title: {
      type: 'string',
      showInIndex: true,
    },
  },
});
```


##### `type`

* Type: `string`

From a `type` of a field, we can infer which component we can use to update it.
There are a few different field types, if the type you specify is not registered
then by default we render a simple text input. Note that you may override this
behaviour with your own component by using `CreateComponent`, `EditComponent`
or `UpdateComponent`.

```js
const Article = createEntity({
  name: 'Article',
  schema: {
    isPublished: {
      type: 'boolean',
    },
  },
});
```
