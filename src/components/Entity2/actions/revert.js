/**
 * @Author: benbriggs
 * @Date:   2018-04-30T11:34:52+01:00
 * @Email:  ben.briggs@thedistance.co.uk
 * @Last modified by:   benbriggs
 * @Last modified time: 2018-04-30T12:13:09+01:00
 * @Copyright: The Distance
 */

const revert = ({ selectCurrent, action }) => () => (dispatch, getState) => {
  const result = selectCurrent(getState());
  if (!result) {
    return;
  }
  result.revert();
  return dispatch(action({ result }));
};

export default revert;
