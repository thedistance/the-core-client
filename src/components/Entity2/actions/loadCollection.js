/**
 * @Author: benbriggs
 * @Date:   2018-04-30T11:30:54+01:00
 * @Email:  ben.briggs@thedistance.co.uk
 * @Last modified by:   benbriggs
 * @Last modified time: 2018-04-30T12:19:16+01:00
 * @Copyright: The Distance
 */

import Parse from 'parse';
import * as R from 'ramda';
import escape from 'escape-string-regexp';

const loadCollection = ({
  className,
  queryTransform = R.identity,
  startedAction,
  completedAction,
}) => ({ pageSize = 20, page = 0, sorted, filtered } = {}) => dispatch => {
  dispatch(startedAction());

  const data = queryTransform(new Parse.Query(className))
    .limit(pageSize)
    .skip(pageSize * page);

  if (filtered && filtered.length) {
    filtered.forEach(({ id, value }) =>
      data.matches(id, new RegExp(escape(value), 'i'))
    );
  }

  if (sorted && sorted.length) {
    sorted.forEach(
      ({ id, desc }) => (desc ? data.descending(id) : data.ascending(id))
    );
  }

  return Promise.all([data.count(), data.find()]).then(([count, result]) =>
    dispatch(completedAction({ result, pages: Math.ceil(count / pageSize) }))
  );
};

export default loadCollection;
