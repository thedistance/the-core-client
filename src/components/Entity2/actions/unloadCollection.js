/**
 * @Author: benbriggs
 * @Date:   2018-04-30T11:48:30+01:00
 * @Email:  ben.briggs@thedistance.co.uk
 * @Last modified by:   benbriggs
 * @Last modified time: 2018-04-30T12:13:24+01:00
 * @Copyright: The Distance
 */

const unloadCollection = ({ selectCollection, action }) => () => (
  dispatch,
  getState
) => dispatch(action({ result: selectCollection(getState()) }));

export default unloadCollection;
