/**
 * @Author: benbriggs
 * @Date:   2018-04-30T11:49:58+01:00
 * @Email:  ben.briggs@thedistance.co.uk
 * @Last modified by:   benbriggs
 * @Last modified time: 2018-04-30T12:13:34+01:00
 * @Copyright: The Distance
 */

const unloadCurrent = ({ selectCurrent, action }) => () => (
  dispatch,
  getState
) => dispatch(action({ result: selectCurrent(getState()) }));

export default unloadCurrent;
