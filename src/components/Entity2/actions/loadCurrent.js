/**
 * @Author: benbriggs
 * @Date:   2018-04-30T11:26:59+01:00
 * @Email:  ben.briggs@thedistance.co.uk
 * @Last modified by:   benbriggs
 * @Last modified time: 2018-04-30T12:19:50+01:00
 * @Copyright: The Distance
 */

import Parse from 'parse';
import * as R from 'ramda';

const loadCurrent = ({
  className,
  queryTransform = R.identity,
  startedSyncAction,
  startedAction,
  completedAction,
  selectCurrent,
}) => ({ id } = {}) => (dispatch, getState) => {
  const current = selectCurrent(getState());

  if (current) {
    return Promise.resolve(
      dispatch(
        startedSyncAction({
          result: current,
        })
      )
    );
  }

  if (!id) {
    return;
  }

  dispatch(startedAction());

  return queryTransform(new Parse.Query(className))
    .get(id)
    .then(result => {
      dispatch(completedAction({ result }));
      return result;
    });
};

export default loadCurrent;
