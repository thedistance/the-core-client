/**
 * @Author: benbriggs
 * @Date:   2018-04-30T10:14:13+01:00
 * @Email:  ben.briggs@thedistance.co.uk
 * @Last modified by:   benbriggs
 * @Last modified time: 2018-05-14T13:29:47+01:00
 * @Copyright: The Distance
 */

import * as R from 'ramda';

export const defaultSelectors = {
  selectCollection: ['collection'],
  selectCurrent: ['current'],
  selectError: ['error'],
  selectErrorMessage: ['error', 'message'],
  selectPages: ['pages'],
};

const createSelectors = (rootPath, selectors = defaultSelectors) =>
  R.map(R.compose(R.path, R.concat(rootPath)), selectors);

export default createSelectors;
