/**
 * @Author: benbriggs
 * @Date:   2018-05-01T14:58:19+01:00
 * @Email:  ben.briggs@thedistance.co.uk
 * @Last modified by:   benbriggs
 * @Last modified time: 2018-05-01T15:17:35+01:00
 * @Copyright: The Distance
 */

import * as R from 'ramda';

const createActionIds = ({
  className,
  startActions,
  endActions,
  syncActions,
}) =>
  R.compose(
    R.map(R.map(action => `${className}:${action}`)),
    R.mergeAll,
    R.juxt([
      R.compose(R.objOf('startActions'), R.prop(0)),
      R.compose(R.objOf('endActions'), R.prop(1)),
      R.compose(R.objOf('syncActions'), R.prop(2)),
    ])
  )([startActions, endActions, syncActions]);

export default createActionIds;
