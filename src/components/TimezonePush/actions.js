/**
 * @Author: Ben Briggs <benbriggs>
 * @Date:   2018-01-10T13:17:24+00:00
 * @Email:  ben.briggs@thedistance.co.uk
 * @Last modified by:   benbriggs
 * @Last modified time: 2018-01-10T14:08:39+00:00
 * @Copyright: The Distance
 */

import selectParse from '../../selectors/parse';
import * as asyncActions from '../../util/asyncActions';
import createAction from '../../util/createAction';

export const loadingPushMessage = 'pushMessage:collection:loading';
export const loadedPushMessage = 'pushMessage:collection:loaded';
export const sendingPushMessage = 'pushMessage:sending';
export const sentPushMessage = 'pushMessage:sent';
export const sendPushMessageFailed = 'pushMessage:send:failed';
export const unloadAll = 'pushMessage:unload';

export const loadingPushMessageAction = asyncActions.createStart({
  type: loadingPushMessage,
});
export const loadedPushMessageAction = asyncActions.createEnd({
  type: loadedPushMessage,
});
export const sendingPushMessageAction = asyncActions.createStart({
  type: sendingPushMessage,
});
export const sentPushMessageAction = asyncActions.createEnd({
  type: sentPushMessage,
});
export const sendPushMessageFailedAction = asyncActions.createError({
  type: sendPushMessageFailed,
});
export const unloadPushMessageAction = createAction(unloadAll);

export const loadPushMessageCollection = params => (dispatch, getState) => {
  const Parse = selectParse(getState());

  dispatch(loadingPushMessageAction());

  return Parse.Cloud.run('list-push-messages', params).then(result =>
    dispatch(loadedPushMessageAction(result))
  );
};

export const sendPushMessage = ({ from, until, message }) => (
  dispatch,
  getState
) => {
  const Parse = selectParse(getState());

  dispatch(sendingPushMessageAction());

  return Parse.Cloud.run('create-push-message', { from, until, message })
    .then(() => dispatch(sentPushMessageAction()))
    .catch(error => dispatch(sendPushMessageFailedAction({ error })));
};

export const unloadPushMessage = () => dispatch => {
  dispatch(unloadPushMessageAction());
};
