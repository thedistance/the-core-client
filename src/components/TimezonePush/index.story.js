/**
 * @Author: Ben Briggs <benbriggs>
 * @Date:   2018-01-09T15:52:07+00:00
 * @Email:  ben.briggs@thedistance.co.uk
 * @Last modified by:   benbriggs
 * @Last modified time: 2018-01-10T14:16:40+00:00
 * @Copyright: The Distance
 */

import React from 'react';
import { action } from '@storybook/addon-actions';
import { storiesOf } from '@storybook/react';
import { withInfo } from '@storybook/addon-info';
import backdrop from '../../../.storybook/backdrop';
import { TimezonePush } from './';

const actions = {
  load: () => Promise.resolve(action('TimezonePush:load')),
  unload: action('TimezonePush:unload'),
  send: action('TimezonePush:send'),
};

storiesOf('TimezonePush', module)
  .addDecorator(backdrop)
  .add('component', withInfo()(() => <TimezonePush actions={actions} />))
  .add(
    'Successful send',
    withInfo()(() => <TimezonePush actions={actions} sent={true} />)
  )
  .add(
    'Failed send',
    withInfo()(() => (
      <TimezonePush
        actions={actions}
        error={{
          message: `Could not schedule the push message. Please try again.`,
        }}
      />
    ))
  );
