/**
 * @Author: Ben Briggs <benbriggs>
 * @Date:   2018-01-09T15:52:40+00:00
 * @Email:  ben.briggs@thedistance.co.uk
 * @Last modified by:   benbriggs
 * @Last modified time: 2018-01-11T12:02:42+00:00
 * @Copyright: The Distance
 */

import React from 'react';
import { connect } from 'react-redux';
import {
  Grid,
  Row,
  Col,
  Form,
  FormControlFeedback,
  Button,
  ButtonToolbar,
} from '@sketchpixy/rubix';
import PropTypes from 'prop-types';
import Alert from '../Alert';
import Panel from '../Panel';
import TextControl from '../TextControl';
import TimeControl from '../TimeControl';
import * as actions from './actions';
import { selectSent, selectError } from './reducer';

export class TimezonePush extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      from: '',
      until: '',
      message: '',
    };
  }

  handleFrom(from) {
    this.setState({ from });
  }

  handleUntil(until) {
    this.setState({ until });
  }

  handleMessage(message) {
    this.setState({ message });
  }

  renderMessage() {
    const { error, sent } = this.props;
    if (error) {
      return <Alert danger>{error.message}</Alert>;
    }
    if (sent) {
      return (
        <Alert success>Your message has been successfully scheduled!</Alert>
      );
    }
    return null;
  }

  sendMessage() {
    this.props.actions.send(this.state);
    this.setState({ message: '', from: '', until: '' });
  }

  render() {
    return (
      <div>
        {this.renderMessage()}
        <Panel title={`Send a Push Message`}>
          <Form autoComplete={false} onSubmit={e => e.preventDefault()}>
            <Grid>
              <Row>
                <Col xs={12}>
                  <TextControl
                    label={`Message`}
                    onChange={this.handleMessage.bind(this)}
                    value={this.state.message}
                  />
                </Col>
              </Row>
              <Row>
                <Col sm={5}>
                  <TimeControl
                    label={'From'}
                    value={this.state.from}
                    onChange={this.handleFrom.bind(this)}
                  />
                  <FormControlFeedback />
                </Col>
                <Col sm={5} smOffset={1}>
                  <TimeControl
                    label={'Until'}
                    value={this.state.until}
                    onChange={this.handleUntil.bind(this)}
                  />
                  <FormControlFeedback />
                </Col>
              </Row>
              <Row>
                <Col xs={12}>
                  {this.state.from && this.state.until ? (
                    <p>
                      If it is not {this.state.from} in their local time yet,
                      the message will be sent at {this.state.from}. If it is
                      past {this.state.from} but before {this.state.until} in
                      their local time, it will be sent straight away.
                      Otherwise, we will wait until tomorrow and send at{' '}
                      {this.state.from}.
                    </p>
                  ) : (
                    <p>
                      The message will be sent to each user at a different time,
                      depending on their local time.
                    </p>
                  )}
                </Col>
              </Row>
              <Row>
                <Col xs={12}>
                  <ButtonToolbar style={{ marginBottom: '1em' }}>
                    <Button onClick={this.sendMessage.bind(this)}>Send</Button>
                  </ButtonToolbar>
                </Col>
              </Row>
            </Grid>
          </Form>
        </Panel>
      </div>
    );
  }
}

TimezonePush.propTypes = {
  actions: PropTypes.objectOf(PropTypes.func).isRequired,
  error: PropTypes.any,
  sent: PropTypes.any,
};

export const mapStateToProps = state => ({
  error: selectError(state),
  sent: selectSent(state),
});

export const mapDispatchToProps = dispatch => ({
  actions: {
    send: params => dispatch(actions.sendPushMessage(params)),
  },
});

export default connect(mapStateToProps, mapDispatchToProps)(TimezonePush);
