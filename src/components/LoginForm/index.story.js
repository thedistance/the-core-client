/**
 * @Author: benbriggs
 * @Date:   2018-04-23T13:14:40+01:00
 * @Email:  ben.briggs@thedistance.co.uk
 * @Last modified by:   benbriggs
 * @Last modified time: 2018-04-23T13:15:46+01:00
 * @Copyright: The Distance
 */

import React from 'react';
import { action } from '@storybook/addon-actions';
import { storiesOf } from '@storybook/react';
import { withInfo } from '@storybook/addon-info';
import backdrop from '../../../.storybook/backdrop';
import LoginForm from './';

storiesOf('LoginForm', module)
  .addDecorator(backdrop)
  .add(
    'component',
    withInfo()(() => <LoginForm onSubmit={action('LoginForm:onSubmit')} />)
  );
