/**
 * @Author: benbriggs
 * @Date:   2018-04-23T13:16:40+01:00
 * @Email:  ben.briggs@thedistance.co.uk
 * @Last modified by:   benbriggs
 * @Last modified time: 2018-04-23T13:17:25+01:00
 * @Copyright: The Distance
 */

import React from 'react';
import { shallow } from 'enzyme';
import toJson from 'enzyme-to-json';
import LoginForm from './';

test('should render', () => {
  const component = shallow(<LoginForm onSubmit={() => {}} />);

  expect(toJson(component.shallow())).toMatchSnapshot();
});
