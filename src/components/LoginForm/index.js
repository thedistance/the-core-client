/**
 * @Author: benbriggs
 * @Date:   2018-04-23T13:05:45+01:00
 * @Email:  ben.briggs@thedistance.co.uk
 * @Last modified by:   benbriggs
 * @Last modified time: 2018-04-23T13:13:42+01:00
 * @Copyright: The Distance
 */

import React from 'react';
import PropTypes from 'prop-types';
import {
  Grid,
  Row,
  Col,
  Alert,
  Form,
  FormGroup,
  InputGroup,
  FormControl,
  Icon,
  Button,
} from '@sketchpixy/rubix';
import Panel from '../Panel';

class LoginForm extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      username: props.username,
      password: '',
    };
  }

  componentDidMount() {
    const el = document.querySelector('html');
    if (el.classList) el.classList.add('authentication');
    else el.className += ' authentication';
  }

  componentWillUnmount() {
    const el = document.querySelector('html');
    if (el.classList) el.classList.remove('authentication');
    else
      el.className = el.className.replace(
        new RegExp('(^|\\b)authentication(\\b|$)', 'gi'),
        ' '
      );
  }

  handleUsername({ target }) {
    this.setState({ username: target.value });
  }

  handlePassword({ target }) {
    this.setState({ password: target.value });
  }

  handleSubmit({ key }) {
    if ((key && key === 'Enter') || !key) {
      this.props.onSubmit(this.state.username, this.state.password);
    }
  }

  render() {
    return (
      <div id="auth-container" className="login">
        <div id="auth-row">
          <div id="auth-cell">
            <Grid>
              <Row>
                <Col
                  sm={4}
                  smOffset={4}
                  xs={10}
                  xsOffset={1}
                  collapseLeft
                  collapseRight
                >
                  <Panel title={this.props.titleMessage}>
                    <div>
                      {this.props.flash && (
                        <Alert bsStyle="warning">{this.props.flash}</Alert>
                      )}
                      <div
                        style={{
                          padding: 25,
                          paddingTop: 0,
                          paddingBottom: 0,
                          margin: 'auto',
                          marginBottom: 25,
                          marginTop: 25,
                        }}
                      >
                        <Form>
                          <FormGroup controlId="username">
                            <InputGroup bsSize="large">
                              <InputGroup.Addon>
                                <Icon glyph="icon-fontello-mail" />
                              </InputGroup.Addon>
                              <FormControl
                                autoFocus
                                type="text"
                                value={this.state.username}
                                onChange={this.handleUsername.bind(this)}
                                className="border-focus-blue"
                                placeholder="username"
                              />
                            </InputGroup>
                          </FormGroup>
                          <FormGroup controlId="password">
                            <InputGroup bsSize="large">
                              <InputGroup.Addon>
                                <Icon glyph="icon-fontello-key" />
                              </InputGroup.Addon>
                              <FormControl
                                type="password"
                                value={this.state.password}
                                onChange={this.handlePassword.bind(this)}
                                onKeyPress={this.handleSubmit.bind(this)}
                                className="border-focus-blue"
                                placeholder="password"
                              />
                            </InputGroup>
                          </FormGroup>
                          <FormGroup>
                            <Grid>
                              <Row>
                                <Col
                                  xs={12}
                                  collapseLeft
                                  collapseRight
                                  className="text-right"
                                >
                                  <Button
                                    outlined
                                    lg
                                    bsStyle="blue"
                                    onClick={this.handleSubmit.bind(this)}
                                  >
                                    Login
                                  </Button>
                                </Col>
                              </Row>
                            </Grid>
                          </FormGroup>
                        </Form>
                      </div>
                    </div>
                  </Panel>
                </Col>
              </Row>
            </Grid>
          </div>
        </div>
      </div>
    );
  }
}

LoginForm.propTypes = {
  onSubmit: PropTypes.func.isRequired,
  flash: PropTypes.string,
  username: PropTypes.string,
  titleMessage: PropTypes.string,
};

LoginForm.defaultProps = {
  username: '',
  titleMessage: 'Sign in to The Core v3',
};

export default LoginForm;
