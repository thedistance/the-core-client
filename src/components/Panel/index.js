/**
 * @Author: Ben Briggs <benbriggs>
 * @Date:   2017-09-26T13:54:19+01:00
 * @Email:  ben.briggs@thedistance.co.uk
 * @Last modified by:   benbriggs
 * @Last modified time: 2018-01-11T09:30:02+00:00
 * @Copyright: The Distance
 */

import React from 'react';
import PropTypes from 'prop-types';
import {
  PanelContainer,
  Panel,
  PanelHeader,
  PanelBody,
  Grid,
  Row,
  Col,
} from '@sketchpixy/rubix';

import LinkButton from '../LinkButton';

class PanelComponent extends React.Component {
  renderButton() {
    const { buttonTo, buttonTitle } = this.props;

    if (!buttonTo || !buttonTitle) {
      return null;
    }

    return (
      <LinkButton pathname={buttonTo} bsStyle="primary">
        {buttonTitle}
      </LinkButton>
    );
  }

  render() {
    const { className, children, title } = this.props;
    const button = this.renderButton();

    const header = button ? (
      <Row>
        <Col xs={6} className="fg-white">
          <h3>{title}</h3>
        </Col>
        <Col xs={6} className="fg-white" style={{ textAlign: 'right' }}>
          {button}
        </Col>
      </Row>
    ) : (
      <Row>
        <Col xs={12} className="fg-white">
          <h3>{title}</h3>
        </Col>
      </Row>
    );

    return (
      <PanelContainer>
        <Panel>
          <PanelHeader className={className}>
            <Grid>{header}</Grid>
          </PanelHeader>
          <PanelBody>{children}</PanelBody>
        </Panel>
      </PanelContainer>
    );
  }
}

PanelComponent.propTypes = {
  buttonTo: PropTypes.string,
  buttonTitle: PropTypes.string,
  children: PropTypes.any,
  className: PropTypes.string,
  title: PropTypes.string,
};

PanelComponent.defaultProps = {
  className: 'bg-blue',
};

export default PanelComponent;
