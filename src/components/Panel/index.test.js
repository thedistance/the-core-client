/**
 * @Author: Ben Briggs <benbriggs>
 * @Date:   2017-12-08T15:09:12+00:00
 * @Email:  ben.briggs@thedistance.co.uk
 * @Last modified by:   benbriggs
 * @Last modified time: 2017-12-08T16:19:30+00:00
 * @Copyright: The Distance
 */

import React from 'react';
import { shallow } from 'enzyme';
import toJson from 'enzyme-to-json';
import Panel from './';

test('should render', () => {
  const component = shallow(<Panel title={`Hello, world`} />);

  expect(toJson(component.shallow())).toMatchSnapshot();
});

test('should render with a button', () => {
  const component = shallow(
    <Panel
      title={`Hello, world`}
      buttonTo={`/path/to/hello/world`}
      buttonTitle={`Edit Hello, world`}
    />
  );

  expect(toJson(component.shallow())).toMatchSnapshot();
});
