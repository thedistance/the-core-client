/**
 * @Author: Ben Briggs <benbriggs>
 * @Date:   2017-12-08T15:24:50+00:00
 * @Email:  ben.briggs@thedistance.co.uk
 * @Last modified by:   benbriggs
 * @Last modified time: 2017-12-08T17:31:29+00:00
 * @Copyright: The Distance
 */

import React from 'react';
import { storiesOf } from '@storybook/react';
import { Grid, Row, Col } from '@sketchpixy/rubix';
import backdrop from '../../../.storybook/backdrop';
import Panel from './';

storiesOf('Panel', module)
  .addDecorator(backdrop)
  .add('Blue (default)', () => (
    <Panel title={`Hello, world`}>
      <Grid>
        <Row>
          <Col xs={12}>
            <p>This is some body text.</p>
          </Col>
        </Row>
      </Grid>
    </Panel>
  ))
  .add('Blue, with a button', () => (
    <Panel
      title={`Hello, world`}
      buttonTo={`/path/to/hello/world`}
      buttonTitle={`Edit Hello, world`}
    >
      <Grid>
        <Row>
          <Col xs={12}>
            <p>This is some body text.</p>
          </Col>
        </Row>
      </Grid>
    </Panel>
  ))
  .add('Red', () => (
    <Panel title={`Hello, world`} className={`bg-red`}>
      <Grid>
        <Row>
          <Col xs={12}>
            <p>This is some body text.</p>
          </Col>
        </Row>
      </Grid>
    </Panel>
  ))
  .add('Green', () => (
    <Panel title={`Hello, world`} className={`bg-green`}>
      <Grid>
        <Row>
          <Col xs={12}>
            <p>This is some body text.</p>
          </Col>
        </Row>
      </Grid>
    </Panel>
  ))
  .add('Purple', () => (
    <Panel title={`Hello, world`} className={`bg-purple`}>
      <Grid>
        <Row>
          <Col xs={12}>
            <p>This is some body text.</p>
          </Col>
        </Row>
      </Grid>
    </Panel>
  ));
