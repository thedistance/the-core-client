/**
 * @Author: Ben Briggs <benbriggs>
 * @Date:   2017-12-19T12:37:51+00:00
 * @Email:  ben.briggs@thedistance.co.uk
 * @Last modified by:   benbriggs
 * @Last modified time: 2017-12-19T12:40:34+00:00
 * @Copyright: The Distance
 */

import React from 'react';
import PropTypes from 'prop-types';
import { FormGroup } from '@sketchpixy/rubix';

const WrappedFormGroup = ({ children, required, validationState }) => (
  <FormGroup
    validationState={validationState}
    bsClass={required && `form-group required`}
  >
    {children}
  </FormGroup>
);

WrappedFormGroup.propTypes = {
  children: PropTypes.any,
  required: PropTypes.bool,
  validationState: PropTypes.any,
};

export default WrappedFormGroup;
