/*
 * Created Date: Thu, 19th Apr 2018, 08:43:51 am
 * Author: Harry Crank
 * Email: harry.crank@thedistance.co.uk
 * Copyright (c) 2018 The Distance
 */

import React from 'react';
import { storiesOf } from '@storybook/react';
import MockDate from 'mockdate';
import backdrop from '../../../.storybook/backdrop';
import ReadOnlyDateControl from './';

MockDate.set('4/19/2018 08:45');

storiesOf('ReadOnlyDateControl', module)
  .addDecorator(backdrop)
  .add('component', () => (
    <ReadOnlyDateControl label={`Date`} value={new Date()} />
  ))
  .add('date not supplied', () => <ReadOnlyDateControl label={`Date`} />);
