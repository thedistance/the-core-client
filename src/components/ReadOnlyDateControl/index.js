/*
 * Created Date: Wed, 18th Apr 2018, 13:17:33 pm
 * Author: Harry Crank
 * Email: harry.crank@thedistance.co.uk
 * Copyright (c) 2018 The Distance
 */

import React from 'react';
import PropTypes from 'prop-types';
import { FormControl } from '@sketchpixy/rubix';
import GenericControl from '../GenericControl';

const ReadOnlyDateControl = props => (
  <GenericControl {...props}>
    <FormControl.Static>
      {props.value
        ? props.value.toLocaleString('en-GB', {
            weekday: 'long',
            year: 'numeric',
            month: 'long',
            day: 'numeric',
            hour: 'numeric',
            minute: 'numeric',
            second: 'numeric',
            timeZone: 'UTC',
          })
        : 'N/A'}
    </FormControl.Static>
  </GenericControl>
);

ReadOnlyDateControl.propTypes = {
  value: PropTypes.instanceOf(Date),
};

export default ReadOnlyDateControl;
