/**
 * @Author: Ben Briggs <benbriggs>
 * @Date:   2017-10-20T09:49:23+01:00
 * @Email:  ben.briggs@thedistance.co.uk
 * @Last modified by:   benbriggs
 * @Last modified time: 2018-01-11T09:59:37+00:00
 * @Copyright: The Distance
 */

const createReducer = (initialState, handlers) => (
  state = initialState,
  action
) => {
  if (Object.prototype.hasOwnProperty.call(handlers, action.type)) {
    return handlers[action.type](state, action);
  }
  return state;
};

module.exports = createReducer;
