/**
 * @Author: Ben Briggs <benbriggs>
 * @Date:   2017-09-27T11:10:32+01:00
 * @Email:  ben.briggs@thedistance.co.uk
 * @Last modified by:   benbriggs
 * @Last modified time: 2018-01-11T09:21:25+00:00
 * @Copyright: The Distance
 */

import { begin, end, pendingTask } from 'react-redux-spinner';

export const createStart = ({ type }) => () => ({
  type,
  [pendingTask]: begin,
});

export const createEnd = ({ type }) => result => ({
  type,
  ...result,
  [pendingTask]: end,
});

export const createError = ({ type }) => error => ({
  type,
  ...error,
  [pendingTask]: end,
});
