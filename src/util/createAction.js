/**
 * @Author: Ben Briggs <benbriggs>
 * @Date:   2017-10-02T10:05:47+01:00
 * @Email:  ben.briggs@thedistance.co.uk
 * @Last modified by:   benbriggs
 * @Last modified time: 2018-01-02T13:59:41+00:00
 * @Copyright: The Distance
 */

const createAction = type => result => ({
  type,
  ...result,
});

export default createAction;
