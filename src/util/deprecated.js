/**
 * @Author: Ben Briggs <benbriggs>
 * @Date:   2018-02-06T14:16:05+00:00
 * @Email:  ben.briggs@thedistance.co.uk
 * @Last modified by:   benbriggs
 * @Last modified time: 2018-02-06T14:49:02+00:00
 * @Copyright: The Distance
 */

function deprecated(method, message) {
  return function(...args) {
    console.warn(`DEPRECATED: ${message}`);
    return method(...args);
  };
}

export default deprecated;
