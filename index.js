/**
 * @Author: Ben Briggs <benbriggs>
 * @Date:   2017-12-07T11:48:55+00:00
 * @Email:  ben.briggs@thedistance.co.uk
 * @Last modified by:   benbriggs
 * @Last modified time: 2017-12-07T11:49:51+00:00
 * @Copyright: The Distance
 */

const createWebpackConfig = require('./lib/createWebpackConfig');

module.exports = {
  createWebpackConfig,
};
