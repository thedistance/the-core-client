# 1.35.4

* Update the-core-utils to 1.7.3.

# 1.35.3

* Update the-core-utils to 1.7.2.

# 1.35.2

* Mirror the bottom margin to the buttons on a File Control for a Read Only File Control.

# 1.35.1

* Add some bottom margin to the buttons on a File Control.

# 1.35.0

* Change the default named actions in Entity3 to also take selectors and action
  creators as parameters.

# 1.34.3

* Add static number type to entity field mapping.

# 1.34.2

* Fix regex filtering with special chars regression in Entity2.

# 1.34.1

* Add getThreadFilterThProps to ListViews.

# 1.33.1

* Make Froala editor get it's licence key from an environment variable.

# 1.33.0

* Change font loading behaviour in createWebpackConfig to use file-loader.
* Remove double processing of woff and woff2 files in createWebpackConfig.

# 1.32.8

* Add an override for the saved alert on the detail view via props.location.state.
* Add a prop to the detail view to enable hiding of the delete button for create views.

# 1.32.7

* Remove the entities plugin from Froala to get better WYSIWYG.

# 1.32.6

* Add support for setting helpText in the entity schema.

# 1.32.5

* Add an optional helpText (renders a HelpBlock) to GenericControl.

# 1.32.4

* Add video toolbar button, disable video uploads.
* Add file upload toolbar button, uses same endpoint as image uploads.

# 1.32.3

* Remove quote plugin and menu item from Froala editor.
* Add paragraph styles for higlight box, quote box, and blockquote.

# 1.32.2

* Add html renderer for viewing froala content in read only mode.

# 1.32.1

* Add hyperlink functionality to froala editor.

# 1.32.0

* Add `saved` property to detail views, for success messages.

# 1.31.1

* Expose plural/slug properties from Entity 3 onto its views.

# 1.31.0

* Add deletion confirmation to Entity 3.

# 1.30.1

* Expose `toolbarStickyOffset` option for froala toolbar.

# 1.30.0

* Add checkboxes control.

# 1.29.0

* Update froala editor to handle file uploads.

# 1.28.0

* Add ReadOnlyFileControl component.
* Add froala editor support.

# 1.27.2

* Pin react-table to v6.7.6 to avoid a React compatibility issue.

# 1.27.1

* Resolve a bug where Entity 3 would not unload collection error messages.

# 1.27.0

* Add Entity3 component.

# 1.26.0

* Add babel minify option to the webpack config.
* Pin node sass to 4.7.2.

# 1.25.3

* Expose initial selector and reducer state to be modified on
  entity construction.

# 1.25.2

* Expose hooks for extending mapStateToProps and mapDispatchToProps.
* Expose action creators and selectors to custom actions.

# 1.25.1

* Fix: Entity2 actionCreators are now merged recursively.

# 1.25.0

* Add Entity2 component.

# 1.24.2

* Fix over the air release index order.

# 1.24.1

* Resolves a bug in read only views where pressing back in a validated model
  would crash the application.

# 1.24.0

* Add UI for creating over the air releases.

# 1.23.4

* Resolves a bug where http errors were not being displayed on model validation.

# 1.23.3

* Resolves a bug where model validation failures were not being displayed.

# 1.23.2

* Resolves a build error with the last release.

# 1.23.1

* Allow inflections to be customised for each entity.

# 1.23.0

* Add `LoginForm` component.

# 1.22.0

* Add `CreateComponent` and `UpdateComponent` for greater flexibility within
  views for editing entities.

# 1.21.2

* Remove requirement for ReadOnlyDateControl's value to be required; resolves
  an issue where the date may not exist yet, for example when a Parse Object
  has not yet been saved.

# 1.21.1

* Allows for more granular read only access in entities.
* Remove requirement for GenericControl to have an onChange handler.

# 1.21.0

* Add `defaultTo` to the entity schema.

# 1.20.0

* Created new ReadOnly components [StaticTextControl, ReadOnlyBooleanControl, ReadOnlyDateControl, ReadOnlyGeoPointControl, ReadOnlyNumberControl, ReadOnlyNumericSelectControl, ReadOnlyPointerControl].
* Updated the Footer component to '2018'.

# 1.19.5

* Refactor the createEntity function to split out the initialisation of actions and views.

# 1.19.4

* Resolve an issue where the allowBlank option was not being passed through
  to enum and enumNumeric components.

# 1.19.3

* Add filterMethod & Filter to the entity schema.

# 1.19.2

* Resolve an issue where React Table styles would not be loaded for the
  PushMessages component.

# 1.19.1

* Resolve an issue where the ArrayControl would fail to render without a value.

# 1.19.0

* Add YesNo component.

# 1.18.1

* Resolve a validation state rendering glitch on ArrayControl.

# 1.18.0

* Add array field type to the entity schema.
* Add ArrayControl component.

# 1.17.0

* Add enumNumeric field type to the entity schema.
* Add NumericSelectControl component.

# 1.16.10

* Support passing a separate accessor for the index/edit views.

# 1.16.9

* Support label/value options in the SelectControl.

# 1.16.8

* Resolves an issue where the PointerControl would not clear the value properly.

# 1.16.7

* Entities now expose extraActions, extraCreateActions & extraEditActions.

# 1.16.6

* Enable additional actions for create/edit views, separate from extraActions.

# 1.16.5

* Enable custom paths for the list view.

# 1.16.4

* Fix prop types validation for the Timeago component.

# 1.16.3

* Allow fields to be shown/hidden in collection/edit views.

# 1.16.2

* Resolve an issue where the router views would be undefined.

# 1.16.1

* Entities now expose a routes array.

# 1.16.0

* Add LoadProgress component.
* Add a helper for creating a combined reducer from all entities.
* Fix a bug where actions from the GenericControl would be passed onto the
  child input.

# 1.15.5

* Expose the camel cased entity name.

# 1.15.4

* Resolves an issue where the page count on entity index would not be updated
  when the data was filtered.

# 1.15.3

* Use a separate state key for PointerControl's "display value", resolves an
  issue where the value would appear blank on initial load.

# 1.15.2

* Allow additional actions to be passed to new/edit entity views.

# 1.15.1

* Resolve a bug where a null value would throw a Parse exception.
* Add PointerControl to the list of available types in the Entity schema.

# 1.15.0

* Add PointerControl component.
* Resolve component clipping issues within panels.

# 1.14.3

* Pass through sortable and filterable to the Entity index view.

# 1.14.2

* Add custom cell renderers for the Entity index view.

# 1.14.1

* Add validation to the Entity schema.

# 1.14.0

* Expose generic views for create/edit.
* Allow a schema to be defined on Entity, to assist with creating generic
  model fields.

# 1.13.2

* Downgrade react-redux-spinner - rendering issues.

# 1.13.1

* Saving an entity will now optionally redirect.
* Added a cancel action to each entity.

# 1.13.0

* Add utility methods for working with Entity schemas (work in progress).

# 1.12.0

* Add TextareaControl component.

# 1.11.0

* Add Welcome component.

# 1.10.3

* Add an option to the SelectControl component to provide a blank value.

# 1.10.2

* Fix PropTypes validator for canCreateNew.

# 1.10.1

* Added a flag for showing the new button in the entity.

# 1.10.0

* PushMessages view now has live-updating timestamps.
* Add MultipleFileControl component.

# 1.9.8

* Resolve a bug where react-table would pass too many parameters back to Parse,
  resulting in a 'Parse Objects not allowed here' error.

# 1.9.7

* Revert push message initial state change.
* PushMessages component will now always render the ReactTable instance, which
  resolves an issue where the component would unmount in the middle of a fetch.

# 1.9.6

* Change push message initial state to be null instead of an array.

# 1.9.5

* Resolves an issue where push messages would not be loaded if undefined.

# 1.9.4

* Should resolve an issue where the loading overlay would not disappear.

# 1.9.3

* Minor break: AdHocPush and TimezonePush will not show the PushMessages
  collection, load it separately with the PushMessages component.

# 1.9.2

* Should delegate loading of push messages entirely to React Table.

# 1.9.1

* Resolves an issue where push messages were not being automatically loaded for
  TimezonePush and were being loaded twice for AdHocPush.
* Resolves an issue where the TimezonePush component was passing too many props
  to its TimeControl subtree.

# 1.9.0

* Add a separate component for timezone sensitive push messages.
* Refactor existing push message handling, now supports filtering/pagination.
* Add TimeControl component.
* Extract existing push message list into a separate component.

# 1.8.3

* Make delete handler optional.

# 1.8.2

* Add delete handler to the gallery.

# 1.8.1

* Resolves a bug where the gallery would appear broken without any images.

# 1.8.0

* Add BooleanControl component.
* Add Gallery component.

# 1.7.2

* Enable new models to be created with filled parameters.

# 1.7.1

* Expose selectPages from the Entity component.

# 1.7.0

* Add onLoadCurrent to the Entity component.

# 1.6.0

* Add Alert component.

# 1.5.0

* Add SelectControl component.

# 1.4.0

* Add Entity component.
* Add ListView component.

# 1.3.0

* Add NotFound component.

# 1.2.1

* Resolves a bug where GeoPointControl's value would not be synced
  to parse-server.

# 1.2.0

* Add PasswordControl component.
* Add Footer component.
* Add FileControl component.
* Add HtmlControl component.
* Add GeoPointControl component.
* Add TextControl component.
* Add NumberControl component.

# 1.1.0

* Add LinkButton component.
* Add Panel component.
* Add AdHocPush component.

# 1.0.0

* Initial release.
