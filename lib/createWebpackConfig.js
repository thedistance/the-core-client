/**
 * @Author: Ben Briggs <benbriggs>
 * @Date:   2017-10-11T09:38:34+01:00
 * @Email:  ben.briggs@thedistance.co.uk
 * @Last modified by:   benbriggs
 * @Last modified time: 2018-08-31T09:58:55+01:00
 * @Copyright: The Distance
 */

const path = require('path');
const webpack = require('webpack');
const ExtractTextPlugin = require('extract-text-webpack-plugin');
const MinifyPlugin = require('babel-minify-webpack-plugin');

const createWebpackConfig = ({
  appId,
  appUrl,
  context,
  devtool,
  env,
  uploadUrl = false,
  babelMinify = false,
  toolbarStickyOffset = false,
  froalaKey = false,
}) => {
  const isDev = env === 'development';

  const extractSass = new ExtractTextPlugin({
    filename: 'stylesheets/[name].css',
    disable: isDev,
  });

  return {
    cache: isDev,
    context,
    devtool,
    entry: [
      isDev && 'webpack-hot-middleware/client',
      './assets/stylesheets/main.scss',
      './src/app.js',
    ].filter(Boolean),
    output: {
      path: path.join(context, 'public'),
      publicPath: '/',
      filename: 'javascript/bundle.js',
      sourceMapFilename: '[file].map',
    },
    module: {
      rules: [
        {
          loader: 'babel-loader',
          include: [
            path.resolve(context, 'src'),
            // This will eventually be phased out.
            path.resolve(context, 'node_modules/the-core-v3-components/src'),
          ],
          query: {
            cacheDirectory: true,
            presets: ['env', 'stage-2', 'react', isDev && 'react-hmre'].filter(
              Boolean
            ),
            plugins: [
              'emotion',
              'transform-class-properties',
              'transform-decorators-legacy',
            ],
          },
        },
        {
          test: /\.s?css$/,
          use: extractSass.extract({
            use: [
              {
                loader: 'css-loader',
              },
              {
                loader: 'sass-loader',
              },
            ],
            // Use style-loader in development
            fallback: 'style-loader',
          }),
        },
        {
          test: /\.woff(\?v=\d+\.\d+\.\d+)?$/,
          loader:
            'file-loader?limit=10000&mimetype=application/font-woff&name=fonts/[hash].[ext]',
        },
        {
          test: /\.woff2(\?v=\d+\.\d+\.\d+)?$/,
          loader:
            'file-loader?limit=10000&mimetype=application/font-woff&name=fonts/[hash].[ext]',
        },
        {
          test: /\.(png|eot|ttf|svg)$/,
          loader: 'file-loader?limit=100000',
        },
      ],
    },
    plugins: [
      new webpack.ContextReplacementPlugin(/^\.\/locale$/, context => {
        if (!/\/moment\//.test(context.context)) {
          return;
        }
        // Context needs to be modified in place
        Object.assign(context, {
          // Include only CJK
          regExp: /^\.\/(ja|ko|zh)/,
          // Point to the locale data folder relative to moment's src/lib/locale
          request: '../../locale',
        });
      }),
      isDev && new webpack.HotModuleReplacementPlugin(),
      isDev && new webpack.NoEmitOnErrorsPlugin(),
      isDev && new webpack.NamedModulesPlugin(),
      new webpack.DefinePlugin({
        'process.env': {
          APP_ID: JSON.stringify(appId),
          APP_URL: JSON.stringify(appUrl),
          UPLOAD_URL: JSON.stringify(uploadUrl),
          NODE_ENV: JSON.stringify(env),
          FROALA_TOOLBAR_OFFSET: JSON.stringify(toolbarStickyOffset),
          FROALA_KEY: JSON.stringify(froalaKey),
        },
      }),
      extractSass,
      !isDev &&
        new webpack.LoaderOptionsPlugin({
          minimize: true,
          debug: false,
        }),
      !babelMinify &&
        !isDev &&
        new webpack.optimize.UglifyJsPlugin({
          compress: {
            warnings: false,
          },
          comments: false,
          sourceMap: false,
          minimize: true,
        }),
      babelMinify && !isDev && new MinifyPlugin(),
    ].filter(Boolean),
  };
};

module.exports = createWebpackConfig;
